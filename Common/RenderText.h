#pragma once

// link with the Render Text library
#pragma comment(lib, "./Resources/lib/RenderText.lib")

// macros for font creation
#define RT_ITALIC 0x00000001
#define RT_UNDERLINE 0x00000002
#define RT_STRIKEOUT 0x00000004

// macros for font weight
#define RT_FW_DONTCARE 0
#define RT_FW_THIN 100
#define RT_FW_EXTRALIGHT 200
#define RT_FW_ULTRALIGHT 200
#define RT_FW_LIGHT 300
#define RT_FW_NORMAL 400
#define RT_FW_REGULAR 400
#define RT_FW_MEDIUM 500
#define RT_FW_SEMIBOLD 600
#define RT_FW_DEMIBOLD 600
#define RT_FW_BOLD 700
#define RT_FW_EXTRABOLD 800
#define RT_FW_ULTRABOLD 800
#define RT_FW_HEAVY 900
#define RT_FW_BLACK 900 

// struct definition for library
typedef struct tagRT_Library
{
    HFONT hLibFont;
    TCHAR fontPath[2048];
    BOOL bFontResource;
} RT_Library;

// function declarations
// initializes a RT_Library object
BOOL RT_Initialize_Library(RT_Library *rt_lib, HDC hdc, TCHAR *fontPath, TCHAR *fontName, int cFontWeight, DWORD dwFontCreationFlags, DWORD dwOffset);
void RT_DrawText(const char *str, int len, DWORD dwOffset);
void RT_Uninitialize_Library(RT_Library *rt_lib, DWORD dwOffset);
