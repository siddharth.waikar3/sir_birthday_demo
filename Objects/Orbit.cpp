// common Header Files
#include "..\Common\Common.h"
#include "Orbit.h"
#include "OrbitMacros.h"
extern GLUquadric* g_Quad;

#define SLICES	300.0f
#define STACK	20.0f
void DrawOrbit(GLfloat innerRadius, GLfloat outerRadius)
{
	glColor4f(COLOR_COMMON);
	gluCylinder(g_Quad, innerRadius, innerRadius, 0.04f, SLICES, 20.0f);

	glColor4f(COLOR_COMMON);
	gluCylinder(g_Quad, outerRadius, outerRadius, 0.04f, SLICES, 20.0f);
	
	glColor4f(COLOR_COMMON);
	gluDisk(g_Quad, innerRadius, outerRadius, SLICES, 150.0f);
}

void DrawOrbitEx(GLfloat innerRadius, GLfloat outerRadius, GLfloat alpha)
{
	glColor4f(0.4f, 0.4f, 0.4f, alpha);
	gluCylinder(g_Quad, innerRadius, innerRadius, 0.04f, SLICES, 20.0f);

	glColor4f(0.4f, 0.4f, 0.4f, alpha);
	gluCylinder(g_Quad, outerRadius, outerRadius, 0.04f, SLICES, 20.0f);

	glColor4f(0.4f, 0.4f, 0.4f, alpha);
	gluDisk(g_Quad, innerRadius, outerRadius, SLICES, 150.0f);
}

void DrawOrbitJourneyScene(GLfloat innerRadius, GLfloat outerRadius)
{
	// local variables
	static GLint slices = 300;
	static GLint stacks = 20;
	static GLfloat height = 0.025f;

	// code
	glColor4f(COLOR_COMMON);

	glPushMatrix();
	{
		glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);
		gluCylinder(g_Quad, innerRadius, innerRadius, height, slices, stacks);
		gluCylinder(g_Quad, outerRadius, outerRadius, height, slices, stacks);
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(0.0f, height, 0.0f);
		glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);
		gluDisk(g_Quad, innerRadius, outerRadius, slices, stacks);
	}
	glPopMatrix();
}
