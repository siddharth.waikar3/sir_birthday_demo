#pragma once

// header files
#include "../Common/Common.h"

// struct definition
typedef struct tagRandSphere
{
    Vector2D pos; // translation
    Vector2D rot; // rotation
    Vector2D scale; // scale
    GLfloat color[4];
} RandSphereAttribs;

// function declarations
void initializeRandSpheres(RandSphereAttribs *randSphereAttribs, GLuint numSpheres, Vector rangeStart, Vector rangeEnd /* bounding box */);
void drawRandSpheres(RandSphereAttribs *randSphereAttribs, Point **randSphereVertices, GLuint numSpheres);
void updateRandSpheres(RandSphereAttribs *randSphereAttribs, GLuint numSpheres, Vector rangeStart, Vector rangeEnd, GLfloat moveSpeed, GLfloat rotSpeed);
