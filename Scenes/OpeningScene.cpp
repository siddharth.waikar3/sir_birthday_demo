// header files
#include "..\Common\Common.h"
#include "..\Common\Camera.h"
#include "..\Common\Texture.h"
#include "..\Common\OAL.h"
#include "..\Common\Timer.h" // for timing perfectly between cell division
#include "..\Common\CUDALibClient.h"
#include "..\Objects\Orbit.h"
#include "..\Objects\years.hpp"
#include "..\Objects\Skybox.h"
#include "..\Objects\SphereCollision.h"
#include "..\Objects\Saturn.h"

// macros
#define FADE_OUT_OPENING_SCENE 1
#define SHOW_OPENING_SCENE_SATURN_ROCKS SHOW_ROCKS_GLOBAL

#define CELL_TEXTURE_PATH  "./Resources/Textures/Saturn.jpg"

// colors
#define YEAR_TEXT_COLOR 1.0f, 1.0f, 1.0f, 1.0f

// timer ids
#define IDT_YEAR_DELAY_TIMER 88889

// for cells
#define CELL_RADIUS 0.05f
#define DT 1.0f / FPS

#define BOUNDING_SPHERE_RADIUS_OP_SCENE 0.5f

#if HIGH_PERF_PC
    #define NUM_CELLS_AFTER_16_SECONDS 100
    #define NUM_CELLS_AFTER_26_SECONDS 300
#else
    #define NUM_CELLS_AFTER_16_SECONDS 25
    #define NUM_CELLS_AFTER_26_SECONDS 50
#endif

// for camera
#define CAMERA_SPEED_AFTER_16_SECONDS 1.0f / (FPS * 3.0f) // distance / (FPS * desiredTimeInSeconds)
#define CAMERA_SPEED_AFTER_27_SECONDS 1.0f / (FPS * 4.0f) 
#define CAMERA_SPEED_TILL_MIDDLE 1.0f / (FPS * 10.0f)
#define CAMERA_SPEED_TILL_FINAL 1.0f / (FPS * 4.0f)
#define CAMERA_SPEED_TILL_FINAL2 1.0f / (FPS * 6.0f)

// for saturn
#define SATURN_RING_FADE_IN_SPEED 1.0f / (FPS * 5.0f)

// for starburst fade effects
#define WHITE_STARBURST_SPEED 1.0f / (FPS * 0.5f)
#define WHITE_STARBURST_RECOVER_SPEED 1.0f / (FPS * 0.5f)

// for black fade in effect
#define OP_SCENE_BLACK_FADE_IN_SPEED 1.0f / (FPS * 5.0f)
#define OP_SCENE_BLACK_FADE_OUT_SPEED 1.0f / (FPS * 1.0f)

// global variables
BOOL g_bOpeningSceneTimerIsOn = FALSE;
GLint g_iYearCounter = 0;
unsigned long long g_iSecondsCounter = 0;
ALfloat g_fSecondsCounter = 0.0f;

GLfloat openingSceneSkyboxRotation = 0.0f;

// for spheres (cells)
GLuint nTotalCells;
BoundingSphere boundingSphere;
SphereAttribs *sphereAttribs;
GLfloat cellMitosisDelayInMs = 0.0f;//(22000.0f / (GLfloat)nTotalCells); // because the cells need to complete their mitosis in (22000 ms) 22 seconds
GLuint cellTexture = 0;
GLuint nCells = 0;
BOOL g_bShowCells = FALSE;
BOOL g_bStartMitosis = FALSE;
BOOL g_bMitosisTimerRunning = FALSE;
StopWatchTimer *mitosisTimer = NULL;

// for saturn
GLfloat g_SaturnRingAlpha = 0.0f;
GLfloat g_SatYRot = 0.0f;
GLfloat g_RingYRot = 0.0f;
BOOL g_bShowSaturn = FALSE;
BOOL g_bShowSaturnRing = FALSE;
BOOL g_bShowMiddleCell = TRUE;

// for music
BOOL g_bOpeningSceneMusicStarted = FALSE;

// state enum definition
enum openingSceneStates
{
    OPENING_SCENE_FADE_IN,
    OPENING_SCENE_CELL_STATE,             //zoom in
    OPENING_SCENE_CELL_MITOSIS_STATE,     //zoom in
    OPENING_SCENE_CELL_UNHIDE_AT_16_SECONDS,
    OPENING_SCENE_CELL_MITOSIS_STATE_100_CELLS,
    OPENING_SCENE_CELL_UNHIDE_AT_26_SECONDS,
    OPENING_SCENE_CELL_MITOSIS_STATE_1000_CELLS,
    OPENING_SCENE_CELL_FADE_IN_STATE, // fade out into saturn 
    OPENING_SCENE_SATURN_STATE, // saturn with ring
    OPENING_SCENE_FADE_OUT,
    END
};

enum openingSceneStates openingSceneState;

// extern global variables
extern FILE *gpFILE;
extern HWND ghwnd;
extern HDC ghdc;
extern UINT iCurrentWidth;
extern UINT iCurrentHeight;
extern enum Scene currentScene;

// for camera
CameraCoordinates openingSceneCameraStart = 
{
	{0.000000f, 0.015000f, 1.255001f},
	{0.000000f, 0.000000f, 0.000000f},
	{0.000000f, 0.000000f, 0.000000f},
	{0.000000f, 1.000000f, 0.000000f},
	0.000000f,
	270.000000f
};

CameraCoordinates openingSceneCameraEnd =
{
	{0.000000f, -0.065000f, 3.550000f},
	{0.000000f, 0.000000f, 0.000000f},
	{0.000000f, 0.000000f, 0.000000f},
	{0.000000f, 1.000000f, 0.000000f},
	0.000000f,
	270.000000f
};

// for fade effects
Color whiteStarburstColor = {1.0f, 1.0f, 1.0f, 0.0f};
GLfloat fadeAlpha = 1.0f;

BOOL g_bStartStarburst = FALSE;
BOOL g_bEndStarburst = FALSE;

BOOL g_bOpeningSceneShowSkybox = FALSE;

// function definitions
void initializeSceneOne(void)
{
    // code
    // initialize timer
    initializeTimer(&mitosisTimer);

    // initialize cell texture
    if(LoadTexture(&cellTexture, CELL_TEXTURE_PATH) == FALSE)
    {
        fprintf(gpFILE, "Failed to load cell texture. Exitting...\n");
        fflush(gpFILE);
        DestroyWindow(ghwnd);
    }

    // calculate num of cells
    nTotalCells = 1000;//(GLuint)(((4.0f / 3.0f) * M_PI * powf(BOUNDING_SPHERE_RADIUS, 3)) / ((4.0f / 3.0f) * M_PI * powf(CELL_RADIUS, 3)));

    // caculate timing
    cellMitosisDelayInMs = 2000;

    // malloc sphereAttribs
    sphereAttribs = (SphereAttribs*)malloc(sizeof(SphereAttribs) * nTotalCells);
    if(sphereAttribs == NULL)
    {
        fprintf(gpFILE, "Failed to allocate memory for sphereAttribs in opening scene.\n");
        DestroyWindow(ghwnd);
    }

    // initialize sphere collision system
    boundingSphere.radius = BOUNDING_SPHERE_RADIUS_OP_SCENE;
    initializeSphereCollisionSystem(CELL_RADIUS, &boundingSphere, sphereAttribs, nTotalCells);

    // sphereAttribs[0].acc.z = 0.0f;
    // sphereAttribs[0].grav.z = 0.0f;
    // GLfloat rand1 = getInterpolatedValue(0.0f, (GLfloat)RAND_MAX, -0.095f, 0.095f, rand());
    // GLfloat rand2 = getInterpolatedValue(0.0f, (GLfloat)RAND_MAX, -0.095f, 0.095f, rand());
    // sphereAttribs[0].acc = {rand1, rand2, 0.0f};
    // sphereAttribs[0].grav = {rand1, rand2, 0.0f};

    // set opening scene state
    openingSceneState = OPENING_SCENE_FADE_IN;
}

void displaySceneOne(void)
{   
    // local variables
    GLfloat cellColor[] = {1.0f, 1.0f, 0.15f, 1.0f};
    static BOOL bOnce = FALSE;

    // code       
    if(!bOnce)
    {
        setCameraCoordinates(openingSceneCameraStart);
        bOnce = TRUE;
    }

    // draw skybox
    if(g_bOpeningSceneShowSkybox)
    {
        drawTexturedSkyboxWithRotation(openingSceneSkyboxRotation);
    }

    // draw year text in the top right
    glPushMatrix();
    {
        // add a projection 
        glMatrixMode(GL_PROJECTION);
        glPushMatrix();
        {
            glLoadIdentity();

            if(iCurrentWidth <= iCurrentHeight)
            {
                glOrtho(-100.0, 
                        100.0, 
                        -100.0 * ((GLfloat)iCurrentHeight / (GLfloat)iCurrentWidth), 
                        100.0 * ((GLfloat)iCurrentHeight / (GLfloat)iCurrentWidth),
                        0.1, 
                        0.1);
            }
            else
            {
                glOrtho(-100.0 * ((GLfloat)iCurrentWidth / (GLfloat)iCurrentHeight),
                        100.0 * ((GLfloat)iCurrentWidth / (GLfloat)iCurrentHeight),
                        -100.0,
                        100.0,
                        0.1,
                        0.1);
            }

            glMatrixMode(GL_MODELVIEW);
            // glPushMatrix();
            // {
            //     char str[255];
            //     sprintf(str, "%d = current number of spheres", nCells);

            //     glLoadIdentity();
            //     applyTransformations(g_Transformations[0]);
            //     glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
            //     draw_text_wgl(ghdc, strlen(str), str, 900000, FALSE);
            // }
            // glPopMatrix();

            glPushMatrix();
            {
                glLoadIdentity();
                applyTransformations({
                                        0.465000f, 0.670000f, 0.000000f,
                                        0.000000f, 0.000000f, 0.000000f,
                                        0.340000f, 0.540000f, 1.000000f
                                    });
                glColor4f(YEAR_TEXT_COLOR);
                drawOrbitText(g_iYearCounter);
            }
            glPopMatrix();

            glMatrixMode(GL_PROJECTION);
        }
        glPopMatrix();
        glMatrixMode(GL_MODELVIEW);
    }
    glPopMatrix();

    // draw middle cell
    if(g_bShowMiddleCell)
    {
        drawSphereVerticesWithTexture(boundingSphere.vertices, 30, 30, cellTexture, GL_QUADS, CELL_RADIUS);
    }

    // draw saturn
    if(g_bShowSaturn)
    {
        glPushMatrix();
        {
            glRotatef(15.500000f, 1.0f, 0.0f, 0.0f);
            glRotatef(-27.0f, 0.0f, 0.0f, 1.0f);
            drawSaturn_v3(g_SatYRot, g_RingYRot, 1.0f, g_SaturnRingAlpha, SHOW_OPENING_SCENE_SATURN_ROCKS);
        }
        glPopMatrix();
    }

    // draw cells 
    if(g_bShowCells)
    {
        drawSphereCollisionSystem(&boundingSphere, sphereAttribs, cellTexture, nCells);
    }

    // white fade in / fade out (starburst effect)
    if(g_bStartStarburst)
    {
        fadeOutEffect_v2(whiteStarburstColor);
    }

    if(g_bEndStarburst)
    {
        fadeInEffect_v2(whiteStarburstColor);
    }

    // black fade in / fade out
    fadeInEffect(fadeAlpha);
    fadeOutEffect(fadeAlpha);
}

void updateSceneOne(void)
{
    // function prototypes
    void OpeningSceneTimer(HWND hwnd, UINT iMsg, UINT_PTR idEvent, DWORD dwTime);
    void uninitializeSceneOne(void);

    // local variables
    // camera
    static BOOL bCameraReachedPoint[6];
    static BOOL bMoveCameraIf16SecondsHavePassed = FALSE;
    static BOOL bMoveCameraIf27SecondsHavePassed = FALSE;
    static GLfloat cameraLerpVal[6] = {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f};
    static BOOL bOnce = FALSE;
    static BOOL bFifthSecondFlag = FALSE;

    // code
    if(!bOnce)
    {
        SetCurrentSourceForStats(g_SceneSources[1], FALSE, FALSE);
        playALSource(g_SceneSources[1]);
        // SetCurrentSourceForStats(g_CompleteTrackSource, FALSE, FALSE);
        // playALSource(g_CompleteTrackSource);
        bOnce = TRUE;
    }

    // update skybox rotation
    updateRotationValue(&openingSceneSkyboxRotation, 360.0f / (FPS * 1000.0f));

    // first beat
    if((totalElapsedTime >= (5.5)) && !bFifthSecondFlag) 
    {
        g_bStartMitosis = TRUE;
        g_bShowCells = TRUE;
        //nCells = nCells + 2;
        openingSceneState = OPENING_SCENE_CELL_MITOSIS_STATE;
        bFifthSecondFlag = TRUE;
    }

    // start saturn ring fade in after 42 seconds
    if(g_iSecondsCounter >= 41)
    {
        updateObjectAlpha(&g_SaturnRingAlpha, SATURN_RING_FADE_IN_SPEED, FADE_IN_FLAG);
    }

    // move camera after 37 seconds
    if(g_iSecondsCounter >= 37)
    {
        if(!bCameraReachedPoint[0])
        {
            bCameraReachedPoint[0] = lerpCamera(&openingSceneCameraStart, &openingSceneCameraEnd, &cameraLerpVal[0], CAMERA_SPEED_TILL_MIDDLE);
        }
    }

    // keep setting mitosis timer after every few ms
    if(!(mitosisTimer->bTimerRunning) && g_bStartMitosis)
    {
        startTimer(mitosisTimer);
        // g_bMitosisTimerRunning = TRUE;
    }

    // -------- query elapsed time and increment cell count -------- 
    if(g_bStartMitosis && ((queryElapsedTimeMilliseconds(mitosisTimer)) >= cellMitosisDelayInMs))
    {
        nCells = nCells + 1;
        
        // if(nCells > nTotalCells)
        // {
        //     nCells = nTotalCells;
        // }

        resetTimer(mitosisTimer);
        startTimer(mitosisTimer);
        // g_bMitosisTimerRunning = FALSE;
    }

    // keep setting the timer after every 1 second
    if(!g_bOpeningSceneTimerIsOn)
    {
        SetTimer(ghwnd, IDT_YEAR_DELAY_TIMER, 1000, (TIMERPROC)OpeningSceneTimer);
        g_bOpeningSceneTimerIsOn = TRUE;
    }

    // keep rotating saturn
    if(g_bShowSaturn)
    {
        updateRotationValue(&g_SatYRot, 360.0f / (FPS * 100.0f));
        updateRotationValue(&g_RingYRot, 360.0f / (FPS * 100.0f));
    }

    // update sphere collision system
    if(g_bShowCells)
    {
        updateSphereCollisionSystem(&boundingSphere, sphereAttribs, nCells);
    }

    switch(openingSceneState)
    {
    case OPENING_SCENE_FADE_IN:
        // fade in
        if(updateAlpha(&fadeAlpha, OP_SCENE_BLACK_FADE_IN_SPEED, FADE_IN_FLAG))
        {
            openingSceneState = OPENING_SCENE_CELL_STATE;
        }
        break;
    case OPENING_SCENE_CELL_STATE:
        break;
    case OPENING_SCENE_CELL_MITOSIS_STATE:
        if(g_iSecondsCounter >= (16))
        {            
            // hide the scene
            if(updateAlpha(&fadeAlpha, 1.0f / (FPS * 1.0f), FADE_OUT_FLAG) == TRUE)
            {
                nCells = NUM_CELLS_AFTER_16_SECONDS;
                openingSceneState = OPENING_SCENE_CELL_UNHIDE_AT_16_SECONDS;
            }
        }
        break;
    case OPENING_SCENE_CELL_UNHIDE_AT_16_SECONDS:
        // unhide the scene
        if(updateAlpha(&fadeAlpha, 1.0f / (FPS * 1.0f), FADE_IN_FLAG) == TRUE)
        {
            openingSceneState = OPENING_SCENE_CELL_MITOSIS_STATE_100_CELLS;
        }
        break;
    case OPENING_SCENE_CELL_MITOSIS_STATE_100_CELLS:
        // hide the scene again
        if(g_iSecondsCounter >= (26))
        {
            if(updateAlpha(&fadeAlpha, 1.0f / (FPS * 1.0f), FADE_OUT_FLAG) == TRUE)
            {
                GLuint nearTotalCells = (GLuint)ceilf((GLfloat)nTotalCells / 1.0f);
                nCells = NUM_CELLS_AFTER_26_SECONDS; 
                // boundingSphere.radius = 0.5f;
                openingSceneState = OPENING_SCENE_CELL_UNHIDE_AT_26_SECONDS;
            }
        }
        break;
    case OPENING_SCENE_CELL_UNHIDE_AT_26_SECONDS:
        // unhide the scene 
        if(updateAlpha(&fadeAlpha, 1.0f / (FPS * 1.0f), FADE_IN_FLAG) == TRUE)
        {
            openingSceneState = OPENING_SCENE_CELL_MITOSIS_STATE_1000_CELLS;
        }
        break;
    case OPENING_SCENE_CELL_MITOSIS_STATE_1000_CELLS:
        // if 36 seconds have passed, zoom out and fade out 
        if(g_iSecondsCounter >= (36))
        {
            if(updateAlpha(&fadeAlpha, 1.0f / (FPS * 1.0f), FADE_OUT_FLAG))
            {
                g_bShowMiddleCell = FALSE;
                g_bShowCells = FALSE;
                g_bOpeningSceneShowSkybox = TRUE;
                g_bShowSaturn = TRUE;
                openingSceneState = OPENING_SCENE_CELL_FADE_IN_STATE;
            }
        }
        break;
    case OPENING_SCENE_CELL_FADE_IN_STATE:
        if(updateAlpha(&fadeAlpha, 1.0f / (FPS * 1.0f), FADE_IN_FLAG))
        {
            openingSceneState = OPENING_SCENE_SATURN_STATE;
        }
        break;
    case OPENING_SCENE_SATURN_STATE:
        // if 49 seconds have passed, start fade out
        if(g_iSecondsCounter == (49))
        {
            openingSceneState = OPENING_SCENE_FADE_OUT;
        }
        break;
    case OPENING_SCENE_FADE_OUT:
#if FADE_OUT_OPENING_SCENE
        if(updateAlpha(&fadeAlpha, 1.0f / (FPS * 1.0f), FADE_OUT_FLAG))
        {
            openingSceneState = END;
        }
#endif
        break;
    case END:
        uninitializeSceneOne();
        ResetGlobalTimer(); // reset global timer
        setCameraCoordinates({{0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 1.0f, 0.0f}, 0.0f, 270.0f});
        currentScene = SCENE_JOURNEY;
        break;
    default:
        break;
    }
}

void uninitializeSceneOne(void)
{
    // code
    // stop source from playing
    stopALSource(g_SceneSources[1]);
    SetCurrentSourceForStats(0, FALSE, FALSE); // unbind the source

    // free sphereAttribs
    if(sphereAttribs)
    {
        free(sphereAttribs);
        sphereAttribs = NULL;
    }

    // delete the cell texture
    DeleteTexture(&cellTexture);

    // release timer
    releaseTimer(&mitosisTimer);  
}

void OpeningSceneTimer(HWND hwnd, UINT iMsg, UINT_PTR idEvent, DWORD dwTime)
{
    // code
    KillTimer(hwnd, idEvent);
    if(idEvent == IDT_YEAR_DELAY_TIMER)
    {
        // if((GLuint)totalElapsedTime > 2)
        if(g_iSecondsCounter > 5)
        {
            // cap year at 2000
            if(g_iYearCounter < 33) /* 0 = 1967, 33 = 2000 */
            {
                g_iYearCounter++;
            }
            else
            {
                g_iYearCounter = 33;
            }
        }

        g_iSecondsCounter++;
    }
    g_bOpeningSceneTimerIsOn = FALSE;
}
