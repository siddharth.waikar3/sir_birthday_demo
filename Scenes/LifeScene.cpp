// header files
#include "..\Common\Common.h"
#include "..\Common\Camera.h"
#include "..\Objects\years.hpp"
#include "..\Objects\SlideText.h" // Contains Function Declarations SlideText by Puja

#include "..\Common\OAL.h"

enum LifeSceneStates
{
    LIFE_SCENE_STATE_ONE,   // Assigned to Aditya Dada
    LIFE_SCENE_STATE_TWO,   // Assigned To Dilip
    LIFE_SCENE_END
};
    
// Macros
#define IDT_LIFE_SCENE_MORGAN_FREEMAN_QUOTE_DELAY 1512824
#define IDT_LIFE_SCENE_FADE_OUT_DELAY 5158718

#define MORGAN_FREEMAN_QUOTE_TRANSLATION_BEGIN -6.0f
#define MORGAN_FREEMAN_QUOTE_TRANSLATION_END -3.0f

#define PATTI_Z_TRANSLATION_BEGIN 0.0f
#define PATTI_Z_TRANSLATION_END -3.0f
#define FPS 60.0f
#define MAX_ALPHA 1.0f

GLfloat pattiTranslationSpeed = fabsf((PATTI_Z_TRANSLATION_END - PATTI_Z_TRANSLATION_BEGIN) / (FPS * 5.0f));
GLfloat morganFreemanTextAlpha = 0.0f;
GLfloat morganFreemanTextAlphaSpeed = MAX_ALPHA / (FPS * 2.0f); // 8.0f = Sceonds, 1.0f = Max Alpha
const GLfloat morganFreemanTextTranslationSpeed = (MORGAN_FREEMAN_QUOTE_TRANSLATION_END - MORGAN_FREEMAN_QUOTE_TRANSLATION_BEGIN) / (FPS * 6.0f);

// macros for text in shot 2
#define  WINDEV_Z_POSITION -8.0f
#define  UNIX_Z_POSITION  -4.0f
// #define  SUNBEAM_Z_POSITION -3.0f
#define  RTR_Z_POSITION -2.0f

//#define TOTAL_SHOT_2_TEXT_TIME 1.5f
#define SHOT_2_TIME_PER_TEXT 375 // in ms
#define ID_TIMER_SHOT_2_UNIX_DELAY  4745613
#define ID_TIMER_SHOT_2_SUNBEAM_DELAY  47456139
#define ID_TIMER_SHOT_2_RTR_DELAY  45674352
#define ID_TIMER_SHOT_2_END_DELAY 214124

// global variable
GLfloat Shot2TextAlphas[4] = {1.0f, 0.0f, 0.0f, 0.0f};

// global variables
struct Transformations dilipTextTransformation[4] = {
                                                        {
                                                            -0.950000f - 0.150000f - 0.024500f, -0.150000f - 0.108000f, WINDEV_Z_POSITION,
                                                            0.000000f, 0.000000f, 0.000000f,
                                                            1.000000f, 1.000000f, 1.000000f
                                                        },
                                                        // transformations[2]
                                                        {
                                                            -0.550000f - 0.150000f - 0.024500f, -0.149999f - 0.108000f, UNIX_Z_POSITION,
                                                            0.000000f, 0.000000f, 0.000000f,
                                                            1.000000f, 1.000000f, 1.000000f
                                                        },
                                                        // transformations[3]
                                                        {
                                                            -1.250000f - 0.150000f - 0.024500f, -0.100000f - 0.108000f, -3.0f,
                                                            0.000000f, 0.000000f, 0.000000f,
                                                            1.000000f, 1.000000f, 1.000000f
                                                        },
                                                        // transformations[4]
                                                        {
                                                            -0.450000f - 0.150000f - 0.024500f, -0.150000f - 0.108000f, RTR_Z_POSITION,
                                                            0.000000f, 0.000000f, 0.000000f,
                                                            1.000000f, 1.000000f, 1.000000f
                                                        }
                                                    };

extern enum Scene currentScene;
enum LifeSceneStates lifeSceneState;
extern struct Transformations lifeSceneTransformation;
extern struct Transformations g_Transformations[NUM_TRANSFORMATION_ELEMENTS];
extern HWND ghwnd;
BOOL bStartSceneFadeOut = FALSE;
BOOL bLifeSceneTimerIsOn = FALSE;
BOOL bShowName = FALSE;

// for fade effect
GLfloat fadeInAlpha = 1.0f;
GLfloat fadeOutAlpha = 0.0f;
BOOL bFadeIn = FALSE;
const GLfloat fadeSpeed = MAX_ALPHA / (FPS * 3.0f);

struct Transformations morganFreeManTextCurrentTransformations = {
    // transformations[1]
	0.060000f, -0.060000f, -3.005000f,
	0.000000f, 0.000000f, 0.000000f,
	1.000000f, 1.000000f, 1.0f
};

struct Transformations pattiCurrentTransformations = {
    // transformations[2]
	0.000000f, 0.000000f, 0.000000f,
	0.000000f, 0.000000f, 0.000000f,
	1.000000f, 1.000000f, 1.000000f
};

GLfloat morganFreemanQuoteCurrentZTranslation;

//०२:४५ : ०३:०५
// function definitions
void initializeSceneSix(void)
{
    // code
    lifeSceneState = LIFE_SCENE_STATE_ONE;
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    // seekALSource(g_CompleteTrackSource, 0, 2, 45);  // hya call ne aapan Gaan cya Minuts ani Second laa gane play hote
    // playALSource(g_CompleteTrackSource);            // haa call actual gaane play krto

    pattiCurrentTransformations.translation.z = PATTI_Z_TRANSLATION_BEGIN;
    morganFreeManTextCurrentTransformations.translation.z = MORGAN_FREEMAN_QUOTE_TRANSLATION_BEGIN;
}

void displaySceneSix(void)
{
    // local variables
    static int once = 0;
    CameraCoordinates ccStart =
    {
        {0.0f, 0.0f, 0.0f},
        {0.0f, 0.0f, 0.0f},
        {0.0f, 0.0f, 0.0f},
        {0.0f, 1.0f, 0.0f},
        0.0f,
        270.0f
    };

    //code
    if(!once)
    {
        setCameraCoordinates(ccStart);
        once = 1;
    }

    switch(lifeSceneState)
    { 
        case LIFE_SCENE_STATE_ONE:
            glPushMatrix();
            {
                applyTransformations(morganFreeManTextCurrentTransformations);
                glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
                drawMorganFreemanQuote(1, 1.0f);
            }
            glPopMatrix();

            glPushMatrix();
            {
                applyTransformations(morganFreeManTextCurrentTransformations);
                glColor4f(1.0f, 1.0f, 1.0f, morganFreemanTextAlpha);
                drawMorganFreemanQuote(2, morganFreemanTextAlpha);
            }
            glPopMatrix();
            
            fadeInEffect (fadeInAlpha); // quad for fade in
            fadeOutEffect(fadeOutAlpha);  // quad for fade out
        break;


        case LIFE_SCENE_STATE_TWO:
            
            glPushMatrix();
            {   
                applyTransformations(dilipTextTransformation[0]);
                glColor4f(1.0f, 1.0f, 0.0f, Shot2TextAlphas[0]);
                drawOrbitText(80);
            }
            glPopMatrix();

            glPushMatrix();
            {
                applyTransformations(dilipTextTransformation[1]);
                glColor4f(1.0f, 1.0f, 0.0f, Shot2TextAlphas[1]);
                drawOrbitText(81);
            }
            glPopMatrix();

            // sunbeam
            // glPushMatrix();
            // {
            //     applyTransformations(dilipTextTransformation[2]);
            //     glColor4f(1.0f, 1.0f, 0.0f, Shot2TextAlphas[2]);
            //     drawOrbitText(82);
            // }
            // glPopMatrix();

            glPushMatrix();
            {
                applyTransformations(dilipTextTransformation[3]);
                glColor4f(1.0f, 1.0f, 0.0f, Shot2TextAlphas[3]);
                drawOrbitText(83);
            }
            glPopMatrix();
        break;

        default:
        break;
    }
}

void updateSceneSix(void)
{
    // function prototypes
    void LifeSceneTimer(HWND hwnd, UINT uiMsg, UINT_PTR idEvent, DWORD dwTime);
    void uninitializeSceneSix(void);

    // local variables
    static BOOL bOnce = FALSE;
    static BOOL bGoToShotTwo = TRUE;
    static BOOL bUNIXDelay = TRUE;
    static BOOL bRTRDelay = TRUE;

    // code
    if(!bOnce)
    {
        SetCurrentSourceForStats(g_SceneSources[2], FALSE, FALSE);
        playALSource(g_SceneSources[2]);
        bOnce = TRUE;
    }
    // if(totalElapsedTime >= 26.0)
    // {
    //     lifeSceneState = LIFE_SCENE_END;
    // }

    if(totalElapsedTime >= 15.0 && !bStartSceneFadeOut)
    {
        bStartSceneFadeOut = TRUE;
    }

    if(totalElapsedTime >= 18.053 && bGoToShotTwo)
    {
        lifeSceneState = LIFE_SCENE_STATE_TWO;
        bGoToShotTwo = FALSE;
    }   

    if(totalElapsedTime >= (18.053 + 0.453) && bUNIXDelay)
    {
        Shot2TextAlphas[0] = 0.0f;
        Shot2TextAlphas[1] = 1.0f;
        bUNIXDelay = FALSE;
    }

    if(totalElapsedTime >= (18.053 + 0.453 + 0.521) && bRTRDelay)
    {
        Shot2TextAlphas[1] = 0.0f;
        Shot2TextAlphas[3] = 1.0f;
        bRTRDelay = FALSE;
    }

    if(totalElapsedTime >= (18.053 + 0.453 + 0.521 + 0.330))
    {
        lifeSceneState = LIFE_SCENE_END;
    }

    switch (lifeSceneState)
    {
        case LIFE_SCENE_STATE_ONE:

        // fade in
        updateAlpha(&fadeInAlpha, fadeSpeed, FADE_IN_FLAG);

        // translate
        if(morganFreeManTextCurrentTransformations.translation.z < MORGAN_FREEMAN_QUOTE_TRANSLATION_END)
        {
            morganFreeManTextCurrentTransformations.translation.z = morganFreeManTextCurrentTransformations.translation.z + morganFreemanTextTranslationSpeed; 
        }
        else if(!bLifeSceneTimerIsOn && !bShowName) // set timer (delay) for Morgan Freeman's name's fade in
        {
            SetTimer(ghwnd, IDT_LIFE_SCENE_MORGAN_FREEMAN_QUOTE_DELAY, 3500, LifeSceneTimer);
            bLifeSceneTimerIsOn = TRUE;
        }

        // fade in for "- Morgan Freeman, Lucy(2014)"
        if(bShowName)
        {
            if(morganFreemanTextAlpha < 1.0f)
            {
                morganFreemanTextAlpha = morganFreemanTextAlpha + morganFreemanTextAlphaSpeed;
            }
        }

        // fade out for scene
        if(bStartSceneFadeOut)
        {
            updateAlpha(&fadeOutAlpha, fadeSpeed, FADE_OUT_FLAG);
        }
        break;

        case LIFE_SCENE_STATE_TWO:
            break;
        case LIFE_SCENE_END:
            uninitializeSceneSix();
            ResetGlobalTimer();
            setCameraCoordinates({{0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 1.0f, 0.0f}, 0.0f, 270.0f});
            currentScene = SCENE_NEW_JOURNEY;
            break;
    }
}

void LifeSceneTimer(HWND hwnd, UINT uiMsg, UINT_PTR idEvent, DWORD dwTime)
{
    // code
    KillTimer(hwnd, idEvent);
    if(idEvent == IDT_LIFE_SCENE_FADE_OUT_DELAY)
    {
        bStartSceneFadeOut = TRUE;
    }
    else if(idEvent == IDT_LIFE_SCENE_MORGAN_FREEMAN_QUOTE_DELAY)
    {
        bShowName = TRUE;
    }
    else if(idEvent == ID_TIMER_SHOT_2_UNIX_DELAY)
    {
        Shot2TextAlphas[0] = 0.0f;
        Shot2TextAlphas[1] = 1.0f;
    }
    else if(idEvent == ID_TIMER_SHOT_2_RTR_DELAY)
    {
        Shot2TextAlphas[1] = 0.0f;
        Shot2TextAlphas[3] = 1.0f;
    }
    else if(idEvent == ID_TIMER_SHOT_2_END_DELAY)
    {
        lifeSceneState = LIFE_SCENE_END;
    }

    bLifeSceneTimerIsOn = FALSE;
}

void uninitializeSceneSix(void)
{
    // code
}
