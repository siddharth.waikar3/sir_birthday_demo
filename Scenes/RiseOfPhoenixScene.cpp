// header files
#include "..\Common\Common.h"
#include "..\Common\Camera.h"
#include "..\Common\Texture.h"
#include "..\Objects\Astroid.h"
#include "..\Objects\Orbit.h"
#include "..\Objects\dagad.h"
#include "..\Objects\Icosahedron.h"
#include "..\Objects\Saturn.h"
#include "..\Objects\Skybox.h"
#include <time.h>

// general macros
#define PLAY_RISE_OF_PHOENIX_MUSIC 0
#define DO_RISE_OF_PHOENIX_ANIMATION 1
#define SHOW_ROCKS_RISE_OF_PHOENIX_SCENE SHOW_ROCKS_GLOBAL

// for orbit
#define RISE_OF_PHOENIX_ORBIT_INNER_RADIUS 20.0f
#define RISE_OF_PHOENIX_ORBIT_OUTER_RADIUS 20.01f
#define RISE_OF_PHOENIX_ORBIT_RADIUS ((RISE_OF_PHOENIX_ORBIT_INNER_RADIUS + RISE_OF_PHOENIX_ORBIT_OUTER_RADIUS) / 2.0f)

// for camera
#define CAMERA_INITIAL_ANGLE_OFFSET -15.0f /* was -15.0f */

// for rocks
#define RISE_OF_PHOENIX_NUM_ROCKS 100
#define RISE_OF_PHOENIX_ROCKS_BOUNDING_CUBE_SIZE 1.0f
#define RISE_OF_PHOENIX_ROCKS_VELOCITY 0.0025f

// enum for states
enum RiseOfPhoenixSceneStates
{
   RISE_OF_PHOENIX_SHOT_ONE,
   RISE_OF_PHOENIX_SHOT_TWO,
   END
};

enum RiseOfPhoenixSceneStates riseOfPhoenixSceneState;

// extern global variables
extern enum Scene currentScene;
extern HWND ghwnd;
extern HDC ghdc;
extern FILE *gpFILE;
extern CameraCoordinates cameraCoordinates;
extern UINT iCurrentWidth;
extern UINT iCurrentHeight;

// global variables
// for speed
GLfloat speedFactor = 1.0f;
GLfloat normalSpeed = 0.5f;
GLfloat slowSpeed   = 0.25f;
GLfloat fastSpeed   = 1.0f;

// for skybox
GLfloat riseOfPhoenixSkyboxRotation = 0.0f;

// for saturn
GLfloat riseOfPhoenixSatYRot                      = 0.0f;
GLfloat riseOfPhoenixRingYRot                     = 0.0f;
GLfloat riseOfPhoenixSaturnRevolutionAngle        = 90.0f;
Translation riseOfPhoenixSaturnTranslation        = {0.0f, 0.0f, 0.0f};
Translation riseOfPhoenixSaturnInitialTranslation = {0.0f, 0.0f, 0.0f};
Translation riseOfPhoenixSaturnStart              = {0.0f, 0.0f, 0.0f};

// for asteroids
GLuint asteroidRockTexture = 0;
BOOL g_bShowAsteroids = TRUE;

// for rocks
IcosahedronAttribs_v2 riseOfPhoenixRockAttribs[RISE_OF_PHOENIX_NUM_ROCKS];
IcosahedronAttribs_v2 riseOfPhoenixRockAttribs_shot2[RISE_OF_PHOENIX_NUM_ROCKS];
BOOL g_bShowRocks = FALSE;

// transformations 
// asteroid 1 transformations
struct Transformations asteroidOneTransformations =
{
	{2.350000f, -0.054999f, -0.615000f},
	{-20.729973f, 0.000000f, 0.000000f},
	{0.570000f, 0.770000f, 0.570000f}
};

struct Transformations asteroidTwoTransformations =
{
	{1.675000f, 0.105001f, 0.285000f},
	{-22.029890f, -0.300012f, -0.999995f},
	{0.550000f, 0.800000f, 0.550000f}
};

// camera coordinates
CameraCoordinates riseOfPhoenixInitialCC =
{
	{3.978162f, 0.231023f/*0.081023f*/, 19.610115f},
	{-0.992363f, -0.000000f, 0.123355f},
	{-0.123355f, 0.000000f, -0.992363f},
	{-0.000000f, 1.000000f, 0.000000f},
	-0.000000f,
	167.0f
};

CameraCoordinates riseOfPhoenixShotTwoCC =
{
	{-0.025295f, 1.504298f, 10.169566f},
	{0.156222f, -0.149535f, 0.976337f},
	{-0.976337f, 0.000000f, 0.156222f},
	{0.023361f, 0.977639f, 0.145997f},
	-8.599998f,
	80.909241f
};
// {
// 	{4.364017f, 0.455971f, 16.713179f},
// 	{-0.755504f, -0.062791f, 0.652129f},
// 	{-0.652129f, 0.000000f, -0.755504f},
// 	{-0.047438f, 0.996057f, 0.040947f},
// 	-3.600000f,
// 	139.200195f
// };

CameraPan riseOfPhoenixCameraPan = {0.0f, -50.0f, FALSE};

// range start, range end for rocks
Range3 rRocksGravity      = {{-RISE_OF_PHOENIX_ROCKS_VELOCITY, -RISE_OF_PHOENIX_ROCKS_VELOCITY, -RISE_OF_PHOENIX_ROCKS_VELOCITY}, 
                             {RISE_OF_PHOENIX_ROCKS_VELOCITY, RISE_OF_PHOENIX_ROCKS_VELOCITY, RISE_OF_PHOENIX_ROCKS_VELOCITY}};

Range3 rRocksTranslation  = {0};
Range3 rRocksTranslation2 = {0};                    

Range3 rRocksRotation     = {{0.0f, 0.0f, 0.0f}, {180.0f, 180.0f, 180.0f}};
Range rRocksScale         = {0.20f, 0.3f};

// for fovy
Lerp_t lerpFovy = {45.0f, 45.0f, 70.0f, 1.0f / (FPS * 9.0f), 0.0f};

// function definitions
void initializeSceneFour(void)
{
    // code
    // initialize saturn position on orbit
    GLfloat tempAngleInRadians = (riseOfPhoenixSaturnRevolutionAngle) * (M_PI / 180.0f);

    riseOfPhoenixSaturnTranslation.x = RISE_OF_PHOENIX_ORBIT_RADIUS * cosf(tempAngleInRadians);
    riseOfPhoenixSaturnTranslation.y = 0.05f;
    riseOfPhoenixSaturnTranslation.z = RISE_OF_PHOENIX_ORBIT_RADIUS * sinf(tempAngleInRadians);

    riseOfPhoenixSaturnInitialTranslation = riseOfPhoenixSaturnTranslation;

    // init camera
    tempAngleInRadians = (riseOfPhoenixSaturnRevolutionAngle + CAMERA_INITIAL_ANGLE_OFFSET) * (M_PI / 180.0f); 

    riseOfPhoenixInitialCC.position.x = RISE_OF_PHOENIX_ORBIT_RADIUS * cosf(tempAngleInRadians);
    riseOfPhoenixInitialCC.position.z = RISE_OF_PHOENIX_ORBIT_RADIUS * sinf(tempAngleInRadians);

    // load asteroid rock texture
    if(LoadTexture(&asteroidRockTexture, "./Resources/textures/Rock.png") == FALSE)
    {
        fprintf(gpFILE, "Failed to load %s image.\n",  "./Resources/textures/Rock.png");
        DestroyWindow(ghwnd);
    }

    // initialize the rocks
    srand(time(NULL));
    Translation riseOfPhoenixRocksInitialTranslation;
        
    riseOfPhoenixRocksInitialTranslation.x = riseOfPhoenixSaturnTranslation.x + asteroidTwoTransformations.translation.x;
    riseOfPhoenixRocksInitialTranslation.y = riseOfPhoenixSaturnTranslation.y + asteroidOneTransformations.translation.y;
    riseOfPhoenixRocksInitialTranslation.z = riseOfPhoenixSaturnTranslation.z + asteroidOneTransformations.translation.z;
    
    rRocksTranslation = {{riseOfPhoenixRocksInitialTranslation.x + 0.5f, riseOfPhoenixRocksInitialTranslation.y - 0.5f, riseOfPhoenixRocksInitialTranslation.z - 0.35f}, 
                         {riseOfPhoenixRocksInitialTranslation.x + 0.5f, riseOfPhoenixRocksInitialTranslation.y + 0.35f, riseOfPhoenixRocksInitialTranslation.z + 0.35f},};
   
    initializeRangeIcosahedrons_Asteroids(riseOfPhoenixRockAttribs, RISE_OF_PHOENIX_NUM_ROCKS / 2, rRocksGravity, rRocksTranslation, rRocksRotation, rRocksScale);
    memcpy((void *)&riseOfPhoenixRockAttribs_shot2[0], 
           (void *)&riseOfPhoenixRockAttribs[0], 
           sizeof(IcosahedronAttribs_v2) * (RISE_OF_PHOENIX_NUM_ROCKS / 2));

    Translation riseOfPhoenixRocksInitialTranslation2;

    riseOfPhoenixRocksInitialTranslation2.x = riseOfPhoenixSaturnTranslation.x + asteroidTwoTransformations.translation.x;
    riseOfPhoenixRocksInitialTranslation2.y = riseOfPhoenixSaturnTranslation.y + asteroidTwoTransformations.translation.y;
    riseOfPhoenixRocksInitialTranslation2.z = riseOfPhoenixSaturnTranslation.z + asteroidTwoTransformations.translation.z;

    rRocksTranslation2 = {{riseOfPhoenixRocksInitialTranslation2.x + 0.5f, riseOfPhoenixRocksInitialTranslation2.y - 0.5f, riseOfPhoenixRocksInitialTranslation2.z - 0.35f}, 
                          {riseOfPhoenixRocksInitialTranslation2.x + 0.5f, riseOfPhoenixRocksInitialTranslation2.y + 0.35f, riseOfPhoenixRocksInitialTranslation2.z + 0.35f},};

    initializeRangeIcosahedrons_Asteroids(&riseOfPhoenixRockAttribs[RISE_OF_PHOENIX_NUM_ROCKS / 2], RISE_OF_PHOENIX_NUM_ROCKS / 2, rRocksGravity, rRocksTranslation2, rRocksRotation, rRocksScale);
    memcpy((void *)&riseOfPhoenixRockAttribs_shot2[RISE_OF_PHOENIX_NUM_ROCKS / 2], 
           (void *)&riseOfPhoenixRockAttribs[RISE_OF_PHOENIX_NUM_ROCKS / 2], 
           sizeof(IcosahedronAttribs_v2) * (RISE_OF_PHOENIX_NUM_ROCKS / 2));
}

void displaySceneFour(void)
{
    // local variables
    static int bOnce = 0;

    // code
    if(!bOnce)
    {
        setCameraCoordinatesUsingLookAt(riseOfPhoenixInitialCC);
        bOnce = 1;
    }

    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    {
        // reset the projection matrix
        glLoadIdentity();

        // add new projection
        gluPerspective(lerpFovy.x, (GLfloat)iCurrentWidth / (GLfloat)iCurrentHeight, 0.1, 10000.0);

        glMatrixMode(GL_MODELVIEW);

        // draw skybox
        drawTexturedSkyboxWithRotation(riseOfPhoenixSkyboxRotation);

        // draw orbit
        DrawOrbitJourneyScene(RISE_OF_PHOENIX_ORBIT_INNER_RADIUS, RISE_OF_PHOENIX_ORBIT_OUTER_RADIUS);

        // draw saturn
        glPushMatrix();
        {
            glTranslatef(riseOfPhoenixSaturnTranslation.x, riseOfPhoenixSaturnTranslation.y, riseOfPhoenixSaturnTranslation.z);
            drawSaturn(riseOfPhoenixSatYRot, riseOfPhoenixRingYRot, SHOW_ROCKS_RISE_OF_PHOENIX_SCENE);
        }
        glPopMatrix();

        // draw asteroids
        if(g_bShowAsteroids)
        {
            glPushMatrix();
            {
                colorIndex = 1;
                glTranslatef(riseOfPhoenixSaturnInitialTranslation.x, riseOfPhoenixSaturnInitialTranslation.y, riseOfPhoenixSaturnInitialTranslation.z);
                applyTransformations(asteroidOneTransformations);
                drawIcosahedron(1);
            }
            glPopMatrix();

            glPushMatrix();
            {
                colorIndex = 2;
                glTranslatef(riseOfPhoenixSaturnInitialTranslation.x, riseOfPhoenixSaturnInitialTranslation.y, riseOfPhoenixSaturnInitialTranslation.z);
                applyTransformations(asteroidTwoTransformations);
                drawIcosahedron(1);
            }
            glPopMatrix();
        }

        // draw rocks
        if((riseOfPhoenixSceneState == RISE_OF_PHOENIX_SHOT_ONE) && g_bShowRocks)
        {
            drawRangeIcosahedrons_Asteroids(riseOfPhoenixRockAttribs, RISE_OF_PHOENIX_NUM_ROCKS / 2, 0, 1.0f, 1);
            drawRangeIcosahedrons_Asteroids(&riseOfPhoenixRockAttribs[RISE_OF_PHOENIX_NUM_ROCKS / 2], RISE_OF_PHOENIX_NUM_ROCKS / 2, 0, 1.0f, 2);
        }

        if((riseOfPhoenixSceneState == RISE_OF_PHOENIX_SHOT_TWO) && g_bShowRocks)
        {
            drawRangeIcosahedrons_Asteroids(riseOfPhoenixRockAttribs_shot2, RISE_OF_PHOENIX_NUM_ROCKS / 2, 0, 1.0f, 1);
            drawRangeIcosahedrons_Asteroids(&riseOfPhoenixRockAttribs_shot2[RISE_OF_PHOENIX_NUM_ROCKS / 2], RISE_OF_PHOENIX_NUM_ROCKS / 2, 0, 1.0f, 2);
        }

        glMatrixMode(GL_PROJECTION);
    }
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);

    // fadeInEffect(riseOfPhoenixFadeInAlpha);
}

void updateSceneFour(void)
{
    // function prototypes
    void uninitializeSceneFour(void);

    // local variables
    static BOOL bOnce = FALSE; 
    static GLfloat angleInRadians = 0.0f;
    static Vec3CUDA saturnPos = {0};
    static GLfloat saturnRadius = 1.0f;
    static GLfloat revSpeed = (360.0f / (FPS * 80.0f));
    static BOOL bFirstCollisionFlag = FALSE;
    static BOOL bSecondCollisionFlag = FALSE;

    // code
#if (DO_RISE_OF_PHOENIX_ANIMATION == 0)
    if(TRUE)
    {
        return ;
    }
#endif

#if PLAY_RISE_OF_PHOENIX_MUSIC
    if(!bOnce)
    {
        SetCurrentSourceForStats(g_CompleteTrackSource, FALSE, FALSE); // bind the source
        seekALSourcef(g_CompleteTrackSource, 0.0f, 1.0f, 58.0f); // seek to the Rise Of Phoenix part (1:58)
        playALSource(g_CompleteTrackSource); // play the source
        bOnce = TRUE;
    }
#endif

    if(totalElapsedTime >= 22.0)
    {
        riseOfPhoenixSceneState = END;
    }

    // speedFactor = g_Transformations[1].translation.x;

    // set the speed factor according to time
    if(totalElapsedTime >= 0.0 && totalElapsedTime < 4.0) // 1:58 to 2:01
    {
        speedFactor = normalSpeed;
    }
    else if(totalElapsedTime >= 4.0 && totalElapsedTime < 7.0) // 2:01 to 2:04
    {
        speedFactor = slowSpeed;
    }
    else if(totalElapsedTime >= 7.0 && totalElapsedTime < 10.0) // 2:04 to 2:07
    {
        speedFactor = fastSpeed;
    } 
    else if(totalElapsedTime >= 10.0 && totalElapsedTime < 14.0) // 2:07 to 2:11
    {
        speedFactor = normalSpeed;
    }
    else if(totalElapsedTime >= 14.0 && totalElapsedTime < 17.0) // 2:11 to 2:14
    {
        speedFactor = slowSpeed;
    }
    else if(totalElapsedTime >= 17.0 && totalElapsedTime <= 20.0) // 2:14 to 2:18
    {
        speedFactor = fastSpeed;
    }

    // update skybox rotation
    updateRotationValue(&riseOfPhoenixSkyboxRotation, (360.0f / (FPS * 100.0f)) * speedFactor);

    // update saturn rotation
    updateRotationValue(&riseOfPhoenixSatYRot, (360.0f / (FPS * 10.0f)) * speedFactor);
    updateRotationValue(&riseOfPhoenixRingYRot, (360.0f / (FPS * 10.0f)) * speedFactor);

    // update saturn revolution
    riseOfPhoenixSaturnRevolutionAngle = riseOfPhoenixSaturnRevolutionAngle - (revSpeed * speedFactor);
    angleInRadians = riseOfPhoenixSaturnRevolutionAngle * (M_PI / 180.0f);

    riseOfPhoenixSaturnTranslation.x = RISE_OF_PHOENIX_ORBIT_RADIUS * cosf(angleInRadians);
    riseOfPhoenixSaturnTranslation.z = RISE_OF_PHOENIX_ORBIT_RADIUS * sinf(angleInRadians);

    // saturn position will be stored in this vector
    saturnPos = {riseOfPhoenixSaturnTranslation.x, riseOfPhoenixSaturnTranslation.y, riseOfPhoenixSaturnTranslation.z};

    // handle scene states
    switch(riseOfPhoenixSceneState)
    {
    case RISE_OF_PHOENIX_SHOT_ONE:
        // lerp fovy
        lerpMyValue(&lerpFovy, 1.0f);

        // update camera revolution
        angleInRadians = (riseOfPhoenixSaturnRevolutionAngle + CAMERA_INITIAL_ANGLE_OFFSET) * (M_PI / 180.0f);
        
        cameraCoordinates.angleY = cameraCoordinates.angleY - (revSpeed * speedFactor);
        cameraCoordinates.position.x = RISE_OF_PHOENIX_ORBIT_RADIUS * cosf(angleInRadians);
        cameraCoordinates.position.z = RISE_OF_PHOENIX_ORBIT_RADIUS * sinf(angleInRadians);

        updateCameraVector();

        // hide asteroids and show rocks after 1 second
        if(totalElapsedTime >= 0.5 && !bFirstCollisionFlag)
        {
            g_bShowAsteroids = FALSE;
            g_bShowRocks = TRUE;
            bFirstCollisionFlag = TRUE;
        }

        // update rocks (for this shot)
        if(g_bShowRocks)
        {
            updateRangeIcosahedrons_Asteroids(riseOfPhoenixRockAttribs, 
                                            RISE_OF_PHOENIX_NUM_ROCKS, 
                                            saturnPos, 
                                            saturnRadius,
                                            speedFactor);
        }
    
        // switch to next state after 10 seconds have passed
        if(totalElapsedTime >= 10.0)
        {
            // reset asteroid and rock positions
            g_bShowAsteroids = TRUE;
            g_bShowRocks = FALSE;

            // reset saturn position
            riseOfPhoenixSaturnRevolutionAngle = 90.0f;
            riseOfPhoenixSaturnTranslation = riseOfPhoenixSaturnStart;

            // reset fovy
            lerpFovy.x = 45.0f;
            lerpFovy.x0 = 45.0f;
            lerpFovy.x1 = 45.0f;
            lerpFovy.speed = 15.0f / (FPS * 5.0f); 
            lerpFovy.tVal = 0.0f;

            // set new camera position
            setCameraCoordinates(riseOfPhoenixShotTwoCC);

            riseOfPhoenixSceneState = RISE_OF_PHOENIX_SHOT_TWO;
        }
        break;
    case RISE_OF_PHOENIX_SHOT_TWO:
        // pan camera
        panCamera(&riseOfPhoenixCameraPan,                                           /* CameraPan struct */
                   ((riseOfPhoenixCameraPan.totalPan / (FPS * 9.0f)) * speedFactor), /* speed */
                   CAMERA_NEGATIVE_PAN_FLAG);                                        /* pan flag*/

        // cameraCoordinates.front.x = saturnPos.x;
        // cameraCoordinates.front.y = saturnPos.y;
        // cameraCoordinates.front.z = saturnPos.z;

        // hide asteroids and show rocks after 11 seconds
        if(totalElapsedTime >= 10.5 && !bSecondCollisionFlag)
        {
            g_bShowAsteroids = FALSE;
            g_bShowRocks = TRUE;
            bSecondCollisionFlag = TRUE;
        }

        // update rocks (for this shot)
        if(g_bShowRocks)
        {
            updateRangeIcosahedrons_Asteroids(riseOfPhoenixRockAttribs_shot2, 
                                                RISE_OF_PHOENIX_NUM_ROCKS, 
                                                saturnPos, 
                                                saturnRadius,
                                                speedFactor);
        }
        break;
    case END:
        uninitializeSceneFour();
        ResetGlobalTimer();
        setCameraCoordinates({{0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 1.0f, 0.0f}, 0.0f, 270.0f});
        currentScene = SCENE_GOLDEN_YEARS;
        break;
    }
/*
     switch(riseOfPhoenixSceneState)
    {
        case ASTERIOD_GOING_STATE:
            if(alpha_bigAsteroid > 0.0f)
            {
                alpha_bigAsteroid -= 0.008f;
            }
            if(alpha_bigAsteroid <= 0.8f )
            {
                alpha_smallAsteroid += 0.5f;
            }
            if(alpha_smallAsteroid >= 1.0f)
            {
              riseOfPhoenixSceneState = ASTERIOD_MOVING_STATE;  
            }
            saturnStartPosition.translation.z += 0.15f;
            break;

        case ASTERIOD_MOVING_STATE:
            if((rangestart.x < 20.0) && (rangestart.x > 5.0))
            {
                rangestart.x += 0.1f;
                rangestart.y += 0.1f;
                rangestart.z += 0.1f;
                rangeEnd.x -= 0.1f;
                rangeEnd.y -= 0.1f;
                rangeEnd.z -= 0.005f;
            }
            else
            {
                rangestart.x += 0.5f;
                rangestart.y += 0.5f;
                rangestart.z += 0.5f;
                rangeEnd.x -= 0.5f;
                rangeEnd.y -= 0.5f;
                rangeEnd.z -= 0.08f;
            }
            initializeRangeIcosahedrons(icoAttribs_ROS, 1080, {0.0f,0.0f,0.0f}, rangestart, rangeEnd);
            translateDagadLeft += 0.03f;
            translateDagadRight += 0.03f;
            saturnStartPosition.translation.z += 0.15f;
            if(saturnStartPosition.translation.z > 2.5f)
            {
                riseOfPhoenixSceneState = END;
            }
            break;
        case END:
            uninitializeSceneFour();
            currentScene = SCENE_GOLDEN_YEARS;
            break;
        default:
            break;      
    }
*/
}

void uninitializeSceneFour(void)
{
    // code
    // stopALSource(g_CompleteTrackSource);
    // SetCurrentSourceForStats(0, FALSE, FALSE); // unbind the source
}
