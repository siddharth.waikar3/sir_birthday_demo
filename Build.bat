cls

del Common.obj ^
OGL.obj ^
Common.obj ^
Camera.obj ^
OpeningScene.obj ^
JourneyScene.obj ^
ObstaclesScene.obj ^
RiseOfPhoenixScene.obj ^
GoldenYearsScene.obj LifeScene.obj ^
NewJourneyScene.obj ^
HandingOverLegacyScene.obj ^
TheEndScene.obj ^
OGL.res ^
RenderText.exp ^
RenderText.obj ^
CreditsScene.obj ^
TitleScene.obj ^
Astroid.obj ^
Years.obj ^
SlideText.obj ^
Skybox.obj ^
Texture.obj ^
Orbit.obj ^
Saturn.obj ^
Icosahedron.obj ^
OrbitText.obj ^
dagad.obj ^
SplashScreen.obj ^
imgui.obj ^
imgui_demo.obj ^
imgui_draw.obj ^
imgui_impl_opengl2.obj ^
imgui_impl_win32.obj ^
imgui_tables.obj ^
imgui_widgets.obj

cl.exe /MP6 /c /EHsc /D DEBUG /D UNICODE /I "C:\Program Files (x86)\OpenAL 1.1 SDK\include" ^
OGL.cpp ^
.\Common\Common.cpp ^
.\Common\Camera.cpp ^
.\Common\Texture.cpp ^
.\Scenes\OpeningScene.cpp ^
.\Scenes\JourneyScene.cpp ^
.\Scenes\ObstaclesScene.cpp ^
.\Scenes\RiseOfPhoenixScene.cpp ^
.\Scenes\GoldenYearsScene.cpp ^
.\Scenes\LifeScene.cpp ^
.\Scenes\NewJourneyScene.cpp ^
.\Scenes\HandingOverLegacyScene.cpp ^
.\Scenes\TheEndScene.cpp ^
.\Scenes\TitleScene.cpp ^
.\Scenes\CreditsScene.cpp ^
.\Scenes\TestScene.cpp ^
.\Objects\Astroid.cpp ^
.\Objects\Years.cpp ^
.\Objects\SlideText.cpp ^
.\Objects\Skybox.cpp ^
.\Objects\Orbit.cpp ^
.\Objects\SphereCollision.cpp ^
.\Objects\Saturn.cpp ^
.\Objects\Icosahedron.cpp ^
.\Objects\OrbitText.cpp ^
.\Objects\dagad.cpp ^
.\Common\SplashScreen.cpp ^
.\imgui/imgui.cpp ^
.\imgui/imgui_demo.cpp ^
.\imgui/imgui_draw.cpp ^
.\imgui/imgui_impl_opengl2.cpp ^
.\imgui/imgui_impl_win32.cpp ^
.\imgui/imgui_tables.cpp ^
.\imgui/imgui_widgets.cpp

rc.exe /I "C:\Program Files (x86)\OpenAL 1.1 SDK\include" OGL.rc

link.exe /out:Sir_Birthday_24_10_2023.exe /LIBPATH:"C:\Program Files (x86)\OpenAL 1.1 SDK\libs\Win64" ^
OGL.obj ^
Common.obj ^
Camera.obj ^
OpeningScene.obj ^
JourneyScene.obj ^
ObstaclesScene.obj ^
RiseOfPhoenixScene.obj ^
GoldenYearsScene.obj ^
LifeScene.obj ^
NewJourneyScene.obj ^
HandingOverLegacyScene.obj ^
TheEndScene.obj ^
TitleScene.obj ^
CreditsScene.obj ^
TestScene.obj ^
Astroid.obj ^
Years.obj ^
SlideText.obj ^
OGL.res ^
Skybox.obj ^
Texture.obj ^
Orbit.obj ^
SphereCollision.obj ^
Saturn.obj ^
Icosahedron.obj ^
OrbitText.obj ^
dagad.obj ^
SplashScreen.obj ^
imgui.obj ^
imgui_demo.obj ^
imgui_draw.obj ^
imgui_impl_opengl2.obj ^
imgui_impl_win32.obj ^
imgui_tables.obj ^
imgui_widgets.obj ^
User32.lib GDI32.lib Winmm.lib OpenAL32.lib /SUBSYSTEM:WINDOWS

Sir_Birthday_24_10_2023.exe
