// header files
#include "..\Common\Common.h"
#include "dagad.h"
#define X_ver .525731112119133606f
#define Z_ver .850650808352039932f


GLfloat vertices[12][3] = { {-X_ver, 0.0, Z_ver}, {X_ver, 0.0, Z_ver}, {-X_ver, 0.0, -Z_ver}, {X_ver, 0.0, -Z_ver},
   {0.0, Z_ver, X_ver}, {0.0, Z_ver, -X_ver}, {0.0, -Z_ver, X_ver}, {0.0, -Z_ver, -X_ver},
   {Z_ver, X_ver, 0.0}, {-Z_ver, X_ver, 0.0}, {Z_ver, -X_ver, 0.0}, {-Z_ver, -X_ver, 0.0}
};

GLint points[20][3] = {{0,4,1}, {0,9,4}, {9,5,4}, {4,5,8}, {4,8,1},
   {8,10,1}, {8,3,10}, {5,3,8}, {5,2,3}, {2,7,3},
   {7,10,3}, {7,6,10}, {7,11,6}, {11,0,6}, {0,1,6},
   {6,1,10}, {9,0,11}, {9,11,2}, {9,2,5}, {7,2,11}
};

void polygon(int maxDepth,GLfloat colorVector[3][4])
{
	//function declaration
	void subDivide_riseOfPhonix(GLfloat* v1, GLfloat* v2, GLfloat* v3, int depth,GLfloat colorVector[3][4]);	
	
	
	for(int i = 0;i<20;i++)
	{
		srand(i);
		subDivide_riseOfPhonix(&vertices[points[i][0]][0], &vertices[points[i][1]][0], &vertices[points[i][2]][0], maxDepth,colorVector);
	}
}

void normalize(GLfloat *v) {
    GLfloat d = sqrt((v[0] * v[0]) + (v[1] * v[1]) + (v[2] * v[2]));
    if (d == 0.0){ return;}
    v[0] /= d;
    v[1] /= d;
    v[2] /= d;

}

void subDivide_riseOfPhonix(GLfloat* v1, GLfloat* v2, GLfloat* v3, int depth, GLfloat colorVector[3][4]) 
{
	void normalize(GLfloat *v);
	void DrawTriangle(GLfloat *v1,GLfloat *v2,GLfloat *v3, GLfloat colorVector[3][4]);

    if (depth == 0) {
		DrawTriangle(v1,v2,v3,colorVector);
        return;
    }
    //midpoint of each edge
    GLfloat v12[3];
    GLfloat v23[3];
    GLfloat v31[3];
    for (int i = 0; i < 3; i++) {
        v12[i] = v1[i] + v2[i];
        v23[i] = v2[i] + v3[i];
        v31[i] = v3[i] + v1[i];
    }
    normalize(v12);
    normalize(v23);
    normalize(v31);

    subDivide_riseOfPhonix(v1, v12, v31, depth - 1,colorVector);
    subDivide_riseOfPhonix(v2, v23, v12, depth - 1,colorVector);
    subDivide_riseOfPhonix(v3, v31, v23, depth - 1,colorVector);
    subDivide_riseOfPhonix(v12, v23, v31, depth - 1,colorVector);
}

void DrawTriangle(GLfloat *v1,GLfloat *v2,GLfloat *v3,GLfloat colorVector[3][4]) 
{
	glBegin(GL_TRIANGLES);
	{
		glColor4fv(colorVector[0]);
		glVertex3fv(v1);

		glColor4fv(colorVector[1]);
		glVertex3fv(v2);
					
		glColor4fv(colorVector[2]);
		glVertex3fv(v3);
	}
	glEnd();
}
