#pragma once

// function declarations
void initializeSceneHandingOverLegacy(void);
void displaySceneHandingOverLegacy(void);
void updateSceneHandingOverLegacy(void);
void uninitializeSceneHandingOverLegacy(void);
