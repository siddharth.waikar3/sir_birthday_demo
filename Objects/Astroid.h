#pragma once

// header files
#include "../Common/Common.h"

// extern variables
extern GLint colorIndex;

// function declarations
void drawIcosahedron(int maxDepth);
