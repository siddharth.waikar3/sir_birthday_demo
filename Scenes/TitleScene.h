#pragma once

// function declarations
void initializeSceneTitle(void);
void displaySceneTitle(void);
void updateSceneTitle(void);
void uninitializeSceneTitle(void);