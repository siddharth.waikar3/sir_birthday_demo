// UNICODE
#ifndef UNICODE
	#define UNICODE
#endif

#pragma once

#define SPHERE_SPEED_DELTA 0.001f

// header files
#include <windows.h> // for the Standard Win32 API
#include <stdio.h>   // for file IO
#include <stdlib.h>  // for malloc(), free() and exit()

#define _USE_MATH_DEFINES // for M_PI constant
#include <math.h>         // for math functions like sinf(), cosf(), fabsf(), floorf(), ceilf(), powf() and so on...

// OpenGL header files
#include <gl/GL.h>
#include <gl/GLU.h> // for the GLU (Graphics Library Utility) native library

// struct definitions
typedef struct tagPoint
{
	GLfloat x, y, z, s, t;
} Point, Vector, Position;

typedef struct tagVec3
{
	GLfloat x, y, z;
} Vec3;

typedef struct tagVector2D
{
    // vector position
    Position pos;

    // vector directions
    GLfloat up;
    GLfloat down;
    GLfloat left;
    GLfloat right;
	GLfloat in;
	GLfloat out;
} Vector2D;

typedef struct tagColor
{
    GLfloat r, g, b, alpha;
} Color;

struct Translation
{
	GLfloat x;
	GLfloat y;
	GLfloat z;
};

struct Rotation
{
	GLfloat x;
	GLfloat y;
	GLfloat z;
};

struct Scale
{
	GLfloat x;
	GLfloat y;
	GLfloat z;
};

struct Transformations
{
	struct Translation translation;
	struct Rotation rotation;
	struct Scale scale;
};
