// header files
#include "Icosahedron.h"
#include "Astroid.h"
#include "../Common/Camera.h"

// macros
#define HALF_GOLDEN_RATIO 1.618033988 / 2.0
#define ICO_EDGE_LEN 0.5f

#define DT 1.0f / 60.0f

#define ROTATION_FACTOR 32.0f
#define EXPLOSION_FACTOR 24.0f

// global variables
GLfloat icosahedronVertices[] =
{   
    -ICO_EDGE_LEN, 0.0f, HALF_GOLDEN_RATIO,  // 0
    ICO_EDGE_LEN, 0.0f, HALF_GOLDEN_RATIO,   // 1
    -ICO_EDGE_LEN, 0.0f, -HALF_GOLDEN_RATIO, // 2
    ICO_EDGE_LEN, 0.0f, -HALF_GOLDEN_RATIO,  // 3

    0.0f, HALF_GOLDEN_RATIO, ICO_EDGE_LEN,   // 4
    0.0f, HALF_GOLDEN_RATIO, -ICO_EDGE_LEN,  // 5
    0.0f, -HALF_GOLDEN_RATIO, ICO_EDGE_LEN,  // 6
    0.0f, -HALF_GOLDEN_RATIO, -ICO_EDGE_LEN, // 7

    HALF_GOLDEN_RATIO, ICO_EDGE_LEN, 0.0f,   // 8
    -HALF_GOLDEN_RATIO, ICO_EDGE_LEN, 0.0f,  // 9
    HALF_GOLDEN_RATIO, -ICO_EDGE_LEN, 0.0f,  // 10
    -HALF_GOLDEN_RATIO, -ICO_EDGE_LEN, 0.0f  // 11
};

GLubyte icosahedronIndices[] =
{
    0, 4, 1,
    0, 9, 4,
    9, 5, 4,
    4, 5, 8,
    4, 8, 1,
    8, 10, 1,
    8, 3, 10,
    5, 3, 8,
    5, 2, 3,
    2, 7, 3,
    7, 10, 3,
    7, 6, 10,
    7, 11, 6,
    11, 0, 6,
    0, 1, 6, 
    6, 1, 10, 
    9, 0, 11, 
    9, 11, 2, 
    9, 2, 5,
    7, 2, 11
};

GLfloat icosahedronTexcoords[] =
{
    0.5f, 1.0f,
    0.0f, 0.0f,
    1.0f, 0.0f,

    0.5f, 1.0f,
    0.0f, 0.0f,
    1.0f, 0.0f,
    
    0.5f, 1.0f,
    0.0f, 0.0f,
    1.0f, 0.0f
};

GLfloat icosahedronVerticesFull[] =
{
    -ICO_EDGE_LEN, 0.0f, HALF_GOLDEN_RATIO,
    0.0f, HALF_GOLDEN_RATIO, ICO_EDGE_LEN,
    ICO_EDGE_LEN, 0.0f, HALF_GOLDEN_RATIO,

    -ICO_EDGE_LEN, 0.0f, HALF_GOLDEN_RATIO,
    -HALF_GOLDEN_RATIO, ICO_EDGE_LEN, 0.0f,
    0.0f, HALF_GOLDEN_RATIO, ICO_EDGE_LEN,

    -HALF_GOLDEN_RATIO, ICO_EDGE_LEN, 0.0f,
    0.0f, HALF_GOLDEN_RATIO, -ICO_EDGE_LEN,
    0.0f, HALF_GOLDEN_RATIO, ICO_EDGE_LEN,

    0.0f, HALF_GOLDEN_RATIO, ICO_EDGE_LEN,
    0.0f, HALF_GOLDEN_RATIO, -ICO_EDGE_LEN,
    HALF_GOLDEN_RATIO, ICO_EDGE_LEN, 0.0f,

    0.0f, HALF_GOLDEN_RATIO, ICO_EDGE_LEN,
    HALF_GOLDEN_RATIO, ICO_EDGE_LEN, 0.0f,
    ICO_EDGE_LEN, 0.0f, HALF_GOLDEN_RATIO,

    HALF_GOLDEN_RATIO, ICO_EDGE_LEN, 0.0f,
    HALF_GOLDEN_RATIO, -ICO_EDGE_LEN, 0.0f,
    ICO_EDGE_LEN, 0.0f, HALF_GOLDEN_RATIO,

    HALF_GOLDEN_RATIO, ICO_EDGE_LEN, 0.0f,
    ICO_EDGE_LEN, 0.0f, -HALF_GOLDEN_RATIO,
    HALF_GOLDEN_RATIO, -ICO_EDGE_LEN, 0.0f,

    0.0f, HALF_GOLDEN_RATIO, -ICO_EDGE_LEN,
    ICO_EDGE_LEN, 0.0f, -HALF_GOLDEN_RATIO,
    HALF_GOLDEN_RATIO, ICO_EDGE_LEN, 0.0f,

    0.0f, HALF_GOLDEN_RATIO, -ICO_EDGE_LEN,
    -ICO_EDGE_LEN, 0.0f, -HALF_GOLDEN_RATIO,
    ICO_EDGE_LEN, 0.0f, -HALF_GOLDEN_RATIO,

    -ICO_EDGE_LEN, 0.0f, -HALF_GOLDEN_RATIO,
    0.0f, -HALF_GOLDEN_RATIO, -ICO_EDGE_LEN,
    ICO_EDGE_LEN, 0.0f, -HALF_GOLDEN_RATIO,

    0.0f, -HALF_GOLDEN_RATIO, -ICO_EDGE_LEN,
    HALF_GOLDEN_RATIO, -ICO_EDGE_LEN, 0.0f,
    ICO_EDGE_LEN, 0.0f, -HALF_GOLDEN_RATIO,

    0.0f, -HALF_GOLDEN_RATIO, -ICO_EDGE_LEN,
    0.0f, -HALF_GOLDEN_RATIO, ICO_EDGE_LEN,
    HALF_GOLDEN_RATIO, -ICO_EDGE_LEN, 0.0f,

    0.0f, -HALF_GOLDEN_RATIO, -ICO_EDGE_LEN,
    -HALF_GOLDEN_RATIO, -ICO_EDGE_LEN, 0.0f,
    0.0f, -HALF_GOLDEN_RATIO, ICO_EDGE_LEN,

    -HALF_GOLDEN_RATIO, -ICO_EDGE_LEN, 0.0f,
    -ICO_EDGE_LEN, 0.0f, HALF_GOLDEN_RATIO,
    0.0f, -HALF_GOLDEN_RATIO, ICO_EDGE_LEN,

    -ICO_EDGE_LEN, 0.0f, HALF_GOLDEN_RATIO,
    ICO_EDGE_LEN, 0.0f, HALF_GOLDEN_RATIO,
    0.0f, -HALF_GOLDEN_RATIO, ICO_EDGE_LEN,

    0.0f, -HALF_GOLDEN_RATIO, ICO_EDGE_LEN,
    ICO_EDGE_LEN, 0.0f, HALF_GOLDEN_RATIO,
    HALF_GOLDEN_RATIO, -ICO_EDGE_LEN, 0.0f,

    -HALF_GOLDEN_RATIO, ICO_EDGE_LEN, 0.0f,  // 9
    -ICO_EDGE_LEN, 0.0f, HALF_GOLDEN_RATIO,
    -HALF_GOLDEN_RATIO, -ICO_EDGE_LEN, 0.0f,  // 11

    -HALF_GOLDEN_RATIO, ICO_EDGE_LEN, 0.0f,  // 9
    -HALF_GOLDEN_RATIO, -ICO_EDGE_LEN, 0.0f,  // 11
    -ICO_EDGE_LEN, 0.0f, -HALF_GOLDEN_RATIO,

    -HALF_GOLDEN_RATIO, ICO_EDGE_LEN, 0.0f,
    -ICO_EDGE_LEN, 0.0f, -HALF_GOLDEN_RATIO,
    0.0f, HALF_GOLDEN_RATIO, -ICO_EDGE_LEN,

    0.0f, -HALF_GOLDEN_RATIO, -ICO_EDGE_LEN,
    -ICO_EDGE_LEN, 0.0f, -HALF_GOLDEN_RATIO,
    -HALF_GOLDEN_RATIO, -ICO_EDGE_LEN, 0.0f
};

GLubyte icosahedronIndicesFull[] =
{
    0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
    21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
    41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59
};

GLfloat icosahedronTexcoordsFull[] =
{
    0.5f, 1.0f,
    0.0f, 0.0f,
    1.0f, 0.0f,
    0.5f, 1.0f,
    0.0f, 0.0f,
    1.0f, 0.0f,
    0.5f, 1.0f,
    0.0f, 0.0f,
    1.0f, 0.0f,
    0.5f, 1.0f,
    0.0f, 0.0f,
    1.0f, 0.0f,
    0.5f, 1.0f,
    0.0f, 0.0f,
    1.0f, 0.0f,
    0.5f, 1.0f,
    0.0f, 0.0f,
    1.0f, 0.0f,
    0.5f, 1.0f,
    0.0f, 0.0f,
    1.0f, 0.0f,
    0.5f, 1.0f,
    0.0f, 0.0f,
    1.0f, 0.0f,
    0.5f, 1.0f,
    0.0f, 0.0f,
    1.0f, 0.0f,
    0.5f, 1.0f,
    0.0f, 0.0f,
    1.0f, 0.0f,
    0.5f, 1.0f,
    0.0f, 0.0f,
    1.0f, 0.0f,
    0.5f, 1.0f,
    0.0f, 0.0f,
    1.0f, 0.0f,
    0.5f, 1.0f,
    0.0f, 0.0f,
    1.0f, 0.0f,
    0.5f, 1.0f,
    0.0f, 0.0f,
    1.0f, 0.0f,
    0.5f, 1.0f,
    0.0f, 0.0f,
    1.0f, 0.0f,
    0.5f, 1.0f,
    0.0f, 0.0f,
    1.0f, 0.0f,
    0.5f, 1.0f,
    0.0f, 0.0f,
    1.0f, 0.0f,
    0.5f, 1.0f,
    0.0f, 0.0f,
    1.0f, 0.0f,
    0.5f, 1.0f,
    0.0f, 0.0f,
    1.0f, 0.0f,
    0.5f, 1.0f,
    0.0f, 0.0f,
    1.0f, 0.0f,
};

extern FILE *gpFILE;
extern CameraCoordinates cameraCoordinates;

// function definitions
void drawRock(GLuint texture, GLenum mode)
{
    // code
    // point to icosahedron's vertices
    glVertexPointer(3, GL_FLOAT, 0, icosahedronVerticesFull);

    // point to icosahedron's tex coords
    glTexCoordPointer(2, GL_FLOAT, 0, icosahedronTexcoordsFull);

    // draw icosahedron
    glBindTexture(GL_TEXTURE_2D, texture);
    glDrawElements(GL_TRIANGLES, 60, GL_UNSIGNED_BYTE, icosahedronIndicesFull);
    glBindTexture(GL_TEXTURE_2D, 0);
}

void initializeRangeIcosahedrons_Asteroids(IcosahedronAttribs_v2 *icoAttribs, GLint numIcosahedrons, Range3 rGravity, Range3 rTranslation, Range3 rRotation, Range rScale)
{
    // code
    for(int i = 0; i < numIcosahedrons; i++)
    {
        // random velocity (gravity) values in the range [rGravity.start, rGravity.end]
        GLfloat gx = getInterpolatedValue(0.0f, (GLfloat)RAND_MAX, rGravity.start.x, rGravity.end.x, rand());  
        GLfloat gy = getInterpolatedValue(0.0f, (GLfloat)RAND_MAX, rGravity.start.y, rGravity.end.y, rand());
        GLfloat gz = getInterpolatedValue(0.0f, (GLfloat)RAND_MAX, rGravity.start.z, rGravity.end.z, rand());

        // random translation from origin values in the range [rTranslation.start, rTranslation.end]
        GLfloat tx = getInterpolatedValue(0.0f, (GLfloat)RAND_MAX, rTranslation.start.x, rTranslation.end.x, rand());
        GLfloat ty = getInterpolatedValue(0.0f, (GLfloat)RAND_MAX, rTranslation.start.y, rTranslation.end.y, rand());
        GLfloat tz = getInterpolatedValue(0.0f, (GLfloat)RAND_MAX, rTranslation.start.z, rTranslation.end.z, rand());

        // random rotation values in the range [rRotation.start, rRotation.end]
        GLfloat rx = getInterpolatedValue(0.0f, (GLfloat)RAND_MAX, rRotation.start.x, rRotation.end.x, rand());
        GLfloat ry = getInterpolatedValue(0.0f, (GLfloat)RAND_MAX, rRotation.start.y, rRotation.end.y, rand());
        GLfloat rz = getInterpolatedValue(0.0f, (GLfloat)RAND_MAX, rRotation.start.z, rRotation.end.z, rand());

        // random scale values in the range [rScale.start, rScale.end]
        GLfloat sx = getInterpolatedValue(0.0f, (GLfloat)RAND_MAX, rScale.start, rScale.end, rand());
        GLfloat sy = sx;
        GLfloat sz = sy;

        // assign the values
        icoAttribs[i].acc = {0.0f, 0.0f, 0.0f};
        icoAttribs[i].gravity = {gx, gy, gz};
        icoAttribs[i].pos = {tx, ty, tz};
        icoAttribs[i].oldPos = {0.0f, 0.0f, 0.0f};
        icoAttribs[i].rotation = {rx, ry, rz};
        icoAttribs[i].scale = {sx, sy, sz};
        icoAttribs[i].radius = sx;
    }
}

void initializeRangeIcosahedrons(IcosahedronAttribs *icoAttribs, GLint numIcosahedrons, Vec3 origin, Vec3 rangeStart, Vec3 rangeEnd)
{
    // code
    for(int i = 0; i < numIcosahedrons; i++)
    {
        // random translation from origin values in the range [rangeStart, rangeEnd]
        GLfloat tx = getInterpolatedValue(0.0f, (GLfloat)RAND_MAX, rangeStart.x, rangeEnd.x, rand());
        GLfloat ty = getInterpolatedValue(0.0f, (GLfloat)RAND_MAX, rangeStart.y, rangeEnd.y, rand());
        GLfloat tz = getInterpolatedValue(0.0f, (GLfloat)RAND_MAX, rangeStart.z, rangeEnd.z, rand());

        // random rotation values in the range [0.0, 180.0]
        GLfloat rx = getInterpolatedValue(0.0f, (GLfloat)RAND_MAX, 0.0f, 180.0f, rand());
        GLfloat ry = getInterpolatedValue(0.0f, (GLfloat)RAND_MAX, 0.0f, 180.0f, rand());
        GLfloat rz = getInterpolatedValue(0.0f, (GLfloat)RAND_MAX, 0.0f, 180.0f, rand());

        // random scale values in the range [0.8, 1.0]
        GLfloat sx = getInterpolatedValue(0.0f, (GLfloat)RAND_MAX, 0.4f, 1.0f, rand());
        GLfloat sy = sx;
        GLfloat sz = sy;

        // assign the values
        icoAttribs[i].originTranslation = origin;
        icoAttribs[i].translationFromOrigin = {tx, ty, tz};
        icoAttribs[i].rotation = {rx, ry, rz};
        icoAttribs[i].scale = {sx, sy, sz};
    }
}

void drawRangeIcosahedrons_Asteroids(IcosahedronAttribs_v2 *icoAttribs, GLint numIcosahedrons, GLuint texture, GLfloat scale, GLint colorIdx)
{
    // code
    colorIndex = colorIdx;
    for(int i = 0; i < numIcosahedrons; i++)
    {
        glPushMatrix();
        {
            glTranslatef(icoAttribs[i].pos.x, icoAttribs[i].pos.y, icoAttribs[i].pos.z);
            glRotatef(icoAttribs[i].rotation.x, 1.0f, 0.0f, 0.0f);
            glRotatef(icoAttribs[i].rotation.y, 0.0f, 1.0f, 0.0f);
            glRotatef(icoAttribs[i].rotation.z, 0.0f, 0.0f, 1.0f);
            glScalef(icoAttribs[i].scale.x, icoAttribs[i].scale.y, icoAttribs[i].scale.z);
            glScalef(scale, scale, scale);
            drawIcosahedron(0);
        }
        glPopMatrix();
    }
}

void drawRangeIcosahedrons(IcosahedronAttribs *icoAttribs, GLint numIcosahedrons, GLuint texture, GLfloat scale)
{
    // code
    // enable some things
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_INDEX_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);

    for(int i = 0; i < numIcosahedrons; i++)
    {
        glPushMatrix();
        {
            glTranslatef(icoAttribs[i].originTranslation.x, icoAttribs[i].originTranslation.y, icoAttribs[i].originTranslation.z);
            glTranslatef(icoAttribs[i].translationFromOrigin.x, icoAttribs[i].translationFromOrigin.y, icoAttribs[i].translationFromOrigin.z);
            glRotatef(icoAttribs[i].rotation.x, 1.0f, 0.0f, 0.0f);
            glRotatef(icoAttribs[i].rotation.y, 0.0f, 1.0f, 0.0f);
            glRotatef(icoAttribs[i].rotation.z, 0.0f, 0.0f, 1.0f);
            glScalef(icoAttribs[i].scale.x, icoAttribs[i].scale.y, icoAttribs[i].scale.z);
            glScalef(scale, scale, scale);
            drawRock(texture, GL_TRIANGLES);
        }
        glPopMatrix();
    }

    // disable somethings
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    glDisableClientState(GL_INDEX_ARRAY);
    glDisableClientState(GL_VERTEX_ARRAY);
}

BOOL updateRangeIcosahedrons_Asteroids(IcosahedronAttribs_v2 *icoAttribs, GLint numIcosahedrons, Vec3CUDA saturnPos, GLfloat saturnRadius, GLfloat speedFactor)
{
    // function prototypes
    void applyGravity_Asteroids(Vec3CUDA *acc, Vec3CUDA gravity, GLfloat speedFactor);
    BOOL checkCollisionWithSaturn(IcosahedronAttribs_v2 *icoObject, Vec3CUDA saturnPos, GLfloat saturnRadius, GLfloat speedFactor);
    void checkCollisionWithCamera(IcosahedronAttribs_v2 *icoObject, GLfloat speedFactor);
    
    // local variables
    BOOL bRet = FALSE;

    // code
    for(int i = 0; i < numIcosahedrons; i++)
    {
        // apply gravity
        applyGravity_Asteroids(&icoAttribs[i].pos, icoAttribs[i].gravity, speedFactor);
        applyGravity_Asteroids(&icoAttribs[i].rotation, icoAttribs[i].gravity, speedFactor * ROTATION_FACTOR);

        // check collision with saturn
        bRet = checkCollisionWithSaturn(&icoAttribs[i], saturnPos, saturnRadius, speedFactor);

        // check collision with camera
        checkCollisionWithCamera(&icoAttribs[i], speedFactor);
    }

    return(bRet);
}

// internal functions
void applyGravity_Asteroids(Vec3CUDA *acc, Vec3CUDA gravity, GLfloat speedFactor)
{
    // code
    acc->x = acc->x + gravity.x * speedFactor;
    acc->y = acc->y + gravity.y * speedFactor;
    acc->z = acc->z + gravity.z * speedFactor;
}

BOOL checkCollisionWithSaturn(IcosahedronAttribs_v2 *icoObject, Vec3CUDA saturnPos, GLfloat saturnRadius, GLfloat speedFactor)
{
    // code
    Vec3CUDA v = vecSub(icoObject->pos, saturnPos);
    GLfloat dist = vecLen(v);
    GLfloat minimumDist = icoObject->radius + saturnRadius;
    if(dist < minimumDist)
    {
        Vec3CUDA n = vecDivScalar(v, dist);
        GLfloat delta = minimumDist - dist;

        icoObject->gravity = vecDivScalar(n, EXPLOSION_FACTOR);
        applyGravity_Asteroids(&icoObject->pos, icoObject->gravity, speedFactor);
        applyGravity_Asteroids(&icoObject->rotation, icoObject->gravity, speedFactor * ROTATION_FACTOR);
        return(TRUE);
    }

    return(FALSE);
}

void checkCollisionWithCamera(IcosahedronAttribs_v2 *icoObject, GLfloat speedFactor)
{
    // code
    Vec3CUDA cameraPosition = {cameraCoordinates.position.x, cameraCoordinates.position.y, cameraCoordinates.position.z};
    Vec3CUDA v = vecSub(icoObject->pos, cameraPosition);
    GLfloat dist = vecLen(v);
    GLfloat minimumDist = icoObject->radius + 0.1f;
    if(dist < minimumDist)
    {
        // normal vector
        Vec3CUDA n = vecDivScalar(v, dist);
        GLfloat delta = minimumDist - dist;

        // tangent of normal
        n.x = cosf(n.x);
        n.y = sinf(n.y);
        n.z = sinf(n.z);

        icoObject->gravity = vecDivScalar(n, EXPLOSION_FACTOR);
        applyGravity_Asteroids(&icoObject->pos, icoObject->gravity, speedFactor);
        applyGravity_Asteroids(&icoObject->rotation, icoObject->gravity, speedFactor * ROTATION_FACTOR);
    }
}

// void updatePosition_Asteroids(IcosahedronAttribs_v2 *icoObject)
// {
//     // code
//     Vec3CUDA velocity = vecSub(icoObject->pos, icoObject->oldPos);

//     // save current position
//     icoObject->oldPos = icoObject->pos;

//     // apply verlet integration
//     icoObject->pos = vecAdd(vecAdd(icoObject->pos, velocity), vecMulScalar(icoObject->acc, DT * DT));

//     // reset acceleration
//     icoObject->acc = {0.0f, 0.0f, 0.0f};
// }
