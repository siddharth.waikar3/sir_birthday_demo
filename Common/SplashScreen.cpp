// header files
#include "SplashScreen.h"

// function definitions
BOOL InitSplashScreen(SplashScreen *splashScreen, HWND hwndParent, HINSTANCE hInstance, WNDPROC SplashWndProc, WCHAR *szBitmapResource, int iFadeInMs)
{
    // local variables
    WCHAR szAppName[] = L"SplashScreen";

    // code
    // initialize the SplashScreen's wndclass
    splashScreen->wc.cbSize        = sizeof(WNDCLASSEXW);
    splashScreen->wc.style         = CS_OWNDC;
    splashScreen->wc.cbClsExtra    = 0;
    splashScreen->wc.cbWndExtra    = 0;
    splashScreen->wc.hInstance     = hInstance;
    splashScreen->wc.lpfnWndProc   = SplashWndProc;
    splashScreen->wc.hbrBackground = NULL;
    splashScreen->wc.hIcon         = NULL;
    splashScreen->wc.hCursor       = LoadCursor(NULL, IDC_ARROW);
    splashScreen->wc.lpszClassName = szAppName;
    splashScreen->wc.lpszMenuName  = NULL;
    splashScreen->wc.hIconSm       = NULL;

    // store the received hInstance for later use
    splashScreen->hInst = hInstance;

    // register the SplashScreen's wndclass
    if(RegisterClassExW(&splashScreen->wc) == FALSE)
    {
        MessageBoxW (NULL, L"Failed to register Splashscreen window class", L"Error", MB_OK | MB_ICONERROR);
        return(FALSE);
    }

    // load the bitmap
    splashScreen->hBitmap = (HBITMAP)LoadImage(splashScreen->hInst, szBitmapResource, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);

    if(splashScreen->hBitmap == NULL)
    {
        MessageBoxW(NULL, L"Failed to load bitmap for SplashScreen.", L"Error", MB_OK | MB_ICONERROR);
        return(FALSE);
    }

    // get bitmap information
    GetObjectW((HGDIOBJ)splashScreen->hBitmap, sizeof(BITMAP), (LPVOID)&splashScreen->bmp);

    // create the SplashScreen window in memory
    splashScreen->hSplashWnd = CreateWindowW(szAppName, 
                                            NULL, 
                                            WS_POPUP | WS_CHILD, 
                                            GetSystemMetrics(SM_CXSCREEN) / 2 - splashScreen->bmp.bmWidth / 2,
                                            GetSystemMetrics(SM_CYSCREEN) / 2 - splashScreen->bmp.bmHeight / 2,
                                            splashScreen->bmp.bmWidth,
                                            splashScreen->bmp.bmHeight,
                                            hwndParent,
                                            NULL,
                                            splashScreen->hInst,
                                            NULL);

    if(splashScreen->hSplashWnd == NULL)
    {
        MessageBoxW(NULL, L"Failed to create SplashScreen window.", L"Error", MB_OK | MB_ICONERROR);
        return(FALSE);
    }

    // storing bitmap into a memory DC for showing it later
    splashScreen->hSplashDC = GetDC(splashScreen->hSplashWnd);
    splashScreen->hMemoryDC = CreateCompatibleDC(splashScreen->hSplashDC);
    SelectObject(splashScreen->hMemoryDC, (HGDIOBJ)splashScreen->hBitmap);

    // animate the window
    AnimateWindow(splashScreen->hSplashWnd, iFadeInMs, AW_ACTIVATE | AW_BLEND);
    
    // update the window
    UpdateWindow(splashScreen->hSplashWnd);

    return(TRUE); 
}

BOOL ShowSplashScreen(SplashScreen *splashScreen, WPARAM wParam)
{
    // code
    // BitBlt the bitmap to the SplashScreen window
    BitBlt((HDC)wParam, 0, 0, splashScreen->bmp.bmWidth, splashScreen->bmp.bmHeight, splashScreen->hMemoryDC, 0, 0, SRCCOPY);
    splashScreen->wParam = wParam;

    return(TRUE);
}

BOOL LoadSplashScreenBmp(SplashScreen *splashScreen, WCHAR *szBitmapResource)
{
    // code
    // delete existing bitmap
    if(splashScreen->hBitmap)
    {
        DeleteObject(splashScreen->hBitmap);
        splashScreen->hBitmap = NULL;
    }

    // load bitmap image
    splashScreen->hBitmap = (HBITMAP)LoadImageW(splashScreen->hInst, szBitmapResource, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);

    if(splashScreen->hBitmap == NULL)
    {
        MessageBoxW(NULL, L"Failed to load bitmap for SplashScreen.", L"Error", MB_OK | MB_ICONERROR);
        return(FALSE);
    }

    // get bitmap information
    GetObjectW((HGDIOBJ)splashScreen->hBitmap, sizeof(BITMAP), (LPVOID)&splashScreen->bmp);

    // storing bitmap into a memory DC for showing it later
    SelectObject(splashScreen->hMemoryDC, (HGDIOBJ)splashScreen->hBitmap);

    // BitBlt the bitmap to the SplashScreen window
    BitBlt((HDC)splashScreen->wParam, 0, 0, splashScreen->bmp.bmWidth, splashScreen->bmp.bmHeight, splashScreen->hMemoryDC, 0, 0, SRCCOPY);
    return(TRUE);
}

HBITMAP LoadBmp(WCHAR *szBitmapID)
{
    // local variables
    HBITMAP hBitmap = NULL;
    
    // code
    hBitmap = (HBITMAP)LoadImageW(GetModuleHandle(NULL), szBitmapID, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);

    if(hBitmap == NULL)
    {
        MessageBoxW(NULL, L"Failed to load bitmap.", L"Error", MB_OK | MB_ICONERROR);
        return(NULL);
    }

    return(hBitmap);
}

BOOL HideSplashScreen(SplashScreen *splashScreen, int iFadeInMs)
{
    // code
    AnimateWindow(splashScreen->hSplashWnd, iFadeInMs, AW_HIDE | AW_BLEND);
    return(TRUE);
}

BOOL DeleteSplashScreen(SplashScreen *splashScreen)
{
    // code
    // release the DCs
    ReleaseDC(splashScreen->hSplashWnd, splashScreen->hMemoryDC);
    ReleaseDC(splashScreen->hSplashWnd, splashScreen->hSplashDC);
    
    // delete the bitmap object
    if(splashScreen->hBitmap)
    {
        DeleteObject(splashScreen->hBitmap);
        splashScreen->hBitmap = NULL;
    }

    // destroy the splash screen window
    if(splashScreen->hSplashWnd)
    {
        DestroyWindow(splashScreen->hSplashWnd);
        splashScreen->hSplashWnd = NULL;
    }

    return(TRUE);
}

