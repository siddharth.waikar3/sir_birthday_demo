// header files
#include "..\Common\Common.h"
#include "..\Common\Camera.h"
#include "..\Common\texture.h"

enum HandingOverLegacySceneStates
{
    HANDINGOVERLEGACY_SCENE_STATE_ONE,
    HANDINGOVERLEGACY_SCENE_STATE_TWO,
    HANDINGOVERLEGACY_SCENE_STATE_THREE,
    HANDINGOVERLEGACY_SCENE_STATE_FOUR,
    END
};
    
// Macros
#define FPS 60.0f
#define MAX_ALPHA 1.0f
#define PHOTO_FADE_SPEED MAX_ALPHA / (FPS * 2.0f)
#define MESSAGE_FADE_SPEED MAX_ALPHA / (FPS * 2.5f)
#define TIMER_ID_HANDINGOVERLEGACY 123
#define PLAY_HANDINGOVERLEGACY_MUSIC 1

// For Images
#define IMAGE_ONE_ASPECT_RATIO 1604.0f / 968.0f
#define IMAGE_TWO_ASPECT_RATIO 1106.0f / 778.0f
#define IMAGE_THREE_ASPECT_RATIO 1060.0f / 778.0f
#define IMAGE_FOUR_ASPECT_RATIO 4072.0f / 1257.0f


// //#define TOTAL_SHOT_2_TEXT_TIME 1.5f
// #define SHOT_2_TIME_PER_TEXT 375 // in ms
// #define ID_TIMER_SHOT_2_UNIX_DELAY  4745613
// #define ID_TIMER_SHOT_2_SUNBEAM_DELAY  47456139
// #define ID_TIMER_SHOT_2_RTR_DELAY  45674352


// global variable for texture
GLuint photoTexture1 = 0;
GLuint photoTexture2 = 0;
GLuint photoTexture3 = 0;
GLuint photoTexture4 = 0;

// Global alpha variable
GLfloat photoAlpha1 = 0.0f; // 0 = fully Transparent, 1 = fully Visible/opaque
GLfloat photoAlpha2 = 0.0f;
GLfloat photoAlpha3 = 0.0f;
GLfloat photoAlpha4 = 0.0f;

// Global Variable for Timer
BOOL bHandingOverLegacyTimerIsOn = FALSE;
int iDelay = 0;

extern enum Scene currentScene;
enum HandingOverLegacySceneStates handingOverLegacySceneState;
extern struct Transformations g_Transformations[NUM_TRANSFORMATION_ELEMENTS];
extern HWND ghwnd;
extern FILE *gpFILE;
// BOOL bStartSceneFadeOut = FALSE;
// BOOL bLifeSceneTimerIsOn = FALSE;
// BOOL bShowName = FALSE;

// for fade effect
// GLfloat fadeInAlpha = 1.0f;
// GLfloat fadeOutAlpha = 0.0f;
// BOOL bFadeIn = FALSE;
// const GLfloat fadeSpeed = MAX_ALPHA / (FPS * 2.0f);

// struct Transformations morganFreeManTextCurrentTransformations = {
//     // transformations[1]
// 	0.060000f, -0.060000f, -3.005000f,
// 	0.000000f, 0.000000f, 0.000000f,
// 	1.000000f, 1.000000f, 1.0f
// };

// function definitions
void initializeSceneHandingOverLegacy(void)
{
    // code
    handingOverLegacySceneState = HANDINGOVERLEGACY_SCENE_STATE_ONE;

    // lifeSceneState = LIFE_SCENE_STATE_TWO;
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    if (LoadTexture(&photoTexture1, "./Resources/textures/HandingOverLegacySceneTextureImages/1_Blessings.png") == FALSE)
    {
        fprintf(gpFILE, "Failed to laod Texture\n");
        DestroyWindow(ghwnd);
    }

    if (LoadTexture(&photoTexture2, "./Resources/textures/HandingOverLegacySceneTextureImages/2_IUnknownSir.png") == FALSE)
    {
        fprintf(gpFILE, "Failed to laod Texture\n");
        DestroyWindow(ghwnd);
    }

    if (LoadTexture(&photoTexture3, "./Resources/textures/HandingOverLegacySceneTextureImages/3_NewJourney.png") == FALSE)
    {
        fprintf(gpFILE, "Failed to laod Texture\n");
        DestroyWindow(ghwnd);
    }

    if (LoadTexture(&photoTexture4, "./Resources/textures/HandingOverLegacySceneTextureImages/4_BestOfLuck.png") == FALSE)
    {
        fprintf(gpFILE, "Failed to laod Texture\n");
        DestroyWindow(ghwnd);
    }
}

void displaySceneHandingOverLegacy(void)
{
    // local variables
    static int once = 0;
    CameraCoordinates ccStart =
    {
        {0.0f, 0.0f, 0.0f},
        {0.0f, 0.0f, 0.0f},
        {0.0f, 0.0f, 0.0f},
        {0.0f, 1.0f, 0.0f},
        0.0f,
        270.0f
    };

    //code
    if(!once)
    {
        setCameraCoordinates(ccStart);
        once = 1;
    }

    // Function Declaration
    void drawTextureUsingAspectRatio(GLuint texture, GLfloat width, GLfloat height, GLfloat alpha);
    //code
    switch(handingOverLegacySceneState)
    {
        case HANDINGOVERLEGACY_SCENE_STATE_ONE:
        glPushMatrix();
        {   
            glTranslatef(0.0f, 0.0f, -3.0f);
            drawTextureUsingAspectRatio(photoTexture1, 1604.0f, 968.0f, photoAlpha1);
        }
        glPopMatrix();
        break;

        case HANDINGOVERLEGACY_SCENE_STATE_TWO:
        glPushMatrix();
        {   
            glTranslatef(0.0f, 0.0f, -3.0f);
            drawTextureUsingAspectRatio(photoTexture2, 1106.0f, 778.0f, photoAlpha2);
        }
        glPopMatrix();
        break;

        case HANDINGOVERLEGACY_SCENE_STATE_THREE:
        glPushMatrix();
        {   
            glTranslatef(0.0f, 0.0f, -3.0f);
            drawTextureUsingAspectRatio(photoTexture3, 1060.0f, 778.0f, photoAlpha3);
        }
        glPopMatrix();
        break;

        case HANDINGOVERLEGACY_SCENE_STATE_FOUR:
        glPushMatrix();
        {   
            glTranslatef(0.0f, 0.0f, -6.0f);
            drawTextureUsingAspectRatio(photoTexture4, 4072.0f, 1257.0f, photoAlpha4);
        }
        glPopMatrix();

        // fadeInEffect (fadeInAlpha); // quad for fade in
        // fadeOutEffect(fadeOutAlpha);  // quad for fade out
        break;

        default:
        break;
    }
}

void drawTextureUsingAspectRatio(GLuint texture, GLfloat width, GLfloat height, GLfloat alpha)
{
    // Local Variables
    GLfloat aspect;
    GLfloat x = 1.0f;
    GLfloat y = 1.0f;

    // Code
    if(width <= height)
    {
        aspect = height / width;
        y = y * aspect;
    }
    else
    {
        aspect = width / height;
        x = x * aspect;
    }

    glBindTexture(GL_TEXTURE_2D, texture);
    glBegin(GL_QUADS);
    {
        glColor4f(1.0f, 1.0f, 1.0f, alpha);

        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(x, y, 0.0f);

        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-x, y, 0.0f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(-x, -y, 0.0f);

        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(x, -y, 0.0f);
        
    }
    glEnd();
    glBindTexture(GL_TEXTURE_2D, 0);
}

void updateSceneHandingOverLegacy(void)
{
    // function prototypes
    void handingOverLegacyTimer(HWND hwnd, UINT uiMsg, UINT_PTR idEvent, DWORD dwTime);
    void uninitializeSceneHandingOverLegacy(void);

    // Local Variables
    static BOOL bFadeOutPhotoOne = FALSE;
    static BOOL bFadeOutPhotoTwo = FALSE;
    static BOOL bFadeOutPhotoThree = FALSE;
    static BOOL bFadeOutPhotoFour = FALSE;
    static BOOL bOnce = FALSE;

    // Code
#if PLAY_HANDINGOVERLEGACY_MUSIC
    if(!bOnce)
    {
        SetCurrentSourceForStats(g_SceneSources[4], FALSE, FALSE);
        playALSource(g_SceneSources[4]);
        bOnce = TRUE;
    }
    // if(!bOnce)
    // {
        // seekALSource(g_CompleteTrackSource, 0, 4, 16);
        // playALSource(g_CompleteTrackSource);
        // bOnce = TRUE;
    // }
#endif

    switch (handingOverLegacySceneState)
    {

        case HANDINGOVERLEGACY_SCENE_STATE_ONE:
            if(!bFadeOutPhotoOne)
            {
                bFadeOutPhotoOne = updateObjectAlpha(&photoAlpha1, PHOTO_FADE_SPEED, FADE_IN_FLAG);   
            }

            if(!bHandingOverLegacyTimerIsOn && (iDelay == 0) && bFadeOutPhotoOne)
            {
                SetTimer(ghwnd, TIMER_ID_HANDINGOVERLEGACY, 4000, handingOverLegacyTimer); // 4000 = 4thousand miliseconds, 1 sec = 1000 miliseconds
                bHandingOverLegacyTimerIsOn = TRUE;
            }

            if(iDelay == 1)
            {
                if(updateObjectAlpha(&photoAlpha1, PHOTO_FADE_SPEED, FADE_OUT_FLAG))
                {
                    handingOverLegacySceneState = HANDINGOVERLEGACY_SCENE_STATE_TWO;
                }
            }
            
            break;
        
        case HANDINGOVERLEGACY_SCENE_STATE_TWO:
            if(!bFadeOutPhotoTwo)
            {
                bFadeOutPhotoTwo = updateObjectAlpha(&photoAlpha2, PHOTO_FADE_SPEED, FADE_IN_FLAG);   
            }

            if(!bHandingOverLegacyTimerIsOn && (iDelay == 1) && bFadeOutPhotoTwo)
            {
                SetTimer(ghwnd, TIMER_ID_HANDINGOVERLEGACY, 4000, handingOverLegacyTimer); // 4000 = 4thousand miliseconds, 1 sec = 1000 miliseconds
                bHandingOverLegacyTimerIsOn = TRUE;
            }

            if(iDelay == 2)
            {
                if(updateObjectAlpha(&photoAlpha2, PHOTO_FADE_SPEED, FADE_OUT_FLAG))
                {
                    handingOverLegacySceneState = HANDINGOVERLEGACY_SCENE_STATE_THREE;
                }
            }
            
            break;
        
        case HANDINGOVERLEGACY_SCENE_STATE_THREE:
            if(!bFadeOutPhotoThree)
            {
                bFadeOutPhotoThree = updateObjectAlpha(&photoAlpha3, PHOTO_FADE_SPEED, FADE_IN_FLAG);   
            }

            if(!bHandingOverLegacyTimerIsOn && (iDelay == 2) && bFadeOutPhotoThree)
            {
                SetTimer(ghwnd, TIMER_ID_HANDINGOVERLEGACY, 4000, handingOverLegacyTimer); // 4000 = 4thousand miliseconds, 1 sec = 1000 miliseconds
                bHandingOverLegacyTimerIsOn = TRUE;
            }

            if(iDelay == 3)
            {
                if(updateObjectAlpha(&photoAlpha3, PHOTO_FADE_SPEED, FADE_OUT_FLAG))
                {
                    handingOverLegacySceneState = HANDINGOVERLEGACY_SCENE_STATE_FOUR;
                }
            }

            break;

        case HANDINGOVERLEGACY_SCENE_STATE_FOUR:
            if(!bFadeOutPhotoFour)
            {
                bFadeOutPhotoFour = updateObjectAlpha(&photoAlpha4, MESSAGE_FADE_SPEED, FADE_IN_FLAG);   
            }

            if(!bHandingOverLegacyTimerIsOn && (iDelay == 3) && bFadeOutPhotoFour)
            {
                SetTimer(ghwnd, TIMER_ID_HANDINGOVERLEGACY, 9000, handingOverLegacyTimer); // 10000 = 10thousand miliseconds, 1 sec = 1000 miliseconds
                bHandingOverLegacyTimerIsOn = TRUE;
            }

            if(iDelay == 4)
            {
                if(updateObjectAlpha(&photoAlpha4, MESSAGE_FADE_SPEED, FADE_OUT_FLAG))
                {
                    handingOverLegacySceneState = END;
                }
            }
            break;
        case END:
            uninitializeSceneHandingOverLegacy();
            ResetGlobalTimer();
            setCameraCoordinates({{0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 1.0f, 0.0f}, 0.0f, 270.0f});
            currentScene = SCENE_CREDITS;
            break;
    }
}

void handingOverLegacyTimer(HWND hwnd, UINT uiMsg, UINT_PTR idEvent, DWORD dwTime)
{
    // code
    KillTimer(hwnd, idEvent);
    if(idEvent == TIMER_ID_HANDINGOVERLEGACY)
    {
        iDelay++;
    }

    bHandingOverLegacyTimerIsOn = FALSE;
}

void uninitializeSceneHandingOverLegacy(void)
{
    // code
    DeleteTexture(&photoTexture1);
    DeleteTexture(&photoTexture2);
    DeleteTexture(&photoTexture3);
    DeleteTexture(&photoTexture4);
}
