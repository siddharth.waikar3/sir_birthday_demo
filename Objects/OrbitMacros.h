#define COLOR_FRONT_OF_PLANE	1.0f, 0.0f, 0.0f
#define COLOR_BACK_OF_PLANE		0.5f, 0.5f, 0.5f
#define COLOR_TOP_OF_PLANE		1.0f, 1.0f, 0.0f
#define COLOR_BOTTOM_OF_PLANE	0.0f, 1.0f, 1.0f
#define COLOR_BACK_WING_OF_PLANE	0.5f, 0.5f, 0.5f

#define COLOR_GLASS_OF_WINDOWS	0.761f,0.761f,0.761f, 1.0f

#define COLOR_COMMON			0.4f, 0.4f, 0.4f, 1.0f

#define ACUTE_ANGLE_TRIANGLE	1
#define OBTUCE_ANGLE_TRIANGLE	2
#define RIGHT_ANGLE_TRIANGLE	3

#define BIT_0	0
#define BIT_1	1

#define DRAW_CUBE_FRONT													BIT_1
#define DRAW_CUBE_BACK													BIT_1
#define DRAW_CUBE_LEFT													BIT_1
#define DRAW_CUBE_RIGHT													BIT_1
#define DRAW_CUBE_UP													BIT_1
#define DRAW_CUBE_DOWN													BIT_1

#define DRAW_TRIANGLE_FRONT												BIT_1
#define DRAW_TRIANGLE_BACK												BIT_1
#define DRAW_TRIANGLE_LEFT												BIT_1
#define DRAW_TRIANGLE_RIGHT												BIT_1
#define DRAW_TRIANGLE_BASE												BIT_1


#define ENABLE_TRANSFORMATION_DISPLAY									BIT_1
#define ENABLE_TRANSFORMATION_DRAW_ORBIT								BIT_1

#define VARIABLE_TRANSFORMATION_DISPLAY									BIT_0
#define VARIABLE_TRANSFORMATION_DRAW_ORBIT								BIT_0
