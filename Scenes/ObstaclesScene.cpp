// header files
#include "../Common/Common.h"
#include "../Scenes/ObstaclesScene.h"
#include "../Objects/Astroid.h"
#include "../Objects/years.hpp"
#include "../Scenes/TestScene.h"
#include "../Objects/Skybox.h"
#include "../Objects/Saturn.h"
#include "../objects/Orbit.h"
#include "../Common/Camera.h"
#include "../Common/RenderText.h"
#include "../imgui/imgui.h"

// Macro's
#define PLAY_OBSTACLE_SCENE_MUSIC  0
#define SET_OBSTACLE_SCENE_CAMERA  1
#define SHOW_ROCKS_OBSTACLES_SCENE SHOW_ROCKS_GLOBAL

#define IDT_IS_IT_THE_END_DELAY 1312
#define IDT_OBSTACLE_SCENE_FADE_OUT_DELAY 1313
#define IDT_OBSTACLE_SCENE_END_DELAY 1314

// #define LERP_START 0.0f
// #define LERP_END   1.0f
// #define OBSTICAL_SPEED LERP_END/(FPS*numberOfDesiredSecounds)  //distance/(FPS * number of desired secounds)

#define OBSTACLE_SCENE_ORBIT_INNER_RADIUS 20.0f
#define OBSTACLE_SCENE_ORBIT_OUTER_RADIUS 20.01f
#define OBSTACLE_SCENE_ORBIT_RADIUS       ((OBSTACLE_SCENE_ORBIT_INNER_RADIUS + OBSTACLE_SCENE_ORBIT_OUTER_RADIUS) / 2.0f) 

#define OBSTACLE_SCENE_FONT_DISPLAYLISTS_OFFSET 1300001 

#define ENABLE_OBSTACLES_SCENE_FADE 1

// enum for holding states
enum ObstaclesSceneStates
{
    OBSTACLES_SCENE_DIABETES,
    OBSTACLES_SCENE_DIABETES_END,
    OBSTACLES_SCENE_BLOOD_PRESSURE,
    OBSTACLES_SCENE_BLOOD_PRESSURE_END,
    OBSTACLES_SCENE_ATTACK_FROM_ALL_SIDES,
    OBSTACLES_SCENE_IS_IT_THE_END,
    FADE_OUT,
    END
};

enum ObstaclesSceneStates obstaclesSceneState;

// external global variables
extern enum Scene currentScene;
extern FILE* gpFILE;
extern HWND ghwnd;
extern HDC ghdc;
extern CameraCoordinates cameraCoordinates;

BOOL g_bObstaclesSceneMusicStarted = FALSE;
GLfloat obstacleSceneFadeOutAlpha = 1.0f;

// for saturn
Translation obstacleSceneSaturnTranslation        = {0.0f, 0.0f, 0.0f};
Translation obstacleSceneSaturnInitialTranslation = {0.0f, 0.0f, 0.0f};
GLfloat obstacleSceneSaturnRotation[2]            = {0.0f, 0.0f}; 
GLfloat obstacleSceneSaturnRevolution             = 0.0f;

// camera
CameraCoordinates obstaclesSceneInitialCC_Shot1 =
{
	{0.000000f, 0.000000f, 0.000000f},
	{0.000000f, 0.000000f, -1.000000f},
	{1.000000f, -0.000000f, 0.000000f},
	{-0.000000f, 1.000000f, 0.000000f},
	0.000000f,
	270.000000f
};

CameraCoordinates obstaclesSceneInitialCC_Shot3 =
{
    {0.222149f, 4.454770f, -0.128258f},
    {0.852868f, -0.173648f, -0.492404f},
    {0.492404f, -0.000000f, 0.852868f},
    {0.148099f, 0.969846f, -0.085505f},
    -10.000000f,
    330.000000f
};

CameraCoordinates obstaclesSceneInitialCC_Shot4 =
{
	{20.002529f, 0.234253f, -1.041493f},
	{-0.091853f, 0.106264f, -0.990086f},
	{0.990086f, 0.000000f, -0.091853f},
	{0.009761f, 0.988708f, 0.105211f},
	6.099999f,
	264.699677f
};

CameraTilt obstaclesSceneCameraTilt_Shot1 = {0.0f, 45.0f, FALSE};

// for obstacles
Transformations diabetesTextTransformations =
{
	{-1.400000f, 1.500000f, 0.000000f},
	{0.000000f, 0.000000f, 0.000000f},
	{1.320000f / 2.0f, 1.000000f / 2.0f, 1.000000f / 2.0f}
};

Transformations bloodPressureTextTransformations =
{
	{-2.850000f, 1.200000f, 0.300000f},
	{0.000000f, 0.000000f, 0.000000f},
	{1.330000f / 2.0f, 1.000000f / 2.0f, 1.000000f / 2.0f}
};

// ******************************************** SHOT 1 - DIABETES ********************************************
Transformations diabetesAsteroidInitalPosition_Shot1 =
{
    {-5.000000f, 6.000000f, -16.500000f},
    {10.000000f, 0.000000f, 90.000000f},
	{0.570000f, 0.770000f, 0.570000f},
};

Transformations diabetesAsteroidFinalPosition_Shot1 =
{
    {0.000000f, 0.000000f, 0.000000f},
    {-75.000000f, 130.000000f, 10.000000f},
	{0.570000f, 0.770000f, 0.570000f}
};

GLfloat diabetesAsteroidSpeedShot1 = 1.0f / (FPS * 16.0f);
GLfloat diabetesAsteroidSpeedShot3 = 1.0f / (FPS * 11.0f);
BOOL bShowDiabetesAsteroid = TRUE;

// ******************************************** SHOT 2 - BLOOD PRESSURE ********************************************
Transformations bloodPressureInitialPosition_Shot2 =
{
    {5.000000f, 6.000000f, -16.500000f},
    {10.000000f, 0.000000f, 90.000000f},
	{0.550000f, 0.800000f, 0.550000f}
};

Transformations bloodPressureFinalPosition_Shot2 =
{
    {0.000000f, 0.000000f, 0.000000f},
    {-75.000000f, 130.000000f, 10.000000f},
	{0.550000f, 0.800000f, 0.550000f}
};

GLfloat bloodPressureAsteroidSpeedShot2 = 1.0f / (FPS * 10.0f);
GLfloat bloodPressureAsteroidSpeedShot3 = 1.0f / (FPS * 11.0f);
BOOL bShowBloodPressureAsteroid = FALSE;

// ******************************************** SHOT 3 - ATTACK FROM ALL SIDES ********************************************
Transformations diabetesAsteroidInitialPosition_Shot3 =
{
    {5.000000f, 4.500000f, -8.000000f},
    {-60.000000f, 0.000000f, 80.000000f},
    {0.570000f, 0.770000f, 0.570000f}
};

Transformations diabetesAsteroidFinalPosition_Shot3 = 
{
    {10.000000f, 3.000000f, -5.500000f},
    {60.000000f, 90.000000f, -80.000000f},
    {0.570000f, 0.770000f, 0.570000f}
};

Transformations bloodPressureAsteroidInitialPosition_Shot3 =
{
    {10.000000f, 5.500000f, -20.000000f},
    {60.000000f, 90.000000f, -80.000000f},
	{0.550000f, 0.800000f, 0.550000f}
};

Transformations bloodPressureAsteroidFinalPosition_Shot3 = 
{
    {15.000000f, 3.000000f, -8.000000f},
    {-60.000000f, 0.000000f, 80.000000f},
	{0.550000f, 0.800000f, 0.550000f}
};

// ******************************************** SHOT 4 - IS IT THE END? ********************************************
Transformations diabetesAsteroidInitialPosition_Shot4 =
{
	{11.844999f, 3.500000f, -18.999996f},
	{-17.999998f, 0.000000f, 0.000000f},
	{0.570000f, 0.770000f, 0.570000f}
};

Transformations diabetesAsteroidFinalPosition_Shot4 =
{
    {0.0f, 0.0f, 0.0f},
    {-75.0f, 0.0f, 0.0f},
    {0.570000f, 0.770000f, 0.570000f}
};

Transformations bloodPressureAsteroidInitialPosition_Shot4 =
{
	{22.999998f, 4.000000f, -18.999998f},
	{-22.999998f, -26.999998f, 0.000000f},
	{0.550000f, 0.800000f, 0.550000f}
};

Transformations bloodPressureAsteroidFinalPosition_Shot4 =
{
    {0.0f, 0.0f, 0.0f},
    {-80.0f, 0.0f, 0.0f},
    {0.550000f, 0.800000f, 0.550000f}
};

// Lerp transformations for asteroids
LerpTransformations diabetesAsteroidTransformations =
{
    diabetesAsteroidInitalPosition_Shot1,
    diabetesAsteroidInitalPosition_Shot1,
    diabetesAsteroidFinalPosition_Shot1,
    0.0f,
    diabetesAsteroidSpeedShot1,
    FALSE
};

LerpTransformations bloodPressureAsteroidTransformations =
{
    bloodPressureInitialPosition_Shot2,
    bloodPressureInitialPosition_Shot2,
    bloodPressureFinalPosition_Shot2,
    0.0f,
    bloodPressureAsteroidSpeedShot2,
    FALSE
};

// arial font for this scene
RT_Library obstacleSceneFont;

// timer flag for this scene
BOOL g_bObstaclesSceneTimerIsOn = FALSE;
BOOL g_bGoToShot4 = FALSE;

// function definitions
void initializeSceneThree(void)
{
    // local variables
    GLfloat tempAngleInRadians;

    // code
    // initialize saturn position
    tempAngleInRadians = obstacleSceneSaturnRevolution * (M_PI / 180.0f);

    obstacleSceneSaturnTranslation.x = OBSTACLE_SCENE_ORBIT_RADIUS * cosf(tempAngleInRadians);
    obstacleSceneSaturnTranslation.y = 0.05f;
    obstacleSceneSaturnTranslation.z = OBSTACLE_SCENE_ORBIT_RADIUS * sinf(tempAngleInRadians);
    
    obstacleSceneSaturnInitialTranslation = obstacleSceneSaturnTranslation;

    // initialize font
    RT_Initialize_Library(&obstacleSceneFont, ghdc, NULL, TEXT("Arial"), RT_FW_DONTCARE, 0, OBSTACLE_SCENE_FONT_DISPLAYLISTS_OFFSET);

     //obstaclesSceneState = OBSTACLES_SCENE_ATTACK_FROM_ALL_SIDES;

    // tval = LERP_START;

    // obstacleDiabetes = obstacleTransformations[indexArray];
    // obstacleBloodPressure = obstacleTransformations[2];
    // saturn = obstacleTransformations[4];
    g_Transformations[0] = bloodPressureAsteroidInitialPosition_Shot3;
    //setCameraCoordinates(obstaclesSceneInitialCC_Shot3);
}

void displaySceneThree(void)
{
    // local variables
    static BOOL bOnce = FALSE;

    // code
    // if(!bOnce)
    // {
    //     setCameraCoordinates(cameraTransformation[0]);
    //     bOnce = TRUE;
    // }

    if(!bOnce)
    {
        setCameraCoordinatesUsingLookAt(obstaclesSceneInitialCC_Shot1);
        bOnce = TRUE;
    }

    drawTexturedSkybox();

    // *********************************** ORBIT and SATURN ***********************************
    if(obstaclesSceneState >= OBSTACLES_SCENE_ATTACK_FROM_ALL_SIDES)
    {
        DrawOrbitJourneyScene(OBSTACLE_SCENE_ORBIT_INNER_RADIUS, OBSTACLE_SCENE_ORBIT_OUTER_RADIUS);

        glPushMatrix();
        {
            glTranslatef(obstacleSceneSaturnTranslation.x, obstacleSceneSaturnTranslation.y, obstacleSceneSaturnTranslation.z);
            drawSaturn(obstacleSceneSaturnRotation[0], obstacleSceneSaturnRotation[1], SHOW_ROCKS_OBSTACLES_SCENE);
        }
        glPopMatrix();
    }

    // *********************************** DIABETES asteroid ***********************************
    if(bShowDiabetesAsteroid)
    {
        // "Diabetes" text
        //if(obstaclesSceneState < OBSTACLES_SCENE_IS_IT_THE_END)
        {
            glPushMatrix();
            {
                //applyTransformationsWithoutRotation(g_Transformations[0]);
                applyTransformationsWithoutRotation(diabetesAsteroidTransformations.current);
                applyTransformations(diabetesTextTransformations);
                drawGradedText("Diabetes", {1.0f, 1.0f, 1.0f, 1.0f}, {0.25f, 0.25f, 0.25f, 1.0f}, OBSTACLE_SCENE_FONT_DISPLAYLISTS_OFFSET);
            }
            glPopMatrix();
        }

        // Asteroid
        glPushMatrix();
        {
            //applyTransformations(g_Transformations[0]);
            applyTransformations(diabetesAsteroidTransformations.current);
            colorIndex = 1;
            drawIcosahedron(1);
        }
        glPopMatrix();
    }

    // *********************************** BLOOD PRESSURE asteroid ***********************************
    if(bShowBloodPressureAsteroid)
    {
        // "Blood Pressure" text
        //if(obstaclesSceneState < OBSTACLES_SCENE_IS_IT_THE_END)
        {
            glPushMatrix();
            {
                //applyTransformationsWithoutRotation(g_Transformations[0]);
                applyTransformationsWithoutRotation(bloodPressureAsteroidTransformations.current);
                applyTransformations(bloodPressureTextTransformations);
                drawGradedText("Blood Pressure", {1.0f, 1.0f, 1.0f, 1.0f}, {0.25f, 0.25f, 0.25f, 1.0f}, OBSTACLE_SCENE_FONT_DISPLAYLISTS_OFFSET);
                // drawOrbitText(86);
            }
            glPopMatrix();
        }

        // Asteroid
        glPushMatrix();
        {
            //applyTransformations(g_Transformations[0]);
            applyTransformations(bloodPressureAsteroidTransformations.current);
            colorIndex = 2;
            drawIcosahedron(1);
        }
        glPopMatrix();
    }

    /*switch (obstaclesSceneState)
    {
    case OBSTACLERS_SCENE_STATE_DIABETES:
        numberOfDesiredSecounds = 10.0f;
        drawTexturedSkybox();
        glPushMatrix();
        {
            applyTransformationsWithoutRotation(obstacleDiabetes);
            applyTransformations({
                    {-2.000000f, 13.500000f+1.5f, -8.500000f},
                    {45.000000f, 0.000000f, 0.000000f},
                    {2.000000f, 1.500000f, 1.400000f}
                });
            drawOrbitText(85);
        }
        glPopMatrix();
        glPushMatrix();
        {
            applyTransformations(obstacleDiabetes);
            applyTransformations({
                0.000000f, 0.100000f, -10.800005f,
                -2835.000000f, -2649.000000f, -3354.000000f,
                1.900000f, 2.200000f, 3.799999f
                });
            colorIndex = 1;
            drawIcosahedron(1);
        }
        glPopMatrix();
        break;

    case OBSTACLERS_SCENE_BLOOD_PRESSURE:
        numberOfDesiredSecounds = 07.0f;
        drawTexturedSkybox();
        glPushMatrix();
        {
            applyTransformationsWithoutRotation(obstacleBloodPressure);
            applyTransformations({
                    {-2.500000f+4.0f, 8.000000f-2.5f, -6.500000f},
                    {53.000000f, 0.000000f, 0.000000f},
                    {1.000000f, 1.000000f, 1.000000f}
                });
            drawOrbitText(86);
        }
        glPopMatrix();
        glPushMatrix();
        {
            applyTransformations(obstacleBloodPressure);
            applyTransformations({
                4.199998f, 1.700000f, -6.399996f,
                -1569.000000f, -1443.000000f, -660.000000f,
                1.500000f, 1.100000f, 2.200000f
                });
            colorIndex = 2;
            drawIcosahedron(1);
        }
        glPopMatrix();
        break;
        
    case OBSTACLES_ATTACK_FROM_ALL_SIDE:
        numberOfDesiredSecounds = 03.0f;
        drawTexturedSkybox();
        glPushMatrix();
        {
            //applyTransformationsWithoutRotation(saturn);
            applyTransformations({
                    {6.500000f, 4.500000f, -12.500000f},
                    {53.000000f, 2.000000f, -23.000000f},
                    {1.000000f+1.0f, 1.000000f+1.0f, 1.000000f+1.0f}
                });
            drawSaturn(obstaclSceneSaturnRotation[0], obstaclSceneSaturnRotation[1], SHOW_ROCKS_OBSTACLES_SCENE);
            applyTransformations({
                    {10.500000f, 3.000000f, 10.500000f},
                    {-3.000000f, -45.000000f, -1.000000f},
                    {0.800000f, 0.500000f, -17.200029f}
                });
            DrawOrbit(20.0f,20.01f);
        }
        glPopMatrix();

        glPushMatrix();
        {
            applyTransformations(obstacleDiabetes);
            applyTransformations({
                4.199998f, 1.700000f, -6.399996f,
                -1569.000000f, -1443.000000f, -660.000000f,
                1.500000f, 1.100000f, 2.200000f
                });
            colorIndex = 2;
            drawIcosahedron(1);
        }
        glPopMatrix();
        glPushMatrix();
        {
            applyTransformations(obstacleBloodPressure);
            applyTransformations({
                    0.000000f, 0.100000f, -10.800005f,
                    -2835.000000f, -2649.000000f, -3354.000000f,
                    1.900000f, 2.200000f, 3.799999f
                });
            colorIndex = 1;
            drawIcosahedron(1);
        }
        glPopMatrix();

        break;
    case SATURN_CAMERA_SWITCH:
        numberOfDesiredSecounds = 03.0f;
        drawTexturedSkybox();
        glPushMatrix();
        {
            applyTransformations({
                    {0.000000f, 1.000000f, -8.500000f},
                    {14.000000f, 1.000000f, 1.000000f},
                    {1.000000f, 1.000000f, 1.000000f}
                });
            drawSaturn(obstaclSceneSaturnRotation[0], obstaclSceneSaturnRotation[1], SHOW_ROCKS_OBSTACLES_SCENE);
            applyTransformations({
                    {-1.500000f, 0.000000f, -2.000000f},
                    {-93.000000f, -179.000000f, -53.000000f},
                    {0.200000f, -0.100000f, 0.200000f}
                });
            DrawOrbit(20.0f, 20.01f);
        }
        glPopMatrix();

        glPushMatrix();
        {
            applyTransformations(obstacleDiabetes);
            applyTransformations({
                4.199998f, 1.700000f, -6.399996f,
                -1569.000000f, -1443.000000f, -660.000000f,
                1.500000f, 1.100000f, 2.200000f
                });
            colorIndex = 2;
            drawIcosahedron(1);
        }
        glPopMatrix();
        glPushMatrix();
        {
            applyTransformations(obstacleBloodPressure);
            applyTransformations({
                    0.000000f, 0.100000f, -10.800005f,
                    -2835.000000f, -2649.000000f, -3354.000000f,
                    1.900000f, 2.200000f, 3.799999f
                });
            colorIndex = 1;
            drawIcosahedron(1);
        }
        glPopMatrix();
        break;
    case FADE_OUT:
        numberOfDesiredSecounds = 01.0f;
#if ENABLE_OBSTACLES_SCENE_FADE
        fadeOutEffect(obstacleSceneFadeOutAlpha);
#endif

        break;
    default:
        break;
    }*/

    fadeOutEffect(obstacleSceneFadeOutAlpha);
}

void updateSceneThree(void)
{
    // function prototypes
    void uninitializeSceneThree(void);
    void obstaclesSceneTimer(HWND hwnd, UINT iMsg, UINT_PTR idEvent, DWORD dwTime);

    // local variables
    static GLfloat cameraRefrence = 0.0f;
    static GLfloat cameraRefrence2 = 0.0f;
    BOOL flag1 = FALSE;
    BOOL flag2 = FALSE;
    BOOL flag3 = FALSE;
    static BOOL bOnce = FALSE;

    static GLfloat saturnRotationSpeed = 360.0f / (FPS * 20.0f);
    static GLfloat saturnRevolutionSpeed = 360.0f / (FPS * 720.0f);
    static GLfloat angleInRadians = 0.0f;

    // code
    if (false)
    {
        obstacleSceneFadeOutAlpha = 0.0f;
        return;
    }

#if PLAY_OBSTACLE_SCENE_MUSIC
    if(!bOnce)
    {
        SetCurrentSourceForStats(g_CompleteTrackSource, FALSE, FALSE);
        seekALSource(g_CompleteTrackSource, 0, 1, 33);
        playALSource(g_CompleteTrackSource);
        bOnce = TRUE;
    }
#endif

    /*if(totalElapsedTime >= (23.0))
    {
        obstaclesSceneState = END;
    }*/

    updateRotationValue(&obstacleSceneSaturnRotation[0], saturnRotationSpeed);
    updateRotationValue(&obstacleSceneSaturnRotation[1], saturnRotationSpeed);

    switch(obstaclesSceneState)
    {
    case OBSTACLES_SCENE_DIABETES:
        // fade in
        updateAlpha(&obstacleSceneFadeOutAlpha, 1.0f / (FPS * 1.0f), FADE_IN_FLAG);

        // tilt camera
        tiltCamera(&obstaclesSceneCameraTilt_Shot1, obstaclesSceneCameraTilt_Shot1.totalTilt / (FPS * 16.0f), CAMERA_POSITIVE_TILT_FLAG);

        // lerp diabetes asteroid
        lerpAnimation_v2(&diabetesAsteroidTransformations, 1.0f);

        if(totalElapsedTime >= 10.0)
        {
            obstaclesSceneState = OBSTACLES_SCENE_DIABETES_END;
        }
        break;
    case OBSTACLES_SCENE_DIABETES_END:
        // tilt camera
        tiltCamera(&obstaclesSceneCameraTilt_Shot1, obstaclesSceneCameraTilt_Shot1.totalTilt / (FPS * 16.0f), CAMERA_POSITIVE_TILT_FLAG);

        // lerp diabetes asteroid
        lerpAnimation_v2(&diabetesAsteroidTransformations, 1.0f);

        if (updateAlpha(&obstacleSceneFadeOutAlpha, 1.0f / (FPS * 0.5f), FADE_OUT_FLAG))
        {
            bShowDiabetesAsteroid = FALSE;
            bShowBloodPressureAsteroid = TRUE;
            cameraCoordinates.angleX = 0.0f;
            obstaclesSceneCameraTilt_Shot1.tilt = 0.0f;
            obstaclesSceneState = OBSTACLES_SCENE_BLOOD_PRESSURE;
        }
        break;
    case OBSTACLES_SCENE_BLOOD_PRESSURE:
        updateAlpha(&obstacleSceneFadeOutAlpha, 1.0f / (FPS * 0.5f), FADE_IN_FLAG);

        // tilt camera
        tiltCamera(&obstaclesSceneCameraTilt_Shot1, obstaclesSceneCameraTilt_Shot1.totalTilt / (FPS * 16.0f), CAMERA_POSITIVE_TILT_FLAG);

        // lerp blood pressure asteroid
        lerpAnimation_v2(&bloodPressureAsteroidTransformations, 1.0f);

        if(totalElapsedTime >= 16.0)
        {
            bShowDiabetesAsteroid = TRUE;
            bShowBloodPressureAsteroid = TRUE;

            // set new coordinates
            /*diabetesAsteroidFinalPosition_Shot3.translation = obstacleSceneSaturnTranslation;
            bloodPressureAsteroidFinalPosition_Shot3.translation = obstacleSceneSaturnTranslation;*/
            
            diabetesAsteroidTransformations.current = diabetesAsteroidInitialPosition_Shot3;
            diabetesAsteroidTransformations.begin   = diabetesAsteroidInitialPosition_Shot3;
            diabetesAsteroidTransformations.end     = diabetesAsteroidFinalPosition_Shot3;
            diabetesAsteroidTransformations.tVal    = 0.0f;
            diabetesAsteroidTransformations.speed   = diabetesAsteroidSpeedShot3;

            bloodPressureAsteroidTransformations.current = bloodPressureAsteroidInitialPosition_Shot3;
            bloodPressureAsteroidTransformations.begin   = bloodPressureAsteroidInitialPosition_Shot3;
            bloodPressureAsteroidTransformations.end     = bloodPressureAsteroidFinalPosition_Shot3;
            bloodPressureAsteroidTransformations.tVal    = 0.0f;
            bloodPressureAsteroidTransformations.speed   = bloodPressureAsteroidSpeedShot3;

            // set camera for 3rd shot
            setCameraCoordinates(obstaclesSceneInitialCC_Shot3);
            
            // switch to next state
            obstaclesSceneState = OBSTACLES_SCENE_ATTACK_FROM_ALL_SIDES; 
        }
        break;
    case OBSTACLES_SCENE_ATTACK_FROM_ALL_SIDES:
        // lerp diabetes asteroid
        lerpAnimation_v2(&diabetesAsteroidTransformations, 1.0f);
        
        // lerp blood pressure asteroid
        lerpAnimation_v2(&bloodPressureAsteroidTransformations, 1.0f);

        // update saturn revolution
        updateRevolutionValue(&obstacleSceneSaturnRevolution, 
                               saturnRevolutionSpeed, 
                               &obstacleSceneSaturnTranslation, 
                               OBSTACLE_SCENE_ORBIT_RADIUS, 
                               REVOLUTION_DECREMENT_ANGLE);

        if((totalElapsedTime >= 20.0) && !g_bGoToShot4)
        {
            g_bGoToShot4 = TRUE;
        }

        if(g_bGoToShot4)
        {
            diabetesAsteroidFinalPosition_Shot4.translation      = obstacleSceneSaturnTranslation;
            bloodPressureAsteroidFinalPosition_Shot4.translation = obstacleSceneSaturnTranslation;
            
            diabetesAsteroidTransformations.current = diabetesAsteroidInitialPosition_Shot4;
            diabetesAsteroidTransformations.begin   = diabetesAsteroidTransformations.current;
            diabetesAsteroidTransformations.end     = diabetesAsteroidFinalPosition_Shot4;
            diabetesAsteroidTransformations.tVal    = 0.0f;
            diabetesAsteroidTransformations.speed   = 1.0f / (FPS * 4.7f);

            bloodPressureAsteroidTransformations.current = bloodPressureAsteroidInitialPosition_Shot4;
            bloodPressureAsteroidTransformations.begin   = bloodPressureAsteroidTransformations.current;
            bloodPressureAsteroidTransformations.end     = bloodPressureAsteroidFinalPosition_Shot4;
            bloodPressureAsteroidTransformations.tVal    = 0.0f;
            bloodPressureAsteroidTransformations.speed   = 1.0f / (FPS * 4.7f);

            // reset saturn position
            obstacleSceneSaturnTranslation =  obstacleSceneSaturnInitialTranslation;

            // set new camera coords
            setCameraCoordinates(obstaclesSceneInitialCC_Shot4);
            obstaclesSceneState = OBSTACLES_SCENE_IS_IT_THE_END;
        }
        break;
    case OBSTACLES_SCENE_IS_IT_THE_END:
        // lerp diabetes asteroid
        lerpAnimation_v2(&diabetesAsteroidTransformations, 1.0f);
        
        // lerp blood pressure asteroid
        lerpAnimation_v2(&bloodPressureAsteroidTransformations, 1.0f);

        // for fade out delay
        if(!g_bObstaclesSceneTimerIsOn)
        {
            SetTimer(ghwnd, IDT_OBSTACLE_SCENE_FADE_OUT_DELAY, 2500, (TIMERPROC)obstaclesSceneTimer);
            g_bObstaclesSceneTimerIsOn = TRUE;
        }
        break;
    case FADE_OUT:
        // lerp diabetes asteroid
        lerpAnimation_v2(&diabetesAsteroidTransformations, 1.0f);
        
        // lerp blood pressure asteroid
        lerpAnimation_v2(&bloodPressureAsteroidTransformations, 1.0f);

        if(updateAlpha(&obstacleSceneFadeOutAlpha, 1.0f / (FPS * 1.0f), FADE_OUT_FLAG))
        {
            bShowDiabetesAsteroid = FALSE;
            bShowBloodPressureAsteroid = FALSE;
            obstaclesSceneState = END;
        }
        break;
    case END:
        uninitializeSceneThree();
        ResetGlobalTimer();
        // setCameraCoordinates({{0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 1.0f, 0.0f}, 0.0f, 270.0f});
        currentScene = SCENE_RISE_OF_PHOENIX;
    default:
        break;
    }

    /*switch (obstaclesSceneState)
    {
    case OBSTACLERS_SCENE_STATE_DIABETES:
        flag1 = lerpAnimation(&obstacleDiabetes, &obstacleTransformations[0], &obstacleTransformations[1], &tval, OBSTICAL_SPEED);
        if (flag1) {
            tval = 0.0f;
            obstaclesSceneState = OBSTACLERS_SCENE_BLOOD_PRESSURE;
        }
        break;
    case OBSTACLERS_SCENE_BLOOD_PRESSURE:
        flag2 = lerpAnimation(&obstacleBloodPressure, &obstacleTransformations[2], &obstacleTransformations[3], &tval, OBSTICAL_SPEED);
        if (flag2) {
            tval = 0.0f;
            cameraRefrence = 0.0f;
            setCameraCoordinates(cameraTransformation[1]);
            obstaclesSceneState = OBSTACLES_ATTACK_FROM_ALL_SIDE;
        }
        break;
    case OBSTACLES_ATTACK_FROM_ALL_SIDE:
        //flag1=lerpAnimation(&saturn, &obstacleTransformations[4], &obstacleTransformations[5], &tval, OBSTICAL_SPEED);
        flag2=lerpAnimation(&obstacleDiabetes, &obstacleTransformations[6], &obstacleTransformations[7], &tval1, OBSTICAL_SPEED);
        flag3=lerpAnimation(&obstacleBloodPressure, &obstacleTransformations[8], &obstacleTransformations[9], &tval2, OBSTICAL_SPEED);

        if ( flag2 && flag3)
        {
            tval = 0.0f;
            tval1 = 0.0f;
            tval2 = 0.0f;
            setCameraCoordinates(cameraTransformation[3]);
            obstaclesSceneState=SATURN_CAMERA_SWITCH;
        }
        break;
    case SATURN_CAMERA_SWITCH:
        flag1=lerpAnimation(&obstacleDiabetes, &obstacleTransformations[10], &obstacleTransformations[11], &tval, OBSTICAL_SPEED);
        flag2=lerpAnimation(&obstacleBloodPressure, &obstacleTransformations[12], &obstacleTransformations[13], &tval1, OBSTICAL_SPEED);

        if(flag1 && flag2)
        {
            obstaclesSceneState = FADE_OUT;
        }
        break;
    case END:
        uninitializeSceneThree();
        ResetGlobalTimer();
        setCameraCoordinates({{0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 1.0f, 0.0f}, 0.0f, 270.0f});
        currentScene = SCENE_RISE_OF_PHOENIX;
        break;
    default:
        break;
    }*/
}

void uninitializeSceneThree(void)
{
    // code
#if PLAY_OBSTACLE_SCENE_MUSIC
    // stopALSource(g_CompleteTrackSource);
    // SetCurrentSourceForStats(0, FALSE, FALSE);
#endif

    // uninitialize RT Lib
    RT_Uninitialize_Library(&obstacleSceneFont, OBSTACLE_SCENE_FONT_DISPLAYLISTS_OFFSET);
}

void obstaclesSceneTimer(HWND hwnd, UINT iMsg, UINT_PTR idEvent, DWORD dwTime)
{
    // code
    KillTimer(hwnd, idEvent);
    switch(idEvent)
    {
    case IDT_IS_IT_THE_END_DELAY:
        g_bGoToShot4 = TRUE;   
        // diabetesAsteroidFinalPosition_Shot4.translation      = obstacleSceneSaturnTranslation;
        // bloodPressureAsteroidFinalPosition_Shot4.translation = obstacleSceneSaturnTranslation;
        
        // diabetesAsteroidTransformations.current = diabetesAsteroidInitialPosition_Shot4;
        // diabetesAsteroidTransformations.begin   = diabetesAsteroidTransformations.current;
        // diabetesAsteroidTransformations.end     = diabetesAsteroidFinalPosition_Shot4;
        // diabetesAsteroidTransformations.tVal    = 0.0f;
        // diabetesAsteroidTransformations.speed   = 1.0f / (FPS * 4.0f);

        // bloodPressureAsteroidTransformations.current = bloodPressureAsteroidInitialPosition_Shot4;
        // bloodPressureAsteroidTransformations.begin   = bloodPressureAsteroidTransformations.current;
        // bloodPressureAsteroidTransformations.end     = bloodPressureAsteroidFinalPosition_Shot4;
        // bloodPressureAsteroidTransformations.tVal    = 0.0f;
        // bloodPressureAsteroidTransformations.speed   = 1.0f / (FPS * 4.0f);

        // // reset saturn position
        // obstacleSceneSaturnTranslation =  obstacleSceneSaturnInitialTranslation;

        // // set new camera coords
        // setCameraCoordinates(obstaclesSceneInitialCC_Shot4);
        // obstaclesSceneState = OBSTACLES_SCENE_IS_IT_THE_END;
        break;
    case IDT_OBSTACLE_SCENE_FADE_OUT_DELAY:
        obstaclesSceneState = FADE_OUT;
        break;
    case IDT_OBSTACLE_SCENE_END_DELAY:
        obstaclesSceneState = END;
        break;
    }

    g_bObstaclesSceneTimerIsOn = FALSE;
}
