// UNICODE
#define UNICODE

// header files
// standard headers
#include <stdio.h>

// windows headers
#include <windows.h>

// cuda headers
#include <cuda.h>

#include "CUDALibServer.h"

// global function declarations
__device__ void applyGravityCUDA(Vec3CUDA *acc, Vec3CUDA gravity);
__device__ void applyConstraintsCUDA(BoundingSphere *bSphere, struct SphereAttribs *sphereObject);
__device__ void updatePositionCUDA(struct SphereAttribs *sphereObject, float dt);
__device__ GLfloat vecLenCUDA(Vec3CUDA vec);
__device__ Vec3CUDA vecAddCUDA(Vec3CUDA vec1, Vec3CUDA vec2);
__device__ Vec3CUDA vecSubCUDA(Vec3CUDA vec1, Vec3CUDA vec2);
__device__ Vec3CUDA vecMulCUDA(Vec3CUDA vec1, Vec3CUDA vec2);
__device__ Vec3CUDA vecMulScalarCUDA(Vec3CUDA vec, GLfloat val);
__device__ Vec3CUDA vecDivScalarCUDA(Vec3CUDA vec, GLfloat val);

// CUDA Kernel
__global__ void sphereCollisionSystemKernelCUDA(struct BoundingSphere *bSphere, struct SphereAttribs *sphereAttribs, int numSpheres, GLfloat dt)
{
    // code
    int i = blockIdx.x * blockDim.x + threadIdx.x;

    if(i < numSpheres)
    {
        applyGravityCUDA(&sphereAttribs[i].acc, sphereAttribs[i].grav);
        applyConstraintsCUDA(bSphere, &sphereAttribs[i]);
        SphereAttribs *objectOne = &sphereAttribs[i];
        for(int j = (i + 1); j < numSpheres; j++)
        {
            SphereAttribs *objectTwo = &sphereAttribs[j];
            Vec3CUDA v = vecSubCUDA(objectOne->pos, objectTwo->pos);
            float dist = vecLenCUDA(v);
            float minimumDist = objectOne->mass + objectTwo->mass;
            if(dist < minimumDist)
            {
                Vec3CUDA n = vecDivScalarCUDA(v, dist);
                float delta = minimumDist - dist;
                
                objectOne->pos = vecAddCUDA(objectOne->pos, vecMulScalarCUDA(n, (SPHERE_SPEED_DELTA /* was 0.5f */ * delta)));
                objectTwo->pos = vecSubCUDA(objectTwo->pos, vecMulScalarCUDA(n, (SPHERE_SPEED_DELTA * delta)));
            }
        }
        updatePositionCUDA(&sphereAttribs[i], dt);
    }
}

__global__ void collisionSolverKernelCUDA (struct SphereAttribs *sphereAttribs, int numSpheres)
{
    // code
    int i = blockIdx.x * blockDim.x + threadIdx.x;

    if(i < numSpheres)
    {
        SphereAttribs *objectOne = &sphereAttribs[i];
        for(int j = (i + 1); j < numSpheres; j++)
        {
            SphereAttribs *objectTwo = &sphereAttribs[j];
            Vec3CUDA v = vecSubCUDA(objectOne->pos, objectTwo->pos);
            float dist = vecLenCUDA(v);
            float minimumDist = objectOne->mass + objectTwo->mass;
            if(dist < minimumDist)
            {
                Vec3CUDA n = vecDivScalarCUDA(v, dist);
                float delta = minimumDist - dist;
                
                objectOne->pos = vecAddCUDA(objectOne->pos, vecMulScalarCUDA(n, (SPHERE_SPEED_DELTA /* was 0.5f */ * delta)));
                objectTwo->pos = vecSubCUDA(objectTwo->pos, vecMulScalarCUDA(n, (SPHERE_SPEED_DELTA * delta)));
            }
        }
    }
}

// device functions 
__device__ void applyGravityCUDA(Vec3CUDA *acc, Vec3CUDA gravity)
{
    // code
    acc->x = acc->x + gravity.x;
    acc->y = acc->y + gravity.y;
    acc->z = acc->z + gravity.z;
}

__device__ void applyConstraintsCUDA(BoundingSphere *bSphere, struct SphereAttribs *sphereObject)
{
    // code
    Vec3CUDA distToBsphere = vecSubCUDA(sphereObject->pos, bSphere->origin);
    GLfloat distance = vecLenCUDA(distToBsphere);
    if(distance > (bSphere->radius - sphereObject->mass))
    {
        Vec3CUDA n = vecDivScalarCUDA(distToBsphere, distance);
        sphereObject->pos = vecAddCUDA(bSphere->origin, vecMulScalarCUDA(n, (bSphere->radius - sphereObject->mass)));
    }
}

__device__ void updatePositionCUDA(struct SphereAttribs *sphereObject, float dt)
{
    // code
    Vec3CUDA velocity = vecSubCUDA(sphereObject->pos, sphereObject->old_pos);

    // save current position
    sphereObject->old_pos = sphereObject->pos;

    // apply verlet integration
    sphereObject->pos = vecAddCUDA(vecAddCUDA(sphereObject->pos, velocity), vecMulScalarCUDA(sphereObject->acc, dt * dt));

    // reset acceleration
    sphereObject->acc = {0.0f, 0.0f, 0.0f};
}

__device__ GLfloat vecLenCUDA(Vec3CUDA vec)
{
    // code
    return(fabsf(sqrtf(powf(vec.x, 2) +  powf(vec.y, 2) + powf(vec.z, 2))));
}

__device__ Vec3CUDA vecAddCUDA(Vec3CUDA vec1, Vec3CUDA vec2)
{
    // local variables
    Vec3CUDA temp;
    
    // code
    temp.x = vec1.x + vec2.x;
    temp.y = vec1.y + vec2.y;
    temp.z = vec1.z + vec2.z;

    return(temp);
}

__device__ Vec3CUDA vecSubCUDA(Vec3CUDA vec1, Vec3CUDA vec2)
{
    // local variables
    Vec3CUDA temp;
    
    // code
    temp.x = vec1.x - vec2.x;
    temp.y = vec1.y - vec2.y;
    temp.z = vec1.z - vec2.z;

    return(temp);
}

__device__ Vec3CUDA vecMulCUDA(Vec3CUDA vec1, Vec3CUDA vec2)
{
    // local variables
    Vec3CUDA temp;

    // code
    temp.x = vec1.x * vec2.x;
    temp.y = vec1.y * vec2.y;
    temp.z = vec1.z * vec2.z;

    return(temp);
}

__device__ Vec3CUDA vecMulScalarCUDA(Vec3CUDA vec, GLfloat val)
{
    // code
    vec.x = vec.x * val;
    vec.y = vec.y * val;
    vec.z = vec.z * val;

    return(vec);
}

__device__ Vec3CUDA vecDivScalarCUDA(Vec3CUDA vec, GLfloat val)
{
    // code
    vec.x = vec.x / val;
    vec.y = vec.y / val;
    vec.z = vec.z / val;

    return(vec);
}

// global variables
SphereAttribs *deviceOutput = NULL;
BoundingSphere *deviceBSphere = NULL;
FILE *gpCUDALog = NULL;

// for initializing anything related to this library (only the log file, as of now)
BOOL initializeCUDALibrary(void)
{
    // local variables
    cudaError_t result = cudaSuccess;

    // code
    // open the log file
    gpCUDALog = fopen("CUDALog.txt", "w");
    if(gpCUDALog == NULL)
    {
        printf("CUDALog.txt cannot be opened.\n");
        return(FALSE);
    }

    // device memory allocation
    result = cudaMalloc((void**)&deviceOutput, sizeof(SphereAttribs) * 10000);
    if(result != cudaSuccess)
    {
        fprintf(gpCUDALog, "cudaMalloc() failed for deviceOutput array due to ERROR %s in file %s at line %d.\n", cudaGetErrorString(result), __FILE__, __LINE__);
        return(FALSE);
    }

    result = cudaMalloc((void**)&deviceBSphere, sizeof(BoundingSphere));
    if(result != cudaSuccess)
    {
        fprintf(gpCUDALog, "cudaMalloc() failed for deviceBSphere due to ERROR %s in file %s at line %d.\n", cudaGetErrorString(result), __FILE__, __LINE__);
        return(FALSE);
    }

    fprintf(gpCUDALog, "CUDA Library initialized successfully.\n");
    return(TRUE);
}

void uninitializeCUDALibrary(void)
{
    // function prototypes
    void cleanupArrays(void);

    // code
    // free device memory 
    cleanupArrays();

    // close the log file
    if(gpCUDALog)
    {
        fprintf(gpCUDALog, "CUDA Library uninitialized successfully.\n");
        fclose(gpCUDALog);
        gpCUDALog = NULL;
    }
}

// worker function (does linear interpolation)
BOOL updateSphereCollisionSystemCUDA(struct BoundingSphere *bSphere, struct SphereAttribs *sphereAttribs, int numSpheres, GLfloat dt)
{
    // function declarations
    void cleanupArrays(void);

    // local variables
    int size = numSpheres * sizeof(SphereAttribs);
    int size_bSphere = sizeof(BoundingSphere);
    cudaError_t result = cudaSuccess;

    // code
    // copy from host to device
    result = cudaMemcpy(deviceOutput, sphereAttribs, size, cudaMemcpyHostToDevice);
    if(result != cudaSuccess)
    {
        fprintf(gpCUDALog, "cudaMemcpy() failed to copy data from sphereAttribs array to deviceOutput due to ERROR %s in file %s at line %d.\n", cudaGetErrorString(result), __FILE__, __LINE__);
        return(FALSE);
    }

    result = cudaMemcpy(deviceBSphere, bSphere, size_bSphere, cudaMemcpyHostToDevice);
    if(result != cudaSuccess)
    {
        fprintf(gpCUDALog, "cudaMemcpy() failed to copy data from bSphere to deviceBSphere due to ERROR %s in file %s at line %d.\n", cudaGetErrorString(result), __FILE__, __LINE__);
        return(FALSE);
    }

    // kernel configuration
    dim3 dimGrid = dim3(numSpheres, 1, 1);
    dim3 dimBlock = dim3(1, 1, 1);

    // CUDA kernel for linear interpolation
    sphereCollisionSystemKernelCUDA <<<dimGrid, dimBlock>>> (deviceBSphere, deviceOutput, numSpheres, dt);

    // copy data back to host array
    result = cudaMemcpy(bSphere, deviceBSphere, size_bSphere, cudaMemcpyDeviceToHost);
    if(result != cudaSuccess)
    {
        fprintf(gpCUDALog, "cudaMemcpy() failed to copy data from deviceOutput to deviceBSphere due to ERROR %s in file %s at line %d.\n", cudaGetErrorString(result), __FILE__, __LINE__);
        return(FALSE);
    }

    result = cudaMemcpy(sphereAttribs, deviceOutput, size, cudaMemcpyDeviceToHost);
    if(result != cudaSuccess)
    {
        fprintf(gpCUDALog, "cudaMemcpy() failed to copy data from deviceOutput to sphereAttribs due to ERROR %s in file %s at line %d.\n", cudaGetErrorString(result), __FILE__, __LINE__);
        return(FALSE);
    }

    return(TRUE);
}

BOOL solveCollisionCUDA(struct SphereAttribs *sphereAttribs, int numSpheres)
{
    // function declarations
    void cleanupArrays(void);

    // local variables
    int size = numSpheres * sizeof(SphereAttribs);
    cudaError_t result = cudaSuccess;

    // code
    // copy from host to device
    result = cudaMemcpy(deviceOutput, sphereAttribs, size, cudaMemcpyHostToDevice);
    if(result != cudaSuccess)
    {
        fprintf(gpCUDALog, "cudaMemcpy() failed to copy data from sphereAttribs array to deviceOutput due to ERROR %s in file %s at line %d.\n", cudaGetErrorString(result), __FILE__, __LINE__);
        return(FALSE);
    }

    // kernel configuration
    dim3 dimGrid = dim3(numSpheres, 1, 1);
    dim3 dimBlock = dim3(1, 1, 1);

    // CUDA kernel for linear interpolation
    collisionSolverKernelCUDA <<<dimGrid, dimBlock>>> (deviceOutput, numSpheres);

    // copy data back to host array
    result = cudaMemcpy(sphereAttribs, deviceOutput, size, cudaMemcpyDeviceToHost);
    if(result != cudaSuccess)
    {
        fprintf(gpCUDALog, "cudaMemcpy() failed to copy data from deviceOutput to sphereAttribs due to ERROR %s in file %s at line %d.\n", cudaGetErrorString(result), __FILE__, __LINE__);
        return(FALSE);
    }

    return(TRUE);
}

void cleanupArrays(void)
{
    // code
    if(deviceBSphere)
    {
        cudaFree(deviceBSphere);
        deviceBSphere = NULL;
    }

    if(deviceOutput)
    {
        cudaFree(deviceOutput);
        deviceOutput = NULL;
    }
}
