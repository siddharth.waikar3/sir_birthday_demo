#pragma once

// header files
#include "../Common/Common.h"

// struct definitions
struct Vec3CUDA
{
    float x, y, z;
};

struct ColorCUDA
{
    float r, g, b, alpha;
};

typedef struct tagCell
{
    Vec3 points[4];
} Cell;

typedef struct tagGrid
{   
    Cell *cells;
} Grid;

#if ENABLE_CUDA
struct BoundingSphere
{
    GLUquadric *qbSphereObj;
    Point *vertices;
    Vec3CUDA origin;
    GLfloat radius;
    GLfloat color[4];
};
#else
struct BoundingSphere
{
    GLUquadric *qbSphereObj;
    Point *vertices;
    Vec3CUDA origin;
    GLfloat radius;
    GLfloat color[4];
};
#endif

#if ENABLE_CUDA
struct SphereAttribs
{
    struct Vec3CUDA acc;
    struct Vec3CUDA grav;
    struct Vec3CUDA old_pos;
    struct Vec3CUDA pos;
    float mass;
    float drag;
    struct ColorCUDA color;
};
#else
struct SphereAttribs
{
    Vec3CUDA acc;
    Vec3CUDA grav;    
    Vec3CUDA old_pos;
    Vec3CUDA pos;
    GLfloat mass; // assumed as radius
    GLfloat drag;
    struct ColorCUDA color;
};
#endif

// function declarations
void initializeSphereCollisionSystem(GLfloat radius, BoundingSphere *bSphere, SphereAttribs *sphereAttribs, GLint numSpheres);
void drawSphereCollisionSystem(BoundingSphere *bSphere, SphereAttribs *sphereAttribs, GLuint texture, GLint numSpheres);
BOOL updateSphereCollisionSystem(BoundingSphere *bSphere, SphereAttribs *sphereAttribs, GLint numSpheres);
void uninitializeSphereCollisionSystem(BoundingSphere *bSphere);

// for vector manipulation
float vecLen(Vec3CUDA vec);
Vec3CUDA vecAdd(Vec3CUDA vec1, Vec3CUDA vec2);
Vec3CUDA vecSub(Vec3CUDA vec1, Vec3CUDA vec2);
Vec3CUDA vecMul(Vec3CUDA vec1, Vec3CUDA vec2);
Vec3CUDA vecMulScalar(Vec3CUDA vec, float val);
Vec3CUDA vecDivScalar(Vec3CUDA vec, float val);
