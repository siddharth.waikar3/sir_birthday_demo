// header files
#include "..\Common\Common.h"
#include "..\Common\Texture.h"
#include "Icosahedron.h"

// global variables, macros
#define SHOW_ROCKS 0

// ------------------------------------------------------ for icosahedrons (saturn ring rocks) ------------------------------------------------------
#define SATURN_RING_TEXTURE_PATH ".\\Resources\\Textures\\Rock.png"
#define SATURN_DISK_TEXTURE_PATH ".\\Resources\\Textures\\SaturnDiskMedium.jpg"
#define NUM_ICOSAHEDRONS_PER_ORIGIN 3 
#define NUM_CIRCUMFERENCE_ORIGINS 360

#define SATURN_RING_X_RANGE_START 0.0f
#define SATURN_RING_X_RANGE_END 0.15f
#define SATURN_RING_Y_RANGE_START 0.0f
#define SATURN_RING_Y_RANGE_END 0.0f
#define SATURN_RING_Z_RANGE_START 0.0f
#define SATURN_RING_Z_RANGE_END 0.15f 

#define SATURN_RING_ROCKS_SCALE 0.01f

// #define SATURN_RING_ROT_SPEED 0.012f + 0.05f

IcosahedronAttribs icoAttribsSaturn[NUM_CIRCUMFERENCE_ORIGINS * NUM_ICOSAHEDRONS_PER_ORIGIN];

GLuint icosahedronTexture = 0;
GLuint saturnDiskTexture = 0;
GLUquadric *qSaturnDisk = NULL;

// ------------------------------------------------------ for saturn ------------------------------------------------------
#define SATURN_LATS 60
#define SATURN_LONGS 60
#define SATURN_TEXTURE_PATH ".\\Resources\\Textures\\Saturn_2.jpg"

#define SATURN_COLOR 0.862745f - 0.4f, 0.780392f - 0.4f, 0.572549f - 0.4f, 1.0f

#define SATURN_D_DISK_COLOR 0.223529f, 0.203922f, 0.207843f
#define SATURN_B_DISK_COLOR 0.788235f, 0.694118f, 0.498039f //0.784314f, 0.662745f, 0.529412f, 1.0f
#define SATURN_SMALL_DISK_1_COLOR 0.223529f, 0.203922f, 0.207843f //0.784314f - 0.1f, 0.662745f - 0.1f, 0.529412f - 0.1f, 1.0f
#define SATURN_A_DISK_COLOR 0.400000f, 0.345098f, 0.243137f //0.784314f - 0.1f, 0.662745f - 0.1f, 0.529412f - 0.1f, 1.0f

// #define SATURN_ROT_SPEED 0.008f + 0.05f

GLuint saturnTexture = 0;
Point *saturnVertices = NULL;

// external variables
extern HWND ghwnd;
extern FILE *gpFILE;

// function definitions
// ------------------------------------------------------ SATURN ------------------------------------------------------
void initializeSaturn(void)
{
    // function prototypes
    void initializeSaturnRing(void);

    // code
    // initialize saturn ring first
    initializeSaturnRing();

    // initialize saturn sphere vertices
    saturnVertices = getSphereVertices(SATURN_LATS, SATURN_LONGS);

    // initialize quadric object
    qSaturnDisk = gluNewQuadric();

    // initialize saturn texture
    if(LoadTexture(&saturnTexture, SATURN_TEXTURE_PATH) == FALSE)
    {
        fprintf(gpFILE, "Failed to load Saturn.jpg.\n");
        DestroyWindow(ghwnd);
    }
}

void drawSaturn(GLfloat satYRot, GLfloat ringYRot, BOOL showRocks)
{
    // function prototypes
    void drawSaturnRing(GLfloat alpha, BOOL showRocks);

    // code
    // draw saturn ring
    glPushMatrix();
    {
        glRotatef(ringYRot, 0.0f, 1.0f, 0.0f);
        glScalef(0.850000f, 1.000000f, 0.850000f);
        drawSaturnRing(1.0f, showRocks);
    }
    glPopMatrix();

    // draw saturn
    glPushMatrix();
    {
        glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
        glRotatef(satYRot, 0.0f, 1.0f, 0.0f);
        // drawSphereVerticesWithTexture(saturnVertices, SATURN_LATS, SATURN_LONGS, saturnTexture, GL_QUADS, 1.0f);
        glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
        glBindTexture(GL_TEXTURE_2D, saturnTexture);
        gluQuadricTexture(qSaturnDisk, GL_TRUE);
        gluSphere(qSaturnDisk, 1.0f, 60, 60);
        glBindTexture(GL_TEXTURE_2D, 0);
    }
    glPopMatrix();
}

void drawSaturn_v2(GLfloat satYRot, GLfloat ringYRot, GLfloat ringAlpha, BOOL showRocks)
{
    // function prototypes
    void drawSaturnRing(GLfloat alpha, BOOL showRocks);

    // code
    // draw saturn
    glPushMatrix();
    {
        glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
        glRotatef(satYRot, 0.0f, 1.0f, 0.0f);
        // drawSphereVerticesWithTexture(saturnVertices, SATURN_LATS, SATURN_LONGS, saturnTexture, GL_QUADS, 1.0f);
        glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
        glBindTexture(GL_TEXTURE_2D, saturnTexture);
        gluQuadricTexture(qSaturnDisk, GL_TRUE);
        gluSphere(qSaturnDisk, 1.0f, 60, 60);
        glBindTexture(GL_TEXTURE_2D, 0);
    }
    glPopMatrix();

    // draw saturn ring
    glPushMatrix();
    {
        glRotatef(ringYRot, 0.0f, 1.0f, 0.0f);
        glScalef(0.850000f, 1.000000f, 0.850000f);
        drawSaturnRing(ringAlpha, showRocks);
    }
    glPopMatrix();
}

void drawSaturn_v3(GLfloat satYRot, GLfloat ringYRot, GLfloat saturnAlpha, GLfloat ringAlpha, BOOL showRocks)
{
    // function prototypes
    void drawSaturnRing(GLfloat alpha, BOOL showRocks);

    // code
    // draw saturn
    glPushMatrix();
    {
        glColor4f(1.0f, 1.0f, 1.0f, saturnAlpha);
        glRotatef(satYRot, 0.0f, 1.0f, 0.0f);
        // drawSphereVerticesWithTextureSpecial(saturnVertices, SATURN_LATS, SATURN_LONGS, saturnTexture, GL_QUADS, 1.0f);
        glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
        glBindTexture(GL_TEXTURE_2D, saturnTexture);
        gluQuadricTexture(qSaturnDisk, GL_TRUE);
        gluSphere(qSaturnDisk, 1.0f, 60, 60);
        glBindTexture(GL_TEXTURE_2D, 0);
    }
    glPopMatrix();

    // draw saturn ring
    glPushMatrix();
    {
        glRotatef(ringYRot, 0.0f, 1.0f, 0.0f);
        glScalef(0.850000f, 1.000000f, 0.850000f);
        drawSaturnRing(ringAlpha, showRocks);
    }
    glPopMatrix();
}

void drawSaturnWithoutRing(GLfloat satYRot)
{
    // code
    // draw saturn
    glPushMatrix();
    {
        glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
        glRotatef(satYRot, 0.0f, 1.0f, 0.0f);
        // drawSphereVerticesWithTexture(saturnVertices, SATURN_LATS, SATURN_LONGS, saturnTexture, GL_QUADS, 1.0f);
        glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
        glBindTexture(GL_TEXTURE_2D, saturnTexture);
        gluQuadricTexture(qSaturnDisk, GL_TRUE);
        gluSphere(qSaturnDisk, 1.0f, 60, 60);
        glBindTexture(GL_TEXTURE_2D, 0);
    }
    glPopMatrix();
}

void uninitializeSaturn(void)
{
    // function prototypes
    void uninitializeSaturnRing(void);

    // code
    // delete saturn texture
    if(saturnTexture)
        glDeleteTextures(1, &saturnTexture);

    // delete quadric object
    if(qSaturnDisk)
        gluDeleteQuadric(qSaturnDisk);

    // release saturn sphere vertices
    freeVertices(&saturnVertices);

    // uninitialize saturn ring
    uninitializeSaturnRing();
}

// ------------------------------------------------------ INTERNAL FUNCTIONS ------------------------------------------------------
// ------------------------------------------------------ SATURN'S RING ------------------------------------------------------
void initializeSaturnRing(void)
{
    // local variables
    GLfloat radius = 1.0f;
    GLint icoArrRow = 0;
    GLint icoArrRowIncrement = NUM_ICOSAHEDRONS_PER_ORIGIN;

    // code
    //srand(time(NULL)); // set seed ONCE

    // initialize saturn ring rocks
    for(GLint angle = 0; angle < 360; angle++)
    {
        GLfloat angleInRadians = (GLfloat)angle * (M_PI / 180.0f);
        GLfloat x = radius * cosf(angleInRadians);
        GLfloat z = radius * sinf(angleInRadians);

        Vec3 origin = {x, 0.0f, z};
        Vec3 rangeStart = {SATURN_RING_X_RANGE_START, SATURN_RING_Y_RANGE_START, SATURN_RING_Z_RANGE_START};
        Vec3 rangeEnd = {SATURN_RING_X_RANGE_END, SATURN_RING_Y_RANGE_END, SATURN_RING_Z_RANGE_END};

        initializeRangeIcosahedrons(&icoAttribsSaturn[icoArrRow], NUM_ICOSAHEDRONS_PER_ORIGIN, origin, rangeStart, rangeEnd);
        icoArrRow = icoArrRow + icoArrRowIncrement;
    }

    // initialize ring texture
    if(LoadTexture(&icosahedronTexture, SATURN_RING_TEXTURE_PATH) == FALSE)
    {
        fprintf(gpFILE, "Failed to load Rock.png.\n");
        DestroyWindow(ghwnd);
    }

    // initialize disk texture
    // if(LoadTexture(&saturnDiskTexture, SATURN_DISK_TEXTURE_PATH) == FALSE)
    // {
    //     fprintf(gpFILE, "Failed to load SaturnDiskMedium.jpg.\n");
    //     DestroyWindow(ghwnd);
    // }
}

void drawSaturnRing(GLfloat alpha, BOOL showRocks)
{
    // function prototypes
    void drawSaturnDisk(GLfloat innerRadius, GLfloat outerRadius, Color color);

    // code
    glColor4f(1.0f, 1.0f, 1.0f, alpha);

    // draw ring's rocks
    // for innermost ring    
    if (showRocks)
    {
        glPushMatrix();
        {
            applyTransformations({
                                    -0.140000f, 0.005000f, -0.125000f,
                                    0.000000f, 1.169789f, 0.000000f,
                                    1.590000f, 1.590000f, 1.590000f
                });
            
            drawRangeIcosahedrons(
                icoAttribsSaturn,
                NUM_CIRCUMFERENCE_ORIGINS * NUM_ICOSAHEDRONS_PER_ORIGIN,  /* number of icosahedrons (rocks)*/
                icosahedronTexture,
                0.005f + 0.000500f /* scale */
            );
        }
        glPopMatrix();

        // for middle part between the rings
        glPushMatrix();
        {
            applyTransformations({
                                    -0.150000f, 0.010000f, -0.150000f,
                                    0.000000f, 0.000000f, 0.000000f,
                                    1.900000f, 1.911000f, 1.910000f
                });
            drawRangeIcosahedrons(icoAttribsSaturn, NUM_CIRCUMFERENCE_ORIGINS * NUM_ICOSAHEDRONS_PER_ORIGIN, icosahedronTexture, 0.005f + 0.000500f);
        }
        glPopMatrix();

        // for outermost ring
        glPushMatrix();
        {
            applyTransformations({
                                    -0.150000f, 0.010000f, -0.125000f,
                                    0.000000f, 0.000000f, 0.000000f,
                                    2.180000f, 2.180000f, 2.180000f
                });
            drawRangeIcosahedrons(icoAttribsSaturn, NUM_CIRCUMFERENCE_ORIGINS * NUM_ICOSAHEDRONS_PER_ORIGIN, icosahedronTexture, 0.005f + 0.000500f);
        }
        glPopMatrix();
    }

    // draw saturn ring disks using gluDisk()
    // B disk
    glPushMatrix();
    {
        glScalef(3.100000f, 1.000000f, 3.100000f);
        drawSaturnDisk(0.5f, 0.6f, {SATURN_B_DISK_COLOR, alpha / 2.0f});
    }
    glPopMatrix();

    // Small disks between B and A disks
    glPushMatrix();
    {
        glScalef(3.150000f, 1.000000f, 3.150000f);
        drawSaturnDisk(0.593f, 0.62f, {SATURN_SMALL_DISK_1_COLOR, alpha / 2.0f});
    }
    glPopMatrix();

    // A disk
    glPushMatrix();
    {
        glScalef(3.749999f + 0.005f, 1.000000f, 3.749999f + 0.005f);
        drawSaturnDisk(0.52f, 0.6f, {SATURN_A_DISK_COLOR, alpha / 2.0f});
    }
    glPopMatrix();
}

void uninitializeSaturnRing(void)
{
    // code
    if(saturnDiskTexture)
        glDeleteTextures(1, &saturnDiskTexture);

    if(icosahedronTexture)
        glDeleteTextures(1, &icosahedronTexture);
}

// used internally by drawSaturnRing()
void drawSaturnDisk(GLfloat innerRadius, GLfloat outerRadius, Color color)
{
    // code
    // 2 cylinders
    glPushMatrix();
    {
        glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
        glColor4f(color.r, color.g, color.b, color.alpha);
        gluCylinder(qSaturnDisk, outerRadius, outerRadius, 0.009f, 60, 60);
    }
    glPopMatrix();
    
    glPushMatrix();
    {
        glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
        glColor4f(color.r, color.g, color.b, color.alpha);
        gluCylinder(qSaturnDisk, innerRadius, innerRadius, 0.009f, 60, 60);
    }
    glPopMatrix();

    // Disk 
    glPushMatrix();
    {
        glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
        glColor4f(color.r, color.g, color.b, color.alpha);
        gluDisk(qSaturnDisk, innerRadius, outerRadius, 60, 60);
    }
    glPopMatrix();

    /*glPushMatrix();
    {
        glTranslatef(0.0f, -0.001f, 0.0f);
        glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
        glColor4f(color.r, color.g, color.b, color.alpha);
        gluDisk(qSaturnDisk, innerRadius, outerRadius, 60, 60);
    }
    glPopMatrix();*/
}
