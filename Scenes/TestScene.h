#pragma once

// function declarations
void initializeTestScene(void);
void displayTestScene(void);
void updateTestScene(void);
void uninitializeTestScene(void);
