// header files
#include "MergeSort.h"

// function definitions
void TopDownMergeSort(GLfloat *a, GLfloat *b, GLint n)
{   
    // function prototypes
    void CopyArray(GLfloat *src, GLint iBegin, GLint iEnd, GLfloat *dest);
    void TopDownSplitMerge(GLfloat *b, GLint iBegin, GLint iEnd, GLfloat *a);

    // code
    CopyArray(a, 0, n, b); // one time copy of A[] to B[]
    TopDownSplitMerge(a, 0, n, b); // sort data from B[] into A[]
}

void TopDownSplitMerge(GLfloat *b, GLint iBegin, GLint iEnd, GLfloat *a)
{
    // function prototypes
    void TopDownMerge(GLfloat *b, GLint iBegin, GLint iMiddle, GLint iEnd, GLfloat *a);

    // code
    if(iEnd - iBegin <= 1)          // if run size == 1
    {   
        return ;                    // consider it sorted
    }

    // split the run longer than 1 item into halves
    GLint iMiddle = (iEnd + iBegin) / 2;                  // iMiddle = middle point
    
    // recursively sort both runs from array A[] into B[]
    TopDownSplitMerge(a, iBegin, iMiddle, b); // sort the left run
    TopDownSplitMerge(a, iMiddle, iEnd, b);   // sort the right run

    // merge the resulting runs from array B[] into array A[]
    TopDownMerge(b, iBegin, iMiddle, iEnd, a);
}

void TopDownMerge(GLfloat *b, GLint iBegin, GLint iMiddle, GLint iEnd, GLfloat *a)
{
    // local variables
    int i = iBegin;
    int j = iMiddle;

    // code
    // while there are elements in the left or right runs...
    for(int k = iBegin; k < iEnd; k++)
    {
        // if left run head exists and is <= existing right run head
        if(i < iMiddle && (j >= iEnd || a[i] <= a[j]))
        {
            b[k] = a[i];
            i = i + 1;
        }
        else
        {
            b[k] = a[j];
            j = j + 1;
        }
    }
}

void CopyArray(GLfloat *src, GLint iBegin, GLint iEnd, GLfloat *dest)
{
    // code
    for(int k = iBegin; k < iEnd; k++)
    {
        dest[k] = src[k];
    }
}
