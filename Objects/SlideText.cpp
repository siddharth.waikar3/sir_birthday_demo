
// UNICODE
#ifndef UNICODE
	#define UNICODE
#endif

// Header Files
#include "..\Common\Common.h"
#include "..\Common\RenderText.h"

// macros
#define SLIDETEXT_FONT1_OFFSET 100001
#define SLIDETEXT_FONT2_OFFSET 110001
#define SLIDETEXT_ARIAL_FONT_OFFSET 120001
#define GOLDEN_COLOR_CODE   0.82f, 0.69f, 0.0f

// global function declarations
void applyTransformations(struct Transformations transformations);

// global variables
extern struct Transformations g_Transformations[NUM_TRANSFORMATION_ELEMENTS];
RT_Library SlideTextFont1;
RT_Library SlideTextFont2;
RT_Library ArialFont;
extern GLfloat cubeVertices[];
extern GLubyte cubeIndices[];
extern HDC ghdc;

const struct Transformations pattiCubeTransformations[4] = {
	// transformations[3]
		{
			-2.000000f, 0.000000f, 0.000000f,
			0.000000f, 0.000000f, 0.000000f,
			0.090000f, 1.000000f, 0.300000f
		},
		// transformations[4]
		{
			2.099999f, 0.000000f, 0.000000f,
			0.000000f, 0.000000f, 0.000000f,
			0.090000f, 1.000000f, 0.300000f
		},
		// transformations[5]
		{
			0.060000f, 1.100000f, 0.000000f,
			0.000000f, 0.000000f, 0.000000f,
			2.177000f - 0.02f, 0.090001f, 0.260001f
		},
		// transformations[6]
		{
			0.050000f, -1.070000f, 0.195000f,
			0.000000f, 0.000000f, 0.000000f,
			2.177000f - 0.02f, 0.090001f, 0.110000f
		},
};

// function defination
void initializSlideText(void)
{
	// code
	RT_Initialize_Library(&SlideTextFont1, ghdc, TEXT("./Resources/fonts/SF TransRobotics.ttf"), TEXT("SF TransRobotics"), RT_FW_DONTCARE, 0, SLIDETEXT_FONT1_OFFSET);
	RT_Initialize_Library(&SlideTextFont2, ghdc, TEXT("./Resources/fonts/nulshock bd.otf"), TEXT("Nulshock Rg"), RT_FW_DONTCARE, 0, SLIDETEXT_FONT2_OFFSET);
	RT_Initialize_Library(&ArialFont, ghdc, NULL, TEXT("Arial"), RT_FW_DONTCARE, 0, SLIDETEXT_ARIAL_FONT_OFFSET);
	// RT_Initialize_Library(&SlideTextFont1, ghdc, NULL, TEXT("Arial"), RT_FW_DONTCARE, 0, SLIDETEXT_FONT1_OFFSET);
	// RT_Initialize_Library(&SlideTextFont2, ghdc, NULL, TEXT("Arial"), RT_FW_DONTCARE, 0, SLIDETEXT_FONT2_OFFSET);
}

void drawMorganFreemanQuote(int iWhichText, GLfloat alpha)
{
	// Local variable
	char str1[] = "\"Highest purpose of a cell is to pass on the knowledge\"";
	char str2[] = "- Morgan Freeman, Lucy (2014)";

	// code
	if(iWhichText == 1)
	{
		glPushMatrix();
		{
			glTranslatef(-9.97f/*-9.950006f*/, 0.700000f, -10.950002f);
			glScalef(1.0f, 1.4f, 1.0f);
			drawGradedText(str1, {1.0f, 1.0f, 1.0f, alpha}, {0.3f, 0.3f, 0.3f, alpha}, SLIDETEXT_FONT1_OFFSET);
		}
		glPopMatrix();
	}

	else if(iWhichText == 2)
	{
		glPushMatrix();
		{
			glTranslatef(-1.650002f, -0.700000f, -10.950009f);
			glScalef(1.0f, 1.4f, 1.0f);
			drawGradedText(str2, {1.0f, 1.0f, 1.0f, alpha}, {0.3f, 0.3f, 0.3f, alpha}, SLIDETEXT_FONT1_OFFSET);
		}
		glPopMatrix();
	}
}

void drawERTR(void)
{
	// local variable
	char str1[] = "ERTR";

	RT_DrawText(str1, strlen(str1), SLIDETEXT_FONT2_OFFSET);
}

void drawRTR(void)
{
	// local variable
	char str1[] = "RTR";

	glPushMatrix();
	{
		applyTransformations({
	-1.300000f, -0.200000f, 0.200000f,
	0.000000f, 0.000000f, 0.000000f,
	1.200000f, 1.200000f, 1.000000f
			});

		//glColor3f(GOLDEN_COLOR_CODE);
		RT_DrawText(str1, strlen(str1), SLIDETEXT_FONT2_OFFSET);

	}
	glPopMatrix();
}

void drawARTR(void)
{
	char str1[] = "ARTR";

	RT_DrawText(str1, strlen(str1), SLIDETEXT_FONT2_OFFSET);
}

void drawWhatsSirThink(Color color, Color gradientColor)
{
	// Local variable
	char str[] = "What Sir thinks";
	
	// code
	drawGradedText(str, color, gradientColor, SLIDETEXT_FONT1_OFFSET);
}

void drawSirsWellWishersThink(Color color, Color gradientColor)
{
	// local variables
	char str[] = "\"What Sir's well-wishers think\"";

	// code
	drawGradedText(str, color, gradientColor, SLIDETEXT_FONT1_OFFSET);
}

void drawAudioVideoSeminar(void)
{
	// local varibal
	char str1[] = "Audio Video Seminar";

	RT_DrawText(str1, strlen(str1), SLIDETEXT_FONT1_OFFSET);
}

void drawAppleFCPMaster(void)
{
	char str1[] = "Apple FCP Master";

	glPushMatrix();
	{
		applyTransformations({
	-1.820000f, -0.010000f, 0.100000f,
	0.000000f, 0.000000f, 0.000000f,
	0.570000f, 0.650000f, 0.120000f
			});

		//glColor3f(GOLDEN_COLOR_CODE);
		RT_DrawText(str1, strlen(str1), SLIDETEXT_FONT1_OFFSET);
	}
	glPopMatrix();
}

void drawARMAccreditedEngineer(void)
{
	char str1[] = "ARM Accredited Engineer";

	glPushMatrix();
	{
		applyTransformations({
	-1.800000f, -0.060000f, 0.100000f,
	0.000000f, 0.000000f, 0.000000f,
	0.400000f, 0.800000f, 1.000000f
			});

		//glColor3f(GOLDEN_COLOR_CODE);
		RT_DrawText(str1, strlen(str1), SLIDETEXT_FONT1_OFFSET);

	}
	glPopMatrix();
}

void drawTheEnd(void)
{
	// local varibal
	char str1[] = "The End ???";

	glPushMatrix();
	{
		applyTransformations({
	-1.906801f, -0.132900f, 0.101000f,
	0.000000f, 0.000000f, 0.000000f,
	0.881000f, 0.891000f, 0.190001f
			});

		glColor3f(0.1f, 0.1f, 0.1f);
		RT_DrawText(str1, strlen(str1), SLIDETEXT_FONT1_OFFSET);
	}
	glPopMatrix();
}

#define PATTICOLOR1 0.505882f, 0.200000f, 0.117647f
#define PATTICOLOR2 0.650980f, 0.333333f, 0.223529f
#define PATTICOLOR3 0.784314f, 0.509804f, 0.349020f
#define PATTICOLOR4 0.494118f, 0.188235f, 0.117647f

#define PATTIDARKCOLOR1 0.505882f - 0.05f, 0.200000f - 0.05f, 0.117647f - 0.05f
#define PATTIDARKCOLOR2 0.650980f - 0.05f, 0.333333f - 0.05f, 0.223529f - 0.05f
#define PATTIDARKCOLOR3 0.784314f - 0.05f, 0.509804f - 0.05f, 0.349020f - 0.05f
#define PATTIDARKCOLOR4 0.494118f - 0.05f, 0.188235f - 0.05f, 0.117647f - 0.05f

void drawPatti(void)
{
	// Local Variables
	GLfloat pattiColors[] = {
		// Front
		PATTICOLOR1, 
		PATTICOLOR2,
		PATTICOLOR3,
		PATTICOLOR4,

		// Back
		PATTICOLOR1, 
		PATTICOLOR2,
		PATTICOLOR3,
		PATTICOLOR4,

		// Left
		PATTIDARKCOLOR1,
		PATTIDARKCOLOR2,
		PATTIDARKCOLOR3,
		PATTIDARKCOLOR4,

		// Right
		PATTIDARKCOLOR1,
		PATTIDARKCOLOR2,
		PATTIDARKCOLOR3,
		PATTIDARKCOLOR4,

		// Top
		PATTICOLOR1, 
		PATTICOLOR2,
		PATTICOLOR3,
		PATTICOLOR4,

		// Bottom
		PATTIDARKCOLOR1,
		PATTIDARKCOLOR2,
		PATTIDARKCOLOR3,
		PATTIDARKCOLOR4,
	};

	// Code
	glPushMatrix();
	{
		glScalef(2.0f, 1.0f, 0.1f);
		glBegin(GL_QUADS);
		{
			// front
			glColor3f(0.34f, 0.08f, 0.01f);
			//glColor3ub(163, 129, 92);
			glVertex3f(1.0f, 1.0f, 1.0f);

			glVertex3f(-1.0f, 1.0f, 1.0f);

			// glColor3f(0.545098f + 0.2f, 0.270588f + 0.2f, 0.074510f + 0.2f);
			//glColor3f(165, 133, 92);
			glVertex3f(-1.0f, -1.0f, 1.0f);

			//glColor3ub(163, 129, 92);
			glVertex3f(1.0f, -1.0f, 1.0f);
		}
		glEnd();
	}
	glPopMatrix();

	glVertexPointer(3, GL_FLOAT, 0, cubeVertices);
	glColorPointer(3, GL_FLOAT, 0, pattiColors);

	glPushMatrix();
	{
		applyTransformations(g_Transformations[3]);

		glPushMatrix();
		{
			applyTransformations(pattiCubeTransformations[0]);

			glDrawElements(GL_QUADS, 24, GL_UNSIGNED_BYTE, cubeIndices);
		}
		glPopMatrix();


		glPushMatrix();
		{
			applyTransformations(pattiCubeTransformations[1]);

			glDrawElements(GL_QUADS, 24, GL_UNSIGNED_BYTE, cubeIndices);
		}
		glPopMatrix();

		glPushMatrix();
		{
			applyTransformations(pattiCubeTransformations[2]);

			glDrawElements(GL_QUADS, 24, GL_UNSIGNED_BYTE, cubeIndices);
		}
		glPopMatrix();

		glPushMatrix();
		{
			applyTransformations(pattiCubeTransformations[3]);

			glDrawElements(GL_QUADS, 24, GL_UNSIGNED_BYTE, cubeIndices);
		}
		glPopMatrix();
		}
	glPopMatrix();
}

void drawWinDev(void)
{
	char str1[] = "WinDev";

	glPushMatrix();
	{
		applyTransformations({
			-1.800000f, -0.060000f, 0.100000f,
			0.000000f, 0.000000f, 0.000000f,
			0.400000f, 0.800000f, 1.000000f
			});

		//glColor3f(GOLDEN_COLOR_CODE);
		RT_DrawText(str1, strlen(str1), SLIDETEXT_FONT1_OFFSET);

	}
	glPopMatrix();
}

void drawOrbitText(char* szString)
{
	RT_DrawText(szString, strlen(szString), SLIDETEXT_ARIAL_FONT_OFFSET);
}

void uninitializSlideText(void)
{
	RT_Uninitialize_Library(&ArialFont, SLIDETEXT_ARIAL_FONT_OFFSET);
	RT_Uninitialize_Library(&SlideTextFont2, SLIDETEXT_FONT2_OFFSET);
	RT_Uninitialize_Library(&SlideTextFont1, SLIDETEXT_FONT1_OFFSET);
}
