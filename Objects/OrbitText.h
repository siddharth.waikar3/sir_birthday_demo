#pragma once

// macros for orbit text
#define WINDEV_2002_TEXT 0x00000001
#define UNIX_2003_TEXT 0x00000002
#define YEAR_2004_TEXT 0x00000004
#define SUNBEAM_2006_TEXT 0x00000008
#define OS_SEMINAR_2008_TEXT 0x0000000F

// function declarations
void initializeJourneySceneText(void);
void drawJourneySceneText(int iWhichText, struct Translation translation, GLfloat yRotation, GLfloat alpha);
void uninitializeJourneySceneText(void);
