// header files
#include "../Common/Common.h"

// macros
#define NFACE 20
#define NVERTEX 12

#define X .525731112119133606 
#define Z .850650808352039932

// global variables
int winW = 512;
int winH = 512;
// These are the 12 vertices for the icosahedron
static GLfloat vdata[NVERTEX][3] = {
   {-X, 0.0, Z}, {X, 0.0, Z}, {-X, 0.0, -Z}, {X, 0.0, -Z},
   {0.0, Z, X}, {0.0, Z, -X}, {0.0, -Z, X}, {0.0, -Z, -X},
   {Z, X, 0.0}, {-Z, X, 0.0}, {Z, -X, 0.0}, {-Z, -X, 0.0}
};

// These are the 20 faces.  Each of the three entries for each 
// vertex gives the 3 vertices that make the face.
static GLint tindices[NFACE][3] = {
   {0,4,1}, {0,9,4}, {9,5,4}, {4,5,8}, {4,8,1},
   {8,10,1}, {8,3,10}, {5,3,8}, {5,2,3}, {2,7,3},
   {7,10,3}, {7,6,10}, {7,11,6}, {11,0,6}, {0,1,6},
   {6,1,10}, {9,0,11}, {9,11,2}, {9,2,5}, {7,2,11}
};

GLfloat colorArray[][3] = { {0.15, 0.15, 0.15},{0.2, 0.2, 0.2},{0.23, 0.23, 0.23},{0.02, 0.02, 0.02},{0.5f, 0.5f, 0.5f},{0.07,0.07,0.07} };
GLint colorIndex = 0;

GLfloat r = 0.4;
GLfloat g = 0.4;
GLfloat b = 0.4;

int testNumber; // Global variable indicating which test number is desired

static GLfloat rotX = 0.0;
static GLfloat dX = 1.0;
static GLfloat updateRate = 10;
static GLuint maxDepth = 1;

void normalize(GLfloat v[3]) {
    GLfloat d = sqrt((v[0] * v[0]) + (v[1] * v[1]) + (v[2] * v[2]));
    if (d == 0.0) return;
    v[0] /= d;
    v[1] /= d;
    v[2] /= d;

}

void drawTriangle(GLfloat* v1, GLfloat* v2, GLfloat* v3)
{

    // Draw a triangle with specified vertices 
    glBegin(GL_TRIANGLES);

    //glColor3f(rand() / ((float)RAND_MAX + 1), rand() / ((float)RAND_MAX + 1), rand() / ((float)RAND_MAX + 1));
    if (colorIndex == 0) {
        glColor3f(0.15f, 0.15f, 0.15f);
        glVertex3fv(v1);

        glColor3f(0.2, 0.2, 0.2);
        glVertex3fv(v2);

        glColor3f(0.07, 0.07, 0.07);
        glVertex3fv(v3);
    }
    else {
        if (colorIndex == 1) {
            glColor3f(0.15f, 0.15f, 0.15f);
            glVertex3fv(v1);

            glColor3f(0.19f, 0.19f, 0.19f);
            glVertex3fv(v2);

            glColor3f(0.10f, 0.10f, 0.10f);
            glVertex3fv(v3);
        }
        if (colorIndex == 2)
        {
            glColor3f(0.31f, 0.31f, 0.31f);
            glVertex3fv(v1);

            glColor3f(0.20f, 0.20f, 0.20f);
            glVertex3fv(v2);

            glColor3f(0.2f, 0.2f, 0.2f);
            glVertex3fv(v3);
        }
    }
    glEnd();

    //glBegin(GL_LINE_LOOP);
    ////glColor3f(0.1, 0.1, 0.1);
    //glVertex3fv(v1);
    //glVertex3fv(v2);
    //glVertex3fv(v3);
    //glEnd();
}

void subDivide(GLfloat* v1, GLfloat* v2, GLfloat* v3, int depth) {

    if (depth == 0) {
        drawTriangle(v1, v2, v3);
        return;
    }
    //midpoint of each edge
    GLfloat v12[3];
    GLfloat v23[3];
    GLfloat v31[3];
    for (int i = 0; i < 3; i++) {
        v12[i] = v1[i] + v2[i];
        v23[i] = v2[i] + v3[i];
        v31[i] = v3[i] + v1[i];
    }
    normalize(v12);
    normalize(v23);
    normalize(v31);

    subDivide(v1, v12, v31, depth - 1);
    subDivide(v2, v23, v12, depth - 1);
    subDivide(v3, v31, v23, depth - 1);
    subDivide(v12, v23, v31, depth - 1);
}

void drawIcosahedron(int maxDepth) {
    for (int i = 0; i < NFACE; i++) {
        srand(i);
        subDivide(&vdata[tindices[i][0]][0], &vdata[tindices[i][1]][0], &vdata[tindices[i][2]][0], maxDepth);
    }
}

