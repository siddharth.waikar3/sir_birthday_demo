#pragma once

void initializSlideText(void);
void drawMorganFreemanQuote(int iWhichText, GLfloat alpha);
void uninitializSlideText(void);
void drawERTR(void);
void drawRTR(void);
void drawARTR(void);
void drawWhatsSirThink(Color color, Color gradientColor);
void drawSirsWellWishersThink(Color color, Color gradientColor);
void drawAudioVideoSeminar(void);
void drawTheEnd(void);
void drawAppleFCPMaster(void);
void drawARMAccreditedEngineer(void);
void drawPatti(void);
