// header files
#include "Common.h"
#include "Camera.h"
#include "OAL.h"
#include "RenderText.h"
#include "Timer.h"
#include <stdio.h> // for file IO

// global variables
GLfloat cubeVertices[] = 
{
	// all 8 vertices
	1.0f, 1.0f, 1.0f,   // front, right top (0th index)
	-1.0f, 1.0f, 1.0f,  // front, left top (1st index)
	-1.0f, -1.0f, 1.0f, // front, left bottom (2nd index)
	1.0f, -1.0f, 1.0f, 	// front, right bottom (3rd index)

	// (from back face's perspective)
	-1.0f, 1.0f, -1.0f, // back, right top (4th index)
	1.0f, 1.0f, -1.0f,  // back, left top (5th index)
	1.0f, -1.0f, -1.0f, // back, left bottom (6th index)
	-1.0f, -1.0f, -1.0f, // back, right bottom (7th index)
};

GLubyte cubeIndices[] =
{
	// front face
	0, 1, 2, 3,

	// back face
	4, 5, 6, 7,

	// left face
	4, 1, 2, 7,

	// right face
	0, 5, 6, 3,

	// top face
	5, 4, 1, 0,

	// bottom face
	3, 2, 7, 6 
};

extern FILE *gpFILE;
extern HWND ghwnd;

extern Vector cameraFront;
extern ALSource g_ActiveALSource;
extern CameraCoordinates cameraCoordinates;
extern StopWatchTimer *stopWatch;

// function definitions
void applyTransformations(Transformations transformations)
{
	// code
	glTranslatef(transformations.translation.x, transformations.translation.y, transformations.translation.z);
	
	glRotatef(transformations.rotation.x, 1.0f, 0.0f, 0.0f);
	glRotatef(transformations.rotation.y, 0.0f, 1.0f, 0.0f);
	glRotatef(transformations.rotation.z, 0.0f, 0.0f, 1.0f);
	
	glScalef(transformations.scale.x, transformations.scale.y, transformations.scale.z);
}

void applyTransformationsWithoutRotation(Transformations transformations)
{
	// code
	glTranslatef(transformations.translation.x, transformations.translation.y, transformations.translation.z);
	glScalef(transformations.scale.x, transformations.scale.y, transformations.scale.z);
}

void subVec(Vector *vec1, Vector *vec2)
{
	// code
	vec1->x = vec1->x - vec2->x;
	vec1->y = vec1->y - vec2->y;
	vec1->z = vec1->z - vec2->z;
}

void addVec(Vector *vec1, Vector *vec2)
{
	// code
	vec1->x = vec1->x + vec2->x;
	vec1->y = vec1->y + vec2->y;
	vec1->z = vec1->z + vec2->z;
}

GLfloat getInterpolatedValue(GLfloat x0, GLfloat x1, GLfloat y0, GLfloat y1, GLfloat x)
{
	return ((y0 * (x1 - x)) + (y1 * (x - x0))) / (x1 - x0);
}

BOOL lerpAnimation(struct Transformations* current, struct Transformations* inital, struct Transformations* end, GLfloat* tval, GLfloat speed)
{
	// code
	if (*tval < 1.0f)
	{
		*tval = *tval + speed;
	}
	else if (*tval > 1.0f)
	{
		*tval = 1.0f;
	}

	// linear interpolation
	current->translation.x = getInterpolatedValue(0.0f, 1.0f, inital->translation.x, end->translation.x, *tval);
	current->translation.y = getInterpolatedValue(0.0f, 1.0f, inital->translation.y, end->translation.y, *tval);
	current->translation.z = getInterpolatedValue(0.0f, 1.0f, inital->translation.z, end->translation.z, *tval);

	current->rotation.x = getInterpolatedValue(0.0f, 1.0f, inital->rotation.x, end->rotation.x, *tval);
	current->rotation.y = getInterpolatedValue(0.0f, 1.0f, inital->rotation.y, end->rotation.y, *tval);
	current->rotation.z = getInterpolatedValue(0.0f, 1.0f, inital->rotation.z, end->rotation.z, *tval);

	current->scale.x = getInterpolatedValue(0.0f, 1.0f, inital->scale.x, end->scale.x, *tval);
	current->scale.y = getInterpolatedValue(0.0f, 1.0f, inital->scale.y, end->scale.y, *tval);
	current->scale.z = getInterpolatedValue(0.0f, 1.0f, inital->scale.z, end->scale.z, *tval);

	if(*tval < 1.0f)
		return(FALSE);
	else
		return(TRUE);
}

void lerpAnimation_v2(LerpTransformations *lerp, GLfloat condition)
{
	// code
	// increment tVal by speed
	lerp->tVal = lerp->tVal + lerp->speed;
	if (lerp->tVal > condition) // if tVal is greater than condition, the object has reached its destination
	{
		lerp->tVal = condition;
		lerp->bReached = TRUE;
	}	
	else 					 	// if tVal is NOT greater than condition, the object hasn't reached its destination yet
	{
		lerp->bReached = FALSE;
	}

	// linear interpolation
	lerp->current.translation.x = getInterpolatedValue(0.0f, 1.0f, lerp->begin.translation.x, lerp->end.translation.x, lerp->tVal);
	lerp->current.translation.y = getInterpolatedValue(0.0f, 1.0f, lerp->begin.translation.y, lerp->end.translation.y, lerp->tVal);
	lerp->current.translation.z = getInterpolatedValue(0.0f, 1.0f, lerp->begin.translation.z, lerp->end.translation.z, lerp->tVal);

	lerp->current.rotation.x = getInterpolatedValue(0.0f, 1.0f, lerp->begin.rotation.x, lerp->end.rotation.x, lerp->tVal);
	lerp->current.rotation.y = getInterpolatedValue(0.0f, 1.0f, lerp->begin.rotation.y, lerp->end.rotation.y, lerp->tVal);
	lerp->current.rotation.z = getInterpolatedValue(0.0f, 1.0f, lerp->begin.rotation.z, lerp->end.rotation.z, lerp->tVal);

	lerp->current.scale.x = getInterpolatedValue(0.0f, 1.0f, lerp->begin.scale.x, lerp->end.scale.x, lerp->tVal);
	lerp->current.scale.y = getInterpolatedValue(0.0f, 1.0f, lerp->begin.scale.y, lerp->end.scale.y, lerp->tVal);
	lerp->current.scale.z = getInterpolatedValue(0.0f, 1.0f, lerp->begin.scale.z, lerp->end.scale.z, lerp->tVal);
}

void drawShadow(GLfloat fAlpha)
{
	// code
	glBegin(GL_QUADS);
	{
		glColor4f(0.0f, 0.0f, 0.0f, fAlpha);
		glVertex3f(1.0f, 1.0f, -1.0f);
		glVertex3f(-1.0f, 1.0f, -1.0f);
		glVertex3f(-1.0f, 1.0f, 1.0f);
		glVertex3f(1.0f, 1.0f, 1.0f);
	}
	glEnd();
}

void drawBasicCube(GLfloat *verticesColors[24])
{
    // local variables
    GLfloat basicCubeVertices[24][3] = {{1.0f, 1.0f, 1.0f},     // front face vertices
                                        {-1.0f, 1.0f, 1.0f},
                                        {-1.0f, -1.0f, 1.0f},
                                        {1.0f, -1.0f, 1.0f},
                                        
                                        {-1.0f, 1.0f, -1.0f},   // back face vertices
                                        {1.0f, 1.0f, -1.0f},
                                        {1.0f, -1.0f, -1.0f},
                                        {-1.0f, -1.0f, -1.0f},
                                        
                                        {-1.0f, 1.0f, 1.0f},    // left face vertices
                                        {-1.0f, 1.0f, -1.0f},
                                        {-1.0f, -1.0f, -1.0f},
                                        {-1.0f, -1.0f, 1.0f},
                                        
                                        {1.0f, 1.0f, -1.0f},    // right face vertices
                                        {1.0f, 1.0f, 1.0f},
                                        {1.0f, -1.0f, 1.0f},
                                        {1.0f, -1.0f, -1.0f},
                                        
                                        {1.0f, 1.0f, -1.0f},    // top face vertices
                                        {-1.0f, 1.0f, -1.0f},
                                        {-1.0f, 1.0f, 1.0f},
                                        {1.0f, 1.0f, 1.0f},
                                    
                                        {1.0f, -1.0f, 1.0f}, // bottom face vertices
                                        {-1.0f, -1.0f, 1.0f},
                                        {-1.0f, -1.0f, -1.0f},
                                        {1.0f, -1.0f, -1.0f}};

    // code
    if(verticesColors == NULL)
    {
        glBegin(GL_QUADS);
        {
            glColor4f(1.0f, 1.0f, 1.0f, 1.0f); // default color if empty array is passed

            // front face
            glVertex3fv(basicCubeVertices[0]);
            glVertex3fv(basicCubeVertices[1]);
            glVertex3fv(basicCubeVertices[2]);
            glVertex3fv(basicCubeVertices[3]);

            // back face
            glVertex3fv(basicCubeVertices[4]);
            glVertex3fv(basicCubeVertices[5]);
            glVertex3fv(basicCubeVertices[6]);
            glVertex3fv(basicCubeVertices[7]);
        
            // left face
            glVertex3fv(basicCubeVertices[8]);
            glVertex3fv(basicCubeVertices[9]);
            glVertex3fv(basicCubeVertices[10]);
            glVertex3fv(basicCubeVertices[11]);
        
            // right face
            glVertex3fv(basicCubeVertices[12]);
            glVertex3fv(basicCubeVertices[13]);
            glVertex3fv(basicCubeVertices[14]);
            glVertex3fv(basicCubeVertices[15]);

            // top face
            glVertex3fv(basicCubeVertices[16]);
            glVertex3fv(basicCubeVertices[17]);
            glVertex3fv(basicCubeVertices[18]);
            glVertex3fv(basicCubeVertices[19]);

            // bottom face
            glVertex3fv(basicCubeVertices[20]);
            glVertex3fv(basicCubeVertices[21]);
            glVertex3fv(basicCubeVertices[22]);
            glVertex3fv(basicCubeVertices[23]);
        }
        glEnd();
    }
    else
    {
        glBegin(GL_QUADS);
        {
            // front face
            glColor4fv(verticesColors[0]);
            glVertex3fv(basicCubeVertices[0]);

            glColor4fv(verticesColors[1]);
            glVertex3fv(basicCubeVertices[1]);

            glColor4fv(verticesColors[2]);
            glVertex3fv(basicCubeVertices[2]);

            glColor4fv(verticesColors[3]);
            glVertex3fv(basicCubeVertices[3]);

            // back face
            glColor4fv(verticesColors[4]);
            glVertex3fv(basicCubeVertices[4]);

            glColor4fv(verticesColors[5]);
            glVertex3fv(basicCubeVertices[5]);

            glColor4fv(verticesColors[6]);
            glVertex3fv(basicCubeVertices[6]);

            glColor4fv(verticesColors[7]);
            glVertex3fv(basicCubeVertices[7]);
        
            // left face
            glColor4fv(verticesColors[8]);
            glVertex3fv(basicCubeVertices[8]);

            glColor4fv(verticesColors[9]);
            glVertex3fv(basicCubeVertices[9]);

            glColor4fv(verticesColors[10]);
            glVertex3fv(basicCubeVertices[10]);

            glColor4fv(verticesColors[11]);
            glVertex3fv(basicCubeVertices[11]);
        
            // right face
            glColor4fv(verticesColors[12]);
            glVertex3fv(basicCubeVertices[12]);

            glColor4fv(verticesColors[13]);
            glVertex3fv(basicCubeVertices[13]);

            glColor4fv(verticesColors[14]);
            glVertex3fv(basicCubeVertices[14]);

            glColor4fv(verticesColors[15]);
            glVertex3fv(basicCubeVertices[15]);

            // top face
            glColor4fv(verticesColors[16]);
            glVertex3fv(basicCubeVertices[16]);

            glColor4fv(verticesColors[17]);
            glVertex3fv(basicCubeVertices[17]);

            glColor4fv(verticesColors[18]);
            glVertex3fv(basicCubeVertices[18]);

            glColor4fv(verticesColors[19]);
            glVertex3fv(basicCubeVertices[19]);

            // bottom face
            glColor4fv(verticesColors[20]);
            glVertex3fv(basicCubeVertices[20]);

            glColor4fv(verticesColors[21]);
            glVertex3fv(basicCubeVertices[21]);

            glColor4fv(verticesColors[22]);
            glVertex3fv(basicCubeVertices[22]);

            glColor4fv(verticesColors[23]);
            glVertex3fv(basicCubeVertices[23]);
        }
        glEnd();
    }
}

void drawBasicCubeEx(GLfloat* verticesColors[24], ULONG face)
{
	// local variables
	GLfloat basicCubeVertices[24][3] = { {1.0f, 1.0f, 1.0f},     // front face vertices
										{-1.0f, 1.0f, 1.0f},
										{-1.0f, -1.0f, 1.0f},
										{1.0f, -1.0f, 1.0f},

										{-1.0f, 1.0f, -1.0f},   // back face vertices
										{1.0f, 1.0f, -1.0f},
										{1.0f, -1.0f, -1.0f},
										{-1.0f, -1.0f, -1.0f},

										{-1.0f, 1.0f, 1.0f},    // left face vertices
										{-1.0f, 1.0f, -1.0f},
										{-1.0f, -1.0f, -1.0f},
										{-1.0f, -1.0f, 1.0f},

										{1.0f, 1.0f, -1.0f},    // right face vertices
										{1.0f, 1.0f, 1.0f},
										{1.0f, -1.0f, 1.0f},
										{1.0f, -1.0f, -1.0f},

										{1.0f, 1.0f, -1.0f},    // top face vertices
										{-1.0f, 1.0f, -1.0f},
										{-1.0f, 1.0f, 1.0f},
										{1.0f, 1.0f, 1.0f},

										{1.0f, -1.0f, 1.0f}, // bottom face vertices
										{-1.0f, -1.0f, 1.0f},
										{-1.0f, -1.0f, -1.0f},
										{1.0f, -1.0f, -1.0f} };

	// code
	if (verticesColors == NULL)
	{
		glBegin(GL_QUADS);
		{
			glColor4f(1.0f, 1.0f, 1.0f, 1.0f); // default color if empty array is passed

			if (face & FRONT_FACE) {
				// front face
				glVertex3fv(basicCubeVertices[0]);
				glVertex3fv(basicCubeVertices[1]);
				glVertex3fv(basicCubeVertices[2]);
				glVertex3fv(basicCubeVertices[3]);
			}

			if (face & BACK_FACE) {
				// back face
				glVertex3fv(basicCubeVertices[4]);
				glVertex3fv(basicCubeVertices[5]);
				glVertex3fv(basicCubeVertices[6]);
				glVertex3fv(basicCubeVertices[7]);
			}

			if (face & LEFT_FACE) {
				// left face
				glVertex3fv(basicCubeVertices[8]);
				glVertex3fv(basicCubeVertices[9]);
				glVertex3fv(basicCubeVertices[10]);
				glVertex3fv(basicCubeVertices[11]);
			}
			
			if (face & RIGHT_FACE) {
				// right face
				glVertex3fv(basicCubeVertices[12]);
				glVertex3fv(basicCubeVertices[13]);
				glVertex3fv(basicCubeVertices[14]);
				glVertex3fv(basicCubeVertices[15]);
			}

			if (face & TOP_FACE) {
				// top face
				glVertex3fv(basicCubeVertices[16]);
				glVertex3fv(basicCubeVertices[17]);
				glVertex3fv(basicCubeVertices[18]);
				glVertex3fv(basicCubeVertices[19]);
			}

			if (face & BOTTOM_FACE) {
				// bottom face
				glVertex3fv(basicCubeVertices[20]);
				glVertex3fv(basicCubeVertices[21]);
				glVertex3fv(basicCubeVertices[22]);
				glVertex3fv(basicCubeVertices[23]);
			}
		}
		glEnd();
	}
	else
	{
		glBegin(GL_QUADS);
		{
			if (face & FRONT_FACE) {
				// front face
				glColor4fv(verticesColors[0]);
				glVertex3fv(basicCubeVertices[0]);

				glColor4fv(verticesColors[1]);
				glVertex3fv(basicCubeVertices[1]);

				glColor4fv(verticesColors[2]);
				glVertex3fv(basicCubeVertices[2]);

				glColor4fv(verticesColors[3]);
				glVertex3fv(basicCubeVertices[3]);
			}

			if (face & BACK_FACE) {
				// back face
				glColor4fv(verticesColors[4]);
				glVertex3fv(basicCubeVertices[4]);

				glColor4fv(verticesColors[5]);
				glVertex3fv(basicCubeVertices[5]);

				glColor4fv(verticesColors[6]);
				glVertex3fv(basicCubeVertices[6]);

				glColor4fv(verticesColors[7]);
				glVertex3fv(basicCubeVertices[7]);
			}

			if (face & LEFT_FACE) {
				// left face
				glColor4fv(verticesColors[8]);
				glVertex3fv(basicCubeVertices[8]);

				glColor4fv(verticesColors[9]);
				glVertex3fv(basicCubeVertices[9]);

				glColor4fv(verticesColors[10]);
				glVertex3fv(basicCubeVertices[10]);

				glColor4fv(verticesColors[11]);
				glVertex3fv(basicCubeVertices[11]);
			}

			if (face & RIGHT_FACE) {
				// right face
				glColor4fv(verticesColors[12]);
				glVertex3fv(basicCubeVertices[12]);

				glColor4fv(verticesColors[13]);
				glVertex3fv(basicCubeVertices[13]);

				glColor4fv(verticesColors[14]);
				glVertex3fv(basicCubeVertices[14]);

				glColor4fv(verticesColors[15]);
				glVertex3fv(basicCubeVertices[15]);
			}

			if (face & TOP_FACE) {
				// top face
				glColor4fv(verticesColors[16]);
				glVertex3fv(basicCubeVertices[16]);

				glColor4fv(verticesColors[17]);
				glVertex3fv(basicCubeVertices[17]);

				glColor4fv(verticesColors[18]);
				glVertex3fv(basicCubeVertices[18]);

				glColor4fv(verticesColors[19]);
				glVertex3fv(basicCubeVertices[19]);
			}

			if (face & BOTTOM_FACE) {
				// bottom face
				glColor4fv(verticesColors[20]);
				glVertex3fv(basicCubeVertices[20]);

				glColor4fv(verticesColors[21]);
				glVertex3fv(basicCubeVertices[21]);

				glColor4fv(verticesColors[22]);
				glVertex3fv(basicCubeVertices[22]);

				glColor4fv(verticesColors[23]);
				glVertex3fv(basicCubeVertices[23]);
			}
		}
		glEnd();
	}
}

void draw_text_wgl(HDC hdc, UINT iStringLength, const char *str, int iIdentifier, BOOL bUninitialize)
{
    // local variables
    static BOOL bInitialize = FALSE;
    static HFONT hFont = NULL;
    static HFONT hFontOriginal = NULL;

    // code
    if(!bInitialize)
    {
        // create new font
        hFont = CreateFontW(32,                   // cHeight
                            0,                    // cWidth
                            0,                    // cEscapement
                            0,                    // cOrientation
                            FW_DONTCARE,          // cWeight
                            FALSE,                // bItalic
                            FALSE,                // bUnderline 
                            FALSE,                // bStrikeOut
                            DEFAULT_CHARSET,      // iCharSet
                            OUT_DEFAULT_PRECIS,   // iOutPrecision
                            CLIP_DEFAULT_PRECIS,  // iClipPrecision
                            CLEARTYPE_QUALITY,    // iQuality
                            VARIABLE_PITCH,       // iPitchAndFamily
                            L"Segoe UI Light");            // pszFaceName 


        hFontOriginal = (HFONT)SelectObject(hdc, hFont);

        // create outlines for the DC currently selected font's first 256 glyphs
        wglUseFontOutlinesW(hdc, 0, 255, iIdentifier, 0.0f, 0.1f, WGL_FONT_POLYGONS, NULL);

        // set up for a string-drawing display list call
        glListBase(iIdentifier);
    
        // draw text by calling the display lists
        if(str != NULL)
            glCallLists(iStringLength, GL_UNSIGNED_BYTE, (void*)str);

        bInitialize = TRUE;
    }
    else
    {
        //SelectObject(hdc, hFont);
        glListBase(iIdentifier);

        if(str != NULL)
            glCallLists(iStringLength, GL_UNSIGNED_BYTE, (void*)str);
    }

    if(bUninitialize)
    {
        glDeleteLists(iIdentifier, 256);

        if(hFont)
        {
            SelectObject(hdc, hFontOriginal);
            DeleteObject(hFont);
            hFont = NULL;
        }
    }
}

Point* getSphereVertices(GLuint lats, GLuint longs)
{	
    // local variables
    Point *vertices = NULL;
	GLint iElement = 0;
	GLfloat latIncrement =  ((M_PI / 2.0f) + (M_PI / 2.0f)) / lats;
	GLfloat longIncrement = (M_PI + M_PI) / longs; 
	size_t size = (((lats+1) * (longs+1)) * sizeof(Point)) * 4;

    GLfloat x, y, z, s, t;
	GLfloat radius = 1.0f;

	// GLfloat latsInRad = getInterpolatedValue(0.0f, lats, -(M_PI / 2.0f), (M_PI / 2.0f), lats);
	// GLfloat longsInRad = getInterpolatedValue(0.0f, longs, -M_PI, M_PI, longs);

    // code
    if(vertices == NULL)
    {
		vertices = (Point *)malloc(size);                 
    }

    for(GLint i = 0; i < lats; i++)
    {
        GLfloat lat0 = getInterpolatedValue(0.0f, lats - 1.0f, -(M_PI / 2.0f), (M_PI / 2.0f), i);
		GLfloat lat1 = getInterpolatedValue(0.0f, lats - 1.0f, -(M_PI / 2.0f), (M_PI / 2.0f), i + 1);
        for(GLint j = 0; j < longs; j++)
        {
            GLfloat lon0 = getInterpolatedValue(0.0f, lats - 1.0f, -M_PI, M_PI, j);
			GLfloat lon1 = getInterpolatedValue(0.0f, lats - 1.0f, -M_PI, M_PI, j + 1);

			// p1
			x = radius * sinf(lon0) * cosf(lat0);
			y = radius * sinf(lon0) * sinf(lat0);
			z = radius * cosf(lon0);
			s = 0.5f + (atan2f(z, x) / (2.0f * M_PI));
			t = 0.5f + (asinf(y) / M_PI);

			vertices[iElement] = {x, y, z, s, t};
			iElement++;

			// p2
			x = radius * sinf(lon0) * cosf(lat1);
			y = radius * sinf(lon0) * sinf(lat1);
			z = radius * cosf(lon0);
			s = 0.5f + (atan2f(z, x) / (2.0f * M_PI));
			t = 0.5f + (asinf(y) / M_PI);

			vertices[iElement] = {x, y, z, s, t};
			iElement++;

			// p3
			x = radius * sinf(lon1) * cosf(lat1);
			y = radius * sinf(lon1) * sinf(lat1);
			z = radius * cosf(lon1);
			s = 0.5f + (atan2f(z, x) / (2.0f * M_PI));
			t = 0.5f + (asinf(y) / M_PI);
		
			vertices[iElement] = {x, y, z, s, t};
			iElement++;

			// p4
			x = radius * sinf(lon1) * cosf(lat0);
			y = radius * sinf(lon1) * sinf(lat0);
			z = radius * cosf(lon1);
			s = 0.5f + (atan2f(z, x) / (2.0f * M_PI));
			t = 0.5f + (asinf(y) / M_PI);

			vertices[iElement] = {x, y, z, s, t};
			iElement++;
		
        }
    }

    return(vertices);
}

void drawSphereVertices(Point *vertices, GLuint lats, GLuint longs, GLfloat color[4], GLfloat colorGradIncrement, GLenum mode, GLfloat radius)
{
	// local variables
	GLint iElement = 0;

    // code
    for(int i = 0; i < (lats * longs); i++)
    {
        glColor4fv(color);
		glBegin(mode);
		{
			glVertex3f(radius * vertices[iElement].x, radius * vertices[iElement].y, radius * vertices[iElement].z);
			glVertex3f(radius * vertices[iElement+1].x, radius * vertices[iElement+1].y, radius * vertices[iElement+1].z);
			glVertex3f(radius * vertices[iElement+2].x, radius * vertices[iElement+2].y, radius * vertices[iElement+2].z);
			glVertex3f(radius * vertices[iElement+3].x, radius * vertices[iElement+3].y, radius * vertices[iElement+3].z);

			iElement = iElement + 4;
		}
		glEnd();
	
        if(i < ((lats * longs) / 2))
        {
            color[0] = color[0] + colorGradIncrement; // 0.00531f
            color[1] = color[1] + colorGradIncrement;
            color[2] = color[2] + colorGradIncrement;
        }
        else
        {
            color[0] = color[0] - colorGradIncrement; // 0.00531f
            color[1] = color[1] - colorGradIncrement;
            color[2] = color[2] - colorGradIncrement;
        }
    }
}

void drawSphereVerticesWithTexture(Point *vertices, GLuint lats, GLuint longs, GLuint texture, GLenum mode, GLfloat radius)
{
	// local variables
	GLint iElement = 0;

    // code
	glBindTexture(GL_TEXTURE_2D, texture);
	glBegin(mode);
	{
		for(int i = 0; i < (lats * longs); i++)
		{
			glColor4f(1.0f, 1.0f, 1.0f, 1.0f);

			glTexCoord2f(vertices[iElement].s, vertices[iElement].t);
			glVertex3f(radius * vertices[iElement].x, radius * vertices[iElement].y, radius * vertices[iElement].z);

			glTexCoord2f(vertices[iElement+1].s, vertices[iElement+1].t);
			glVertex3f(radius * vertices[iElement+1].x, radius * vertices[iElement+1].y, radius * vertices[iElement+1].z);

			glTexCoord2f(vertices[iElement+2].s, vertices[iElement+2].t);
			glVertex3f(radius * vertices[iElement+2].x, radius * vertices[iElement+2].y, radius * vertices[iElement+2].z);

			glTexCoord2f(vertices[iElement+3].s, vertices[iElement+3].t);
			glVertex3f(radius * vertices[iElement+3].x, radius * vertices[iElement+3].y, radius * vertices[iElement+3].z);
			
			iElement = iElement + 4;
		}
	}
	glEnd();
	glBindTexture(GL_TEXTURE_2D, 0);  
}

void drawSphereVerticesWithTextureSpecial(Point *vertices, GLuint lats, GLuint longs, GLuint texture, GLenum mode, GLfloat radius)
{
	// local variables
	GLint iElement = 0;

    // code
	glBindTexture(GL_TEXTURE_2D, texture);
	glBegin(mode);
	{
		for(int i = 0; i < (lats * longs); i++)
		{
			glTexCoord2f(vertices[iElement].s, vertices[iElement].t);
			glVertex3f(radius * vertices[iElement].x, radius * vertices[iElement].y, radius * vertices[iElement].z);

			glTexCoord2f(vertices[iElement+1].s, vertices[iElement+1].t);
			glVertex3f(radius * vertices[iElement+1].x, radius * vertices[iElement+1].y, radius * vertices[iElement+1].z);

			glTexCoord2f(vertices[iElement+2].s, vertices[iElement+2].t);
			glVertex3f(radius * vertices[iElement+2].x, radius * vertices[iElement+2].y, radius * vertices[iElement+2].z);

			glTexCoord2f(vertices[iElement+3].s, vertices[iElement+3].t);
			glVertex3f(radius * vertices[iElement+3].x, radius * vertices[iElement+3].y, radius * vertices[iElement+3].z);
			
			iElement = iElement + 4;
		}
	}
	glEnd();
	glBindTexture(GL_TEXTURE_2D, 0);  
}

Point* getCircleVertices(GLfloat radius)
{
	// local variables
	int i = 0;
	Point *pCircleVertices = NULL;

	// code
	pCircleVertices = (Point *)malloc(sizeof(Point) * 361);

	for(GLfloat angleInDegrees = 0.0f; angleInDegrees <= 360.0f; angleInDegrees = angleInDegrees + 1.0f)
    {
        GLfloat angleInRadians = angleInDegrees * (M_PI / 180.0f);
        GLfloat x = radius * cosf(angleInRadians);
        GLfloat y = radius * sinf(angleInRadians);

		pCircleVertices[i].x = x;
		pCircleVertices[i].y = y;
		pCircleVertices[i].z = 0.0f;

		i++;
    }

	return(pCircleVertices);
}

Vec3* getCircleVerticesXZ(GLfloat radius, GLfloat increment, BOOL bCounterClockwise)
{
	// local variables
	int i = 0;
	
	Vec3 *pCircleVertices = NULL;
	size_t size = sizeof(Vec3) * (size_t)ceilf(((360.0f / increment) + 1));

	GLfloat startAngle = (bCounterClockwise) ? 360.0f : 0.0f;
	GLfloat endAngle = (bCounterClockwise) ? 0.0f : 360.0f;

	// code
	// malloc size for vertices
	pCircleVertices = (Vec3*)malloc(size);

	// calculate the vertices
	if(bCounterClockwise)
	{
		for(GLfloat angleInDegrees = startAngle; angleInDegrees >= endAngle; angleInDegrees = angleInDegrees - increment)
		{
			GLfloat angleInRadians = angleInDegrees * (M_PI / 180.0f);
			GLfloat x = radius * cosf(angleInRadians);
			GLfloat z = radius * sinf(angleInRadians);

			pCircleVertices[i].x = x;
			pCircleVertices[i].y = 0.0f;
			pCircleVertices[i].z = z;

			i++;
		}
	}
	else
	{
		for(GLfloat angleInDegrees = startAngle; angleInDegrees <= endAngle; angleInDegrees = angleInDegrees + increment)
		{
			GLfloat angleInRadians = angleInDegrees * (M_PI / 180.0f);
			GLfloat x = radius * cosf(angleInRadians);
			GLfloat z = radius * sinf(angleInRadians);

			pCircleVertices[i].x = x;
			pCircleVertices[i].y = 0.0f;
			pCircleVertices[i].z = z;

			i++;
		}	
	}

	return(pCircleVertices);
}

void drawCircleVertices(Point *circleVertices, GLenum mode)
{
	// code
	glBegin(mode);
	{
		for(int i = 0; i <= 360; i++)
		{
			glVertex3f(circleVertices[i].x, circleVertices[i].y, circleVertices[i].z);
		}
	}
	glEnd();

}

void drawQuadVertices(Point *vertices)
{
	// code
	glBegin(GL_QUADS);
	{	
		for(int i = 0; i < 4; i++)
		{
			glVertex3f(vertices[0].x, vertices[0].y, vertices[0].z);
			glVertex3f(vertices[1].x, vertices[1].y, vertices[1].z);
			glVertex3f(vertices[2].x, vertices[2].y, vertices[2].z);
			glVertex3f(vertices[3].x, vertices[3].y, vertices[3].z);
		}
	}
	glEnd();
}

void freeVertices(Point **vertices)
{
	// code
	if(*vertices)
	{
		free(*vertices);
		*vertices = NULL;
	}
}

void fadeOutEffect(GLfloat alpha)
{
    // code
    glPushMatrix();
    {
        glLoadIdentity();
		glTranslatef(0.0f, 0.0f, -0.15f);
        glColor4f(0.0f, 0.0f, 0.0f, alpha);
        glBegin(GL_QUADS);
        {
            glVertex3f(1.0f, 1.0f, 0.0f);
            glVertex3f(-1.0f, 1.0f, 0.0f);
            glVertex3f(-1.0f, -1.0f, 0.0f);
            glVertex3f(1.0f, -1.0f, 0.0f);
        }
        glEnd();
    }
    glPopMatrix();
}

void fadeInEffect(GLfloat alpha)
{
    // code
    glPushMatrix();
    {
        glLoadIdentity();
		glTranslatef(0.0f, 0.0f, -0.15f);
        glColor4f(0.0f, 0.0f, 0.0f, alpha);
        glBegin(GL_QUADS);
        {
            glVertex3f(1.0f, 1.0f, 0.0f);
            glVertex3f(-1.0f, 1.0f, 0.0f);
            glVertex3f(-1.0f, -1.0f, 0.0f);
            glVertex3f(1.0f, -1.0f, 0.0f);
        }
        glEnd();
    }
    glPopMatrix();
}

void fadeOutEffect_v2(Color color)
{
	// code
	glPushMatrix();
	{
		glLoadIdentity();
		glTranslatef(0.0f, 0.0f, -0.15f);
		glColor4f(color.r, color.g, color.b, color.alpha);
		glBegin(GL_QUADS);
		{
			glVertex3f(1.0f, 1.0f, 0.0f);
			glVertex3f(-1.0f, 1.0f, 0.0f);
			glVertex3f(-1.0f, -1.0f, 0.0f);
			glVertex3f(1.0f, -1.0f, 0.0f);
		}
		glEnd();
	}
	glPopMatrix();
}

void fadeInEffect_v2(Color color)
{
	// code
	glPushMatrix();
	{
		glLoadIdentity();
		glTranslatef(0.0f, 0.0f, -0.15f);
		glColor4f(color.r, color.g, color.b, color.alpha);
		glBegin(GL_QUADS);
		{
			glVertex3f(1.0f, 1.0f, 0.0f);
			glVertex3f(-1.0f, 1.0f, 0.0f);
			glVertex3f(-1.0f, -1.0f, 0.0f);
			glVertex3f(1.0f, -1.0f, 0.0f);
		}
		glEnd();
	}
	glPopMatrix();
}

BOOL updateAlpha(GLfloat *alpha, GLfloat speed, UINT dwFlag)
{
	// code
	switch(dwFlag)
	{
	case FADE_IN_FLAG:
		if(*alpha > 0.0f)
		{
			*alpha = *alpha - speed;
		}
		else
		{
			*alpha = 0.0f;
			return(TRUE);
		}
		break;

	case FADE_OUT_FLAG:
		if(*alpha < 1.0f)
		{
			*alpha = *alpha + speed;
		}
		else
		{
			*alpha = 1.0f;
			return(TRUE);
		}
		break;
		
	default:
		break;
	}

	return(FALSE);
}

BOOL updateObjectAlpha(GLfloat *alpha, GLfloat speed, UINT dwFlag)
{
	// code
	switch(dwFlag)
	{
	case FADE_OUT_FLAG:
		if(*alpha > 0.0f)
		{
			*alpha = *alpha - speed;
		}
		else
		{
			*alpha = 0.0f;
			return(TRUE);
		}
		break;

	case FADE_IN_FLAG:
		if(*alpha < 1.0f)
		{
			*alpha = *alpha + speed;
		}
		else
		{
			*alpha = 1.0f;
			return(TRUE);
		}
		break;
		
	default:
		break;
	}

	return(FALSE);
}

void updateRotationValue(GLfloat *val, GLfloat speed)
{
	// code
	*val = *val + speed;
	if(*val >= 360.0f)
	{
		*val = *val - 360.0f;
	}
}

void updateRevolutionValue(GLfloat *val, GLfloat speed, Translation *translation, GLfloat orbitRadius, GLuint uFlag)
{
	// local variables
	GLfloat angleInRadians;

	// code
	switch(uFlag)
	{
	case REVOLUTION_INCREMENT_ANGLE:
		*val = *val + speed;
		break;
	case REVOLUTION_DECREMENT_ANGLE:
		*val = *val - speed;
		break;
	}

	angleInRadians = (*val) * (M_PI / 180.0f);

	translation->x = orbitRadius * cosf(angleInRadians);
	translation->z = orbitRadius * sinf(angleInRadians);
}

void lerpMyValue(Lerp_t *lerp_val, GLfloat condition)
{
	// function prototypes
	GLfloat getInterpolatedValue(GLfloat x0, GLfloat x1, GLfloat y0, GLfloat y1, GLfloat x);

	// code
	// increment tVal by speed
	lerp_val->tVal = lerp_val->tVal + lerp_val->speed;
	if(lerp_val->tVal > condition)
	{
		lerp_val->tVal = condition;
		lerp_val->bReached = TRUE;
	}
	else
	{
		lerp_val->bReached = FALSE;
	}

	// get interpolated value
	lerp_val->x = getInterpolatedValue(0.0f, 1.0f, lerp_val->x0, lerp_val->x1, lerp_val->tVal);
}

void drawGradedText(const char *str, Color color, Color gradientColor, DWORD dwOffset)
{
	// code
	glPushMatrix();
	{
		glScalef(1.0f, 1.0f, 0.0025f);
		glColor4f(color.r, color.g, color.b, color.alpha);
		RT_DrawText(str, strlen(str), dwOffset);
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(0.0f, 0.0f, -0.0015f);
		glColor4f(gradientColor.r, gradientColor.g, gradientColor.b, gradientColor.alpha);
		RT_DrawText(str, strlen(str), dwOffset);
	}
	glPopMatrix();
}

// wrapper functions for OpenAL library's library
BOOL AttachWaveFileToALBuffer(DWORD dwResourceID, DWORD dwResourceType, ALuint buffer)
{
    // code
	// load resource and attach it to buffer
    HRSRC hWavHandle = FindResourceW(GetModuleHandle(NULL), MAKEINTRESOURCEW(dwResourceID), MAKEINTRESOURCEW(dwResourceType));
    HGLOBAL hGlobalMusicHandle = LoadResource(GetModuleHandle(NULL), hWavHandle);
    
    ALvoid *pointerToWaveFileData = (ALvoid*)LockResource(hGlobalMusicHandle);
    ALsizei waveFileDataSize = SizeofResource(GetModuleHandle(NULL), hWavHandle);

    if(attachALWaveFileToBuffer(buffer, AL_FORMAT_STEREO16, pointerToWaveFileData, waveFileDataSize, 44100) == FALSE)
    {
        return(FALSE);
    }

    return(TRUE);
}

BOOL AttachWaveFileToALBuffer_v2(DWORD dwResourceID, DWORD dwResourceType, ALuint buffer, ALuint wavFileFreqHz)
{
    // code
	// load resource and attach it to buffer
    HRSRC hWavHandle = FindResourceW(GetModuleHandle(NULL), MAKEINTRESOURCEW(dwResourceID), MAKEINTRESOURCEW(dwResourceType));
    HGLOBAL hGlobalMusicHandle = LoadResource(GetModuleHandle(NULL), hWavHandle);
    
    ALvoid *pointerToWaveFileData = (ALvoid*)LockResource(hGlobalMusicHandle);
    ALsizei waveFileDataSize = SizeofResource(GetModuleHandle(NULL), hWavHandle);

    if(attachALWaveFileToBuffer(buffer, AL_FORMAT_STEREO16, pointerToWaveFileData, waveFileDataSize, wavFileFreqHz) == FALSE)
    {
        return(FALSE);
    }

    return(TRUE);
}

/*void RegisterALSourceForPlayback(ALuint source, BOOL bPaused, BOOL bMuted)
{
	// local variables
	ALSourceNode *pNewNode = NULL;
	ALSourceNode *pTraveler = NULL;

	// code
	// malloc() new node
	pNewNode = (ALSourceNode*)malloc(sizeof(ALSourceNode) * 1);

	if(pNewNode == NULL)
	{
		fprintf(gpFILE, "RegisterSourceForPlayback() : malloc() failed.\n");
		DestroyWindow(ghwnd);
	}

	// assign values to it
	pNewNode->sourceID = source;
	pNewNode->IsSourcePaused = bPaused;
	pNewNode->IsSourceMuted = bMuted;
	pNewNode->pForward = NULL;

	// if engine doesn't exist, new node is the engine.
	if(pALSourceEngine == NULL)
	{
		pALSourceEngine = pNewNode;
	}
	else // if engine exists, traverse the engine until pForward is found NULL, then assign the new node to that pForward
	{
		pTraveler = pALSourceEngine;
		while(pTraveler->pForward != NULL)
		{
			pTraveler = pTraveler->pForward;
		}
		pTraveler->pForward = pNewNode;
	}

	fflush(gpFILE);
}

BOOL UnregisterALSourceForPlayback(ALuint source)
{
	// local variables
	ALSourceNode *pBackNode = NULL; 
	ALSourceNode *pTraveler = pALSourceEngine;

	// code
	// traverse the engine ONLY if source is not found and pForward isn't NULL.
	while((pTraveler->sourceID != source) && (pTraveler->pForward != NULL))
	{
		pBackNode = pTraveler;
		pTraveler = pTraveler->pForward;
	}

	// if source is found, delete that node
	if(pTraveler->sourceID == source)
	{
		if(pBackNode != NULL)
			pBackNode->pForward = pTraveler->pForward; // unlink back node from this node
		
		// free this node
		free(pTraveler);
		pTraveler = NULL;

		return(TRUE);
	}
	else
	{
		return(FALSE);
	}

	return(FALSE);
}*/

void SetCurrentSourceForStats(ALuint source, BOOL bPaused, BOOL bMuted)
{
	// code
	g_ActiveALSource.sourceID = source;
	g_ActiveALSource.IsSourcePaused = bPaused;
	g_ActiveALSource.IsSourceMuted = bMuted;
}

void ResetGlobalTimer(void)
{
	// code
	stopTimer(stopWatch);

	// resetTimer(stopWatch);
	stopWatch->startTime.QuadPart = 0;
	stopWatch->endTime.QuadPart = 0;
	stopWatch->totalElapsedTime = 0.0;
	totalElapsedTime = 0.0;

	startTimer(stopWatch);
}

#ifdef DEBUG

#define RED_COLOR 1.0f, 0.0f, 0.0f, 1.0f
#define GREEN_COLOR 0.0f, 1.0f, 0.0f, 1.0f
#define BLUE_COLOR 0.0f, 0.0f, 1.0f, 1.0f

#define THE_MATRIX_GREEN_COLOR 0.000000f, 1.000000f, 0.254902f, 1.0f

	// global variables
	extern HDC ghdc;
	extern GLfloat step;
	//extern struct Transformations g_Transformations;
	extern int g_iCurrentTransformation;
	extern enum TransformationMode g_CurrentTransformationMode;

	void debugText(BOOL bDebugMode, BOOL bUninitialize)
	{
		// local variables
		static BOOL bInitialize = FALSE;
		static HFONT hFont = NULL;
		char currentStepValue[255];
		char selectedScene[255];
		char selectedTransformationMode[255];
		char selectedTransformationObject[255];
		char currTranslation[255];
		char currRotation[255];
		char currScale[255];
		char totalElapsedTimeInSeconds[255];

        int iDebugTextDisplayList = 20001;

		// code
		// initialize
		if(!bInitialize)
		{
			// create new font
			hFont = CreateFontW(32,                   // cHeight
								0,                    // cWidth
								0,                    // cEscapement
								0,                    // cOrientation
								FW_DONTCARE,          // cWeight
								FALSE,                // bItalic
								FALSE,                // bUnderline 
								FALSE,                // bStrikeOut
								DEFAULT_CHARSET,      // iCharSet
								OUT_DEFAULT_PRECIS,   // iOutPrecision
								CLIP_DEFAULT_PRECIS,  // iClipPrecision
								CLEARTYPE_QUALITY,    // iQuality
								VARIABLE_PITCH,       // iPitchAndFamily
								L"Arial");            // pszFaceName 

			// select newly created font into the DC
			SelectObject(ghdc, hFont);

			// create glyphs of the currently selected font and set display lists offset
			wglUseFontBitmapsW(ghdc, 0, 256, iDebugTextDisplayList);

			glListBase(iDebugTextDisplayList);

			bInitialize = TRUE;
		}

		// uninitialize
		if(bUninitialize)
		{
			if(hFont)
			{
				DeleteObject(hFont);
				hFont = NULL;
			}

			glDeleteLists(iDebugTextDisplayList, 256);
			return ;
		}

		// fill up strings with real-time collected data
		{
			switch(g_CurrentTransformationMode)
			{
				case TRANSLATION:
					sprintf(selectedTransformationMode, "Selected transformation mode = TRANSLATION");
					break;
				case ROTATION:
					sprintf(selectedTransformationMode, "Selected transformation mode = ROTATION");
					break;
				case SCALE:
					sprintf(selectedTransformationMode, "Selected transformation mode = SCALE");
					break;
				default:
					break;
			}

            sprintf(selectedTransformationObject, "Selected object for transformation = g_Transformations[%d] (OBJECT %d)", g_iCurrentTransformation, g_iCurrentTransformation);
		
			sprintf(currTranslation, "TRANSLATION = %f, %f, %f", 
					g_Transformations[g_iCurrentTransformation].translation.x,
					g_Transformations[g_iCurrentTransformation].translation.y,
					g_Transformations[g_iCurrentTransformation].translation.z);

			sprintf(currRotation, "ROTATION = %f, %f, %f", 
					g_Transformations[g_iCurrentTransformation].rotation.x,
					g_Transformations[g_iCurrentTransformation].rotation.y,
					g_Transformations[g_iCurrentTransformation].rotation.z);
			
			sprintf(currScale, "SCALE = %f, %f, %f", 
					g_Transformations[g_iCurrentTransformation].scale.x,
					g_Transformations[g_iCurrentTransformation].scale.y,
					g_Transformations[g_iCurrentTransformation].scale.z);
		}

		// print current step value
		sprintf(currentStepValue, "STEP VALUE = %f", step);

		// print current elapsed time
		sprintf(totalElapsedTimeInSeconds, "%.4llf", totalElapsedTime);

		// render debug text
		if(bDebugMode)
		{
			// current step value
			glPushMatrix();
			{
				glColor4f(RED_COLOR);

				glRasterPos3f(-0.072000f, -0.025000f, -0.1f);

				glListBase(iDebugTextDisplayList);
				glCallLists(strlen(currentStepValue), GL_UNSIGNED_BYTE, currentStepValue);
			}
			glPopMatrix();

			// selected transformation mode
			glPushMatrix();
			{
				glColor4f(GREEN_COLOR);

				glRasterPos3f(-0.072000f, -0.035000f, -0.1f);

				glListBase(iDebugTextDisplayList);
				glCallLists(strlen(selectedTransformationMode), GL_UNSIGNED_BYTE, selectedTransformationMode);
			}
			glPopMatrix();

			// selected transformation object
			glPushMatrix();
			{
				glColor4f(BLUE_COLOR);

				glRasterPos3f(-0.072000f, -0.030000f, -0.1f);

				glListBase(iDebugTextDisplayList);
				glCallLists(strlen(selectedTransformationObject), GL_UNSIGNED_BYTE, selectedTransformationObject);
			}
			glPopMatrix();

			// current translation, rotation and scale values
			// translation
			glPushMatrix();
			{
				glColor4f(THE_MATRIX_GREEN_COLOR);

				glRasterPos3f(-0.072000f, 0.036500f, -0.1f);

				glListBase(iDebugTextDisplayList);
				glCallLists(strlen(currTranslation), GL_UNSIGNED_BYTE, currTranslation);
			}
			glPopMatrix();

			// rotation
			glPushMatrix();
			{
				glColor4f(THE_MATRIX_GREEN_COLOR);

				glRasterPos3f(-0.072000f, 0.031000f, -0.1f);

				glListBase(iDebugTextDisplayList);
				glCallLists(strlen(currRotation), GL_UNSIGNED_BYTE, currRotation);
			}
			glPopMatrix();

			// scale
			glPushMatrix();
			{
				glColor4f(THE_MATRIX_GREEN_COLOR);

				glRasterPos3f(-0.072000f, 0.025500f, -0.1f);

				glListBase(iDebugTextDisplayList);
				glCallLists(strlen(currScale), GL_UNSIGNED_BYTE, currScale);
			}
			glPopMatrix();

			// total elapsed time
			glPushMatrix();
			{
				glColor4f(THE_MATRIX_GREEN_COLOR);
				glRasterPos3f(0.056000f, -0.035000f, -0.1f);
				glListBase(iDebugTextDisplayList);
				glCallLists(strlen(totalElapsedTimeInSeconds), GL_UNSIGNED_BYTE, totalElapsedTimeInSeconds);
			}
			glPopMatrix();
		}
	}

	
#define SOUND_STATS_DISPLAY_LISTS_OFFSET 25199999

    void soundStats(BOOL bPlaying, BOOL bMuted, BOOL bUninitialize)
    {
        // local variables
        static HFONT hFont = NULL;
        static BOOL bInitialized = FALSE;
        char playingOrPaused[255];
        char mutedOrUnmuted[255];
        char playbackPosition[255];

        ALfloat position = 0;

        // initialize 
        if(!bInitialized)
        {
            // create the font
            hFont = CreateFontW(32, 0, 0, 0, FW_DONTCARE, FALSE, FALSE, FALSE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, CLEARTYPE_QUALITY, VARIABLE_PITCH, L"Arial");
            
            // select font into the dc
            SelectObject(ghdc, (HGDIOBJ)hFont);

            // create display lists for font bitmaps
            wglUseFontBitmapsW(ghdc, 0, 256, SOUND_STATS_DISPLAY_LISTS_OFFSET);

            // select the starting display list
            glListBase(SOUND_STATS_DISPLAY_LISTS_OFFSET);

            bInitialized = TRUE;
        }
        
        // uninitialize
        if(bUninitialize)
        {
            if(hFont)
            {
                DeleteObject((HGDIOBJ)hFont);
                hFont = NULL;
            }

            glDeleteLists(SOUND_STATS_DISPLAY_LISTS_OFFSET, 256);
            return ;
        }

        // print real-time data (text) on strings 
        sprintf(playingOrPaused, "Sound is %s.", (bPlaying) ? "PLAYING" : "PAUSED");        
        sprintf(mutedOrUnmuted, "Sound is %s.", (bMuted) ? "MUTED" : "UNMUTED");

		if(g_ActiveALSource.sourceID != 0)
		{
        	getALSourcePlaybackPositionInSeconds(g_ActiveALSource.sourceID, &position);
		}
		else
		{
			position = 0;
		}

        ALuint hours = (ALuint)(position / 3600.0f);
        ALuint minutes = (ALuint)((position - (hours * 3600.0f)) / 60.0f);
        ALuint seconds = (ALuint)((position - (hours * 3600.0f) - (minutes * 60.0f)));

        if(minutes < 10 && seconds < 10)
        {
            sprintf(playbackPosition, "0%d:0%d:0%d", hours, minutes, seconds);
        }
        else if(minutes >= 10 && seconds < 10)
        {
            sprintf(playbackPosition, "0%d:%d:0%d", hours, minutes, seconds);
        }
        else if(minutes >= 10 && seconds >= 10)
        {
            sprintf(playbackPosition, "0%d:%d:%d", hours, minutes, seconds);
        }
        else if(minutes < 10 && seconds >= 10)
        {
            sprintf(playbackPosition, "0%d:0%d:%d", hours, minutes, seconds);
        }
		else
		{
			sprintf(playbackPosition, "00:00:00");
		}
        // else if(hours > 10 && minutes < 10 && seconds < 10) // these conditions are IMPOSSIBLE... unless?
        // {
        //     sprintf(playbackPosition, "%d:0%d:0%d", hours, minutes, seconds);
        // }
        // else if(hours > 10 && minutes > 10 && seconds < 10)
        // {
        //     sprintf(playbackPosition, "%d:%d:0%d", hours, minutes, seconds);
        // }
        // else if(hours > 10 && minutes > 10 && seconds > 10)
        // {
        //     sprintf(playbackPosition, "%d:%d:%d", hours, minutes, seconds);
        // }
        // else if(hours > 10 && minutes < 10 && seconds > 10)
        // {
        //     sprintf(playbackPosition, "%d:0%d:%d", hours, minutes, seconds);
        // }
        // else if(hours > 10 && minutes > 10 && seconds > 10)
        // {
        //     sprintf(playbackPosition, "%d:%d:%d", hours, minutes, seconds);
        // }

        // render text
        // PLAYING / PAUSED
        glPushMatrix();
        {
            glColor4f(0.0f, 1.0f, 0.5f, 1.0f); // green + blue color (half cyan?)

            // set the raster position for bitmaps
            glRasterPos3f(-0.026500f, -0.040000f, -0.1f);
            glListBase(SOUND_STATS_DISPLAY_LISTS_OFFSET);
            glCallLists(strlen(playingOrPaused), GL_UNSIGNED_BYTE, playingOrPaused);
        }
        glPopMatrix();
        
        // MUTED / UNMUTED
        glPushMatrix();
        {
            glColor4f(0.0f, 1.0f, 0.5f, 1.0f); // green + blue color (half cyan?)

            // set the raster position for bitmaps
            glRasterPos3f(0.001000f, -0.040000f, -0.1f);
            glListBase(SOUND_STATS_DISPLAY_LISTS_OFFSET);
            glCallLists(strlen(mutedOrUnmuted), GL_UNSIGNED_BYTE, mutedOrUnmuted);
        }
        glPopMatrix();
    
        // current playback position in the format --> "HH:MM:SS"
        glPushMatrix();
        {
            glColor4f(0.0f, 1.0f, 0.5f, 1.0f);

            // set the raster position
            glRasterPos3f(-0.005000f, -0.035000f, -0.1f);
            glListBase(SOUND_STATS_DISPLAY_LISTS_OFFSET);
            glCallLists(strlen(playbackPosition), GL_UNSIGNED_BYTE, playbackPosition);
        }
        glPopMatrix();
    }

#define CAM_STATS_DISPLAY_LISTS_OFFSET 204189241

	void cameraStats(BOOL bUninitialize)
	{
		// local variables
		static BOOL bInitialized = FALSE;
		static HFONT hFont = NULL;
		char camPosition[255];
		char camFront[255];
		char camRight[255];
		char camUp[255];
		char camAngles[255]; 

		// code
		// initialize 
        if(!bInitialized)
        {
            // create the font
            hFont = CreateFontW(32, 0, 0, 0, FW_DONTCARE, FALSE, FALSE, FALSE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, CLEARTYPE_QUALITY, VARIABLE_PITCH, L"Arial");
            
            // select font into the dc
            SelectObject(ghdc, (HGDIOBJ)hFont);

            // create display lists for font bitmaps
            wglUseFontBitmapsW(ghdc, 0, 256, CAM_STATS_DISPLAY_LISTS_OFFSET);

            // select the starting display list
            glListBase(CAM_STATS_DISPLAY_LISTS_OFFSET);

            bInitialized = TRUE;
        }
        
        // uninitialize
        if(bUninitialize)
        {
            if(hFont)
            {
                DeleteObject((HGDIOBJ)hFont);
                hFont = NULL;
            }

            glDeleteLists(CAM_STATS_DISPLAY_LISTS_OFFSET, 256);
            return ;
        }

		// fill strings
		sprintf(camPosition, "CAMERA POSITION = %f, %f, %f", cameraCoordinates.position.x, cameraCoordinates.position.y, cameraCoordinates.position.z);
		sprintf(camFront, "CAMERA FRONT = %f, %f, %f", cameraCoordinates.front.x, cameraCoordinates.front.y, cameraCoordinates.front.z);
		sprintf(camRight, "CAMERA RIGHT = %f, %f, %f", cameraCoordinates.right.x, cameraCoordinates.right.y, cameraCoordinates.right.z);
		sprintf(camUp, "CAMERA UP = %f, %f, %f", cameraCoordinates.up.x, cameraCoordinates.up.y, cameraCoordinates.up.z);
		sprintf(camAngles, "CAMERA ANGLE X = %f, CAMERA ANGLE Y = %f", cameraCoordinates.angleX, cameraCoordinates.angleY);

		// draw text
		// position
		glPushMatrix();
		{
			glColor4f(THE_MATRIX_GREEN_COLOR);

			glRasterPos3f(-0.072000f, 0.016000f, -0.1f);

			glListBase(CAM_STATS_DISPLAY_LISTS_OFFSET);
			glCallLists(strlen(camPosition), GL_UNSIGNED_BYTE, camPosition);
		}
		glPopMatrix();

		// front
		// glPushMatrix();
		// {
		// 	glColor4f(THE_MATRIX_GREEN_COLOR);

		// 	glRasterPos3f(0.004000f, 0.018500f, -0.1f);

		// 	glListBase(CAM_STATS_DISPLAY_LISTS_OFFSET);
		// 	glCallLists(strlen(camFront), GL_UNSIGNED_BYTE, camFront);
		// }
		// glPopMatrix();

		// right
		// glPushMatrix();
		// {
		// 	glColor4f(THE_MATRIX_GREEN_COLOR);

		// 	glRasterPos3f(g_Transformations[2].translation.x, g_Transformations[2].translation.y, -0.1f);

		// 	glListBase(CAM_STATS_DISPLAY_LISTS_OFFSET);
		// 	glCallLists(strlen(camRight), GL_UNSIGNED_BYTE, camRight);
		// }
		// glPopMatrix();

		// up
		glPushMatrix();
		{
			glColor4f(THE_MATRIX_GREEN_COLOR);

			glRasterPos3f(-0.072000f, 0.010000f, -0.1f);

			glListBase(CAM_STATS_DISPLAY_LISTS_OFFSET);
			glCallLists(strlen(camUp), GL_UNSIGNED_BYTE, camUp);
		}
		glPopMatrix();

		// angles
		glPushMatrix();
		{
			glColor4f(THE_MATRIX_GREEN_COLOR);

			glRasterPos3f(-0.072000f, 0.004000f, -0.1f);

			glListBase(CAM_STATS_DISPLAY_LISTS_OFFSET);
			glCallLists(strlen(camAngles), GL_UNSIGNED_BYTE, camAngles);
		}
		glPopMatrix();
	}

#endif
