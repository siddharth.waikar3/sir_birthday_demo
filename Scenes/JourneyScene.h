#pragma once

// function declarations
void initializeSceneTwo(void);
void displaySceneTwo(void);
void updateSceneTwo(void);
void uninitializeSceneTwo(void);
