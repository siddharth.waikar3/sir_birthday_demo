#pragma once

// header files
#include "../Common/Common.h"

// macros
#define CAMERA_TRANSLATION_SPEED 0.5f
#define CAMERA_ROTATION_SPEED 1.0f
#define CAMERA_CENTER_TRANSLATION_SPEED 0.5f

// general purpose
#define CAMERA_POSITIVE_TILT_FLAG 0x00000001
#define CAMERA_NEGATIVE_TILT_FLAG 0x00000002

#define CAMERA_POSITIVE_PAN_FLAG 0x00000001
#define CAMERA_NEGATIVE_PAN_FLAG 0x00000002

// struct definitions
typedef struct tagCameraPosition
{
    GLfloat x, y, z;
} CameraPosition;

typedef struct tagCameraFront
{
    GLfloat x, y, z;
} CameraFront;

typedef struct tagCameraRight
{
    GLfloat x, y, z;
} CameraRight;

typedef struct tagCameraUp
{
    GLfloat x, y, z;
} CameraUp;

typedef struct tagCameraCoordinates
{
    CameraPosition position;
    CameraFront front;
    CameraRight right;
    CameraUp up;
    GLfloat angleX; // yaw
    GLfloat angleY; // pitch
} CameraCoordinates;

typedef struct tagLerpCameraCoordinates
{
	CameraCoordinates begin;
	CameraCoordinates end;
	GLfloat tVal;
	GLfloat speed;
	BOOL bReached;
} LerpCameraCoordinates;

typedef struct tagCameraTilt
{
    GLfloat tilt;
    GLfloat totalTilt;
    BOOL bReached;
} CameraTilt;

typedef struct tagCameraPan
{
    GLfloat pan;
    GLfloat totalPan;
    BOOL bReached;
} CameraPan;

// function declarations
// for keyboard movement
void moveForward(void);
void moveBackward(void);
void moveLeft(void);
void moveRight(void);
void moveUp(void);
void moveDown(void);
void turnUp(void);
void turnDown(void);
void turnLeft(void);
void turnRight(void);

// for general use, but important
void setCameraCoordinates(CameraCoordinates coordinates);
void setCameraCoordinatesUsingLookAt(CameraCoordinates coordinates);
void updateCameraVector(void);

// for animation
BOOL lerpCamera(const CameraCoordinates *initial, const CameraCoordinates *final, GLfloat *lerpVal, GLfloat speed);
void lerpCamera_v2(LerpCameraCoordinates *coords, GLfloat condition);
BOOL moveCameraInCircle(GLfloat deltaRadius, GLfloat circlingSpeed, GLfloat totalRotation, GLfloat *rotation, GLfloat totalRotateAfter, GLfloat *rotateAfter);
void tiltCamera(CameraTilt *tilt, GLfloat speed, GLuint tiltFlag);
void panCamera(CameraPan *pan, GLfloat speed, GLuint panFlag);
