// header files
// windows header files
#include <windows.h>

// C header files
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// OpenGL header files
#include <gl/GL.h>
#include <gl/GLU.h>

// OpenAL header files
#include <al.h>
#include <alc.h> // "Audio Library Context" (ALC)

// link the OpenAL library
#pragma comment(lib, "OpenAL32.lib")

// DllMain
BOOL WINAPI DllMain(DWORD dwReason, HMODULE hModule, LPVOID lpReserved)
{
    // code
    switch(dwReason)
    {
    case DLL_PROCESS_ATTACH:
        break;
    case DLL_PROCESS_DETACH:
        break;
    case DLL_THREAD_ATTACH:
        break;
    case DLL_THREAD_DETACH:
        break;
    default:
        break;
    }

    return(TRUE);
}

// global variables
FILE *gpALLog = NULL;
BOOL bPrintExplicitLogs = FALSE;
SYSTEMTIME sysTime = {0};

// OpenAL related global variables
ALenum error = 0;
ALCdevice *Device = NULL; 
ALCcontext *Context = NULL;
ALfloat defaultSoundSpeed = 0.0f;

BOOL initializeALLib(BOOL bPrintLogs)
{
    // function prototypes
    void DisplayALError(const char *str, ALenum error);
    void cleanup(void);

    // code
    // open the log file
    gpALLog = fopen("ALLog.txt", "w");
    if(gpALLog == NULL)
    {
        printf("An error occurred. fopen() failed to open the log file \"ALLog.txt\".\n");
        return(FALSE);
    }
    
    bPrintExplicitLogs = bPrintLogs;
    if(bPrintExplicitLogs)
    {
        fprintf(gpALLog, "The log file was opened successfully for ALLib in initializeALLib().\n");
    }

    // initialization
    Device = alcOpenDevice(NULL); // select the preferred device for context creation

    if(Device)
    {
        if(bPrintExplicitLogs)
        {
            fprintf(gpALLog, "alcOpenDevice() succeeded.\n");
        }

        Context = alcCreateContext(Device, NULL);
        alcMakeContextCurrent(Context); // make the acquired context current
    
        if(bPrintExplicitLogs)
        {
            fprintf(gpALLog, "alcCreateContext() and alcMakeContextCurrent() succeeded.\n");
        }
    }
    else
    {
        fprintf(gpALLog, "alcOpenDevice() failed.\n");
        cleanup();
        return(FALSE);
    }

    defaultSoundSpeed = alGetFloat(AL_SPEED_OF_SOUND);
    error = alGetError();
    if(error != AL_NO_ERROR)
    {
        DisplayALError("alGetFloat() : ", error);
        cleanup();
        return(FALSE);
    }

    if(bPrintExplicitLogs)
    {
        GetLocalTime(&sysTime);
        fprintf(gpALLog, "AL Library was sucessfully initialized at time %2u:%2u:%2u:%4u.\n", sysTime.wHour, sysTime.wMinute, sysTime.wSecond, sysTime.wMilliseconds);
    }

    fflush(gpALLog);

    return(TRUE);
}

BOOL getALBuffers(ALsizei nBuffers, ALuint *buffers)
{
    // function prototypes
    void DisplayALError(const char *str, ALenum error);
    void cleanup(void);

    // code
    // Generate buffers
    alGetError(); // clear error code

    alGenBuffers(nBuffers, buffers);
    error = alGetError();
    if(error != AL_NO_ERROR)
    {
        DisplayALError("alGenBuffers() : ", error);
        cleanup();
        return(FALSE);
    }

    return(TRUE);
}

BOOL getALSources(ALsizei nSources, ALuint *sources)
{
    // function prototypes
    void DisplayALError(const char *str, ALenum error);
    void cleanup(void);

    // code
    // Generate sources
    alGetError(); // clear error code

    alGenSources(nSources, sources);
    error = alGetError();
    if(error != AL_NO_ERROR)
    {
        DisplayALError("alGenSources() : ", error);
        cleanup();
        return(FALSE);
    }

    return(TRUE);
}

BOOL attachALWaveFileToBuffer(ALuint buffer, ALenum format, ALvoid *data, ALsizei size, ALsizei freq)
{
    // function prototypes
    void DisplayALError(const char *str, ALenum error);
    void cleanup(void);

    // code
    alGetError(); // clear error code

    alBufferData(
                buffer, 
                format /* wave file format */, 
                data /* pointer to wave file data */, 
                size /* size of wave file */, 
                freq /* frequency of buffer in Hz */
                );

    error = alGetError();
    if(error != AL_NO_ERROR)
    {
        DisplayALError("alBufferData() : ", error);
        cleanup();
        return(FALSE);
    }

    return(TRUE);
}

BOOL attachALBufferToSource(ALuint *buffer, ALuint *source)
{
    // function prototypes
    void DisplayALError(const char *str, ALenum error);
    void cleanup(void);

    // code
    alGetError(); // clear error code

    alSourcei(*source, AL_BUFFER, *buffer);
    error = alGetError();
    if(error != AL_NO_ERROR)
    {
        DisplayALError("alSourcei() failed to attach buffer to source : ", error);
        cleanup();
        return(FALSE);
    }

    return(TRUE);
}

BOOL playALSource(ALuint source)
{
    // function prototypes
    void DisplayALError(const char *str, ALenum error);
    void cleanup(void);

    // code
    alGetError(); // clear error code

    alSourcePlay(source);
    error = alGetError();
    if(error != AL_NO_ERROR)
    {
        DisplayALError("alSourcePlay() : ", error);
        cleanup();
        return(FALSE);
    }

    return(TRUE);
}

BOOL pauseALSource(ALuint source)
{
    // function prototypes
    void DisplayALError(const char *str, ALenum error);
    void cleanup(void);

    // code
    alGetError(); // clear error code

    alSourcePause(source);
    error = alGetError();
    if(error != AL_NO_ERROR)
    {
        DisplayALError("alSourcePause() : ", error);
        cleanup();
        return(FALSE);
    }

    return(TRUE);
}

BOOL stopALSource(ALuint source)
{
    // function prototypes
    void DisplayALError(const char *str, ALenum error);
    void cleanup(void);

    // code
    alGetError(); // clear error code

    alSourceStop(source);
    error = alGetError();
    if(error != AL_NO_ERROR)
    {
        DisplayALError("alSourceStop() : ", error);
        cleanup();
        return(FALSE);
    }

    return(TRUE);
}

BOOL muteALSource(ALuint source, ALfloat *oldVolume)
{
    // function prototypes
    void DisplayALError(const char *str, ALenum error);
    void cleanup(void);

    // local variables
    ALfloat minGain = 0;

    // code
    alGetError();

    // save current gain in oldVolume
    alGetSourcef(source, AL_GAIN, oldVolume);
    error = alGetError();
    if(error != AL_NO_ERROR)
    {
        DisplayALError("alGetSourcef() : ", error);
        cleanup();
        return(FALSE);
    }

    // get the minimum gain for this source
    alGetSourcef(source, AL_MIN_GAIN, &minGain);
    error = alGetError();
    if(error != AL_NO_ERROR)
    {
        DisplayALError("alGetSourcef() : ", error);
        cleanup();
        return(FALSE);
    }

    // set the gain of the audio to minimum gain
    alSourcef(source, AL_GAIN, minGain);
    error = alGetError();
    if(error != AL_NO_ERROR)
    {
        DisplayALError("alSourcef() : ", error);
        cleanup();
        return(FALSE);
    }

    return(TRUE);
}

BOOL unmuteALSource(ALuint source, ALfloat volume)
{
    // function prototypes
    void DisplayALError(const char *str, ALenum error);
    void cleanup(void);

    // code
    alGetError();

    // set the gain of the audio to volume
    alSourcef(source, AL_GAIN, volume);
    error = alGetError();
    if(error != AL_NO_ERROR)
    {
        DisplayALError("alSourcef() : ", error);
        cleanup();
        return(FALSE);
    }

    return(TRUE);
}

BOOL getALSourcePlaybackPositionInSeconds(ALuint source, ALfloat *position)
{
    // function prototypes
    void DisplayALError(const char *str, ALenum error);
    void cleanup(void);

    // code
    alGetError(); // clear error code

    alGetSourcef(source, AL_SEC_OFFSET, position);
    error = alGetError();
    if(error != AL_NO_ERROR)
    {
        DisplayALError("alGetSourcef() : ", error);
        cleanup();
        return (FALSE);
    }

    return(TRUE);
}

BOOL seekALSource(ALuint source, ALuint hours, ALuint minutes, ALuint seconds)
{
    // function prototypes
    void DisplayALError(const char *str, ALenum error);
    void cleanup(void);

    // local variables
    ALuint positionInSeconds;

    // code
    alGetError();

    positionInSeconds = (hours * 3600) + (minutes * 60) + seconds;

    alSourcei(source, AL_SEC_OFFSET, positionInSeconds);
    error = alGetError();
    if(error != AL_NO_ERROR)
    {
        DisplayALError("alSourcei() : ", error);
        cleanup();
        return(FALSE);
    }

    return(TRUE);
}

BOOL setALSourceSpeed(ALfloat factor)
{
    // function prototypes
    void DisplayALError(const char *str, ALenum error);
    void cleanup(void);

    // code
    alGetError();

    alSpeedOfSound(defaultSoundSpeed * factor);
    error = alGetError();
    if(error != AL_NO_ERROR)
    {
        DisplayALError("alSpeedOfSound() : ", error);
        cleanup();
        return(FALSE);
    }

    return(TRUE);
}

BOOL setALSourceVolume(ALuint source, ALfloat volume)
{
    // function prototypes
    void DisplayALError(const char *str, ALenum error);
    void cleanup(void);

    // code
    alGetError();

    alSourcef(source, AL_GAIN, volume);
    error = alGetError();
    if(error != AL_NO_ERROR)
    {
        DisplayALError("alSourcef() : ", error);
        cleanup();
        return(FALSE);
    }

    return(TRUE);
}

void deleteALSources(ALsizei nSources, ALuint *sources)
{
    // code
    alDeleteSources(nSources, sources);
}

void deleteALBuffers(ALsizei nBuffers, ALuint *buffers)
{
    // code
    alDeleteBuffers(nBuffers, buffers);
}

void uninitializeALLib(void)
{
    // function prototypes
    void cleanup(void);

    // code
    cleanup();

    if(gpALLog)
    {
        GetLocalTime(&sysTime);

        fprintf(gpALLog, "AL library was uninitialized successfully at time %2u:%2u:%2u:%4u.\n", sysTime.wHour, sysTime.wMinute, sysTime.wSecond, sysTime.wMilliseconds);
        fclose(gpALLog);
        gpALLog = NULL;
    }
}

// internal functions
void DisplayALError(const char *str, ALenum error)
{
    // local variables
    WCHAR temp[2048];

    // code
    // print error in file
    fprintf(gpALLog, "%s %d\n", str, error);

    // print error on UNICODE-compliant string
    wsprintfW(temp, L"%s %d", str, error);

    // show error using MessageBox
    MessageBoxW(NULL, temp, L"Error", MB_OK | MB_ICONERROR);
}

void cleanup(void)
{
    // code
    Context = alcGetCurrentContext();
    Device = alcGetContextsDevice(Context);
    alcMakeContextCurrent(NULL);
    alcDestroyContext(Context);
    alcCloseDevice(Device);
}
