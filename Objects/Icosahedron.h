#pragma once

#include "SphereCollision.h"
#include "..\Common\Common.h"

// struct definitions
typedef struct tagIcosahedronAttribs
{
    Vec3 originTranslation;
    Vec3 translationFromOrigin;
    Vec3 rotation;
    Vec3 scale;
} IcosahedronAttribs;

typedef struct tagIcosahedronAttribs_v2
{
    Vec3CUDA acc;
    Vec3CUDA gravity;
    Vec3CUDA pos;
    Vec3CUDA oldPos;
    Vec3CUDA rotation;
    Vec3CUDA scale;
    GLfloat radius;
} IcosahedronAttribs_v2;

// function declarations
void initializeRangeIcosahedrons(IcosahedronAttribs *icoAttribs, GLint numIcosahedrons, Vec3 origin, Vec3 rangeStart, Vec3 rangeEnd);
void drawRangeIcosahedrons(IcosahedronAttribs *icoAttribs, GLint numIcosahedrons, GLuint texture, GLfloat scale);
void drawRock(GLuint texture, GLenum mode);

// for asteroid rocks (rise of phoenix)
void initializeRangeIcosahedrons_Asteroids(IcosahedronAttribs_v2 *icoAttribs, GLint numIcosahedrons, Range3 rGravity, Range3 rTranslation, Range3 rRotation, Range rScale);
void drawRangeIcosahedrons_Asteroids(IcosahedronAttribs_v2 *icoAttribs, GLint numIcosahedrons, GLuint texture, GLfloat scale, GLint colorIdx);
BOOL updateRangeIcosahedrons_Asteroids(IcosahedronAttribs_v2 *icoAttribs, GLint numIcosahedrons, Vec3CUDA saturnPos, GLfloat saturnRadius, GLfloat speedFactor);
