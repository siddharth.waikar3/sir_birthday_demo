// header files
#include "Texture.h"
#include "Common.h"

// for Sean Barrett's image loader
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

// for texture loading from file
BOOL LoadTexture(GLuint *textureName, const char *filename)
{
    // local variables
    int width, height, imageComponents;
    unsigned char *imageData;
    
    // code
    // load the image
    imageData = stbi_load(filename, &width, &height, &imageComponents, 0);
    if(imageData == NULL)
    {
        return (FALSE);
    }

    // pixel un-packing strategy
    glPixelStorei(GL_UNPACK_ALIGNMENT, 4 /* word alignment */);

    // generate the texture
    glGenTextures(1, textureName);

    // bind the texture
    glBindTexture(GL_TEXTURE_2D, *textureName);

    // set texture parameters first
    // glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    // glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

    // generate mipmaps using GLU
    gluBuild2DMipmaps(GL_TEXTURE_2D,                             /* target */
                      imageComponents,                           /* components */
                      width,                                     /* width */
                      height,                                    /* height */
                      (imageComponents == 4) ? GL_RGBA : GL_RGB, /* format (pixel format) */
                      GL_UNSIGNED_BYTE,                          /* type */
                      imageData                                  /* data */
                     );                       

    // unbind the texture
    glBindTexture(GL_TEXTURE_2D, 0);

    // free the image data
    stbi_image_free((void *)imageData);

    return(TRUE);
}

BOOL LoadCubeMapTexture(GLuint *textureName, const char *filename)
{
    // local variables
    int width, height, imageComponents;
    unsigned char *imageData;
    
    // code
    // stbi_set_flip_vertically_on_load(TRUE);

    // load the image
    imageData = stbi_load(filename, &width, &height, &imageComponents, 0);
    if(imageData == NULL)
    {
        return (FALSE);
    }

    // pixel un-packing strategy
    glPixelStorei(GL_UNPACK_ALIGNMENT, 4 /* word alignment */);

    // generate the texture
    glGenTextures(1, textureName);

    // bind the texture
    glBindTexture(GL_TEXTURE_2D, *textureName);

    // set texture parameters first
    // glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    // glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    // glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BORDER, 0);
    //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

    // generate mipmaps using GLU
    gluBuild2DMipmaps(GL_TEXTURE_2D,                             /* target */
                      imageComponents,                           /* components */
                      width,                                     /* width */
                      height,                                    /* height */
                      (imageComponents == 4) ? GL_RGBA : GL_RGB, /* format (pixel format) */
                      GL_UNSIGNED_BYTE,                          /* type */
                      imageData                                  /* data */
                     );                       

    // make a 2D image from texture data
    // glTexImage2D(GL_TEXTURE_2D,
    //             0,
    //             GL_RGBA,
    //             width,
    //             height,
    //             1,
    //             GL_RGBA,
    //             GL_UNSIGNED_BYTE,
    //             imageData);

    // unbind the texture
    glBindTexture(GL_TEXTURE_2D, 0);

    // free the image data
    stbi_image_free((void *)imageData);

    return(TRUE);
}

void drawTexQuad(GLuint textureName)
{
    // local variables
    GLfloat whiteColor[] = {1.0f, 1.0f, 1.0f, 1.0f};

    // code 
    glBindTexture(GL_TEXTURE_2D, textureName);
    glColor4fv(whiteColor);
    glBegin(GL_QUADS);
    {
        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(1.0f, 0.5f, 0.0f);

        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-1.0f, 0.5f, 0.0f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(-1.0f, -0.5f, 0.0f);

        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(1.0f, -0.5f, 0.0f);
    }
    glEnd();
    glBindTexture(GL_TEXTURE_2D, 0);
}

void drawTexCube(GLuint textureName)
{
    // code
    glColor4f(1.0f, 1.0f, 1.0f, 1.0f);

    glBindTexture(GL_TEXTURE_2D, textureName);
    glBegin(GL_QUADS);
    {
        // cube
        // front face
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(1.0f, 1.0f, 1.0f); 

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(-1.0f, 1.0f, 1.0f);

        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-1.0f, -1.0f, 1.0f);

        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(1.0f, -1.0f, 1.0f);

        // right face
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(1.0f, 1.0f, -1.0f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(1.0f, 1.0f, 1.0f);

        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(1.0f, -1.0f, 1.0f);

        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(1.0f, -1.0f, -1.0f);

        // back face
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(-1.0f, 1.0f, -1.0f); 

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(1.0f, 1.0f, -1.0f); 

        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(1.0f, -1.0f, -1.0f); 

        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(-1.0f, -1.0f, -1.0f);

        // left face
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(-1.0f, 1.0f, 1.0f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(-1.0f, 1.0f, -1.0f);

        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-1.0f, -1.0f, -1.0f);

        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(-1.0f, -1.0f, 1.0f);

        // top face
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(1.0f, 1.0f, -1.0f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(-1.0f, 1.0f, -1.0f);

        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-1.0f, 1.0f, 1.0f);

        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(1.0f, 1.0f, 1.0f);

        // bottom face
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(1.0f, -1.0f, 1.0f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(-1.0f, -1.0f, 1.0f);

        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-1.0f, -1.0f, -1.0f);

        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(1.0f, -1.0f, -1.0f);
    }
    glEnd();
    glBindTexture(GL_TEXTURE_2D, 0);
}

void drawTexCube_v2(GLuint *textures)
{
    // code
    glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
    glBindTexture(GL_TEXTURE_2D, textures[0]);
    glBegin(GL_QUADS);
    {
        // cube
        // front face
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(1.0f, 1.0f, 1.0f); 

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(-1.0f, 1.0f, 1.0f);

        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-1.0f, -1.0f, 1.0f);

        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(1.0f, -1.0f, 1.0f);
    }
    glEnd();

    glBindTexture(GL_TEXTURE_2D, textures[1]);
    glBegin(GL_QUADS);
    {
        // back face
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(1.0f, 1.0f, -1.0f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(1.0f, 1.0f, 1.0f);

        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(1.0f, -1.0f, 1.0f);

        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(1.0f, -1.0f, -1.0f);
    }
    glEnd();

    glBindTexture(GL_TEXTURE_2D, textures[2]);
    glBegin(GL_QUADS);
    {
        // right face
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(-1.0f, 1.0f, -1.0f); 

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(1.0f, 1.0f, -1.0f); 

        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(1.0f, -1.0f, -1.0f); 

        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(-1.0f, -1.0f, -1.0f);
    }
    glEnd();

    glBindTexture(GL_TEXTURE_2D, textures[3]);
    glBegin(GL_QUADS);
    {
        // left face
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(-1.0f, 1.0f, 1.0f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(-1.0f, 1.0f, -1.0f);

        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-1.0f, -1.0f, -1.0f);

        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(-1.0f, -1.0f, 1.0f);
    }
    glEnd();

    glBindTexture(GL_TEXTURE_2D, textures[4]);
    glBegin(GL_QUADS);
    {
        // top face
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(1.0f, 1.0f, -1.0f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(-1.0f, 1.0f, -1.0f);

        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-1.0f, 1.0f, 1.0f);

        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(1.0f, 1.0f, 1.0f);
    }
    glEnd();

    glBindTexture(GL_TEXTURE_2D, textures[5]);
    glBegin(GL_QUADS);
    {
        // bottom face
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(1.0f, -1.0f, 1.0f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(-1.0f, -1.0f, 1.0f);

        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-1.0f, -1.0f, -1.0f);

        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(1.0f, -1.0f, -1.0f);
    }
    glEnd();
    glBindTexture(GL_TEXTURE_2D, 0);
}

void DeleteTexture(GLuint *textureName)
{
    // code
    if(*textureName)
    {
        glDeleteTextures(1, textureName);
        *textureName = 0;
    }
}
