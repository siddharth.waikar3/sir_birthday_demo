// common header files
#include <windows.h>
#include <commctrl.h> // for loading animation control
#include <stdio.h>  // for file I/O
#include <stdlib.h> // for exit()
#include "OGL.h"    // for the window
#include "./Common/Common.h" // for common code
#include "./Common/Camera.h" // for camera functionality
#include "./Common/OAL.h"    // for the OpenAL library
#include "./Common/Timer.h"  // for timer
#include "./Common/SplashScreen.h" // for SplashScreen

#ifdef ENABLE_CUDA
    #include "./Common/CUDALibClient.h" // for CUDA library
#endif

#include "./Scenes/IncludeScenes.h" // for scenes

// objects
#include "./Objects/Saturn.h"
#include "./Objects/Skybox.h"
#include "./Objects/SlideText.h"
#include "./Objects/OrbitText.h"

// OpenGL header files
#include <gl/GL.h>
#include <gl/GLU.h>

// imgui
#include "imgui/imgui.h"
#include "imgui/imgui_impl_win32.h"
#include "imgui/imgui_impl_opengl2.h"

// macros
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#define START_GAMELOOP 0

// for mouse rotation of scene
#define X_LESS_THAN    0x00000001
#define X_GREATER_THAN 0x00000002
#define Y_LESS_THAN    0x00000004
#define Y_GREATER_THAN 0x00000008

#define STOPPING_FORCE 1.0f

// for splashscreen
#define IDT_SPLASHSCREEN_ANIMATION 1000

// OpenGL libraries
#pragma comment(lib, "OpenGL32.lib")
#pragma comment(lib, "Glu32.lib")

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK SplashWndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam);

// global variable declarations
HWND ghwnd = NULL;
DWORD dwStyle = 0;
BOOL gbFullscreen = FALSE;
WINDOWPLACEMENT wpPrev = {sizeof(WINDOWPLACEMENT)};

BOOL gbActive = FALSE;

#if START_GAMELOOP
BOOL gbPlay = TRUE;
#else
BOOL gbPlay = FALSE;
#endif

BOOL gbMuted = FALSE;

FILE *gpFILE = NULL;
FILE *gpConfigFILE = NULL;

StopWatchTimer *stopWatch = NULL;
BOOL g_bIsStopWatchTimerOn = FALSE;
long double totalElapsedTime = 0.0f;

StopWatchTimer *benchmarkTimer = NULL;
StopWatchTimer *splashScreenDelay = NULL;

// for SplashScreen
SplashScreen splashScreen;
BOOL g_bSplashAnimationFlags[3]; 

// OpenGL related global variables
HDC ghdc = NULL;
HGLRC ghrc = NULL;
UINT iCurrentWidth;
UINT iCurrentHeight;

GLUquadric* g_Quad;

// for FPS locking
GLfloat desiredFPS = 60.0f;
GLfloat deltaTime = 0.0f;

CameraCoordinates cameraCoordinates;
struct Transformations g_Transformations[NUM_TRANSFORMATION_ELEMENTS];
enum TransformationMode g_CurrentTransformationMode;
int g_iCurrentTransformation = 0;

GLfloat step = 1.0f;

enum Scene currentScene;

// FOR MOUSE ROTATION OF SCENE
Vector vMouseLMBStart = {0};
Vector vMouseLMBEnd = {0}; 
RECT rc;

BOOL g_bIsShiftKeyPressed = FALSE;
BOOL g_IsCameraRotationToggled = FALSE;

// OpenAL related global variables
// ALSourceNode *pALSourceEngine = NULL;
ALSource g_ActiveALSource;

ALuint g_CompleteTrackBuffer;
ALuint g_CompleteTrackSource;

ALuint g_SceneBuffers[NUM_TRACKS];
ALuint g_SceneSources[NUM_TRACKS];

ALfloat g_Volume = 1.0f; 

BOOL bDebug = TRUE;
BOOL gbShowSoundStats = TRUE;
BOOL gbShowCameraStats = FALSE;

extern IMGUI_IMPL_API LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

// entry-point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
    // function declarations
    int initialize(void);
    void display(void);
    void update(void);
    void uninitialize(void);

    // local variable declarations
    WNDCLASSEX wndclass;
    HWND hwnd;
    MSG msg;
    TCHAR szAppName[] = TEXT("BirthdayDemoWindow");
    int iResult = 0;
    BOOL bDone = FALSE;

    // for FPS locking
	LARGE_INTEGER startingTime;
	LARGE_INTEGER endingTime;
	LARGE_INTEGER frequency;

    // init the SS delay timer
    initializeTimer(&splashScreenDelay);

    // for elapsed time
    initializeTimer(&stopWatch);

    // for benchmarking
    initializeTimer(&benchmarkTimer);

    // code
    // open the log file
    gpFILE = fopen("Log.txt", "w");
    if(gpFILE == NULL)
    {
        MessageBox(NULL, TEXT("Log file cannot be opened."), TEXT("Error"), MB_OK | MB_ICONERROR);
        exit(0);
    }
    fprintf(gpFILE, "Program started successfully.\n");

    // open the config file
    gpConfigFILE = fopen("Config.ini", "r");
    if(gpConfigFILE == NULL)
    {
        MessageBoxW(NULL, L"Failed to open config file.", L"Error", MB_OK | MB_ICONERROR);
        exit(0);
    }

    // WNDCLASSEX initialization
    wndclass.cbSize = sizeof(WNDCLASSEX);
    wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
    wndclass.cbClsExtra = 0;
    wndclass.cbWndExtra = 0;
    wndclass.hInstance = hInstance;
    wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
    wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
    wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
    wndclass.lpfnWndProc = WndProc;
    wndclass.lpszClassName = szAppName;
    wndclass.lpszMenuName = NULL;
    wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

    // WNDCLASSEX registration
    RegisterClassEx(&wndclass);

    // Create the window in memory
    hwnd = CreateWindowEx(WS_EX_APPWINDOW,
                           szAppName,
                           TEXT("\"Legacy\" - presented to you by AstroMediComp's RTR 5.0's Render Group"),
                           WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
                           (GetSystemMetrics(SM_CXSCREEN) / 2) - (WIN_WIDTH / 2),
                           (GetSystemMetrics(SM_CYSCREEN) / 2) - (WIN_HEIGHT / 2),
                           WIN_WIDTH,
                           WIN_HEIGHT,
                           HWND_DESKTOP,
                           NULL,
                           hInstance,
                           NULL);
    
    // copy the local window handle into the global window handle
    ghwnd = hwnd;

    // initialize splash screen
    if(InitSplashScreen(&splashScreen, hwnd, GetModuleHandle(NULL), SplashWndProc, MAKEINTRESOURCEW(MY_SPLASHSCREEN_BITMAP), 500) == FALSE) 
    {
        fprintf(gpFILE, "InitSplashScreen() failed.\n");
        DestroyWindow(ghwnd);
    }
    
#if SPLASHSCREEN_DELAY
    startTimer(splashScreenDelay);
    
    while(queryElapsedTimeMilliseconds(splashScreenDelay) <= 4750.0)
        ;

    stopTimer(splashScreenDelay);
    resetTimer(splashScreenDelay);
#endif

    // initialization
    iResult = initialize();
    if(iResult != 0)
    {
        MessageBox(hwnd, TEXT("initialize() failed."), TEXT("Error"), MB_OK | MB_ICONERROR);
        DestroyWindow(hwnd);
    }

    // delete the splashScreen
    DeleteSplashScreen(&splashScreen);

    QueryPerformanceFrequency(&frequency);
	QueryPerformanceCounter(&startingTime);
	GLfloat timePerFrame = 1.0f / desiredFPS;
	GLfloat inverseFrequency = 1.0f / (GLfloat)frequency.QuadPart;

    if(gbPlay)
        totalElapsedTime = (queryElapsedTimeMilliseconds(stopWatch) / 1000.0);

    // imgui
	IMGUI_CHECKVERSION();
	if (ImGui::CreateContext() == NULL)
	{
		fprintf(gpFILE, "ImGui::CreateContext() failed.\n");
		uninitialize();
	}
	if (ImGui_ImplWin32_InitForOpenGL(ghwnd) == false)
	{
		fprintf(gpFILE, "ImGui_ImplWin32_InitForOpenGL() failed.\n");
		uninitialize();
	}
	if (ImGui_ImplOpenGL2_Init() == false)
	{
		fprintf(gpFILE, "ImGui_ImplOpenGL2_Init() failed.\n");
		uninitialize();
	}
	ImGui::StyleColorsDark();

    // Gameloop
    while(bDone == FALSE)
    {
        if(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
        {
            if(msg.message == WM_QUIT)
            {
                bDone = TRUE;
            }
            else
            {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
            }
        }
        else
        {
            if(gbActive == TRUE)
            { 
                // Render
                ImGui_ImplWin32_NewFrame();
				ImGui_ImplOpenGL2_NewFrame();
				ImGui::NewFrame();
                // ImGui::ShowDemoWindow();

                display();

                if(gbPlay)
                {
                    totalElapsedTime = (queryElapsedTimeMilliseconds(stopWatch) / 1000.0);

                    // Update
                    update();
                }

                // FPS lock
				QueryPerformanceCounter(&endingTime);
				deltaTime = ((GLfloat)(endingTime.QuadPart - startingTime.QuadPart)) * inverseFrequency;
				while (deltaTime < timePerFrame)
				{
					QueryPerformanceCounter(&endingTime);
					deltaTime = ((GLfloat)(endingTime.QuadPart - startingTime.QuadPart)) * inverseFrequency;
				}
				startingTime = endingTime;
            }
        }
    }

    // uninitialization
    uninitialize();

    return((int)msg.wParam);
}

// callback function
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
    // function prototypes
    void ToggleFullscreen(void);
    void resize(int width, int height);
    void TransformUsingKeyboard(char cPressedKey);
    void MoveCameraUsingKeys(char cPressedKey);
    void PauseOrPlayALSource(void);
    void MuteOrUnmuteALSource(void);
    void IncreaseOrDecreaseVolumeOfALSources(BOOL bIncrease);

    // local variables
    int iDelta = 0; // mouse wheel rotation value
    static int iLButtonDownX = 0;
	static int iLButtonDownY = 0;
	static int iLButtonUpX = 0;
	static int iLButtonUpY = 0;

    GLfloat fLButtonXLogicalCoord = 0.0f;
	GLfloat fLButtonYLogicalCoord = 0.0f;

    // code
    if (ImGui_ImplWin32_WndProcHandler(hwnd, iMsg, wParam, lParam))
	{
		return true;
	}

    switch(iMsg)
    {
    case WM_SETFOCUS:
        gbActive = TRUE;
        break;
    case WM_KILLFOCUS:
        gbActive = FALSE;
        break;
    case WM_SIZE:
        resize(LOWORD(lParam), HIWORD(lParam));
        GetClientRect(hwnd, &rc);

        iCurrentWidth = rc.right - rc.left;
        iCurrentHeight = rc.bottom - rc.top;

        break;
    case WM_ERASEBKGND:
        return(0);
    case WM_KEYDOWN:
        switch(LOWORD(wParam))
        {
        case VK_ESCAPE:
            PlaySoundW(NULL, NULL, 0);
            DestroyWindow(hwnd);
            break;
        // for pausing / playing animation
        case VK_TAB:
            gbPlay = (!gbPlay) ? TRUE : FALSE;
            if(g_bIsStopWatchTimerOn)
            {
                stopTimer(stopWatch);
                g_bIsStopWatchTimerOn = FALSE;
            }
            else
            {
                startTimer(stopWatch);
                g_bIsStopWatchTimerOn = TRUE;
            }
            PauseOrPlayALSource();
            break;
        // for resetting camera position
        case VK_NUMPAD0:
            cameraCoordinates.position = {0.0f, 0.0f, 0.0f};
            cameraCoordinates.front = {0.0f, 0.0f, 0.0f};
            cameraCoordinates.right = {0.0f, 0.0f, 0.0f};
            cameraCoordinates.up = {0.0f, 1.0f, 0.0f};
            cameraCoordinates.angleX = 0.0f;
            cameraCoordinates.angleY = 270.0f;
            break;

        case VK_UP:
            if(g_bIsShiftKeyPressed)
            {
                moveUp();
            }
            else
            {
                moveForward();
            }
            break;
        case VK_DOWN:
            if(g_bIsShiftKeyPressed)
            {
                moveDown();
            }
            else
            {
                moveBackward();
            }
            break;
        case VK_LEFT:
            moveLeft();
            break;
        case VK_RIGHT:
            moveRight();
            break;

        case VK_F1:  
            gbShowSoundStats = !gbShowSoundStats;
            break;

        case VK_F2:
            IncreaseOrDecreaseVolumeOfALSources(FALSE);
            break;
        case VK_F3:
            IncreaseOrDecreaseVolumeOfALSources(TRUE);
            break;

        case VK_F6:
            gbShowCameraStats = !gbShowCameraStats;
            break;

        case VK_SHIFT:
            g_bIsShiftKeyPressed = TRUE;
            break;
        }
        break;
    case WM_KEYUP:
        switch(LOWORD(wParam))
        {
        case VK_SHIFT:
            g_bIsShiftKeyPressed = FALSE;
            break;
        }
        break;
    case WM_CHAR:
        switch (LOWORD(wParam))
        {
        case 'f':
        case 'F':
            ToggleFullscreen();
            gbFullscreen = (!gbFullscreen) ? TRUE : FALSE;
            break;

        // camera rotation
        case 'W':
        case 'w':
            turnUp();
            break;

        case 'A':
        case 'a':
            turnLeft();
            break;

        // s is handled below
        
        case 'D':
        case 'd':
#ifdef DEBUG
            if(g_bIsShiftKeyPressed)
            {
                bDebug = !bDebug;
            }
            else
#endif
                turnRight();
            break; 

        case 'M':
        case 'm':
            MuteOrUnmuteALSource();
            gbMuted = !gbMuted;
            break;

        case 't':
        case 'T':
            g_CurrentTransformationMode = TRANSLATION;
            break;
        case 'r':
        case 'R':
            g_CurrentTransformationMode = ROTATION;
            break;
        case 's':
        case 'S':
            if(g_bIsShiftKeyPressed)
            {
                g_CurrentTransformationMode = SCALE;
            }
            else
            {
                turnDown();
            }
            break;
        case 'O':
        case 'o':
            g_iCurrentTransformation++;
            if(g_iCurrentTransformation > (NUM_TRANSFORMATION_ELEMENTS - 1))
                g_iCurrentTransformation = NUM_TRANSFORMATION_ELEMENTS - 1;
            break;
        
        case 'P':
        case 'p':
            g_iCurrentTransformation--;
            if(g_iCurrentTransformation < 0)
                g_iCurrentTransformation = 0;
            break;

        case 'X':
        case 'x':
        case 'Y':
        case 'y':
        case 'Z':
        case 'z':
            TransformUsingKeyboard(LOWORD(wParam));
            break;
        
        case '-':
            step /= 10.0f;
            break;
        case '+':
            step *= 10.0f;
            break;
        default:
            break;
        }
        break;

    case WM_MOUSEWHEEL:
		iDelta = GET_WHEEL_DELTA_WPARAM(wParam);

        g_Transformations[g_iCurrentTransformation].translation.z += (iDelta / 120) * (step * TRANSLATION_SPEED);
		return(0);
	
	// when user presses the LMB
	case WM_LBUTTONDOWN:
        
        vMouseLMBStart.x = getInterpolatedValue(0.0f, (GLfloat)rc.right - (GLfloat)rc.left, -360.0f /* was -360.0f */, 360.0f, LOWORD(lParam));
        vMouseLMBStart.y = getInterpolatedValue(0.0f, (GLfloat)rc.bottom - (GLfloat)rc.top, -360.0f, 360.0f, HIWORD(lParam));
        vMouseLMBStart.z = 0.0f; // always going to be 0.0f
        return(0);

	// when mouse is moving across the screen
	case WM_MOUSEMOVE:
        vMouseLMBEnd.x = getInterpolatedValue(0.0f, (GLfloat)rc.right - (GLfloat)rc.left, -360.0f, 360.0f, LOWORD(lParam));
        vMouseLMBEnd.y = getInterpolatedValue(0.0f, (GLfloat)rc.bottom - (GLfloat)rc.top, -360.0f, 360.0f, HIWORD(lParam));
        vMouseLMBEnd.z = 0.0f; // always 0.0f
        subVec(&vMouseLMBEnd, &vMouseLMBStart);
        return(0);

	case WM_RBUTTONDOWN:
        g_IsCameraRotationToggled = !g_IsCameraRotationToggled; // toggle camera rotation
		// g_CurrentTransformationMode = (g_CurrentTransformationMode == TRANSLATION) ? ROTATION : (g_CurrentTransformationMode == ROTATION) ? SCALE : TRANSLATION; 
		return(0);
    case WM_CLOSE:
        DestroyWindow(hwnd);
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:    
        break;
    }

    return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

// SplashScreen window procedure
LRESULT CALLBACK SplashWndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
    // code
    switch(iMsg)
    {
    case WM_ERASEBKGND:
        ShowSplashScreen(&splashScreen, wParam);
        break;
    default:
        break;
    }

    return(DefWindowProcW(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
    // local variables
    MONITORINFO monitorinfo = {sizeof(MONITORINFO)};

    // code
    if(gbFullscreen == FALSE)
    {
        dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
        if(dwStyle & WS_OVERLAPPEDWINDOW)
        {
            if(GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &monitorinfo))
            {
                SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);

                SetWindowPos(ghwnd,
                             HWND_TOP,
                             monitorinfo.rcMonitor.left,
                             monitorinfo.rcMonitor.top,
                             monitorinfo.rcMonitor.right - monitorinfo.rcMonitor.left,
                             monitorinfo.rcMonitor.bottom - monitorinfo.rcMonitor.top,
                             SWP_NOZORDER | SWP_FRAMECHANGED);
            }
        }

#ifdef DEBUG
        ShowCursor(TRUE);
#else
        ShowCursor(FALSE);
#endif

    }
    else
    {
        SetWindowPlacement(ghwnd, &wpPrev);

        SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

        SetWindowPos(ghwnd,
                     HWND_TOP,
                     0,
                     0,
                     0,
                     0,
                     SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

        ShowCursor(TRUE);
    }
}

int initialize(void)
{
    // function prototypes
    void initializeScene(void);
    void selectSceneFromFile(void);
    void initializeAllObjects(void);
    void ToggleFullscreen(void);

    // local variables
    PIXELFORMATDESCRIPTOR pfd;
    int iPixelFormatIndex = 0;

    // code
    ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

    // step 1 : initialize the PIXELFORMATDESCRIPTOR
    pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
    pfd.nVersion = 1;
    pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
    pfd.iPixelType = PFD_TYPE_RGBA;
    pfd.cColorBits = 32;
    pfd.cRedBits = 8;
    pfd.cGreenBits = 8;
    pfd.cBlueBits = 8;
    pfd.cAlphaBits = 8;
    pfd.cDepthBits = 32;

    // step 2 : get the DC
    ghdc = GetDC(ghwnd);
    if(ghdc == NULL)
    {
        fprintf(gpFILE, "GetDC() failed.\n");
        return(-1);
    }

    // step 3 : choose the pixel format and get its index from the OS
    iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
    if(iPixelFormatIndex == 0)
    {
        fprintf(gpFILE, "ChoosePixelFormat() failed.\n");
        return(-2);
    }

    // step 4 : set the pixel format using obtained index
    if(SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
    {
        fprintf(gpFILE, "SetPixelFormat() failed.\n");
        return(-3);
    }

    // step 5 : create the OpenGL rendering context
    ghrc = wglCreateContext(ghdc);
    if(ghrc == NULL)
    {
        fprintf(gpFILE, "wglCreateContext() failed.\n");
        return(-4);
    }

    // step 6 : make the RC current
    if(wglMakeCurrent(ghdc, ghrc) == FALSE)
    {
        fprintf(gpFILE, "wglMakeCurrent() failed.\n");
        return(-5);
    }

    // OpenGL starts here...!
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    // enable depth
    glEnable(GL_DEPTH_TEST);
    glClearDepth(1.0f);
    glDepthFunc(GL_LEQUAL);
    
    // smooth shading
    glShadeModel(GL_SMOOTH);

    // beautification
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

    // for alpha blending
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    // enable 2D textures
    glEnable(GL_TEXTURE_2D);

    // enable some things
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_INDEX_ARRAY);

    g_bSplashAnimationFlags[0] = TRUE;
    LoadSplashScreenBmp(&splashScreen, MAKEINTRESOURCEW(MY_SPLASHSCREEN_BITMAP_2));

#ifdef DEBUG
    // initialize debug text
    debugText(FALSE, FALSE);
#endif

#ifdef ENABLE_CUDA
    // initialize CUDA library
    initializeCUDALibrary();
#endif

    // initialize g_Transformations 
    for(int i = 0; i < NUM_TRANSFORMATION_ELEMENTS; i++)
    {
        g_Transformations[i].scale = {1.0f, 1.0f, 1.0f};
    }

    // initialize all objects
    initializeAllObjects();

    g_bSplashAnimationFlags[1] = TRUE;
    LoadSplashScreenBmp(&splashScreen, MAKEINTRESOURCEW(MY_SPLASHSCREEN_BITMAP_3));
    
    // initialize camera coordinates
    cameraCoordinates.position = {0.0f, 0.0f, 0.0f};
    cameraCoordinates.front = {0.0f, 0.0f, 0.0f};
    cameraCoordinates.right = {0.0f, 0.0f, 0.0f};
    cameraCoordinates.up = {0.0f, 1.0f, 0.0f};
    cameraCoordinates.angleX = 0.0f;
    cameraCoordinates.angleY = 270.0f;

    updateCameraVector();

    // select scene from config file
    selectSceneFromFile();

    startTimer(benchmarkTimer);

    // initialize scene
    // initializeScene();
    initializeSceneTitle();
    initializeSceneOne();
    initializeSceneTwo();
    initializeSceneThree();
    initializeSceneFour();
    initializeSceneFive();
    initializeSceneSix();
    initializeSceneSeven();
    initializeSceneEight();
    initializeSceneHandingOverLegacy();
    initializeSceneCredits();

    g_bSplashAnimationFlags[2] = TRUE;
    LoadSplashScreenBmp(&splashScreen, MAKEINTRESOURCEW(MY_SPLASHSCREEN_BITMAP_4));
    
    stopTimer(benchmarkTimer);
    fprintf(gpFILE, "Time required to initialize scene : %.4f\n", queryElapsedTimeMilliseconds(benchmarkTimer));
    resetTimer(benchmarkTimer);

    // start stopwatch
    if(gbPlay)
    {
        startTimer(stopWatch);
        g_bIsStopWatchTimerOn = TRUE;
        totalElapsedTime = (queryElapsedTimeMilliseconds(stopWatch) / 1000.0);
    }

    g_Quad = gluNewQuadric();

#if SPLASHSCREEN_DELAY
    // splashScreen disappear delay
    startTimer(splashScreenDelay);

    while(queryElapsedTimeMilliseconds(splashScreenDelay) <= 4000)
        ;

    stopTimer(splashScreenDelay);
    resetTimer(splashScreenDelay);

    releaseTimer(&splashScreenDelay);
#endif

    // hide the SplashScreen
    HideSplashScreen(&splashScreen, 500);

    // show the window
    ShowWindow(ghwnd, SW_SHOW);
    SetForegroundWindow(ghwnd);
    SetFocus(ghwnd);

    fflush(gpFILE);

    // start in fullscreen
    ToggleFullscreen();
    gbFullscreen = TRUE;

    return(0);
}

void initializeAllObjects(void)
{
    // code
    // benchmarking
    startTimer(benchmarkTimer);

    // ------------------------------------------------- initialize music track -------------------------------------------------
    // initialize OpenAL library
    initializeALLib(TRUE);

    // generate buffer
    if(getALBuffers(NUM_TRACKS, g_SceneBuffers) == FALSE)
    {
        fprintf(gpFILE, "getALBuffers() failed.\n");
        DestroyWindow(ghwnd);
    }

    // generate a source
    if(getALSources(NUM_TRACKS, g_SceneSources) == FALSE)
    {
        fprintf(gpFILE, "getALSources() failed.\n");
        DestroyWindow(ghwnd);
    }

    // --------------------------------------------------------------------------------------------------
    // FOR SCENE TITLE
    if(AttachWaveFileToALBuffer(IDWAVE_TITLE_SCENE_MUSIC, WAVE, g_SceneBuffers[0]) == FALSE)
    {
        fprintf(gpFILE, "AttachWaveFileToALBuffer() failed. Exitting now...\n");
        DestroyWindow(ghwnd);
    }

    // attach buffer to source
    if(attachALBufferToSource(&g_SceneBuffers[0], &g_SceneSources[0]) == FALSE)
    {
        fprintf(gpFILE, "attachALBufferToSource() failed. Exitting now...\n");
        DestroyWindow(ghwnd);
    }

    // --------------------------------------------------------------------------------------------------
    // FOR SCENE ONE 
    // attach a wave file from a resource to the buffer
    if(AttachWaveFileToALBuffer(IDWAVE_SCENE_ONE_MUSIC, WAVE, g_SceneBuffers[1]) == FALSE)
    {
        fprintf(gpFILE, "AttachWaveFileToALBuffer() failed.\n");
        DestroyWindow(ghwnd);
    }

    // attach buffer to source
    if(attachALBufferToSource(&g_SceneBuffers[1], &g_SceneSources[1]) == FALSE)
    {
        fprintf(gpFILE, "attachALBufferToSource() failed.\n");
        DestroyWindow(ghwnd);
    }

    // --------------------------------------------------------------------------------------------------
    // FOR SCENES TWO to FIVE
    if(AttachWaveFileToALBuffer(IDWAVE_SCENE_TWO_TO_FIVE_MUSIC, WAVE, g_SceneBuffers[3]) == FALSE)
    {
        fprintf(gpFILE, "AttachWaveFileToALBuffer() failed.\n");
        DestroyWindow(ghwnd);   
    }

    // attach buffer to source
    if(attachALBufferToSource(&g_SceneBuffers[3], &g_SceneSources[3]) == FALSE)
    {
        fprintf(gpFILE, "attachALBufferToSource() failed.\n");
        DestroyWindow(ghwnd);
    }

    // --------------------------------------------------------------------------------------------------
    // FOR LIFE SCENE and the NEW JOURNEY
    // attach a wave file from a resource to the buffer
    if(AttachWaveFileToALBuffer(IDWAVE_LIFE_SCENE_MUSIC, WAVE, g_SceneBuffers[2]) == FALSE)
    {
        fprintf(gpFILE, "AttachWaveFileToALBuffer() failed.\n");
        DestroyWindow(ghwnd);
    }

    // attach buffer to source
    if(attachALBufferToSource(&g_SceneBuffers[2], &g_SceneSources[2]) == FALSE)
    {
        fprintf(gpFILE, "attachALBufferToSource() failed.\n");
        DestroyWindow(ghwnd);
    }


    // --------------------------------------------------------------------------------------------------
    // FOR HANDING OVER LEGACY SCENE and CREDITS SCENE
    if(AttachWaveFileToALBuffer(IDWAVE_SCENE_EIGHT_AND_NINE_MUSIC, WAVE, g_SceneBuffers[4]) == FALSE)
    {
        fprintf(gpFILE, "AttachWaveFileToALBuffer() failed in file %s, at line %d.\n", __FILE__, __LINE__);
        DestroyWindow(ghwnd);
    }

    // attach buffer to source
    if(attachALBufferToSource(&g_SceneBuffers[4], &g_SceneSources[4]) == FALSE)
    {
        fprintf(gpFILE, "attachALBufferToSource() failed.\n");
        DestroyWindow(ghwnd);
    }

    // --------------------------- COMPLETE TRACK ---------------------------
    // generate a buffer
    if(getALBuffers(1, &g_CompleteTrackBuffer) == FALSE)
    {
        fprintf(gpFILE, "getALBuffers() failed.\n");
        DestroyWindow(ghwnd);
    }

    // generate a source
    if(getALSources(1, &g_CompleteTrackSource) == FALSE)
    {
        fprintf(gpFILE, "getALSources() failed.\n");
        DestroyWindow(ghwnd);
    }

    // attach a wave file from a resource to the buffer
    if(AttachWaveFileToALBuffer(IDWAVE_COMPLETE_TRACK, WAVE, g_CompleteTrackBuffer) == FALSE)
    {
        fprintf(gpFILE, "AttachWaveFileToALBuffer() failed.\n");
        DestroyWindow(ghwnd);
    }

    // attach buffer to source
    if(attachALBufferToSource(&g_CompleteTrackBuffer, &g_CompleteTrackSource) == FALSE)
    {
        fprintf(gpFILE, "attachALBufferToSource() failed.\n");
        DestroyWindow(ghwnd);
    }

    stopTimer(benchmarkTimer);
    fprintf(gpFILE, "Time required to initialize OpenAL library and music : %.4f\n", queryElapsedTimeMilliseconds(benchmarkTimer));

    resetTimer(benchmarkTimer);
    startTimer(benchmarkTimer);

    // ------------------------------------------------- initialize skybox -------------------------------------------------
    initializeSkybox();

    stopTimer(benchmarkTimer);
    fprintf(gpFILE, "Time required to initialize skybox : %.4f\n", queryElapsedTimeMilliseconds(benchmarkTimer));

    resetTimer(benchmarkTimer);
    startTimer(benchmarkTimer);

    // ------------------------------------------------- initialize saturn -------------------------------------------------
    initializeSaturn();

    stopTimer(benchmarkTimer);
    fprintf(gpFILE, "Time required to initialize saturn : %.4f\n", queryElapsedTimeMilliseconds(benchmarkTimer));

    resetTimer(benchmarkTimer);
    startTimer(benchmarkTimer);

    // ------------------------------------------------- initialize slide text -------------------------------------------------
    initializSlideText();

    // ------------------------------------------------- initialize orbit text -------------------------------------------------
    initializeJourneySceneText();

    stopTimer(benchmarkTimer);
    fprintf(gpFILE, "Time required to initialize slide text and orbit text : %.4f\n", queryElapsedTimeMilliseconds(benchmarkTimer));

    resetTimer(benchmarkTimer);
}

void uninitializeAllObjects(void)
{
    // code
    // !!!!!!!!!! NOTE : uninitialize in the REVERSE order of the initialization sequence !!!!!!!!!!
    // ------------------------------------------------- uninitialize orbit text -------------------------------------------------
    uninitializeJourneySceneText();

    // ------------------------------------------------- uninitialize slide text -------------------------------------------------
    uninitializSlideText();

    // ------------------------------------------------- uninitialize saturn -------------------------------------------------
    uninitializeSaturn();

    // ------------------------------------------------- uninitialize skybox -------------------------------------------------
    uninitializeSkybox();

    // ------------------------------------------------- uninitialize music track -------------------------------------------------
    // stop playing sources
    // for(int i = 0; i < NUM_TRACKS; i++)
    // {
    //     stopALSource(g_SceneSources[i]);
    // }

    // delete source and buffer for complete track
    deleteALSources(1, &g_CompleteTrackSource);
    deleteALBuffers(1, &g_CompleteTrackBuffer);

    // delete sources and buffers for single - single tracks    
    deleteALSources(NUM_TRACKS, g_SceneSources);
    deleteALBuffers(NUM_TRACKS, g_SceneBuffers);

    // uninitialize OpenAL library
    uninitializeALLib();
}

void selectSceneFromFile(void)
{
    // local variables
    char scene[255];
    char *sceneNames[] = {"TEST_SCENE", "SCENE_TITLE", "SCENE_OPENING", "SCENE_JOURNEY", "SCENE_OBSTACLES", 
                          "SCENE_RISE_OF_PHOENIX", "SCENE_GOLDEN_YEARS", "SCENE_LIFE", "SCENE_NEW_JOURNEY", 
                          "SCENE_HANDING_OVER_LEGACY", "SCENE_THE_END", "SCENE_CREDITS"};
    char *sceneNumbers[] = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11"};

    // select scene from config file
    fscanf(gpConfigFILE, "SCENE = %s", scene);
    if((strcmp(scene, sceneNames[0]) == 0) || (strcmp(scene, sceneNumbers[0]) == 0))
    {
        currentScene = TEST_SCENE;
    }
    else if((strcmp(scene, sceneNames[1]) == 0) || (strcmp(scene, sceneNumbers[1]) == 0))
    {
        currentScene = SCENE_TITLE;
    }
    else if((strcmp(scene, sceneNames[2]) == 0) || (strcmp(scene, sceneNumbers[2]) == 0))
    {
        currentScene = SCENE_OPENING;
    }
    else if((strcmp(scene, sceneNames[3]) == 0) || (strcmp(scene, sceneNumbers[3]) == 0))
    {
        currentScene = SCENE_JOURNEY;
    }
    else if((strcmp(scene, sceneNames[4]) == 0) || (strcmp(scene, sceneNumbers[4]) == 0))
    {
        currentScene = SCENE_OBSTACLES;
    }
    else if((strcmp(scene, sceneNames[5]) == 0) || (strcmp(scene, sceneNumbers[5]) == 0))
    {
        currentScene = SCENE_RISE_OF_PHOENIX;
    }
    else if((strcmp(scene, sceneNames[6]) == 0) || (strcmp(scene, sceneNumbers[6]) == 0))
    {
        currentScene = SCENE_GOLDEN_YEARS;
    }
    else if((strcmp(scene, sceneNames[7]) == 0) || (strcmp(scene, sceneNumbers[7]) == 0))
    {
        currentScene = SCENE_LIFE;
    }
    else if((strcmp(scene, sceneNames[8]) == 0) || (strcmp(scene, sceneNumbers[8]) == 0))
    {
        currentScene = SCENE_NEW_JOURNEY;
    }
    else if((strcmp(scene, sceneNames[9]) == 0) || (strcmp(scene, sceneNumbers[9]) == 0))
    {
        currentScene = SCENE_HANDING_OVER_LEGACY;
    }
    else if((strcmp(scene, sceneNames[10]) == 0) || (strcmp(scene, sceneNumbers[10]) == 0))
    {
        currentScene = SCENE_THE_END;
    }
    else if((strcmp(scene, sceneNames[11]) == 0) || (strcmp(scene, sceneNumbers[11]) == 0))
    {
        currentScene = SCENE_CREDITS;
    }
}

void initializeScene(void)
{
    // code
    switch(currentScene)
    {
    case TEST_SCENE:
        initializeTestScene();
        break;
    case SCENE_TITLE:
        initializeSceneTitle();
        break;
    case SCENE_OPENING:
        initializeSceneOne();
        break;
    case SCENE_JOURNEY:
        initializeSceneTwo();
        break;
    case SCENE_OBSTACLES:
        initializeSceneThree();
        break;
    case SCENE_RISE_OF_PHOENIX:
        initializeSceneFour();
        break;
    case SCENE_GOLDEN_YEARS:
        initializeSceneFive();
        break;
    case SCENE_LIFE:
        initializeSceneSix();
        break;
    case SCENE_NEW_JOURNEY:
        initializeSceneSeven();
        break;
    case SCENE_HANDING_OVER_LEGACY:
        initializeSceneHandingOverLegacy();
        break;
    case SCENE_THE_END:
        initializeSceneEight();
        break;
    case SCENE_CREDITS:
        initializeSceneCredits();
        break;
    default:
        fprintf(gpFILE, "Invalid scene %d.\n", currentScene);
        break;
    }
}

void resize(int width, int height)
{
    // avoid a divide by 0 situation
    if(height <= 0)
    {
        height = 1;
    }

    // reset the projection matrix
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    // set the viewport
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);

    // set up perspective projection
    gluPerspective(45.0, (GLdouble)width / (GLdouble)height, 0.1, 10000.0);
}

void display(void)
{
    // code
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

#ifdef DEBUG
    debugText(bDebug, FALSE);
    if(gbShowSoundStats)
    {
        soundStats(gbPlay, gbMuted, FALSE);
    }
    
    if(gbShowCameraStats)
    {
        cameraStats(FALSE);
    }
#endif

    // camera
    gluLookAt(cameraCoordinates.position.x, cameraCoordinates.position.y, cameraCoordinates.position.z, // eyex, eyey, eyez
              cameraCoordinates.position.x + cameraCoordinates.front.x, // centerx
              cameraCoordinates.position.y + cameraCoordinates.front.y, // centery
              cameraCoordinates.position.z + cameraCoordinates.front.z, // centerz
              cameraCoordinates.up.x, cameraCoordinates.up.y, cameraCoordinates.up.z); // upx, upy, upz

    switch(currentScene)
    {
    case TEST_SCENE:
        displayTestScene();
        break;
    case SCENE_TITLE:
        displaySceneTitle();
        break;
    case SCENE_OPENING:
        displaySceneOne();
        break;
    case SCENE_JOURNEY:
        displaySceneTwo();
        break;
    case SCENE_OBSTACLES:
        displaySceneThree();
        break;
    case SCENE_RISE_OF_PHOENIX:
        displaySceneFour();
        break;
    case SCENE_GOLDEN_YEARS:
        displaySceneFive();
        break;
    case SCENE_LIFE:
        displaySceneSix();
        break;
    case SCENE_NEW_JOURNEY:
        displaySceneSeven();
        break;
    case SCENE_HANDING_OVER_LEGACY:
       displaySceneHandingOverLegacy();
        break;
    case SCENE_THE_END:
        displaySceneEight();
        break;
    case SCENE_CREDITS:
        displaySceneCredits();
        break;
    default:
        fprintf(gpFILE, "Invalid scene %d.\n", currentScene);
        break;
    }

    ImGui::Render();
	ImGui_ImplOpenGL2_RenderDrawData(ImGui::GetDrawData());

    SwapBuffers(ghdc);
}

void update(void)
{
    // function prototypes
    void updateCameraUsingMouse(void);

    // code
    switch(currentScene)
    {
    case TEST_SCENE:
        updateTestScene();
        break;
    case SCENE_TITLE:
        updateSceneTitle();
        break;
    case SCENE_OPENING:
        updateSceneOne();
        break;
    case SCENE_JOURNEY:
        updateSceneTwo();
        break;
    case SCENE_OBSTACLES:
        updateSceneThree();
        break;
    case SCENE_RISE_OF_PHOENIX:
        updateSceneFour();
        break;
    case SCENE_GOLDEN_YEARS:
        updateSceneFive();
        break;
    case SCENE_LIFE:
        updateSceneSix();
        break;
    case SCENE_NEW_JOURNEY:
        updateSceneSeven();
        break;
    case SCENE_HANDING_OVER_LEGACY:
        updateSceneHandingOverLegacy();
        break;
    case SCENE_THE_END:
        updateSceneEight();
        break;
    case SCENE_CREDITS:
        updateSceneCredits();
        break;
    default:
        fprintf(gpFILE, "Invalid scene %d.\n", currentScene);
        break;
    }

    updateCameraUsingMouse();
}

void updateCameraUsingMouse(void)
{
    // code
    if(g_IsCameraRotationToggled == TRUE)
    {
        cameraCoordinates.angleX = cameraCoordinates.angleX - (step * 0.005000f * vMouseLMBEnd.y);
        cameraCoordinates.angleY = cameraCoordinates.angleY + (step * 0.005000f * vMouseLMBEnd.x); 
        
        // bounding range for angleX and angleY
        if(cameraCoordinates.angleX >= 90.0f)
        {
            cameraCoordinates.angleX = -89.0f;
        }
        else if(cameraCoordinates.angleX <= -90.0f)
        {
            cameraCoordinates.angleX = 89.0f;
        }

        if(cameraCoordinates.angleY <= 0.0f)
        {
            cameraCoordinates.angleY = 360.0f + cameraCoordinates.angleY;
        }
        else if(cameraCoordinates.angleY >= 360.0f)
        {
            cameraCoordinates.angleY = cameraCoordinates.angleY - 360.0f;
        }

        updateCameraVector();
    }
}

void uninitialize(void)
{
    // function prototypes
    void ToggleFullscreen(void);
    void uninitializeAllObjects(void);

    // code
    // if application is exitting in fullscreen
    if(gbFullscreen)
    {
        ToggleFullscreen();
        gbFullscreen = FALSE;
    }

    if (g_Quad)
    {
        gluDeleteQuadric(g_Quad);
        g_Quad = NULL;
    }

#ifdef DEBUG
    // uninitialize camera stats
    cameraStats(TRUE);

    // uninitialize sound stats
    soundStats(0, 0, TRUE);

    // uninitialize debug text
    debugText(0, TRUE);
#endif

#ifdef ENABLE_CUDA
    // uninitialize CUDA library
    uninitializeCUDALibrary();
#endif

    // uninitialize the scenes
    switch (currentScene)
    {
        case SCENE_TITLE:
            uninitializeSceneTitle();
            break;
        case SCENE_OPENING:
            uninitializeSceneOne();
            break;
        case SCENE_JOURNEY:
            uninitializeSceneTwo();
            break;
        case SCENE_OBSTACLES:
            uninitializeSceneThree();
            break;
        case SCENE_RISE_OF_PHOENIX:
            uninitializeSceneFour();
            break;
        case SCENE_GOLDEN_YEARS:
            uninitializeSceneFive();
            break;
        case SCENE_LIFE:
            uninitializeSceneSix();
            break;
        case SCENE_NEW_JOURNEY:
            uninitializeSceneSeven();
            break;
        case SCENE_HANDING_OVER_LEGACY:
            uninitializeSceneHandingOverLegacy();
            break;
        case SCENE_THE_END:
            uninitializeSceneEight();
            break;
        case SCENE_CREDITS:
            uninitializeSceneCredits();
            break;
        default:
            fprintf(gpFILE, "Invalid scene %d.\n", currentScene);
            break;
    }

    // uninitialize all objects
    uninitializeAllObjects();

    // make the RC not current
    if(wglGetCurrentContext() == ghrc)
    {
        wglMakeCurrent(NULL, NULL);
    }

    // delete the RC
    if(ghrc)
    {
        wglDeleteContext(ghrc);
        ghrc = NULL;
    }

    // release the DC
    if(ghdc)
    {
        ReleaseDC(ghwnd, ghdc);
        ghdc = NULL;
    }

    // destroy window
    if(ghwnd)
    {
        DestroyWindow(ghwnd);
        ghwnd = NULL;
    }

    // delete the SplashScreen
    // DeleteSplashScreen(&splashScreen);

    // release benchmark timer
    stopTimer(benchmarkTimer);

    if(benchmarkTimer)
    {
        releaseTimer(&benchmarkTimer);
        benchmarkTimer = NULL;
    }

    // release timer
    stopTimer(stopWatch);
    
    if(stopWatch)
    {
        releaseTimer(&stopWatch);
        stopWatch = NULL;
    }

    // close the config file
    if(gpConfigFILE)
    {
        fclose(gpConfigFILE);
        gpConfigFILE = NULL;
    }

    // close the log file
    if(gpFILE)
    {
#ifdef DEBUG
        fprintf(gpFILE, "gbShowSoundStats = %d\n", gbShowSoundStats);
#endif

        // for camera
        fprintf(gpFILE, "// cameraCoordinates\n");
        fprintf(gpFILE, "{\n\t{%ff, %ff, %ff},\n\t{%ff, %ff, %ff},\n\t{%ff, %ff, %ff},\n\t{%ff, %ff, %ff},\n\t%ff,\n\t%ff\n}\n", 
                cameraCoordinates.position.x, cameraCoordinates.position.y, cameraCoordinates.position.z,
                cameraCoordinates.front.x, cameraCoordinates.front.y, cameraCoordinates.front.z,
                cameraCoordinates.right.x, cameraCoordinates.right.y, cameraCoordinates.right.z,
                cameraCoordinates.up.x, cameraCoordinates.up.y, cameraCoordinates.up.z,
                cameraCoordinates.angleX,
                cameraCoordinates.angleY);

        // for object
        fprintf(gpFILE, "\n************** g_Transformations[%d] values **************\n", NUM_TRANSFORMATION_ELEMENTS);
        for(int i = 0; i < NUM_TRANSFORMATION_ELEMENTS; i++)
        {
            fprintf(gpFILE, "// transformations[%d]\n", i);
            fprintf(gpFILE, "{\n\t{%ff, %ff, %ff},\n\t{%ff, %ff, %ff},\n\t{%ff, %ff, %ff}\n}\n", 
                g_Transformations[i].translation.x, g_Transformations[i].translation.y, g_Transformations[i].translation.z,
                g_Transformations[i].rotation.x, g_Transformations[i].rotation.y, g_Transformations[i].rotation.z,
                g_Transformations[i].scale.x, g_Transformations[i].scale.y, g_Transformations[i].scale.z);
        
            fflush(gpFILE);
        }
        
        fprintf(gpFILE, "*******************************************************************************\n\n");

        // temporary solution to credit scene problem
        fprintf(gpFILE, "// translation vals\n");
        for(int i = 0; i < NUM_TRANSFORMATION_ELEMENTS; i++)
        {
            fprintf(gpFILE, "%ff,\n", g_Transformations[i].translation.x);
        }

        fprintf(gpFILE, "Program ended successfully.\n");
        fclose(gpFILE);
        gpFILE = NULL;
    }
}

void TransformUsingKeyboard(char cPressedKey)
{
    switch(cPressedKey)
    {
    // ******************* FOR X *******************
    case 'X':
        switch(g_CurrentTransformationMode)
        {
        case TRANSLATION:
            g_Transformations[g_iCurrentTransformation].translation.x += step * TRANSLATION_SPEED;
            break;
        case ROTATION:
            g_Transformations[g_iCurrentTransformation].rotation.x += step * ROTATION_SPEED;
            break;
        case SCALE:
            g_Transformations[g_iCurrentTransformation].scale.x += step * SCALE_SPEED;
            break;
        }
        break;
    case 'x':
        switch(g_CurrentTransformationMode)
        {
        case TRANSLATION:
            g_Transformations[g_iCurrentTransformation].translation.x -= step * TRANSLATION_SPEED;
            break;
        case ROTATION:
            g_Transformations[g_iCurrentTransformation].rotation.x -= step * ROTATION_SPEED;
            break;
        case SCALE:
            g_Transformations[g_iCurrentTransformation].scale.x -= step * SCALE_SPEED;
            break;
        }
        break;
    
    // ******************* FOR Y *******************
    case 'Y':
        switch(g_CurrentTransformationMode)
        {
        case TRANSLATION:
            g_Transformations[g_iCurrentTransformation].translation.y += step * TRANSLATION_SPEED;
            break;
        case ROTATION:
            g_Transformations[g_iCurrentTransformation].rotation.y += step * ROTATION_SPEED;
            break;
        case SCALE:
            g_Transformations[g_iCurrentTransformation].scale.y += step * SCALE_SPEED;
            break;
        }
        break;
    case 'y':
        switch(g_CurrentTransformationMode)
        {
        case TRANSLATION:
            g_Transformations[g_iCurrentTransformation].translation.y -= step * TRANSLATION_SPEED;
            break;
        case ROTATION:
            g_Transformations[g_iCurrentTransformation].rotation.y -= step * ROTATION_SPEED;
            break;
        case SCALE:
            g_Transformations[g_iCurrentTransformation].scale.y -= step * SCALE_SPEED;
            break;
        }
        break;
    
    // ******************* FOR Z *******************
    case 'Z':
        switch(g_CurrentTransformationMode)
        {
        case TRANSLATION:
            g_Transformations[g_iCurrentTransformation].translation.z += step * TRANSLATION_SPEED;
            break;
        case ROTATION:
            g_Transformations[g_iCurrentTransformation].rotation.z += step * ROTATION_SPEED;
            break;
        case SCALE:
            g_Transformations[g_iCurrentTransformation].scale.z += step * SCALE_SPEED;
            break;
        }
        break;
    case 'z':
        switch(g_CurrentTransformationMode)
        {
        case TRANSLATION:
            g_Transformations[g_iCurrentTransformation].translation.z -= step * TRANSLATION_SPEED;
            break;
        case ROTATION:
            g_Transformations[g_iCurrentTransformation].rotation.z -= step * ROTATION_SPEED;
            break;
        case SCALE:
            g_Transformations[g_iCurrentTransformation].scale.z -= step * SCALE_SPEED;
            break;
        }
        break;
    }
}

// for manipulating sound playback on keypress
void PauseOrPlayALSource(void)
{
	// code
    if(g_ActiveALSource.sourceID == 0)
    {
        return ;
    }

    switch(g_ActiveALSource.IsSourcePaused)
    {
    case TRUE:
        playALSource(g_ActiveALSource.sourceID);
        g_ActiveALSource.IsSourcePaused = FALSE;
        break;
    case FALSE:
        pauseALSource(g_ActiveALSource.sourceID);
        g_ActiveALSource.IsSourcePaused = TRUE;
        break;
    }
}

void MuteOrUnmuteALSource(void)
{
	// code
    if(g_ActiveALSource.sourceID == 0)
    {
        return ;
    }

    switch(g_ActiveALSource.IsSourceMuted)
    {
    case TRUE:
        unmuteALSource(g_ActiveALSource.sourceID, g_ActiveALSource.volume);
        g_ActiveALSource.IsSourceMuted = FALSE;
        break;
    case FALSE:
        muteALSource(g_ActiveALSource.sourceID, &(g_ActiveALSource.volume));
        g_ActiveALSource.IsSourceMuted = TRUE;
        break;
    }
}

void IncreaseOrDecreaseVolumeOfALSources(BOOL bIncrease)
{
	// code
	// if source doesn't exist, return
	if(g_ActiveALSource.sourceID == 0)
	{
		return ;
	}

	// increase / decrease volume
    if(bIncrease)
    {
        if(g_Volume < 5.0f)
            g_Volume = g_Volume + 0.05f;
    }
    else
    {
        if(g_Volume > 0.0f)
            g_Volume = g_Volume - 0.05f;
    }

    if(g_Volume > 5.0f)
        g_Volume = 5.0f;
    else if(g_Volume < 0.0f)
        g_Volume = 0.0f;

    setALSourceVolume(g_ActiveALSource.sourceID, g_Volume);
}
