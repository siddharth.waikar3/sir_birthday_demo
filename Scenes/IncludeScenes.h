#pragma once

// include all scene related header files
#include "OpeningScene.h"
#include "JourneyScene.h"
#include "ObstaclesScene.h"
#include "RiseOfPhoenixScene.h"
#include "GoldenYearsScene.h"
#include "LifeScene.h"
#include "NewJourneyScene.h"
#include "HandingOverLegacyScene.h"
#include "TheEndScene.h"
#include "TitleScene.h"
#include "CreditsScene.h"
#include "TestScene.h"

