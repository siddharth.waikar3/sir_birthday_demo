// UNICODE
#ifndef UNICODE
	#define UNICODE
#endif

#pragma once

// header files
#include <windows.h> // for the Standard Win32 API
#include <stdio.h>   // for file IO
#include <stdlib.h>  // for malloc(), free() and exit()

#define _USE_MATH_DEFINES // for M_PI constant
#include <math.h>         // for math functions like sinf(), cosf(), fabsf(), floorf(), ceilf(), powf() and so on...

// OpenGL header files
#include <gl/GL.h>
#include <gl/GLU.h> // for the GLU (Graphics Library Utility) native library

// OpenAL header files
#include <al.h>
#include "OAL.h"

// macros
// global
#ifdef NODEBUG
	#define HIGH_PERF_PC 1
	#define SPLASHSCREEN_DELAY 1
	#define SHOW_ROCKS_GLOBAL 1
#else
	#define HIGH_PERF_PC 0 
	#define SPLASHSCREEN_DELAY 0
	#define SHOW_ROCKS_GLOBAL 0
#endif

#define BOUNDING_SPHERE_RADIUS 0.3f

// FPS
#define FPS 60.0f

// for music
#define MUSIC_OFFSET 33

// music ids
#define IDWAVE_SCENE_ONE_MUSIC    	      98944
#define IDWAVE_LIFE_SCENE_MUSIC   	      98945
#define IDWAVE_TITLE_SCENE_MUSIC  	      98946
#define IDWAVE_SCENE_TWO_TO_FIVE_MUSIC    98947
#define IDWAVE_SCENE_EIGHT_AND_NINE_MUSIC 98948
#define IDWAVE_END_SCENE_VOICEOVER_MUSIC  98949
#define IDWAVE_END_SCENE_SURPRISE_TRACK   98950

#define IDWAVE_THE_END_SCENE_TEST_MUSIC 1421
#define IDWAVE_COMPLETE_TRACK 3268923

#define WAVE 25419
#define MP3 32522

#define TRANSLATION_SPEED 0.5f
#define ROTATION_SPEED 1.0f
#define SCALE_SPEED 0.1f

#define DEBUGTEXT_DISPLAYLISTS_OFFSET 10001

#define WHITE_COLOR 1.0f, 1.0f, 1.0f, 1.0f
#define RED_COLOR 1.0f, 0.0f, 0.0f, 1.0f
#define GREEN_COLOR 0.0f, 1.0f, 0.0f, 1.0f
#define BLUE_COLOR 0.0f, 0.0f, 1.0f, 1.0f


#define GOLD_COLOR_LIGHT 1.000000f, 0.843137f, 0.000000f, 1.0f
#define GOLD_COLOR_DARK 0.458824f, 0.388235f, 0.000000f, 1.0f

#define GOLD_COLOR_LIGHT_RGB 1.000000f, 0.843137f, 0.000000f
#define GOLD_COLOR_DARK_RGB 0.458824f, 0.388235f, 0.000000f

#define NUM_TRANSFORMATION_ELEMENTS 50
#define NUM_TRACKS 6

//flasgs for Cube
#define FRONT_FACE 1
#define RIGHT_FACE 2
#define LEFT_FACE 4
#define BACK_FACE 8
#define TOP_FACE 16
#define BOTTOM_FACE 32
#define ALL_FACES (FRONT_FACE | RIGHT_FACE | LEFT_FACE | BACK_FACE | TOP_FACE | BOTTOM_FACE)

// flags for lerp animation
#define LERP_X 0x00000001
#define LERP_Y 0x00000002
#define LERP_Z 0x00000004

#define LERP_X_ROT 0x00000008
#define LERP_Y_ROT 0x00000010
#define LERP_Z_ROT 0x00000020

#define LERP_X_SCALE 0x00000040
#define LERP_Y_SCALE 0x00000080
#define LERP_Z_SCALE 0x00000100

#define LERP_GREATER_THAN 0x00000200
#define LERP_LESS_THAN 0x0000400

#define LERP_NOT_REACHED FALSE
#define LERP_REACHED TRUE

// flags for fade effect
#define FADE_IN_FLAG 0x00000001
#define FADE_OUT_FLAG 0x00000002

// revolution flags
#define REVOLUTION_INCREMENT_ANGLE 0x00000001
#define REVOLUTION_DECREMENT_ANGLE 0x00000002

// show ring rocks
#define SHOWROCKSON 1
#define SHOWROCKSOFF 0

// globle variables
extern struct Transformations g_Transformations[NUM_TRANSFORMATION_ELEMENTS];
extern ALuint g_CompleteTrackSource;
extern long double totalElapsedTime;
extern ALuint g_SceneSources[NUM_TRACKS];

// struct definitions
typedef struct tagDim2
{
    GLfloat w, h;
} Dim2, Dimensions;

typedef struct tagPoint
{
	GLfloat x, y, z, s, t;
} Point, Vector, Position;

typedef struct tagVec3
{
	GLfloat x, y, z;
} Vec3;

typedef struct tagRange3
{
	Vec3 start;
	Vec3 end;
} Range3;

typedef struct tagRange
{
	GLfloat start, end;	
} Range;

typedef struct tagVector2D
{
    // vector position
    Position pos;

    // vector directions
    GLfloat up;
    GLfloat down;
    GLfloat left;
    GLfloat right;
	GLfloat in;
	GLfloat out;
} Vector2D;

typedef struct tagColor
{
    GLfloat r, g, b, alpha;
} Color;

struct Translation
{
	GLfloat x;
	GLfloat y;
	GLfloat z;
};

struct Rotation
{
	GLfloat x;
	GLfloat y;
	GLfloat z;
};

struct Scale
{
	GLfloat x;
	GLfloat y;
	GLfloat z;
};

struct Transformations
{
	struct Translation translation;
	struct Rotation rotation;
	struct Scale scale;
};

struct ALSource
{   
    ALuint sourceID;
    BOOL IsSourcePaused;
    BOOL IsSourceMuted;
    ALfloat volume;
};

typedef struct tagLerp_t
{
	GLfloat x, x0, x1, speed, tVal;
	BOOL bReached;
} Lerp_t;

typedef struct tagLerpTransformations
{
	struct Transformations current;
	struct Transformations begin;
	struct Transformations end;
	GLfloat tVal;
	GLfloat speed;
	BOOL bReached;
} LerpTransformations;

// enums
enum TransformationMode
{
	TRANSLATION,
	ROTATION,
	SCALE
};

enum Scene
{
	TEST_SCENE,
	SCENE_TITLE,
	SCENE_OPENING,
	SCENE_JOURNEY,
	SCENE_OBSTACLES,
	SCENE_RISE_OF_PHOENIX,
	SCENE_GOLDEN_YEARS,
	SCENE_LIFE,
	SCENE_NEW_JOURNEY,
	SCENE_HANDING_OVER_LEGACY,
	SCENE_THE_END,
	SCENE_CREDITS
};

// function prototypes
GLfloat getInterpolatedValue(GLfloat x0, GLfloat x1, GLfloat y0, GLfloat y1, GLfloat x);
void applyTransformations(Transformations transformations);
void applyTransformationsWithoutRotation(Transformations transformations);
void addVec(Vector *vec1, Vector *vec2);
void subVec(Vector *vec1, Vector *vec2);
void debugText(int iDebugMode, BOOL bUninitialize);
BOOL lerpAnimation(struct Transformations* current, struct Transformations* inital, struct Transformations* end, GLfloat* tval, GLfloat speed);
// colors : front back left right top bottom
void drawBasicCube(GLfloat *verticesColors[24]); 
// draws shadow quad
void drawShadow(GLfloat fAlpha);
// for drawing text using wgl
void draw_text_wgl(HDC hdc, UINT iStringLength, const char *str, int iIdentifier, BOOL bUninitialize);
// sphere
Point* getSphereVertices(GLuint lats, GLuint longs);
void drawSphereVertices(Point *vertices, GLuint lats, GLuint longs, GLfloat color[4], GLfloat colorGradIncrement, GLenum mode, GLfloat radius);
void drawSphereVerticesWithTexture(Point *vertices, GLuint lats, GLuint longs, GLuint texture, GLenum mode, GLfloat radius);
void drawSphereVerticesWithTextureSpecial(Point *vertices, GLuint lats, GLuint longs, GLuint texture, GLenum mode, GLfloat radius);
// circle
Point* getCircleVertices(GLfloat radius);
Vec3* getCircleVerticesXZ(GLfloat radius, GLfloat increment, BOOL bCounterClockwise);
void drawCircleVertices(Point *circleVertices, GLenum mode);
void drawQuadVertices(Point* vertices);
void drawBasicCubeEx(GLfloat* verticesColors[24], ULONG face);
// for all vertices
void freeVertices(Point **vertices);
// for Fade-in
void fadeInEffect(GLfloat alpha);
void fadeInEffect_v2(Color color);
// for Fade-out
void fadeOutEffect(GLfloat alpha);
void fadeOutEffect_v2(Color color);

// OpenAL library's library's wrapper functions 
BOOL AttachWaveFileToALBuffer(DWORD dwResourceID, DWORD dwResourceType, ALuint buffer);
BOOL AttachWaveFileToALBuffer_v2(DWORD dwResourceID, DWORD dwResourceType, ALuint buffer, ALuint wavFileFreqHz);
// void RegisterALSourceForPlayback(ALuint source, BOOL bPaused, BOOL bMuted);
// BOOL UnregisterALSourceForPlayback(ALuint source);
void SetCurrentSourceForStats(ALuint source, BOOL bPaused, BOOL bMuted);

// for resetting the global timer
void ResetGlobalTimer(void);

// for updating alpha
BOOL updateAlpha(GLfloat *alpha, GLfloat speed, UINT dwFlag);

// for updating object alpha
BOOL updateObjectAlpha(GLfloat *alpha, GLfloat speed, UINT dwFlag);

// for updating any rotation value (and looping it)
void updateRotationValue(GLfloat *val, GLfloat speed);

// for updating any revolution value (and looping it)
void updateRevolutionValue(GLfloat *val, GLfloat speed, Translation *translation, GLfloat orbitRadius, GLuint uFlag);

// for linear interpolating any value
void lerpMyValue(Lerp_t *lerp_val, GLfloat condition);

//! updated lerp animation function
//! @param lerp a struct which holds all the necessary things required to lerp an object
//! @param condition tVal condition to lerp to. Can be anything from 0.0 to 1.0  
void lerpAnimation_v2(LerpTransformations *lerp, GLfloat condition);

//! for drawing graded text
//! @param str text to draw
//! @param color color for "front" of the text
//! @param gradientColor gradient color for "back" of the text, for that 3D-ish effect
//! @param dwOffset display lists offset of a font
void drawGradedText(const char *str, Color color, Color gradientColor, DWORD dwOffset);

// for sound stats
void soundStats(BOOL bPlaying, BOOL bMuted, BOOL bUninitialize);

// for camera
void cameraStats(BOOL bUninitialize);
