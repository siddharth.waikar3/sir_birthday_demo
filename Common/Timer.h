#pragma once

// header files
#include "Common.h"

// link timer library
#pragma comment(lib, "./Resources/lib/Timer.lib")

// struct definition for helper timer
typedef struct tagStopWatchTimer
{
    LARGE_INTEGER startTime;
    LARGE_INTEGER endTime;
    BOOL bTimerRunning;
    double totalElapsedTime; // in milliseconds
    double frequency; // in milliseconds
} StopWatchTimer;

// function declarations for helper timer
BOOL initializeTimer(StopWatchTimer **timer);
void startTimer(StopWatchTimer *timer);
void stopTimer(StopWatchTimer *timer);
void resetTimer(StopWatchTimer *timer);
void releaseTimer(StopWatchTimer **timer);
double queryElapsedTimeMilliseconds(StopWatchTimer *timer);
