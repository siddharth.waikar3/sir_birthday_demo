#pragma once

// header files
#include "./CUDACommon.h"
#include "./SphereCollision.h"

// function declarations
// extern "C" __declspec(dllexport) BOOL initializeCUDALibrary(void);
// extern "C" __declspec(dllexport) void uninitializeCUDALibrary(void);
// extern "C" __declspec(dllexport) BOOL solveCollisionUsingCUDA(struct SphereAttribs *sphereAttribs, int numSpheres);
