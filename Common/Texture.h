#pragma once

#include "..\Common\Common.h"

// function prototypes
BOOL LoadTexture(GLuint *textureName, const char *filename);
BOOL LoadCubeMapTexture(GLuint *textureName, const char *filename);
void drawTexQuad(GLuint textureName);
void drawTexCube(GLuint textureName);
void drawTexCube_v2(GLuint *textures);
void DeleteTexture(GLuint *textureName);
