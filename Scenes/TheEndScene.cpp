// header files
#include "..\Common\Common.h"
#include "..\Common\Texture.h" // for textures
#include "..\Common\RenderText.h" // for text
#include "..\Objects\SlideText.h"

// macros
// texture paths
#define SIR_OUTLINE_TEXTURE_PATH "./Resources/textures/TheEndImages/SirOutline.png"
#define SIR_OUTLINE_WIDTH 1284.0f
#define SIR_OUTLINE_HEIGHT 1264.0f

// for text
#define THE_END_SCENE_TEXT_TRANSLATION_SPEED 1.0f / (FPS * 3.0f)
#define THE_END_SCENE_SIR_TEXT_TRANSLATION_SPEED (18.0f / 4.0f)

#define TIMER_ID_HIDE_WELL_WISHER_TEXT 1000
#define TIMER_ID_HIDE_WHAT_SIR_THINKS_TEXT 2000
#define TIMER_ID_SHOW_SIR_TEXT 3000
#define TIMER_ID_THE_END_QUESTION_MARK_DELAY 4000

#define COOLVETICA_END_SCENE_DISPLAYLISTS_OFFSET 191891

// colors 
#define THE_END_SCENE_CYAN_COLOR 0.0f, 1.0f, 1.0f
#define THE_END_SCENE_DARK_CYAN_COLOR 0.0f, 0.25f, 0.25f

// scene states enum 
enum TheEndSceneStates
{
    BEGIN,
    HIDE_WELL_WISHER_TEXT,
    SHOW_WHAT_SIR_THINKS_TEXT,
    HIDE_WHAT_SIR_THINKS_TEXT,
    SHOW_SIR_OUTLINE,
    HIDE_SIR_OUTLINE,
    SHOW_SIR_TEXT,
    THE_END,
	THE_END_QUESTION_MARKS,
	END
};

// important global variables
enum TheEndSceneStates theEndSceneState;
extern FILE *gpFILE;
extern HWND ghwnd; // this global variable is required to recognize which window will we set the timer for
extern HDC ghdc;

// for textures
GLuint sirText[4];
char *sirTextPaths[4] = 
{
    "./Resources/textures/TheEndImages/ARTR.png",
    "./Resources/textures/TheEndImages/ERTR.png",
    "./Resources/textures/TheEndImages/AVSeminar.png",
    "./Resources/textures/TheEndImages/SanskarVargText.png"
};

Dim2 sirTextDimensions[4] = 
{
    {747.0f, 210.0f},
    {1346.0f, 449.0f},
    {696.0f, 210.0f},
    {870.0f, 215.0f}
};

GLuint sirOutline = 0;

// for music
ALuint theEndSceneBuffers[2];
ALuint theEndSceneSources[2];

// for text
LerpTransformations sirsWellWishersText = 
{
    // current
    {
        -5.659999f, 0.000000f, -11.550014f,
        0.000000f, 0.000000f, 0.000000f,
        1.000000f, 1.000000f, 1.000000f
    },

    // begin
    {
        -5.659999f, 0.000000f, -11.550014f,
        0.000000f, 0.000000f, 0.000000f,
        1.000000f, 1.000000f, 1.000000f
    },

    // end
    {
        -5.659999f, 0.000000f, -8.050014f,
        0.000000f, 0.000000f, 0.000000f,
        1.000000f, 1.000000f, 1.000000f
    },

    // tVal
    0.0f,

    // speed
    THE_END_SCENE_TEXT_TRANSLATION_SPEED
};

Lerp_t sirOutlineQuadY = {-0.025000f, -0.025000f, -2.0f, 1.0f / (FPS * 6.5f), 0.0f};
Lerp_t sirTextZTranslation = {-20.0f, -20.0f, 0.1f, 1.0f / (FPS * THE_END_SCENE_SIR_TEXT_TRANSLATION_SPEED), 0.0f};

GLfloat theEndTextAlpha = 1.0f;
GLfloat sirTextAlpha = 1.0f;
GLfloat whatSirsWellWishersThinkAlpha = 0.0f;
GLfloat whatSirThinksAlpha = 0.0f;
GLfloat endSceneFadeAlpha = 0.0f;

GLuint sirTextIdx = 0;
GLint iQuestionMarkCount = 0;

BOOL endSceneTimerStarted = FALSE;

RT_Library coolveticaEndScene;

// timer callback
void switchToNextState(HWND hwnd, UINT unnamedParam2, UINT_PTR timerIdentifier, DWORD unnamedParam4);

// function definitions
void initializeSceneEight(void)
{    
    // code
    // initialize coolvetica font
    RT_Initialize_Library(&coolveticaEndScene, ghdc, TEXT("./Resources/fonts/coolvetica rg.otf"), TEXT("Coolvetica Rg"), RT_FW_DONTCARE, 0, COOLVETICA_END_SCENE_DISPLAYLISTS_OFFSET);

    // load textures
    if(LoadTexture(&sirOutline, SIR_OUTLINE_TEXTURE_PATH) == FALSE)
    {
        fprintf(gpFILE, "Couldn't load image.\n");
        DestroyWindow(ghwnd);
    }

    for(int i = 0; i < 4; i++)
    {
        if(LoadTexture(&sirText[i], sirTextPaths[i]) == FALSE)
        {
            fprintf(gpFILE, "LoadTexture() failed in file %s at line %d.\n", __FILE__, __LINE__);
            DestroyWindow(ghwnd);
        }
    }

    // initialize fade alpha
    endSceneFadeAlpha = 0.0f;

    // initialize voiceover and surprise track for this scene
    // get AL buffers
    if(getALBuffers(2, theEndSceneBuffers) == FALSE)
    {
        fprintf(gpFILE, "getALBuffers() failed in file %s, line %d.\n", __FILE__, __LINE__);
        DestroyWindow(ghwnd);
    }

    // get AL sources
    if(getALSources(2, theEndSceneSources) == FALSE)
    {
        fprintf(gpFILE, "getALSources() failed in file %s, line %d.\n", __FILE__, __LINE__);
        DestroyWindow(ghwnd);
    }

    // attach wave files to buffers
    if(AttachWaveFileToALBuffer(IDWAVE_END_SCENE_VOICEOVER_MUSIC, WAVE, theEndSceneBuffers[0]) == FALSE)
    {
        fprintf(gpFILE, "AttachWaveFileToALBuffer() failed in file %s, line %d.\n", __FILE__, __LINE__);
        DestroyWindow(ghwnd);
    }

    if(AttachWaveFileToALBuffer(IDWAVE_END_SCENE_SURPRISE_TRACK, WAVE, theEndSceneBuffers[1]) == FALSE)
    {
        fprintf(gpFILE, "AttachWaveFileToALBuffer() failed in file %s, line %d.\n", __FILE__, __LINE__);
        DestroyWindow(ghwnd);
    }

    // attach voiceover buffer to source
    if(attachALBufferToSource(&theEndSceneBuffers[0], &theEndSceneSources[0]) == FALSE)
    {
        fprintf(gpFILE, "attachALBufferToSource() failed in file %s, line %d.\n", __FILE__, __LINE__);
        DestroyWindow(ghwnd);
    }

    // attach surprise track buffer to source
    if(attachALBufferToSource(&theEndSceneBuffers[1], &theEndSceneSources[1]) == FALSE)
    {
        fprintf(gpFILE, "attachALBufferToSource() failed in file %s, line %d.\n", __FILE__, __LINE__);
        DestroyWindow(ghwnd);
    }

    // initialize scene state   
    theEndSceneState = BEGIN;
}

void displaySceneEight(void)
{
    // function prototypes
    void drawPhotoQuad(GLuint texture, GLfloat width, GLfloat height);
    void drawQuadInAspectRatio(GLfloat width, GLfloat height);
    void drawShadowyText(const char *str, Color color, Color gradientColor, int iNumShadows);

    // code
    // draw "What Sir's Well Wishers Think"
    if (theEndSceneState >= BEGIN && theEndSceneState <= HIDE_WELL_WISHER_TEXT)
    {
        glPushMatrix();
        {
            applyTransformations(sirsWellWishersText.current);
            drawSirsWellWishersThink({THE_END_SCENE_CYAN_COLOR, whatSirsWellWishersThinkAlpha}, 
                                     {THE_END_SCENE_DARK_CYAN_COLOR, whatSirsWellWishersThinkAlpha});
        }
        glPopMatrix();
    }

    // draw "What Sir Thinks"
    else if (theEndSceneState >= SHOW_WHAT_SIR_THINKS_TEXT && theEndSceneState <= SHOW_SIR_OUTLINE)
    {
        glPushMatrix();
        {
            // applyTransformations(whatSirThinksText.current);
            glTranslatef(-3.000000f, 3.900000f, -11.550000f);
            drawWhatsSirThink({THE_END_SCENE_CYAN_COLOR, whatSirThinksAlpha},
                              {THE_END_SCENE_DARK_CYAN_COLOR, whatSirThinksAlpha});
        }
        glPopMatrix();
    }

    // draw Sir's outline and the quad to hide it
    if(theEndSceneState == SHOW_SIR_OUTLINE)
    {
        glPushMatrix();
        {
            glTranslatef(0.050000f, -0.250000f, -0.600000f);

            // outline
            glPushMatrix();
            {
                glTranslatef(0.000000f, -0.025000f, -2.400000f);
                glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
                drawPhotoQuad(sirOutline, SIR_OUTLINE_WIDTH, SIR_OUTLINE_HEIGHT);
            }
            glPopMatrix();

            // quad
            glPushMatrix();
            {
                glTranslatef(0.000000f, sirOutlineQuadY.x, -2.400000f);
                glColor4f(0.0f, 0.0f, 0.0f, 1.0f);
                drawQuadInAspectRatio(SIR_OUTLINE_WIDTH, SIR_OUTLINE_HEIGHT);
            }
            glPopMatrix();
        }
        glPopMatrix();
    }

    // draw ARTR, ERTR, Audio-Video Seminar and Sanskar Varg text 
    if(theEndSceneState == SHOW_SIR_TEXT)
    {
        glPushMatrix();
        {
            glTranslatef(0.0f, 0.0f, sirTextZTranslation.x);

            // shadow
            glPushMatrix();
            {
                glTranslatef(0.000000f, 0.100000f, -0.250000f);
                glScalef(1.5f, 1.5f, 1.5f);

                glColor4f(1.0f, 1.0f, 1.0f, sirTextAlpha / 4.0f);
                drawPhotoQuad(sirText[sirTextIdx], sirTextDimensions[sirTextIdx].w, sirTextDimensions[sirTextIdx].h);
            }
            glPopMatrix();

            // main text
            glColor4f(1.0f, 1.0f, 1.0f, sirTextAlpha);
            drawPhotoQuad(sirText[sirTextIdx], sirTextDimensions[sirTextIdx].w, sirTextDimensions[sirTextIdx].h);
        }
        glPopMatrix();

    }

    // draw "The End???"
    if(theEndSceneState == THE_END)
    {
        static char *str;
    
        switch(iQuestionMarkCount)
        {
        case 0:
            str = "";
            break;
        case 1:
            str = "?";
            break;
        case 2:
            str = "??";
            theEndTextAlpha = 0.5f;
            break;
        case 3:
            str = "???";
            theEndTextAlpha = 0.15f;
            break;
        default:
            str = "???";
            break;
        }

        glPushMatrix();
        {
            glTranslatef(-1.400000f, -0.200000f, -5.0f);
            drawGradedText("The End", {1.0f, 1.0f, 1.0f, theEndTextAlpha}, {0.4f, 0.4f, 0.4f, theEndTextAlpha}, COOLVETICA_END_SCENE_DISPLAYLISTS_OFFSET);
        }
        glPopMatrix();

        if(iQuestionMarkCount)
        {
            glPushMatrix();
            {
                glTranslatef(1.599999f, -0.200000f, -5.000000f);
                drawGradedText(str, {1.0f, 1.0f, 1.0f, 1.0f}, {0.4f, 0.4f, 0.4f, 1.0f}, COOLVETICA_END_SCENE_DISPLAYLISTS_OFFSET);
            }
            glPopMatrix();
        }
    }
    
    fadeInEffect(endSceneFadeAlpha);
}

void drawPhotoQuad(GLuint texture, GLfloat width, GLfloat height)
{
    // local variables
    GLfloat aspect;
    GLfloat x = 1.0f;
    GLfloat y = 1.0f;

    // code
    if(width <= height)
    {
        aspect = height / width;
        y = y * aspect;
    }
    else
    {
        aspect = width / height;
        x = x * aspect;
    }

    glBindTexture(GL_TEXTURE_2D, texture);
    glBegin(GL_QUADS);
    {
        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(x, y, 0.0f);

        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-x, y, 0.0f);

        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(-x, -y, 0.0f);

        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(x, -y, 0.0f);
        
    }
    glEnd();
    glBindTexture(GL_TEXTURE_2D, 0);
}

void drawQuadInAspectRatio(GLfloat width, GLfloat height)
{
    // local variables
    GLfloat aspect;
    GLfloat x = 1.0f;
    GLfloat y = 1.0f;

    // code
    if(width <= height)
    {
        aspect = height / width;
        y = y * aspect;
    }
    else
    {
        aspect = width / height;
        x = x * aspect;
    }

    glBegin(GL_QUADS);
    {
        glVertex3f(x, y, 0.0f); 
        glVertex3f(-x, y, 0.0f);
        glVertex3f(-x, -y, 0.0f);
        glVertex3f(x, -y, 0.0f);    
    }
    glEnd();

}

void updateSceneEight(void)
{   
    // function prototypes
    void uninitializeSceneEight(void);

    // local variables
    static BOOL bOnce = FALSE; 
    static BOOL bPlaySurpriseTrackOnce = TRUE;
    static BOOL bFadeOutSanskarVargText = FALSE;

    // code
    switch (theEndSceneState)
    {
    case BEGIN:
        if(!bOnce)
        {
            SetCurrentSourceForStats(theEndSceneSources[0], FALSE, FALSE);
            playALSource(theEndSceneSources[0]);
            bOnce = TRUE;
        }

        // fade in
        updateObjectAlpha(&whatSirsWellWishersThinkAlpha, 1.0f / (FPS * 1.5f), FADE_IN_FLAG);

        // animate first text
        lerpAnimation_v2(&sirsWellWishersText, 1.0f);

        // if text reached its destination, set delay timer which will switch to next state
        if(sirsWellWishersText.bReached)
        {
            // timer code
            if (endSceneTimerStarted == FALSE)
            {
                SetTimer(ghwnd, TIMER_ID_HIDE_WELL_WISHER_TEXT, 22000 /* 22 seconds */, (TIMERPROC)switchToNextState);
                endSceneTimerStarted = TRUE;
            }
        }
        break;
    case HIDE_WELL_WISHER_TEXT:
        // fade out
        if(updateObjectAlpha(&whatSirsWellWishersThinkAlpha, 1.0f / (FPS * 0.996f), FADE_OUT_FLAG))
        {
            theEndSceneState = SHOW_WHAT_SIR_THINKS_TEXT;
        }
        break;
    case SHOW_WHAT_SIR_THINKS_TEXT:
        // play surprise track
        if(bPlaySurpriseTrackOnce)
        {
            // unbind the source
            SetCurrentSourceForStats(0, FALSE, FALSE);

            // bind the next source
            SetCurrentSourceForStats(theEndSceneSources[1], FALSE, FALSE);

            // start playing source
            playALSource(theEndSceneSources[1]);
        
            bPlaySurpriseTrackOnce = FALSE;
        }
        
        // fade in
        updateObjectAlpha(&whatSirThinksAlpha, 1.0f / (FPS * 1.0f), FADE_IN_FLAG);  

        // if text reached its destination, set delay timer which will switch to next state
        if(!endSceneTimerStarted)
        {
            SetTimer(ghwnd, TIMER_ID_HIDE_WHAT_SIR_THINKS_TEXT, 2500 /* 2.5 seconds */, (TIMERPROC)switchToNextState);
            endSceneTimerStarted = TRUE;
        }
        break;
    case SHOW_SIR_OUTLINE:
        lerpMyValue(&sirOutlineQuadY, 1.0f);

        if(sirOutlineQuadY.bReached)
        {
            theEndSceneState = HIDE_SIR_OUTLINE;
        }
        break;
    case HIDE_SIR_OUTLINE:
        if(!endSceneTimerStarted)
        {
            SetTimer(ghwnd, TIMER_ID_SHOW_SIR_TEXT, 500, (TIMERPROC)switchToNextState);
            endSceneTimerStarted = TRUE;
        }
        break;
    case SHOW_SIR_TEXT:  
        updateObjectAlpha(&sirTextAlpha, 1.0f / (FPS * THE_END_SCENE_SIR_TEXT_TRANSLATION_SPEED), FADE_OUT_FLAG);        
        lerpMyValue(&sirTextZTranslation, 1.0f);

        if(sirTextZTranslation.bReached)
        {
            // reset z translation
            sirTextZTranslation.x = sirTextZTranslation.x0;
            sirTextZTranslation.tVal = 0.0f;

            // reset alpha
            sirTextAlpha = 1.0f;

            // increment the text index
            sirTextIdx++;
            if(sirTextIdx > 3)
            {
                sirTextIdx = 3;
                theEndSceneState = THE_END;
            }
        }
        break;
    case THE_END:
        // 6:34 -- appearance of "The End" && 6:36, 6:38, 6:40 -- question mark timestamps
        if(!endSceneTimerStarted)
        {
            SetTimer(ghwnd, TIMER_ID_THE_END_QUESTION_MARK_DELAY, 1500, (TIMERPROC)switchToNextState);
            endSceneTimerStarted = TRUE;
        }
        else if(iQuestionMarkCount > 5)
        {
            theEndSceneState = END;
        }
        break;
    case END:
        uninitializeSceneEight();
        break;
    default:
        break;
    }
}

void drawShadowyText(const char *str, Color color, Color gradientColor)
{
    // local variables
    GLfloat alphaDecrement    = 0.5f;
    GLfloat translationValueX = 0.0f;
    GLfloat translationValueZ = -3.0f;
    GLfloat scaleValue        = 1.0f;

    // code
    for(int i = 0; i <= 1; i++)
    {
        glPushMatrix();
        {
            glColor4f(color.r, color.g, color.b, color.alpha);
            glTranslatef(translationValueX, 0.0f, translationValueZ);
            glScalef(1.0f, 1.0f, 0.005f);
            RT_DrawText(str, strlen(str), COOLVETICA_END_SCENE_DISPLAYLISTS_OFFSET);
        }
        glPopMatrix();

        glPushMatrix();
        {
            glColor4f(gradientColor.r, gradientColor.g, gradientColor.b, gradientColor.alpha);
            glTranslatef(translationValueX, 0.0f, translationValueZ - 0.005f);
            glScalef(1.0f, 1.0f, 1.0f);
            RT_DrawText(str, strlen(str), COOLVETICA_END_SCENE_DISPLAYLISTS_OFFSET);
        }
        glPopMatrix();
        
        translationValueX = translationValueX - (alphaDecrement * 2.0f);
        translationValueZ = translationValueZ - (alphaDecrement * 2.0f);

        color.alpha         = color.alpha - alphaDecrement;
        gradientColor.alpha = gradientColor.alpha - alphaDecrement;
    }
}

void switchToNextState(HWND hwnd, UINT unnamedParam2, UINT_PTR timerIdentifier, DWORD unnamedParam4)
{
    // code
    KillTimer(hwnd, timerIdentifier);

    endSceneTimerStarted = FALSE;

    switch (timerIdentifier)
    {
    case TIMER_ID_HIDE_WELL_WISHER_TEXT:
        theEndSceneState = HIDE_WELL_WISHER_TEXT;
        break;
    case TIMER_ID_HIDE_WHAT_SIR_THINKS_TEXT:
        theEndSceneState = SHOW_SIR_OUTLINE;
        break;
    case TIMER_ID_SHOW_SIR_TEXT:
        theEndSceneState = SHOW_SIR_TEXT;
        break;
    case TIMER_ID_THE_END_QUESTION_MARK_DELAY:
        iQuestionMarkCount++;
        break;
    }
}

void uninitializeSceneEight(void)
{
    // code
    // unbind the source
    SetCurrentSourceForStats(0, FALSE, FALSE);

    // delete the source
    deleteALSources(2, theEndSceneSources);

    // delete the buffers
    deleteALBuffers(2, theEndSceneBuffers);

    // delete ARTR, ERTR, Audio Video Seminar, Sanskar Varg text textures
    for(int i = 0; i < 4; i++)
    {
        DeleteTexture(&sirText[i]);
    }

    // delete texture
    DeleteTexture(&sirOutline);

    // uninitialize RT lib
    RT_Uninitialize_Library(&coolveticaEndScene, COOLVETICA_END_SCENE_DISPLAYLISTS_OFFSET);
}

