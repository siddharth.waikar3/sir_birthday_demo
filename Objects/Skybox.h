#pragma once

// function declarations
void initializeSkybox(void);
void drawTexturedSkybox(void);
void drawTexturedSkyboxWithRotation(GLfloat angle);
void uninitializeSkybox(void);
