// header files
#include "../Common/Common.h"
#include "RandomSphereGeneration.h"

// global variables
extern GLint stack;
extern GLint slice;

// function definitions
void initializeRandSpheres(RandSphereAttribs *randSphereAttribs, GLuint numSpheres, Vector rangeStart, Vector rangeEnd /* bounding box */)
{
    // code
    for(int i = 0; i < numSpheres; i++)
    {
        GLfloat randValue = getInterpolatedValue(0.0f, (GLfloat)RAND_MAX, 0.0f, 10.0f, rand());

        GLfloat tx = getInterpolatedValue(0.0f, (GLfloat)RAND_MAX, rangeStart.x, rangeEnd.x, rand());
        GLfloat ty = getInterpolatedValue(0.0f, (GLfloat)RAND_MAX, rangeStart.y, rangeEnd.y, rand());
        GLfloat tz = getInterpolatedValue(0.0f, (GLfloat)RAND_MAX, rangeStart.z, rangeEnd.z, rand());

        GLfloat rx = getInterpolatedValue(0.0f, (GLfloat)RAND_MAX, 0.0f, 90.0f, rand());
        GLfloat ry = getInterpolatedValue(0.0f, (GLfloat)RAND_MAX, 0.0f, 90.0f, rand());
        GLfloat rz = getInterpolatedValue(0.0f, (GLfloat)RAND_MAX, 0.0f, 90.0f, rand());

        GLfloat r = getInterpolatedValue(0.0f, (GLfloat)RAND_MAX, 0.0f, 1.0f, rand());
        GLfloat g = getInterpolatedValue(0.0f, (GLfloat)RAND_MAX, 0.0f, 1.0f, rand());
        GLfloat b = getInterpolatedValue(0.0f, (GLfloat)RAND_MAX, 0.0f, 1.0f, rand());
    
        // assign the values
        randSphereAttribs[i].pos.pos = {tx, ty, tz};
        randSphereAttribs[i].rot.pos = {rx, ry, rz};
        randSphereAttribs[i].scale.pos = {0.1f, 0.1f, 0.1f};

        randSphereAttribs[i].color[0] = r;
        randSphereAttribs[i].color[1] = g; 
        randSphereAttribs[i].color[2] = b;
        randSphereAttribs[i].color[3] = 1.0f;

        // apply random direction values
        if(randValue >= 0.0f && randValue < 5.0f)
        {
            randSphereAttribs[i].pos.up = 1;
            randSphereAttribs[i].pos.left = 1;

            randSphereAttribs[i].rot.down = 1;
            randSphereAttribs[i].rot.right = 1;

            randSphereAttribs[i].pos.in = 1;
            randSphereAttribs[i].rot.out = 1;
        }
        else if(randValue >= 5.0f && randValue < 10.0f)
        {
            randSphereAttribs[i].pos.down = 1;
            randSphereAttribs[i].pos.right = 1;

            randSphereAttribs[i].rot.up = 1;
            randSphereAttribs[i].rot.right = 1;

            randSphereAttribs[i].pos.out = 1;
            randSphereAttribs[i].rot.in = 1;
        }
    }
}

void drawRandSpheres(RandSphereAttribs *randSphereAttribs, Point **randSphereVertices, GLuint numSpheres)
{
    // code
    for(int i = 0; i < numSpheres; i++)
    {
        glPushMatrix();
        {
            glTranslatef(randSphereAttribs[i].pos.pos.x, randSphereAttribs[i].pos.pos.y, randSphereAttribs[i].pos.pos.z);
            glRotatef(randSphereAttribs[i].rot.pos.x, 1.0f, 0.0f, 0.0f);
            glRotatef(randSphereAttribs[i].rot.pos.y, 0.0f, 1.0f, 0.0f);
            glRotatef(randSphereAttribs[i].rot.pos.z, 0.0f, 0.0f, 1.0f);
            glScalef(randSphereAttribs[i].scale.pos.x, randSphereAttribs[i].scale.pos.y, randSphereAttribs[i].scale.pos.z);

            drawSphereVertices(randSphereVertices, 50, 50, randSphereAttribs[i].color, -0.015f, GL_TRIANGLE_STRIP);
        }
        glPopMatrix();
    }
}

void updateRandSpheres(RandSphereAttribs *randSphereAttribs, GLuint numSpheres, Vector rangeStart, Vector rangeEnd, GLfloat moveSpeed, GLfloat rotSpeed)
{
    // code
    for(int i = 0; i < numSpheres; i++)
    {
        // ---------- TRANSLATION -----------
        if(randSphereAttribs[i].pos.up)
        {
            randSphereAttribs[i].pos.pos.y = randSphereAttribs[i].pos.pos.y + moveSpeed;
            if(randSphereAttribs[i].pos.pos.y >= rangeEnd.y)
            {
                randSphereAttribs[i].pos.down = 1;
                randSphereAttribs[i].pos.up = 0;
            }
        }

        if(randSphereAttribs[i].pos.down)
        {
            randSphereAttribs[i].pos.pos.y = randSphereAttribs[i].pos.pos.y - moveSpeed;
            if(randSphereAttribs[i].pos.pos.y <= rangeStart.y)
            {
                randSphereAttribs[i].pos.up = 1;
                randSphereAttribs[i].pos.down = 0;
            }
        }

        if(randSphereAttribs[i].pos.left)
        {
            randSphereAttribs[i].pos.pos.x = randSphereAttribs[i].pos.pos.x - moveSpeed;
            if(randSphereAttribs[i].pos.pos.x <= rangeStart.x)
            {
                randSphereAttribs[i].pos.right = 1;
                randSphereAttribs[i].pos.left = 0;
            } 
        }
        
        if(randSphereAttribs[i].pos.right)
        {
            randSphereAttribs[i].pos.pos.x = randSphereAttribs[i].pos.pos.x + moveSpeed;
            if(randSphereAttribs[i].pos.pos.x >= rangeEnd.x)
            {
                randSphereAttribs[i].pos.left = 1;
                randSphereAttribs[i].pos.right = 0;
            }
        }

        if(randSphereAttribs[i].pos.in)
        {
            randSphereAttribs[i].pos.pos.z = randSphereAttribs[i].pos.pos.z - moveSpeed;
            if(randSphereAttribs[i].pos.pos.z <= rangeStart.z)
            {
                randSphereAttribs[i].pos.out = 1;
                randSphereAttribs[i].pos.in = 0;
            } 
        }

        if(randSphereAttribs[i].pos.out)
        {
            randSphereAttribs[i].pos.pos.z = randSphereAttribs[i].pos.pos.z + moveSpeed;
            if(randSphereAttribs[i].pos.pos.z >= rangeEnd.z)
            {
                randSphereAttribs[i].pos.in = 1;
                randSphereAttribs[i].pos.out = 0;
            } 
        }

        // ----------- ROTATION -----------
        if(randSphereAttribs[i].rot.up)
        {
            randSphereAttribs[i].rot.pos.y = randSphereAttribs[i].rot.pos.y + moveSpeed;
            if(randSphereAttribs[i].rot.pos.y >= rangeEnd.y)
            {
                randSphereAttribs[i].rot.down = 1;
                randSphereAttribs[i].rot.up = 0;
            }
        }

        if(randSphereAttribs[i].rot.down)
        {
            randSphereAttribs[i].rot.pos.y = randSphereAttribs[i].rot.pos.y - moveSpeed;
            if(randSphereAttribs[i].rot.pos.y <= rangeStart.y)
            {
                randSphereAttribs[i].rot.up = 1;
                randSphereAttribs[i].rot.down = 0;
            }
        }

        if(randSphereAttribs[i].rot.left)
        {
            randSphereAttribs[i].rot.pos.x = randSphereAttribs[i].rot.pos.x - moveSpeed;
            if(randSphereAttribs[i].rot.pos.x <= rangeStart.x)
            {
                randSphereAttribs[i].rot.right = 1;
                randSphereAttribs[i].rot.left = 0;
            } 
        }
        
        if(randSphereAttribs[i].rot.right)
        {
            randSphereAttribs[i].rot.pos.x = randSphereAttribs[i].rot.pos.x + moveSpeed;
            if(randSphereAttribs[i].rot.pos.x >= rangeEnd.x)
            {
                randSphereAttribs[i].rot.left = 1;
                randSphereAttribs[i].rot.right = 0;
            }
        }

        if(randSphereAttribs[i].rot.in)
        {
            randSphereAttribs[i].rot.pos.z = randSphereAttribs[i].rot.pos.z - moveSpeed;
            if(randSphereAttribs[i].rot.pos.z <= rangeStart.z)
            {
                randSphereAttribs[i].rot.out = 1;
                randSphereAttribs[i].rot.in = 0;
            } 
        }

        if(randSphereAttribs[i].pos.out)
        {
            randSphereAttribs[i].rot.pos.z = randSphereAttribs[i].rot.pos.z + moveSpeed;
            if(randSphereAttribs[i].rot.pos.z >= rangeEnd.z)
            {
                randSphereAttribs[i].rot.in = 1;
                randSphereAttribs[i].rot.out = 0;
            } 
        }
    }
}
