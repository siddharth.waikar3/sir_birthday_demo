#pragma once

//#pragma comment(lib, "CUDALibrary.lib")

// header files
#include "Common.h"
#include "../Objects/SphereCollision.h"

// function declarations
BOOL initializeCUDALibrary(void);
void uninitializeCUDALibrary(void);
BOOL updateSphereCollisionSystemCUDA(struct BoundingSphere *bSphere, struct SphereAttribs *sphereAttribs, int numSpheres, GLfloat dt);
BOOL solveCollisionCUDA(struct SphereAttribs *sphereAttribs, int numSpheres);
