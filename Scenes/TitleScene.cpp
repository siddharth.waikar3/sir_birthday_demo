// header files
#include "..\Common\Common.h"
#include "..\Common\RenderText.h" // for font rendering
#include "..\objects\Skybox.h"
#include <stdio.h>
#include <WinUser.h>

// imgui
#include "../imgui/imgui.h"

#define MY_FONT_TITLE 1010
#define MY_FONT_GROUP_NAME 2020
#define MY_FONT_DEMO_NAME 3030
#define FRAMES_PER_SECOND 60.0

#define FADE_IN_LETTER_SECONDS 0.226f // 226 ms
#define INITIAL_DELAY_T_SCENE_MS 108

#define TIMER_ID_1 5555
#define DEMO_NAME_ZOOM_OUT_SECONDS 3.0f
#define IDT_TITLESCENE_DELAY 21421
#define IDT_ASTROMEDICOMP_DELAY 21422
#define IDT_INITIAL_DELAY 21423
#define IDT_SWITCH_TO_DEMO_NAME_STATE 21424

enum TitleSceneStates
{
    TITLE_SCENE_NAME_STATE,
    TITLE_SCENE_NAME_DELAY,
    TITEL_SCENE_DEMO_NAME_STATE,
    TITEL_SCENE_DEMO_NAME_DELAY,
    END
};

extern enum Scene currentScene;
extern HDC ghdc;
extern HWND ghwnd;
enum TitleSceneStates TitleSceneState;
extern struct Transformations TitleSceneTransformation;
extern struct Transformations g_Transformations[NUM_TRANSFORMATION_ELEMENTS];
extern FILE* gpFILE;
RT_Library title_font, group_name_font, demo_name_font;
int index = 0;


char astro[16] = "ASTROMEDICOMP'S";
char grp_name[25] = "RTR-5 RENDER GROUP";
char grp_presents[10] = "PRESENTS";
char demo_name[10] = "LEGACY";
GLfloat alpha[15] = { 0.0 };
GLfloat groupNameAlpha = 0.0f;
GLfloat presentNameAlpha = 0.0f;
GLfloat demoNameAlpha = 0.0f;
GLboolean isTimerOn = FALSE;

UINT AMCFadeInDelayMs = 1389;
GLfloat startZpos = -54.0000000f;
GLfloat endZpos = -5.000000f;
GLfloat startYpos = 0.000000f;
GLfloat endYpos = -0.350000f;
GLfloat startXpos = 0.000000f;
GLfloat endXpos = -2.750000f;
GLfloat fdOutAlpha = 0.0f;
GLint iTitleSceneDelay = 0;
BOOL bTitleSceneTimerIsOn = FALSE;
BOOL bInitialDelay = TRUE;

LerpTransformations ltlegacyTransformation = 
{   
    // current
    {
	    {-7.100000f, -0.200000f, -4.000000f},
        {0.000000f, 0.000000f, 0.000000f},
    	{1.000000f, 1.000000f, 1.000000f}
    },

    // initial
    {
	    {/*-5.1f*/-7.100000f, -0.200000f, -4.000000f},
        {0.000000f, 0.000000f, 0.000000f},
    	{1.000000f, 1.000000f, 1.000000f}
    },

    // final
    {
	    {/*5.1f*/-2.100000f, -0.200000f, -4.000000f},
        {0.000000f, 0.000000f, 0.000000f},
    	{1.000000f, 1.000000f, 1.000000f}
    },

    // tVal
    0.0f,

    // speed
    1.0f / (FPS * 3.5f)
    // 1.0f / (FPS * 3.5f)
};

Transformations legacyTransformation = 
{
    {/*5.1f*/-1.865000f, -0.200000f, -4.000000f},
    {0.000000f, 0.000000f, 0.000000f},
    {1.000000f, 1.000000f, 1.000000f}
};

// function definitions
void initializeSceneTitle(void)
{
    // code
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    // initialize font library
    RT_Initialize_Library(&title_font, ghdc, TEXT("./Resources/fonts/coolvetica rg.otf"), TEXT("Coolvetica Rg"), RT_FW_DEMIBOLD, NULL, MY_FONT_TITLE);
    RT_Initialize_Library(&group_name_font, ghdc, TEXT("./Resources/fonts/coolvetica rg.otf"), TEXT("Coolvetica Rg"), RT_FW_ULTRALIGHT, NULL, MY_FONT_GROUP_NAME);
    RT_Initialize_Library(&demo_name_font, ghdc, TEXT(".\\Resources\\fonts\\LTCushion-Bold.ttf"), TEXT("LT Cushion"), RT_FW_BOLD, NULL, MY_FONT_DEMO_NAME);

    TitleSceneState = TITLE_SCENE_NAME_STATE;
}

void displaySceneTitle(void)
{
    // function prototypes
    void drawAstroMediComp(void);

    //variable declaration
    static const struct Transformations AMCTransformations =
    {
    	{-0.150000f, 0.500000f, -7.000000f},
        {0.000000f, 0.000000f, 0.000000f},
        {1.000000f, 1.000000f, 1.000000f}
    };

    static const struct Transformations groupName =
    {
        {-3.149999f, -0.600000f, -7.000003f},
        {0.000000f, 0.000000f, 0.000000f},
        {0.800000f, 0.800000f, 0.800000f}
    };

    static const struct Transformations presentName =
    {
        {-1.300000f, -1.300000f, -7.000000f},
        {0.000000f, 0.000000f, 0.000000f},
        {0.600000f, 0.600000f, 0.700000f}
    };

    static Color color1 = {0.925490f, 0.623529f, 0.019608f, 1.0f};
    static Color color2 = {0.843137f, 0.415686f, 0.011765f, 1.0f};

    // code    
    if(TitleSceneState >= TITLE_SCENE_NAME_STATE && TitleSceneState < TITEL_SCENE_DEMO_NAME_STATE)
    {
        glPushMatrix();
        {
            applyTransformations(AMCTransformations);
            drawAstroMediComp();
        }
        glPopMatrix();

        glPushMatrix();
        {
            applyTransformations(groupName);
            drawGradedText(grp_name, {0.0f, 1.0f, 0.0f, groupNameAlpha}, {0.0f, 0.3f, 0.0f, groupNameAlpha}, MY_FONT_GROUP_NAME);
        }
        glPopMatrix();

        glPushMatrix();
        {
            applyTransformations(presentName);
            drawGradedText(grp_presents, {0.0f, 1.0f, 0.0f, presentNameAlpha}, {0.0f, 0.3f, 0.0f, presentNameAlpha}, MY_FONT_GROUP_NAME);
        }
        glPopMatrix();
    }

    if(TitleSceneState >= TITEL_SCENE_DEMO_NAME_STATE)
    {
        glPushMatrix();
        {
            applyTransformations(legacyTransformation);
            drawGradedText(demo_name, color1, color2, MY_FONT_DEMO_NAME);
        }
        glPopMatrix();
    }

    fadeOutEffect(fdOutAlpha);
}

void drawAstroMediComp(void)
{
    // local variables
    const char *str = "ASTROMEDICOMP's";
    GLfloat pos[] =
    {
        -4.499998f + 0.020f,
        -3.850000f +  0.020f,
        -3.250000f +  0.020f,
        -2.649999f +  0.020f,
        -1.999999f +  0.020f,
        -1.300000f +  0.020f,
        -0.500000f +  0.020f,
        0.150001f  +  0.020f,
        0.800000f  +  0.020f,
        1.100000f  +  0.020f,
        1.750000f  +  0.020f,
        2.499999f,
        3.299998f,
        4.049997f,
        4.349998f,
    };

    // code
    for(int i = 0; i < strlen(str); i++)
    {
        if(i > 12)
        {
            glColor4f(1.0f, 1.0f, 0.0f, alpha[13]);
        }
        else
        {
            glColor4f(1.0f, 1.0f, 0.0f, alpha[i]);
        }

        glPushMatrix();
        {
            glTranslatef(pos[i], 0.0f, 0.0f);
            glScalef(1.0f, 1.0f, 0.005f);
            RT_DrawText(&str[i], 1, MY_FONT_TITLE);
        }
        glPopMatrix();

        if(i > 12)
        {
            glColor4f(0.4f, 0.4f, 0.0f, alpha[13]);
        }
        else
        {
            glColor4f(0.4f, 0.4f, 0.0f, alpha[i]);
        }

        glPushMatrix();
        {
            glTranslatef(pos[i], 0.0f, -0.001f);
            RT_DrawText(&str[i], 1, MY_FONT_TITLE);
        }
        glPopMatrix();
    }
}

void updateSceneTitle(void)
{
    // function prototypes
    void uninitializeSceneTitle(void);
    void titleSceneTimer(HWND hwnd, UINT iMsg, UINT_PTR idEvent, DWORD dwTime);

    // variable declarations
    static BOOL bFadeInAlpha = FALSE;
    static BOOL bOnce = FALSE;

    // code
    // if(true)
    // {
    //     fdOutAlpha = 0.0f;
    //     return;
    // }

    if(!bOnce)
    {
        SetCurrentSourceForStats(g_SceneSources[0], FALSE, FALSE);
        playALSource(g_SceneSources[0]);
        bOnce = TRUE;
    }

    switch (TitleSceneState)
    {
    case TITLE_SCENE_NAME_STATE:

        // initial delay
        if(!bTitleSceneTimerIsOn && bInitialDelay)
        {
            SetTimer(ghwnd, IDT_INITIAL_DELAY, INITIAL_DELAY_T_SCENE_MS, (TIMERPROC)titleSceneTimer);
            bTitleSceneTimerIsOn = TRUE;
        }

        if(!bInitialDelay)
        {
            // fade in ASTROMEDICOMP'S
            if (alpha[index] < 1.0f)
            {
                alpha[index] = alpha[index] + (1.0f / (FPS * FADE_IN_LETTER_SECONDS));
            }
            else
            {
                if(!bTitleSceneTimerIsOn && index < 14)
                {
                    SetTimer(ghwnd, IDT_ASTROMEDICOMP_DELAY, AMCFadeInDelayMs, (TIMERPROC)titleSceneTimer);
                    bTitleSceneTimerIsOn = TRUE;
                }
            }

            // fade in Group Name and Presents
            if (alpha[13] >= 1.0f)
            {
                if (groupNameAlpha < 1.0f) {
                    groupNameAlpha = groupNameAlpha + (1.0f / (FPS * 2.5f));
                    presentNameAlpha = presentNameAlpha + (1.0f / (FPS * 2.5f));
                }
            }

            // delay after present fades in
            if (presentNameAlpha > 0.0f)
            {
                if(!bTitleSceneTimerIsOn && iTitleSceneDelay == 0)
                {
                    SetTimer(ghwnd, IDT_TITLESCENE_DELAY, /*2000*/ 9000, (TIMERPROC)titleSceneTimer);
                    bTitleSceneTimerIsOn = TRUE;
                }
                
                if(iTitleSceneDelay == 1)
                {
                    TitleSceneState = TITLE_SCENE_NAME_DELAY;
                }
            }
        }
        break;

    // FADE OUT SCENE
    case TITLE_SCENE_NAME_DELAY:
        if(updateAlpha(&fdOutAlpha, 1.0f/FPS, FADE_OUT_FLAG)) 
        {
            if(!bTitleSceneTimerIsOn)
            {
                SetTimer(ghwnd, IDT_SWITCH_TO_DEMO_NAME_STATE, 200, (TIMERPROC)titleSceneTimer);
                bTitleSceneTimerIsOn = TRUE;
            }
        }
        break;

    // FADE IN DEMO NAME
    case TITEL_SCENE_DEMO_NAME_STATE:
        
        if(!bFadeInAlpha)
        {
            // bFadeInAlpha = updateAlpha(&fdOutAlpha, 1.0f / FPS, FADE_IN_FLAG);
            fdOutAlpha = 0.0f;
            bFadeInAlpha = TRUE;
        }

        // lerpAnimation_v2(&ltlegacyTransformation, 1.0f);
        if(bFadeInAlpha)
        {
            if(!bTitleSceneTimerIsOn && iTitleSceneDelay == 1)
            {
                SetTimer(ghwnd, IDT_TITLESCENE_DELAY, 2500, (TIMERPROC)titleSceneTimer);
                bTitleSceneTimerIsOn = TRUE;
            }

            if(iTitleSceneDelay == 2)
            {
                TitleSceneState = TITEL_SCENE_DEMO_NAME_DELAY;
            } 
        }
        break;
            // distance/FPS * time
    
    // FADE OUT SCENE 
    case TITEL_SCENE_DEMO_NAME_DELAY:
        if(updateAlpha(&fdOutAlpha, 1.0f / FPS * 1.0f, FADE_OUT_FLAG))
        {
            fdOutAlpha = 1.0f; 
            if(!bTitleSceneTimerIsOn && iTitleSceneDelay == 2)
            {
                SetTimer(ghwnd, IDT_TITLESCENE_DELAY, 2000, (TIMERPROC)titleSceneTimer);
                bTitleSceneTimerIsOn = TRUE;
            }

            if(iTitleSceneDelay == 3)
            {
                TitleSceneState = END;        
            }
        }
        break;
    
    // GO TO NEXT SCENE
    case END:
        uninitializeSceneTitle();
        ResetGlobalTimer();
        currentScene = SCENE_OPENING;   
        break;
    } 
}

void uninitializeSceneTitle(void)
{
    // code
    // stopALSource(g_SceneSources[0]);
    SetCurrentSourceForStats(0, FALSE, FALSE);

    fprintf(gpFILE, "Successfully unbound g_SceneSources[0].\n");
    fflush(gpFILE);

    RT_Uninitialize_Library(&demo_name_font, MY_FONT_DEMO_NAME);
    RT_Uninitialize_Library(&group_name_font, MY_FONT_GROUP_NAME);
    RT_Uninitialize_Library(&title_font, MY_FONT_TITLE);

    fprintf(gpFILE, "Leaving uninitializeSceneTitle().\n");
    fflush(gpFILE);
}

void titleSceneTimer(HWND hwnd, UINT iMsg, UINT_PTR idEvent, DWORD dwTime)
{
    // code
    KillTimer(hwnd, idEvent);
    if(idEvent == IDT_TITLESCENE_DELAY)
    {
        iTitleSceneDelay++;
    }
    else if(idEvent == IDT_ASTROMEDICOMP_DELAY)
    {
        index++;
        if(index > 14)
        {
            index = 14;
        }

        // switch(index)
        // {
        // case 1:
        //     AMCFadeInDelayMs = 1397;
        //     break;
        // case 2:
        //     AMCFadeInDelayMs = 1450;
        //     break;
        // case 3:
        //     AMCFadeInDelayMs = 997;
        //     break;
        // case 4:
        //     AMCFadeInDelayMs = 1331;
        //     break;
        // case 5:
        //     AMCFadeInDelayMs = 1352;
        //     break;
        // case 6:
        //     AMCFadeInDelayMs = 1109;
        //     break;
        // case 7:
        //     AMCFadeInDelayMs = 1397;
        //     break;
        // case 8:
        //     AMCFadeInDelayMs = 1443;
        //     break;
        // case 9:
        //     AMCFadeInDelayMs = 1538;
        //     break;
        // case 10:
        //     AMCFadeInDelayMs = 1044;
        //     break;
        // case 11:
        //     AMCFadeInDelayMs = 1476;
        //     break;
        // case 12:
        //     AMCFadeInDelayMs = 1397;
        //     break;
        // case 13:
        //     AMCFadeInDelayMs = 1397;
        //     break;
        // }
    }
    else if(idEvent == IDT_INITIAL_DELAY)
    {
        bInitialDelay = FALSE;
    }
    else if(idEvent == IDT_SWITCH_TO_DEMO_NAME_STATE)
    {
        TitleSceneState = TITEL_SCENE_DEMO_NAME_STATE;
    }

    bTitleSceneTimerIsOn = FALSE;
}
