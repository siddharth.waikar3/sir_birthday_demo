#pragma once

#pragma comment(lib, "./Resources/lib/OAL.lib")

// header files
#include <windows.h>
#include <al.h>

// function declarations
// library functions
BOOL initializeALLib(BOOL bPrintLogs);
BOOL getALBuffers(ALsizei nBuffers, ALuint *buffers);
BOOL getALSources(ALsizei nSources, ALuint *sources);
BOOL attachALWaveFileToBuffer(ALuint buffer, ALenum format, ALvoid *data, ALsizei size, ALsizei freq);
BOOL attachALBufferToSource(ALuint *buffer, ALuint *source);
BOOL playALSource(ALuint source);
BOOL pauseALSource(ALuint source);
BOOL stopALSource(ALuint source);
BOOL muteALSource(ALuint source, ALfloat *oldVolume);
BOOL unmuteALSource(ALuint source, ALfloat volume);
BOOL getALSourcePlaybackPositionInSeconds(ALuint source, ALfloat *position);
BOOL seekALSource(ALuint source, ALuint hours, ALuint minutes, ALuint seconds);
BOOL seekALSourcef(ALuint source, ALfloat hours, ALfloat minutes, ALfloat seconds);
BOOL setALSourceSpeed(ALfloat factor);
BOOL setALSourceVolume(ALuint source, ALfloat volume);
void deleteALSources(ALsizei nSources, ALuint *sources);
void deleteALBuffers(ALsizei nBuffers, ALuint *buffers);
void uninitializeALLib(void);

// wrapper functions
BOOL AttachWaveFileToALBuffer(DWORD dwResourceID, DWORD dwResourceType, ALuint buffer);
