
#pragma once

void DrawOrbit(GLfloat, GLfloat);
void DrawOrbitEx(GLfloat, GLfloat, GLfloat);
void DrawOrbitJourneyScene(GLfloat innerRadius, GLfloat outerRadius);

//MACROS
#define DISABLE	0
#define ENABLE_LOGS	FALSE
#define LOG_FILE	"Log.txt"
#define WIN_WIDTH	800
#define WIN_HEIGHT	600
#define OBJECT_TRANSLATION_SPEED	0.1f
#define OBJECT_ROTATION_SPEED		5.0f
#define OBJECT_SCALE_SPEED			0.1f

struct ColorConfig
{
	GLfloat r;
	GLfloat g;
	GLfloat b;
	GLfloat a;
};

struct CubeConfig
{
	GLfloat x;
	GLfloat y;
	GLfloat z;

	GLfloat h;
	GLfloat w;
	GLfloat d;

	struct ColorConfig front;
	struct ColorConfig back;
	struct ColorConfig left;
	struct ColorConfig right;
	struct ColorConfig top;
	struct ColorConfig bottom;
};

struct TriangleConfig
{
	GLfloat x;
	GLfloat y;
	GLfloat z;

	GLfloat h;
	GLfloat b;
	GLfloat d;

	struct ColorConfig front;
	struct ColorConfig back;
	struct ColorConfig left;
	struct ColorConfig right;
	struct ColorConfig base;
};
