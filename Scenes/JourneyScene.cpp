// header files 
#include "../Common/Common.h"
#include "../Common/Camera.h"
#include "../Objects/Saturn.h"
#include "../Objects/Skybox.h"
#include "../Objects/Orbit.h"
#include "../Objects/OrbitText.h"
#include "../Scenes/ObstaclesScene.h"

// macros
#define PLAY_JOURNEY_SCENE_MUSIC        1
#define ENABLE_JOURNEY_SCENE_FADE       1
#define DO_JOURNEY_SCENE_ANIMATION      1
#define SET_JOURNEY_SCENE_CAMERA        1
#define STARTING_FROM_JOURNEY_SCENE     1
#define SHOW_JOURNEY_SCENE_SATURN_ROCKS SHOW_ROCKS_GLOBAL

#define IDT_SECONDS_TIMER 1312

#if STARTING_FROM_JOURNEY_SCENE
    #define JOURNEY_SCENE_SECONDS_OFFSET 0
#else
    #define JOURNEY_SCENE_SECONDS_OFFSET 50
#endif

#define JOURNEY_SCENE_SATURN_ROTATION_ANGLE_TILL_UNIX 360.0f
#define JOURNEY_SCENE_SATURN_ROTATION_SPEED_TILL_UNIX 90.0f / (FPS * 18.0f)  
#define JOURNEY_SCENE_TEXT_FADE_SPEED 1.0f / (FPS * 2.0f)

// enum for scene and its variable 
enum JourneySceneStates
{
    BEGINNING_OF_WINDEV,
    MILESTONES,
    BEGINNING_OF_UNIX,
    BEGINNING_OF_SUNBEAM,
    YEARS_PASSED_ON,
    FADE_OUT,
	END
};

enum JourneySceneStates journeySceneState;

// extern global variables
extern FILE *gpFILE;
extern HWND ghwnd;
extern CameraCoordinates cameraCoordinates;
extern enum Scene currentScene;

// global variables
// GLfloat journeySceneFadeInAlpha  = 1.0f;
GLfloat journeySceneFadeOutAlpha = 1.0f;
GLfloat journeySceneSaturnRev    = -15.0f;

// for timer
unsigned long long g_JourneySceneElapsedSeconds = 0;
BOOL g_bJourneySceneTimerIsOn                   = FALSE;

// for text
BOOL g_bDrawWinDev2002Text    = TRUE;
BOOL g_bDrawUNIX2003Text      = FALSE;
BOOL g_bDraw2004YearText      = FALSE;
BOOL g_bDrawSunbeam2006Text   = FALSE;
BOOL g_bDrawOSSeminar2008Text = FALSE;

GLfloat journeySceneWinDevAlpha    = 1.0f;
GLfloat journeySceneUNIXAlpha      = 0.0f;
GLfloat journeyScene2004Alpha      = 0.0f;
GLfloat journeySceneSunbeamAlpha   = 0.0f;
GLfloat journeySceneOSSeminarAlpha = 0.0f;

struct Transformations journeySceneTextTransformations[] =
{
    {
        {0.000000f, 0.000000f, 0.000000f},
        {0.000000f, 0.000000f, 0.000000f},
        {0.500000f, 0.500000f, 1.000000f}
    },
    {
        {0.000000f, 0.000000f, 0.000000f},
        {0.000000f, 0.000000f, 0.000000f},
        {0.5f, 0.5f, 1.0f}
    },
    {
        {0.000000f, 0.000000f, 0.000000f},
        {0.000000f, 0.000000f, 0.000000f},
        {0.5f, 0.5f, 1.0f}
    },
    {
        {0.000000f, 0.000000f, 0.000000f},
        {0.000000f, 0.000000f, 0.000000f},
        {0.5f, 0.5f, 1.0f}
    },
    {
        {0.000000f, 0.000000f, 0.000000f},
        {0.000000f, 0.000000f, 0.000000f},
        {0.5f, 0.5f, 1.0f}
    }
};

// for orbit
GLfloat journeySceneOrbitInnerRadius = 20.0f;
GLfloat journeySceneOrbitOuterRadius = 20.01f;
GLfloat journeySceneOrbitRadius      = (journeySceneOrbitInnerRadius + journeySceneOrbitOuterRadius) / 2;

// for saturn
struct Translation journeySceneSaturnTranslation    = {journeySceneOrbitRadius, 0.05f, 0.0f};
GLfloat journeySceneSatAndRingRot[2]                = {0.0f, 0.0f};
Vec3* journeySceneSaturnTranslationTillUNIX         = NULL;
GLint journeySceneSaturnTranslationTillUNIXElements = 4320;

// for saturn revolution
GLint iSaturnRevolutionElement = 0;
GLuint journeySceneSaturnRevolutionLast = 0;

// for camera revolution
GLint iCameraRevolutionElement = 0;
GLuint journeySceneCameraRevolutionLast = 0;

// for skybox
GLfloat journeySceneSkyboxRotation = 0.0f;

// camera
const CameraCoordinates journeySceneCamStart =
{
	{19.995113f, 0.201660f, -1.078648f},
	{-0.271931f, 0.050593f, -0.960986f},
	{0.960986f, 0.000000f, -0.271931f},
	{0.013758f, 0.997440f, 0.048619f},
	0.0f,
	263.0f
};

const CameraCoordinates onSaturnRing =
{
	{18.493723f, 0.201660f, -0.163322f},
	{-0.272280f, 0.000000f, -0.962218f},
	{0.962218f, 0.000000f, -0.272280f},
	{0.000000f, 1.000000f, 0.000000f},
	0.000000f,
	263.0f
};
 
CameraCoordinates behindSaturn =
{
	{19.995113f, 4.701660f, -1.078648f},
	{-0.271931f, 0.050593f, -0.960986f},
	{0.960986f, 0.000000f, -0.271931f},
	{0.013758f, 0.997440f, 0.048619f},
	-30.0f,
	256.0f
};

LerpCameraCoordinates journeySceneCameraCoordinates =
{
    // begin 
    journeySceneCamStart,
    
    // end
    behindSaturn,

    // tVal
    0.0f,

    // speed
    1.0f / (FPS * 7.0f)
};

// for UNIX
struct Translation temp = {0};
GLfloat tempRotY = 0.0f;

// function definitions
void initializeSceneTwo(void)
{
    // code
    // set the journey scene state
    journeySceneState = BEGINNING_OF_WINDEV;

    // calculate the vertices required to revolve around a circle
    journeySceneSaturnTranslationTillUNIX = getCircleVerticesXZ(journeySceneOrbitRadius,  /* radius */
                                                                JOURNEY_SCENE_SATURN_ROTATION_SPEED_TILL_UNIX, /* speed / increment */ 
                                                                TRUE); /* do you need counter clockwise element calculation? */

    // iSaturnRevolutionElement = (GLuint)getInterpolatedValue(0.0f, 360.0f, 0.0f, journeySceneSaturnTranslationTillUNIXElements, 30.0f);
    journeySceneSaturnRevolutionLast = (GLuint)journeySceneSaturnTranslationTillUNIXElements;//getInterpolatedValue(0.0f, 360.0f, 0.0f, journeySceneSaturnTranslationTillUNIXElements, JOURNEY_SCENE_SATURN_ROTATION_ANGLE_TILL_UNIX + 30.0f);
    journeySceneCameraRevolutionLast = (GLuint)journeySceneSaturnTranslationTillUNIXElements;//getInterpolatedValue(0.0f, 360.0f, 0.0f, journeySceneSaturnTranslationTillUNIXElements, JOURNEY_SCENE_SATURN_ROTATION_ANGLE_TILL_UNIX);

    // initial saturn position
    iSaturnRevolutionElement = (GLuint)getInterpolatedValue(0.0f, 360.0f, 0.0f, journeySceneSaturnTranslationTillUNIXElements, 355.0f);

    journeySceneSaturnTranslation.x = journeySceneSaturnTranslationTillUNIX[iSaturnRevolutionElement].x;
    journeySceneSaturnTranslation.z = journeySceneSaturnTranslationTillUNIX[iSaturnRevolutionElement].z;

    // WinDev position
    journeySceneTextTransformations[0].rotation.y = 24.000000f;
    journeySceneTextTransformations[0].translation.x = journeySceneSaturnTranslation.x + 0.000000f;//- 3.5f;
    journeySceneTextTransformations[0].translation.y = journeySceneSaturnTranslation.y + 0.175f;//+ 1.0f;
    journeySceneTextTransformations[0].translation.z = journeySceneSaturnTranslation.z + -9.000000f;//- 3.5f;
}

void displaySceneTwo(void)
{
    // local variables
    static BOOL bOnce = FALSE;

    // code
#if SET_JOURNEY_SCENE_CAMERA
    if(!bOnce)
    {    
        // set camera coordinates
        setCameraCoordinatesUsingLookAt(journeySceneCamStart);
        bOnce = TRUE;
    }
#endif

    // draw skybox
    drawTexturedSkyboxWithRotation(journeySceneSkyboxRotation);

    // draw saturn's orbit
    DrawOrbitJourneyScene(journeySceneOrbitInnerRadius, journeySceneOrbitOuterRadius);

    // draw saturn
    glPushMatrix();
    {
        // glRotatef(journeySceneSaturnRev, 0.0f, 1.0f, 0.0f);
        glTranslatef(journeySceneSaturnTranslation.x, journeySceneSaturnTranslation.y, journeySceneSaturnTranslation.z);
        drawSaturn(journeySceneSatAndRingRot[0], journeySceneSatAndRingRot[1], SHOW_JOURNEY_SCENE_SATURN_ROCKS);
    }
    glPopMatrix();

    // draw "2002" and "WinDev"
    if(journeySceneState >= BEGINNING_OF_WINDEV && journeySceneState < BEGINNING_OF_UNIX)
    {
        glPushMatrix();
        {
            applyTransformations(journeySceneTextTransformations[0]);
            // glColor4f(1.0f, 1.0f, 1.0f, journeySceneWinDevAlpha);
            drawJourneySceneText(WINDEV_2002_TEXT, {0.0f, 0.0f, 0.0f}, 0.0f, journeySceneWinDevAlpha);
        }
        glPopMatrix();
    }

    // draw "2003" and "UNIX"
    if(journeySceneState >= BEGINNING_OF_UNIX && journeySceneState < BEGINNING_OF_SUNBEAM)
    {
        glPushMatrix();
        {
            applyTransformations(journeySceneTextTransformations[1]);
            drawJourneySceneText(UNIX_2003_TEXT, 
                                {0.0f, 0.0f, 0.0f}, 
                                0.0f, 
                                journeySceneUNIXAlpha);
        }
        glPopMatrix();
    }

    // draw "2004" and "SunBeam"
    if(g_bDrawSunbeam2006Text)
    {
        glPushMatrix();
        {
            applyTransformations(journeySceneTextTransformations[2]);
            // glColor4f(1.0f, 1.0f, 1.0f, journeySceneSunbeamAlpha);
            drawJourneySceneText(SUNBEAM_2006_TEXT, {0.0f, 0.0f, 0.0f}, 0.0f, journeySceneSunbeamAlpha);
        }
        glPopMatrix();
    }

    // draw "2008" and "OS Seminar"
    if(g_bDrawOSSeminar2008Text)
    {
        glPushMatrix();
        {
            applyTransformations(journeySceneTextTransformations[3]);
            // glColor4f(1.0f, 1.0f, 1.0f, journeySceneOSSeminarAlpha);
            drawJourneySceneText(OS_SEMINAR_2008_TEXT, {0.0f, 0.0f, 0.0f}, 0.0f, journeySceneOSSeminarAlpha);
        }
        glPopMatrix();
    }

    // for fade in / fade out
#if ENABLE_JOURNEY_SCENE_FADE
    fadeOutEffect(journeySceneFadeOutAlpha);
#endif

    // journeySceneTextTransformations[3].rotation.y = tempRotY + g_Transformations[0].rotation.y;
    // journeySceneTextTransformations[3].translation.x = temp.x + g_Transformations[0].translation.x;
    // journeySceneTextTransformations[3].translation.y = temp.y + g_Transformations[0].translation.y;
    // journeySceneTextTransformations[3].translation.z = temp.z + g_Transformations[0].translation.z;
}

void updateSceneTwo(void)
{
    // function prototypes
    void uninitializeSceneTwo(void);
    void journeySceneTimer(HWND hwnd, UINT iMsg, UINT_PTR idEvent, DWORD dwTime);

    // local variables
    GLfloat totalRot = -360.0f;
    GLfloat totalRotAfter = 0.0f;
    GLfloat deltaRadius = 0.35f;
    GLfloat circlingSpeed = -90.0f / (FPS * 18.0f);
    GLfloat saturnRevSpeed = (75.0f / (FPS * 18.0f));

    static GLfloat rot = 0.0f;
    static GLfloat rotAfter = 0.0f;

    static BOOL bStartSaturnRevolution = FALSE;
    static BOOL bStartMovingCameraBehindSaturn = FALSE;

    static BOOL bFadeInWinDevText = FALSE;
    static BOOL bFadeOutWinDevText = FALSE;

    static BOOL bFadeInUNIXText = FALSE;
    static BOOL bFadeOutUNIXText = FALSE;

    static BOOL bFadeIn2004Text = FALSE;
    static BOOL bFadeOut2004Text = FALSE;

    static BOOL bStartFadeOutAt21Seconds = FALSE;
    static BOOL bStartFadeInAt21Seconds = FALSE;

    static BOOL bFadeOutSunbeamText = FALSE;
    static BOOL bFadeInSunbeamText = FALSE;

    static BOOL bFadeOutOSSeminarText = FALSE;
    static BOOL bFadeInOSSeminarText = FALSE;

    // camera
    static GLfloat camLerpValStartToSaturn = 0.0f;
    static BOOL bCameraReachedPoint[10];
    static BOOL bRotateSkyboxOnce = FALSE;

    // for scene
    static BOOL bStartSceneFadeOut = FALSE;
    static BOOL bStartSceneFadeIn = FALSE;
    static BOOL bOnce = FALSE;

    // code
#if (DO_JOURNEY_SCENE_ANIMATION == 0)
    if(TRUE)
    {
        return ;
    }
#endif

#if PLAY_JOURNEY_SCENE_MUSIC
    if(!bOnce)
    {
        SetCurrentSourceForStats(g_SceneSources[3], FALSE, FALSE);
        playALSource(g_SceneSources[3]);
        bOnce = TRUE;
    }
#endif

    // rotate skybox
    updateRotationValue(&journeySceneSkyboxRotation, 360.0f / (FPS * 1000.0f));

    // move cam around orbit (behind saturn)
    if(bStartMovingCameraBehindSaturn)
    {
        cameraCoordinates.angleY = cameraCoordinates.angleY + circlingSpeed;
        rot = rot + circlingSpeed;

        cameraCoordinates.position.x = journeySceneSaturnTranslationTillUNIX[iCameraRevolutionElement].x; //cameraCoordinates.position.x + cameraCoordinates.front.x * -circlingSpeed * deltaRadius;  
        cameraCoordinates.position.z = journeySceneSaturnTranslationTillUNIX[iCameraRevolutionElement].z;//cameraCoordinates.position.z + cameraCoordinates.front.z * -circlingSpeed * deltaRadius;  

        iCameraRevolutionElement++;

        if(iCameraRevolutionElement >= journeySceneCameraRevolutionLast)
        {
            iCameraRevolutionElement = 0;
        }

        updateCameraVector();
    }

    // update saturn revolution
    if(bStartSaturnRevolution)
    {
        journeySceneSaturnTranslation.x = journeySceneSaturnTranslationTillUNIX[iSaturnRevolutionElement].x;
        journeySceneSaturnTranslation.z = journeySceneSaturnTranslationTillUNIX[iSaturnRevolutionElement].z;
        
        iSaturnRevolutionElement++;
        if(iSaturnRevolutionElement >= journeySceneSaturnRevolutionLast)
        {
            iSaturnRevolutionElement = 0;
        }
    }

    // update saturn rotation
    updateRotationValue(&journeySceneSatAndRingRot[0], 0.5f);
    updateRotationValue(&journeySceneSatAndRingRot[1], 0.75f);

    // set timer which will update every second
    if(!g_bJourneySceneTimerIsOn)
    {
        SetTimer(ghwnd, IDT_SECONDS_TIMER, 1000, (TIMERPROC)journeySceneTimer);
        g_bJourneySceneTimerIsOn = TRUE;
    }

    switch(journeySceneState)
    {
    case BEGINNING_OF_WINDEV:
        updateAlpha(&journeySceneFadeOutAlpha, 1.0f / (FPS * 1.0f), FADE_IN_FLAG);

        if(totalElapsedTime >= 3.0)
        {
            journeySceneState = MILESTONES;
        }
        break;
    case MILESTONES:

        // animate camera to a point behind saturn
        lerpCamera_v2(&journeySceneCameraCoordinates, 1.0f);

        if(!bStartSaturnRevolution)
        {
            bStartSaturnRevolution = TRUE;
        }
        
        if(updateObjectAlpha(&journeySceneWinDevAlpha, 1.0f / (FPS * 4.0f), FADE_OUT_FLAG) && journeySceneCameraCoordinates.bReached)
        {
            bStartMovingCameraBehindSaturn = TRUE;

            iCameraRevolutionElement = iSaturnRevolutionElement + 60;

            cameraCoordinates.angleX = 0.0f;
            cameraCoordinates.angleY = journeySceneTextTransformations[1].rotation.y = getInterpolatedValue(0.0f, 
                                                                                                            journeySceneSaturnTranslationTillUNIXElements, 
                                                                                                            0.0f, 
                                                                                                            360.0f,
                                                                                                            iSaturnRevolutionElement + 60) - 162.0f;
            
            cameraCoordinates.position.x = journeySceneSaturnTranslationTillUNIX[iCameraRevolutionElement].x;
            cameraCoordinates.position.y = 0.351660f;
            cameraCoordinates.position.z = journeySceneSaturnTranslationTillUNIX[iCameraRevolutionElement].z;
            
            updateCameraVector();

            journeySceneState = BEGINNING_OF_UNIX;
        }
        break;
    case BEGINNING_OF_UNIX:
        
        if(totalElapsedTime >= 10.8 && !g_bDrawUNIX2003Text)
        {
            g_bDrawUNIX2003Text = TRUE;
            journeySceneTextTransformations[1].rotation.y = getInterpolatedValue(0.0f, 
                                                                                journeySceneSaturnTranslationTillUNIXElements, 
                                                                                0.0f, 
                                                                                360.0f,
                                                                                iSaturnRevolutionElement + 350) + 3.000000f;

            // tempRotY = journeySceneTextTransformations[1].rotation.y;
            // temp.x = journeySceneSaturnTranslationTillUNIX[iSaturnRevolutionElement + 350].x;
            // temp.y = journeySceneSaturnTranslation.y;
            // temp.z = journeySceneSaturnTranslationTillUNIX[iSaturnRevolutionElement + 350].z;

            journeySceneTextTransformations[1].translation.x = journeySceneSaturnTranslationTillUNIX[iSaturnRevolutionElement + 350].x + -4.000000f; 
            journeySceneTextTransformations[1].translation.y = journeySceneSaturnTranslation.y;
            journeySceneTextTransformations[1].translation.z = journeySceneSaturnTranslationTillUNIX[iSaturnRevolutionElement + 350].z + -1.850000f;
        }

        if(totalElapsedTime >= 10.8 && !bFadeOutUNIXText)
        {
            bFadeOutUNIXText = updateObjectAlpha(&journeySceneUNIXAlpha, JOURNEY_SCENE_TEXT_FADE_SPEED, FADE_IN_FLAG);
        }

        if(totalElapsedTime >= 15.0 && bFadeOutUNIXText)
        {
            if(updateObjectAlpha(&journeySceneUNIXAlpha, JOURNEY_SCENE_TEXT_FADE_SPEED, FADE_OUT_FLAG))
            {
                journeySceneState = BEGINNING_OF_SUNBEAM;
            }
        }
        break;
    case BEGINNING_OF_SUNBEAM:
        if(totalElapsedTime >= 20.0 && !bStartSceneFadeIn)
        {   
            bStartSceneFadeIn = updateAlpha(&journeySceneFadeOutAlpha, 1.0f / (FPS * 0.7f), FADE_OUT_FLAG);
        }

        if(bStartSceneFadeIn)
        {
            if(!bRotateSkyboxOnce)
            {
                journeySceneSkyboxRotation = journeySceneSkyboxRotation + 180.0f;
                bRotateSkyboxOnce = TRUE;
            }

            updateAlpha(&journeySceneFadeOutAlpha, 1.0f / (FPS * 0.7f), FADE_IN_FLAG);
            if(!g_bDrawSunbeam2006Text)
            {
                g_bDrawSunbeam2006Text = TRUE;
                journeySceneTextTransformations[2].rotation.y = getInterpolatedValue(0.0f, 
                                                                                    journeySceneSaturnTranslationTillUNIXElements, 
                                                                                    0.0f, 
                                                                                    360.0f,
                                                                                    iSaturnRevolutionElement + 50) + 45.000000f;

                // temp.x = journeySceneSaturnTranslationTillUNIX[iSaturnRevolutionElement + 50].x;
                // temp.y = journeySceneSaturnTranslation.y;
                // temp.z = journeySceneSaturnTranslationTillUNIX[iSaturnRevolutionElement + 50].z;

                journeySceneTextTransformations[2].translation.x = journeySceneSaturnTranslationTillUNIX[iSaturnRevolutionElement + 50].x + -11.050004f; 
                journeySceneTextTransformations[2].translation.y = journeySceneSaturnTranslation.y;
                journeySceneTextTransformations[2].translation.z = journeySceneSaturnTranslationTillUNIX[iSaturnRevolutionElement + 50].z + 8.500006f;
            }

            if(totalElapsedTime >= 21.5f && !bFadeOutSunbeamText)
            {
                bFadeOutSunbeamText = updateObjectAlpha(&journeySceneSunbeamAlpha, 1.0f / (FPS * 1.0f), FADE_IN_FLAG);
            }
        }

        if(totalElapsedTime >= 29.0f && bFadeOutSunbeamText)
        {
            if(updateObjectAlpha(&journeySceneSunbeamAlpha, 1.0f / (FPS * 1.0f), FADE_OUT_FLAG))
            {
                journeySceneState = YEARS_PASSED_ON;
            }
        }
        break;
    case YEARS_PASSED_ON:
        if(totalElapsedTime >= 31.5 && !g_bDrawOSSeminar2008Text)
        {
            g_bDrawOSSeminar2008Text = TRUE;
            journeySceneTextTransformations[3].rotation.y = getInterpolatedValue(0.0f, 
                                                                                journeySceneSaturnTranslationTillUNIXElements, 
                                                                                0.0f, 
                                                                                360.0f,
                                                                                iSaturnRevolutionElement + 50) + 50.300020f;//50.000000f;

            // tempRotY = journeySceneTextTransformations[3].rotation.y;
            // temp.x = journeySceneSaturnTranslationTillUNIX[iSaturnRevolutionElement + 50].x;
            // temp.y = journeySceneSaturnTranslation.y;
            // temp.z = journeySceneSaturnTranslationTillUNIX[iSaturnRevolutionElement + 50].z;

            journeySceneTextTransformations[3].translation.x = journeySceneSaturnTranslationTillUNIX[iSaturnRevolutionElement + 50].x + -0.750001f;//-0.950000f; 
            journeySceneTextTransformations[3].translation.y = journeySceneSaturnTranslation.y;
            journeySceneTextTransformations[3].translation.z = journeySceneSaturnTranslationTillUNIX[iSaturnRevolutionElement + 50].z + 17.699958f;//18.0f;
        }

        if(g_bDrawOSSeminar2008Text && !bFadeOutOSSeminarText)
        {
            bFadeOutOSSeminarText = updateObjectAlpha(&journeySceneOSSeminarAlpha, 1.0f / (FPS * 2.0f), FADE_IN_FLAG);
        }

        if(totalElapsedTime >= 40.5 && bFadeOutOSSeminarText)
        {
            updateObjectAlpha(&journeySceneOSSeminarAlpha, 1.0f / (FPS * 1.0f), FADE_OUT_FLAG);
        }

        if(totalElapsedTime >= 42.0)
        {
            journeySceneState = FADE_OUT;
        }
        break;
    case FADE_OUT:
        if(updateAlpha(&journeySceneFadeOutAlpha, 1.0f / (FPS * 1.0f), FADE_OUT_FLAG))
        {
            journeySceneState = END;
        }
        break;
    case END:
        uninitializeSceneTwo();
        ResetGlobalTimer();
        setCameraCoordinates({{0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 1.0f, 0.0f}, 0.0f, 270.0f});
        currentScene = SCENE_OBSTACLES;
        break;
    }
/*
    // enable flags based on seconds
    switch(g_JourneySceneElapsedSeconds)
    {
    // ---------------------------- FOR WINDEV ---------------------------- 
    case 3:
        // draw and fade out 2002 WinDev text
        // journeySceneTextTransformations[0].rotation.y = getInterpolatedValue(0.0f, 
        //                                                                      journeySceneSaturnTranslationTillUNIXElements, 
        //                                                                      0.0f, 
        //                                                                      360.0f,
        //                                                                      iSaturnRevolutionElement);
                                                                             
        // journeySceneTextTransformations[0].translation.x = journeySceneSaturnTranslation.x - 3.5f;
        // journeySceneTextTransformations[0].translation.y = journeySceneSaturnTranslation.y + 1.0f;
        // journeySceneTextTransformations[0].translation.z = journeySceneSaturnTranslation.z - 3.5f;
        g_bDrawWinDev2002Text = TRUE;
        bFadeOutWinDevText = TRUE;
        break;

    case 6:
        // fade in 2002 WinDev text
        bFadeOutWinDevText = FALSE;
        bFadeInWinDevText = TRUE;
        break;

    // ---------------------------- FOR UNIX && 2004 text ----------------------------
    case 11:
        // draw and fade out 2003 UNIX text
        journeySceneTextTransformations[1].rotation.y = getInterpolatedValue(0.0f, 
                                                                             journeySceneSaturnTranslationTillUNIXElements, 
                                                                             0.0f, 
                                                                             360.0f,
                                                                             iSaturnRevolutionElement);
                                                                             
        journeySceneTextTransformations[1].translation.x = journeySceneSaturnTranslation.x - 1.5f;
        journeySceneTextTransformations[1].translation.y = journeySceneSaturnTranslation.y + 1.0f;
        journeySceneTextTransformations[1].translation.z = journeySceneSaturnTranslation.z - 3.5f;
        g_bDrawUNIX2003Text = TRUE;
        bFadeOutUNIXText = TRUE;
        break;

    // case 13:
    //     // draw and fade out 2004 text
    //     journeySceneTextTransformations[2].rotation.y = getInterpolatedValue(0.0f, 
    //                                                                          journeySceneSaturnTranslationTillUNIXElements, 
    //                                                                          0.0f, 
    //                                                                          360.0f,
    //                                                                          iSaturnRevolutionElement);
                                                                             
    //     journeySceneTextTransformations[2].translation.x = journeySceneSaturnTranslation.x - 5.5f;
    //     journeySceneTextTransformations[2].translation.y = journeySceneSaturnTranslation.y + 1.0f;
    //     journeySceneTextTransformations[2].translation.z = journeySceneSaturnTranslation.z - 2.5f;
    //     g_bDraw2004YearText = TRUE;
    //     bFadeOut2004Text = TRUE;
    //     break;

    // case 18:
    //     // fade in 2004 text
    //     bFadeOut2004Text = FALSE;
    //     bFadeIn2004Text = TRUE;
    //     break;

    case 14:
        // fade in 2003 UNIX text
        bFadeOutUNIXText = FALSE;
        bFadeInUNIXText = TRUE;
        break;

    // ---------------------------- FADE OUT SCENE TO BLACK ----------------------------
    case 20:
        bStartFadeOutAt21Seconds = TRUE;
        break;

    case 22:
        // draw and fade out Sunbeam 2006 text
        journeySceneTextTransformations[3].rotation.y = getInterpolatedValue(0.0f, 
                                                                             journeySceneSaturnTranslationTillUNIXElements, 
                                                                             0.0f, 
                                                                             360.0f,
                                                                             iSaturnRevolutionElement);
                                                                             
        journeySceneTextTransformations[3].translation.x = journeySceneSaturnTranslation.x + 4.2f;
        journeySceneTextTransformations[3].translation.y = journeySceneSaturnTranslation.y + 1.0f;
        journeySceneTextTransformations[3].translation.z = journeySceneSaturnTranslation.z + 11.5f;
        
        g_bDrawSunbeam2006Text = TRUE;
        bFadeOutSunbeamText = TRUE;
        break;
    
    case 24:
        // fade in Sunbeam 2006 text
        bFadeOutSunbeamText = FALSE;
        bFadeInSunbeamText = TRUE;
        break;

    case 27:
        // draw and fade out OS Seminar 2008 text
        journeySceneTextTransformations[4].rotation.y = getInterpolatedValue(0.0f, 
                                                                             journeySceneSaturnTranslationTillUNIXElements, 
                                                                             0.0f, 
                                                                             360.0f,
                                                                             iSaturnRevolutionElement);
                                                                             
        journeySceneTextTransformations[4].translation.x = journeySceneSaturnTranslation.x + 3.8f;
        journeySceneTextTransformations[4].translation.y = journeySceneSaturnTranslation.y + 1.0f;
        journeySceneTextTransformations[4].translation.z = journeySceneSaturnTranslation.z + 11.5f;
        g_bDrawOSSeminar2008Text = TRUE;
        bFadeOutOSSeminarText = TRUE;
        break;

    case 30:
        bFadeOutOSSeminarText = FALSE;
        bFadeInOSSeminarText = TRUE;
        break;

    case 43:
        bStartSceneFadeOut = TRUE;
        break;
    }

    switch(journeySceneState)
    {
    case BEGINNING_OF_WINDEV:
        // fade in to scene
        updateAlpha(&journeySceneFadeInAlpha, 1.0f / (FPS * 1.5f), FADE_IN_FLAG);

        // lerp camera to the back of saturn simulaneously
        if(lerpCamera(&journeySceneCamStart, &behindSaturn, &camLerpValStartToSaturn, 1.0f / (FPS * 3.0f)))
        {
            //setCameraCoordinates(behindSaturn);
            bStartSaturnRevolution = TRUE;
            bStartMovingCameraBehindSaturn = TRUE;
            journeySceneState = MILESTONES;    
        }
        break;
    case MILESTONES:
        break;
    case END:
        uninitializeSceneTwo();
        currentScene = SCENE_OBSTACLES;
        return;
    default:
        break;
    }

    // FADE OUT scene before ending it
    if(bStartSceneFadeOut)
    {
        if(updateAlpha(&journeySceneFadeOutAlpha, 1.0f / (FPS * 1.0f), FADE_OUT_FLAG))
        {
            journeySceneState = END;
        }
    }

    // FADE OUT SCENE at 20 / 21 seconds (elapsed time)
    if(bStartFadeOutAt21Seconds)
    {
        if(updateAlpha(&journeySceneFadeOutAlpha, 1.0f / (FPS * 1.5f), FADE_OUT_FLAG))
        {
            // cameraCoordinates.angleY = cameraCoordinates.angleY + 7.0f; 
            updateCameraVector();

            journeySceneFadeOutAlpha = 0.0f;
            journeySceneFadeInAlpha = 1.0f;
            bStartFadeInAt21Seconds = TRUE;
            bStartFadeOutAt21Seconds = FALSE;
        }
    }

    // fade in scene at 21 seconds
    if(bStartFadeInAt21Seconds)
    {
        if(updateAlpha(&journeySceneFadeInAlpha, 1.0f / (FPS * 1.5f), FADE_IN_FLAG))
        {
            bStartFadeInAt21Seconds = FALSE;
        }
    }

    // ----------------------------------------------------------------------------------------------
    // fade out WinDev text
    if(bFadeOutWinDevText)
    {
        updateAlpha(&journeySceneWinDevAlpha, 1.0f / (FPS * 2.0f), FADE_OUT_FLAG);
    }

    // fade in WinDev text
    if(bFadeInWinDevText)
    {
        if(updateAlpha(&journeySceneWinDevAlpha, 1.0f / (FPS * 2.0f), FADE_IN_FLAG))
        {
            g_bDrawWinDev2002Text = FALSE;
            bFadeInWinDevText = FALSE;
        }
    }

    // ----------------------------------------------------------------------------------------------
    // fade out UNIX text
    if(bFadeOutUNIXText)
    {
        updateAlpha(&journeySceneUNIXAlpha, 1.0f / (FPS * 2.0f), FADE_OUT_FLAG);
    }

    // fade in UNIX text
    if(bFadeInUNIXText)
    {
        if(updateAlpha(&journeySceneUNIXAlpha, 1.0f / (FPS * 2.0f), FADE_IN_FLAG))
        {
            g_bDrawUNIX2003Text = FALSE;
            bFadeInUNIXText = FALSE;
        }
    }

    // ----------------------------------------------------------------------------------------------
    // fade out 2004 text
    if(bFadeOut2004Text)
    {
        updateAlpha(&journeyScene2004Alpha, 1.0f / (FPS * 2.0f), FADE_OUT_FLAG);
    }

    // fade in 2004 text
    if(bFadeIn2004Text)
    {
        if(updateAlpha(&journeyScene2004Alpha, 1.0f / (FPS * 2.0f), FADE_IN_FLAG))
        {
            g_bDraw2004YearText = FALSE;
            bFadeIn2004Text = FALSE;
        }
    }

    // ----------------------------------------------------------------------------------------------
    // fade out Sunbeam text
    if(bFadeOutSunbeamText)
    {
        updateAlpha(&journeySceneSunbeamAlpha, 1.0f / (FPS * 2.0f), FADE_OUT_FLAG);
    }

    // fade in Sunbeam text
    if(bFadeInSunbeamText)
    {
        if(updateAlpha(&journeySceneSunbeamAlpha, 1.0f / (FPS * 2.0f), FADE_IN_FLAG))
        {
            g_bDrawSunbeam2006Text = FALSE;
            bFadeInSunbeamText = FALSE;
        }
    }

    // ----------------------------------------------------------------------------------------------
    // fade out OS Seminar
    if(bFadeOutOSSeminarText)
    {
        updateAlpha(&journeySceneOSSeminarAlpha, 1.0f / (FPS * 2.0f), FADE_OUT_FLAG);
    }

    // fade in OS Seminar
    if(bFadeInOSSeminarText)
    {
        if(updateAlpha(&journeySceneOSSeminarAlpha, 1.0f / (FPS * 2.0f), FADE_IN_FLAG))
        {
            g_bDrawOSSeminar2008Text = FALSE;
            bFadeInOSSeminarText = FALSE;
        }
    }
    
    // ----------------------------------------------------------------------------------------------

*/
}

void uninitializeSceneTwo(void)
{
    // code
    // stopALSource(g_SceneSources[3]);
    // SetCurrentSourceForStats(0, FALSE, FALSE);
    
    if(journeySceneSaturnTranslationTillUNIX)
    {
        free(journeySceneSaturnTranslationTillUNIX);
        journeySceneSaturnTranslationTillUNIX = NULL;
    }
}

void journeySceneTimer(HWND hwnd, UINT iMsg, UINT_PTR idEvent, DWORD dwTime)
{
    // code
    KillTimer(hwnd, idEvent);
    if(idEvent == IDT_SECONDS_TIMER)
    {
        g_JourneySceneElapsedSeconds++;
    }
    g_bJourneySceneTimerIsOn = FALSE;
}
