#pragma once

#include "Common.h"

// struct for SplashScreen object
typedef struct tagSplashScreen
{
    WNDCLASSEXW wc;
    HWND hSplashWnd;
    HINSTANCE hInst;
    HBITMAP hBitmap;
    BITMAP bmp;
    HDC hSplashDC, hMemoryDC;
    WPARAM wParam;
} SplashScreen;

// function declarations
BOOL InitSplashScreen(SplashScreen *splashScreen, HWND hwndParent, HINSTANCE hInstance, WNDPROC SplashWndProc, WCHAR *szBitmapResource, int iFadeInMs);
BOOL ShowSplashScreen(SplashScreen *splashScreen, WPARAM wParam);
BOOL HideSplashScreen(SplashScreen *splashScreen, int iFadeInMs);
BOOL LoadSplashScreenBmp(SplashScreen *splashScreen, WCHAR *szBitmapResource);
BOOL DeleteSplashScreen(SplashScreen *splashScreen);
