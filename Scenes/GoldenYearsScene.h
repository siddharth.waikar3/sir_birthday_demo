#pragma once

// function declarations
void initializeSceneFive(void);
void displaySceneFive(void);
void updateSceneFive(void);
void uninitializeSceneFive(void);