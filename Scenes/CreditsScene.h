#pragma once

// function declarations
void initializeSceneCredits(void);
void displaySceneCredits(void);
void updateSceneCredits(void);
void uninitializeSceneCredits(void);