// header files
#include "SphereCollision.h"
#include "../Common/MergeSort.h"

#ifdef ENABLE_CUDA
    #include "../Common/CUDALibClient.h" // for CUDA library
#endif

#include <time.h>

#define DT 1.0f / 60.0f
#define TOTAL_LATS 30
#define TOTAL_LONGS 30

#define SPHERE_GRAVITY 0.095f

// function definitions
void initializeSphereCollisionSystem(GLfloat radius, BoundingSphere *bSphere, SphereAttribs *sphereAttribs, GLint numSpheres)
{
    // code
    bSphere->qbSphereObj = gluNewQuadric();
    bSphere->origin = {0.0f, 0.0f, 0.0f};
    // bSphere->radius = BOUNDING_SPHERE_RADIUS;
    bSphere->color[0] = 0.5f;
    bSphere->color[1] = 0.5f;
    bSphere->color[2] = 0.5f;
    bSphere->color[3] = 0.1f;
    bSphere->vertices = getSphereVertices(TOTAL_LATS, TOTAL_LONGS);

    // srand(time(NULL));
    for(int i = 0; i < numSpheres; i++)
    {
        // calculate random values for sphere center
        GLfloat cx = getInterpolatedValue(0.0f, (GLfloat)RAND_MAX, -0.6f, 0.6f, rand());
        GLfloat cy = getInterpolatedValue(0.0f, (GLfloat)RAND_MAX, -0.6f, 0.6f, rand());
        GLfloat cz = getInterpolatedValue(0.0f, (GLfloat)RAND_MAX, -0.6f, 0.6f, rand());

        // calculate random values for gravity / velocity
        GLfloat gx = getInterpolatedValue(0.0f, (GLfloat)RAND_MAX, -SPHERE_GRAVITY, SPHERE_GRAVITY, rand());
        GLfloat gy = getInterpolatedValue(0.0f, (GLfloat)RAND_MAX, -SPHERE_GRAVITY, SPHERE_GRAVITY, rand());
        GLfloat gz = getInterpolatedValue(0.0f, (GLfloat)RAND_MAX, -SPHERE_GRAVITY, SPHERE_GRAVITY, rand());

        // calculate random color values
        GLfloat r = getInterpolatedValue(0.0f, (GLfloat)RAND_MAX, 0.0f, 1.0f, rand());
        GLfloat g = getInterpolatedValue(0.0f, (GLfloat)RAND_MAX, 0.0f, 1.0f, rand());
        GLfloat b = getInterpolatedValue(0.0f, (GLfloat)RAND_MAX, 0.0f, 1.0f, rand());

        // assign random values
        sphereAttribs[i].grav = {gx, gy, gz};
        sphereAttribs[i].old_pos = {0.0f, 0.0f, 0.0f};
        sphereAttribs[i].pos = {0.0f, 0.0f, 0.0f};
        sphereAttribs[i].color = {r, g, b, 1.0f};
        sphereAttribs[i].mass = radius;
        sphereAttribs[i].drag = 0.001f;
    }
}

void drawSphereCollisionSystem(BoundingSphere *bSphere, SphereAttribs *sphereAttribs, GLuint texture, GLint numSpheres)
{
    // code
    for(int i = 0; i < numSpheres; i++)
    {
        // draw each sphere
        glPushMatrix();
        {
            glTranslatef(sphereAttribs[i].pos.x, sphereAttribs[i].pos.y, sphereAttribs[i].pos.z);

            glColor4f(sphereAttribs[i].color.r, sphereAttribs[i].color.g, sphereAttribs[i].color.b, sphereAttribs[i].color.alpha);
            drawSphereVerticesWithTexture(bSphere->vertices, TOTAL_LATS, TOTAL_LONGS, texture, GL_QUADS, sphereAttribs[i].mass);
        }
        glPopMatrix();
    }

    // draw bounding sphere (silhouette)
    //drawSphereVertices(bSphere->vertices, TOTAL_LATS, TOTAL_LONGS, bSphere->color, 0.0f, GL_LINE_STRIP, bSphere->radius);
}

BOOL updateSphereCollisionSystem(BoundingSphere *bSphere, struct SphereAttribs *sphereAttribs, GLint numSpheres)
{
    // function prototypes
    void applyGravity(Vec3CUDA *acc, Vec3CUDA gravity);
    void applyConstraints(BoundingSphere *bSphere, SphereAttribs *sphereObject);
    void updatePosition(SphereAttribs *sphereObject, GLfloat dt);
    void solveCollision(SphereAttribs *sphereAttribs, GLint numSpheres);

    // local variables
    // GLint subSteps = 1;
    // GLfloat subDT = (DT) / (GLfloat)subSteps;

    // code
    for(int i = 0; i < numSpheres; i++)
    {   
        // apply gravity
        applyGravity(&sphereAttribs[i].acc, sphereAttribs[i].grav); /* gravity was originally {0.0f, -1.0f, 0.0f} */

        // check for collision between sphere and bounding sphere (apply constraints)
        applyConstraints(bSphere, &sphereAttribs[i]);

        // update position
        updatePosition(&sphereAttribs[i], DT);            
    }

#ifdef ENABLE_CUDA
    solveCollisionCUDA(sphereAttribs, numSpheres);
#else
    // solve collisions
    solveCollision(sphereAttribs, numSpheres);
#endif

    return(FALSE);
}

void uninitializeSphereCollisionSystem(BoundingSphere *bSphere)
{
    // code
    if(bSphere->vertices)
    {
        freeVertices(&bSphere->vertices);
    }

    if(bSphere->qbSphereObj)
    {
        gluDeleteQuadric(bSphere->qbSphereObj);
        bSphere->qbSphereObj = NULL;
    }
}

// -------------------------------------------------- INTERNAL FUNCTIONS --------------------------------------------------
BOOL objectOutOfBounds(GLfloat bSphereRadius, GLfloat distance)
{
    // code
    if(distance > bSphereRadius)
    {
        return(TRUE);
    }

    return(FALSE);
}

void applyGravity(Vec3CUDA *acc, Vec3CUDA gravity)
{
    // code
    acc->x = acc->x + gravity.x;
    acc->y = acc->y + gravity.y;
    acc->z = acc->z + gravity.z;
}

void applyConstraints(BoundingSphere *bSphere, struct SphereAttribs *sphereObject)
{
    // function prototypes
    GLfloat vecLen(Vec3CUDA vec);
    Vec3CUDA vecAdd(Vec3CUDA vec1, Vec3CUDA vec2);
    Vec3CUDA vecSub(Vec3CUDA vec1, Vec3CUDA vec2);
    Vec3CUDA vecMulScalar(Vec3CUDA vec, GLfloat val);
    Vec3CUDA vecDivScalar(Vec3CUDA vec, GLfloat val);

    // code
    Vec3CUDA distToBsphere = vecSub(sphereObject->pos, bSphere->origin);
    GLfloat distance = vecLen(distToBsphere);
    if(distance > (bSphere->radius - sphereObject->mass))
    {
        Vec3CUDA n = vecDivScalar(distToBsphere, distance);
        sphereObject->pos = vecAdd(bSphere->origin, vecMulScalar(n, (bSphere->radius - sphereObject->mass)));
    }
}

void updatePosition(struct SphereAttribs *sphereObject, GLfloat dt)
{
    // function prototypes
    Vec3CUDA vecAdd(Vec3CUDA vec1, Vec3CUDA vec2);
    Vec3CUDA vecSub(Vec3CUDA vec1, Vec3CUDA vec2);
    Vec3CUDA vecMulScalar(Vec3CUDA vec, GLfloat val);

    // code
    Vec3CUDA velocity = vecSub(sphereObject->pos, sphereObject->old_pos);

    // save current position
    sphereObject->old_pos = sphereObject->pos;

    // apply verlet integration
    sphereObject->pos = vecAdd(vecAdd(sphereObject->pos, velocity), vecMulScalar(sphereObject->acc, dt * dt));

    // reset acceleration
    sphereObject->acc = {0.0f, 0.0f, 0.0f};
}

void solveCollision(struct SphereAttribs *sphereAttribs, GLint numSpheres)
{
    // function prototypes
    Vec3CUDA vecAdd(Vec3CUDA vec1, Vec3CUDA vec2);
    Vec3CUDA vecSub(Vec3CUDA vec1, Vec3CUDA vec2);
    Vec3CUDA vecDivScalar(Vec3CUDA vec, GLfloat val);
    Vec3CUDA vecMulScalar(Vec3CUDA vec, GLfloat val);
    GLfloat vecLen(Vec3CUDA vec);

    // code
    for(int i = 0; i < numSpheres; i++)
    {
        SphereAttribs *objectOne = &sphereAttribs[i];
        for(int j = (i + 1); j < numSpheres; j++)
        {
            SphereAttribs *objectTwo = &sphereAttribs[j];
            Vec3CUDA v = vecSub(objectOne->pos, objectTwo->pos);
            GLfloat dist = vecLen(v);
            GLfloat minimumDist = objectOne->mass + objectTwo->mass;
            if(dist < minimumDist)
            {
                Vec3CUDA n = vecDivScalar(v, dist);
                GLfloat delta = minimumDist - dist;
                
                objectOne->pos = vecAdd(objectOne->pos, vecMulScalar(n, (0.001f /* was 0.5f */ * delta)));
                objectTwo->pos = vecSub(objectTwo->pos, vecMulScalar(n, (0.001f * delta)));
            }
        }
    }
}

float vecLen(Vec3CUDA vec)
{
    // code
    return(fabsf(sqrtf(powf(vec.x, 2) +  powf(vec.y, 2) + powf(vec.z, 2))));
}

Vec3CUDA vecAdd(Vec3CUDA vec1, Vec3CUDA vec2)
{
    // local variables
    Vec3CUDA temp;
    
    // code
    temp.x = vec1.x + vec2.x;
    temp.y = vec1.y + vec2.y;
    temp.z = vec1.z + vec2.z;

    return(temp);
}

Vec3CUDA vecSub(Vec3CUDA vec1, Vec3CUDA vec2)
{
    // local variables
    Vec3CUDA temp;
    
    // code
    temp.x = vec1.x - vec2.x;
    temp.y = vec1.y - vec2.y;
    temp.z = vec1.z - vec2.z;

    return(temp);
}

Vec3CUDA vecMul(Vec3CUDA vec1, Vec3CUDA vec2)
{
    // local variables
    Vec3CUDA temp;

    // code
    temp.x = vec1.x * vec2.x;
    temp.y = vec1.y * vec2.y;
    temp.z = vec1.z * vec2.z;

    return(temp);
}

Vec3CUDA vecMulScalar(Vec3CUDA vec, float val)
{
    // code
    vec.x = vec.x * val;
    vec.y = vec.y * val;
    vec.z = vec.z * val;

    return(vec);
}

Vec3CUDA vecDivScalar(Vec3CUDA vec, float val)
{
    // code
    vec.x = vec.x / val;
    vec.y = vec.y / val;
    vec.z = vec.z / val;

    return(vec);
}
