// header files
#include "..\Common\Common.h"
#include "..\Objects\SlideText.h"
#include "..\Objects\Saturn.h"
#include "..\Common\Camera.h"
#include "..\Objects\Skybox.h"
#include "..\Objects\Orbit.h"
#include "..\Objects\Icosahedron.h"

#define SHOW_ROCKS_GOLDEN_YEARS_SCENE SHOW_ROCKS_GLOBAL
#define RISE_OF_PHOENIX_NUM_ROCKS 100

GLfloat g_lerpVal = 0.0f;
GLfloat g_lerpSpeed = 0.001f;

GLfloat g_fCameraSpeed;

Vec3* g_OrbitRotation = NULL;
int g_iNumberOfElements = 1200;
extern CameraCoordinates cameraCoordinates;
extern IcosahedronAttribs_v2 riseOfPhoenixRockAttribs_shot2[RISE_OF_PHOENIX_NUM_ROCKS];

#define CAMERA_SPEED    90.0f / (FPS * 18.0f)
//#define CAMERA_SPEED    0.2f
#define GOLDEN_YEARS_SCENE_SECONDS_OFFSET 0
enum GoldenYearsSceneState
{
    TEXT_2009 = 1,
    TEXT_APPLE_FCP,
    TEXT_MASTER,
    TEXT_2012,
    TEXT_FUNDAMENTAL1,
    TEXT_FUNDAMENTAL2,
    TEXT_2016,
    TEXT_ARM_ARM,
    TEXT_ARM_ACCREDITED,
    TEXT_ARM_ENGINEER,
    TEXT_2017,
    TEXT_RTR,
    TEXT_2022,
    TEXT_WINDEV,
    OBJECT_SATURN,

    TOTAL_NUMBERS
};

extern HWND ghwnd;
extern enum Scene currentScene;
extern FILE* gpFILE;
enum GoldenYearsSceneState goldenYearSceneState;
extern struct Transformations g_Transformations[NUM_TRANSFORMATION_ELEMENTS];
extern UINT iCurrentWidth;
extern UINT iCurrentHeight;

#define IDT_GOLDEN_YEARS_TIMER 1001
#define GOLDEN_COLOR_CODE   0.82f, 0.69f, 0.0f
#define ORBIT_INNER_RADIUS    10.0f
#define ORBIT_OUTER_RADIUS    10.01f
#define PERFECT_RADIUS  ((ORBIT_INNER_RADIUS+ORBIT_OUTER_RADIUS)/2.0f)

#define SCENE_START_TIME    139
#define TEXT_1              SCENE_START_TIME + 0 
#define TEXT_2              SCENE_START_TIME + 4 
#define TEXT_3              SCENE_START_TIME + 8 
#define TEXT_4              SCENE_START_TIME + 12
#define TEXT_FADE_OUT       SCENE_START_TIME + 15
#define TEXT_5              SCENE_START_TIME + 16
#define STOP_CAMERA         SCENE_START_TIME + 17
#define DRAW_SATURN         SCENE_START_TIME + 19
#define LERP_CAMERA         SCENE_START_TIME + 21
#define SCENE_FADEOUT       SCENE_START_TIME + 23

unsigned long long g_GoldenYearsSceneElapsedSeconds = SCENE_START_TIME;

GLfloat goldenYearsSkyboxRotation = 0.0f;

const struct Transformations Ring = {
    {0.000000f, 0.000000f, 0.000000f},
    {-90.000000f, 0.000000f, 0.000000f},
    {1.000000f, 1.000000f, 1.000000f}
};
BOOL turnoncamera = TRUE;
BOOL lerpcamera = FALSE;

GLfloat g_fSaturnRotation = 0.0f;
GLfloat g_fRingRotation = 0.0f;

// for text
BOOL g_bDrawText1 = FALSE;
BOOL g_bDrawText2 = FALSE;
BOOL g_bDrawText3 = FALSE;
BOOL g_bDrawText4 = FALSE;
BOOL g_bDrawText5 = FALSE;

GLfloat g_fAlpha_Text1 = 0.0f;
GLfloat g_fAlpha_Text2 = 0.0f;
GLfloat g_fAlpha_Text3 = 0.0f;
GLfloat g_fAlpha_Text4 = 0.0f;
GLfloat g_fAlpha_Text5 = 0.0f;
GLfloat g_fAlpha_Scene1 = 0.0f;
GLfloat g_fAlpha_Scene2 = 0.0f;
BOOL bDrawSaturn = FALSE;

BOOL g_bGoldenYearsTimerIsOn = FALSE;

UINT g_iGoldenYearsSecondCounter = 0;

#define ORIGINAL_TEXT_COORDINATES       1
#define ORIGINAL_CAMERACOORDINATES      1


struct Translation GoldenYearsSceneTextTranslation = { PERFECT_RADIUS, 0.05f, 0.0f };
struct Transformations GoldenYearsSceneTextTransformations[TOTAL_NUMBERS] = {0};

CameraCoordinates g_CameraCoordinates = {
    {11.774279f, 7.229740f, -11.072616f},
    {-0.871296f, -0.484810f, 0.076179f},
    {-0.076179f, 0.000000f, -0.871296f},
    {-0.422413f, 0.764960f, 0.036932f},
    -29.000000f,
    175.003250f
};


CameraCoordinates g_InitialPos = {
    {0.855691f, 0.200000f, -9.968341f},
    {-0.996440f, 0.000000f, -0.084304f},
    {0.084304f, 0.000000f, -0.996440f},
    {0.000000f, 1.000000f, 0.000000f},
    0.000000f,
    184.835983f
};

CameraCoordinates g_TargetPos = {
    {1.866503f, 0.700000f, -9.881333f},
    {-0.984050f, -0.156434f, -0.084697f},
    {0.084697f, 0.000000f, -0.984050f},
    {-0.153939f, 0.975528f, -0.013250f},
    -9.000000f,
    184.919312f
};

// function definitions
void initializeSceneFive(void)
{
    // code
    GLfloat fInterpolateValue90;
    GLfloat fInterpolateValue180;
    GLfloat fInterpolateValue270;
    GLfloat fInterpolateValue360;

    void InitializeTextPos(void);

    g_fCameraSpeed = CAMERA_SPEED;
    //g_iNumberOfElements = (int)ceilf(360.0f / CAMERA_SPEED);
    g_OrbitRotation = getCircleVerticesXZ(
                                                PERFECT_RADIUS,              /* radius */
                                                CAMERA_SPEED,              /* speed / increment */
                                                TRUE                /* do you need counter clockwise element calculation? */
                                             ); 


    InitializeTextPos();
}

void displaySceneFive(void)
{
    void drawOrbitText(int iChange);
    void drawAppleFCPMaster(void);
    void drawOrbitText(char* szString);
    void drawARMAccreditedEngineer(void);
    void drawRTR(void);
    void drawWinDev(void);

    static int once1 = 0;
    static int once2 = 0;
    static int once3 = 0;

#if(ORIGINAL_CAMERACOORDINATES)
    CameraCoordinates cc = {
        {0.000000f, 0.200000f, 0.000000f},
        {0.000000f, 0.000000f, 0.000000f},
        {0.000000f, 0.000000f, 0.000000f},
        {0.000000f, 1.000000f, 0.000000f},
        0.000000f,
        270.0f
    };
#else
    CameraCoordinates cc = {
    {11.774279f, 7.229740f, -11.072616f},
    {-0.871296f, -0.484810f, 0.076179f},
    {-0.076179f, 0.000000f, -0.871296f},
    {-0.422413f, 0.764960f, 0.036932f},
    -29.000000f,
    175.003250f
    };
#endif
    
    //code
    if(!once3)
    {
        setCameraCoordinates(cc);
        // setCameraCoordinatesUsingLookAt(g_InitialPos);
        once3 = 1;
    }

    drawTexturedSkyboxWithRotation(goldenYearsSkyboxRotation);

    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    {
        // reset the projection matrix
        glLoadIdentity();

        // add new projection
        gluPerspective(75.0, (GLdouble)iCurrentWidth / (GLdouble)iCurrentHeight, 0.1, 10000.0);

        glMatrixMode(GL_MODELVIEW);

        // rocks from rise of phoenix
        glPushMatrix();
        {
            applyTransformations({
                                    {-10.000000f, 1.000000f, 15.500000f},
                                    {0.000000f, 90.000000f, 0.000000f},
                                    {1.000000f, 1.000000f, 1.000000f}
                                });
            drawRangeIcosahedrons_Asteroids(riseOfPhoenixRockAttribs_shot2, RISE_OF_PHOENIX_NUM_ROCKS / 2, 0, 1.0f, 1);
            drawRangeIcosahedrons_Asteroids(&riseOfPhoenixRockAttribs_shot2[RISE_OF_PHOENIX_NUM_ROCKS / 2], RISE_OF_PHOENIX_NUM_ROCKS / 2, 0, 1.0f, 2);
        }
        glPopMatrix();

        glPushMatrix();
        {
            applyTransformations(Ring);
            DrawOrbit(ORBIT_INNER_RADIUS, ORBIT_OUTER_RADIUS);
        }
        glPopMatrix();

        //
        //====================================================================================================
        //
        glColor4f(GOLDEN_COLOR_CODE, g_fAlpha_Text1);
        glPushMatrix();
        {
    #if(ORIGINAL_TEXT_COORDINATES)
            applyTransformations(GoldenYearsSceneTextTransformations[TEXT_2009]);
    #else
            applyTransformations(g_Transformations[TEXT_2009]);
    #endif
            drawOrbitText("2009");
        }
        glPopMatrix();

        glPushMatrix();
        {
    #if(ORIGINAL_TEXT_COORDINATES)
            applyTransformations(GoldenYearsSceneTextTransformations[TEXT_APPLE_FCP]);
    #else
            applyTransformations(g_Transformations[TEXT_APPLE_FCP]);
    #endif
            //drawAppleFCPMaster();
            drawOrbitText("Apple FCP");
        }
        glPopMatrix();

        glPushMatrix();
        {
    #if(ORIGINAL_TEXT_COORDINATES)
            applyTransformations(GoldenYearsSceneTextTransformations[TEXT_MASTER]);
    #else
            applyTransformations(g_Transformations[TEXT_MASTER]);
    #endif
            drawOrbitText("Master");
        }
        glPopMatrix();

        //
        //====================================================================================================
        //
        glColor4f(GOLDEN_COLOR_CODE, g_fAlpha_Text2);
        glPushMatrix();
        {
    #if(ORIGINAL_TEXT_COORDINATES)
            applyTransformations(GoldenYearsSceneTextTransformations[TEXT_2012]);
    #else
            applyTransformations(g_Transformations[TEXT_2012]);
    #endif
            drawOrbitText("2012");
        }
        glPopMatrix();

        glPushMatrix();
        {
            #if(ORIGINAL_TEXT_COORDINATES)
                applyTransformations(GoldenYearsSceneTextTransformations[TEXT_FUNDAMENTAL1]);
            #else
                applyTransformations(g_Transformations[TEXT_FUNDAMENTAL1]);
            #endif
            drawOrbitText("Fundamentals");
        }
        glPopMatrix();

        glPushMatrix();
        {
        #if(ORIGINAL_TEXT_COORDINATES)
            applyTransformations(GoldenYearsSceneTextTransformations[TEXT_FUNDAMENTAL2]);
        #else
            applyTransformations(g_Transformations[TEXT_FUNDAMENTAL2]);
        #endif
            drawOrbitText("Seminar");
        }
        glPopMatrix();

        //
        //====================================================================================================
        //
        glColor4f(GOLDEN_COLOR_CODE, g_fAlpha_Text3);
        glPushMatrix();
        {
    #if(ORIGINAL_TEXT_COORDINATES)
            applyTransformations(GoldenYearsSceneTextTransformations[TEXT_2016]);
    #else
            applyTransformations(g_Transformations[TEXT_2016]);
    #endif
            drawOrbitText("2016");
        }
        glPopMatrix();

        glPushMatrix();
        {
    #if(ORIGINAL_TEXT_COORDINATES)
            applyTransformations(GoldenYearsSceneTextTransformations[TEXT_ARM_ARM]);
    #else
            applyTransformations(g_Transformations[TEXT_ARM_ARM]);
    #endif
            //drawARMAccreditedEngineer();
            drawOrbitText("ARM");
        }
        glPopMatrix();

        glPushMatrix();
        {
    #if(ORIGINAL_TEXT_COORDINATES)
            applyTransformations(GoldenYearsSceneTextTransformations[TEXT_ARM_ACCREDITED]);
    #else
            applyTransformations(g_Transformations[TEXT_ARM_ACCREDITED]);
    #endif
            //drawARMAccreditedEngineer();
            drawOrbitText("Accredited");
        }
        glPopMatrix();

        glPushMatrix();
        {
    #if(ORIGINAL_TEXT_COORDINATES)
            applyTransformations(GoldenYearsSceneTextTransformations[TEXT_ARM_ENGINEER]);
    #else
            applyTransformations(g_Transformations[TEXT_ARM_ENGINEER]);
    #endif
            drawOrbitText("Engineer");

        }
        glPopMatrix();

        //
        //====================================================================================================
        //
        glColor4f(GOLDEN_COLOR_CODE, g_fAlpha_Text4);
        glPushMatrix();
        {
    #if(ORIGINAL_TEXT_COORDINATES)
            applyTransformations(GoldenYearsSceneTextTransformations[TEXT_2017]);
    #else
            applyTransformations(g_Transformations[TEXT_2017]);
    #endif
            drawOrbitText("2017");
        }
        glPopMatrix();

        glPushMatrix();
        {
    #if(ORIGINAL_TEXT_COORDINATES)
            applyTransformations(GoldenYearsSceneTextTransformations[TEXT_RTR]);
    #else
            applyTransformations(g_Transformations[TEXT_RTR]);
    #endif
            drawOrbitText("RTR");
        }
        glPopMatrix();

        //
        //====================================================================================================
        //
        glColor4f(GOLDEN_COLOR_CODE, g_fAlpha_Text5);
        glPushMatrix();
        {
    #if(ORIGINAL_TEXT_COORDINATES)
            applyTransformations(GoldenYearsSceneTextTransformations[TEXT_2022]);
    #else
            applyTransformations(g_Transformations[TEXT_2022]);
    #endif
            drawOrbitText("2022");
        }
        glPopMatrix();

        glPushMatrix();
        {
    #if(ORIGINAL_TEXT_COORDINATES)
            applyTransformations(GoldenYearsSceneTextTransformations[TEXT_WINDEV]);
    #else
            applyTransformations(g_Transformations[TEXT_WINDEV]);
    #endif
            //drawWinDev();
            drawOrbitText("WinDev");
        }
        glPopMatrix();

        if(bDrawSaturn)
        {
            glPushMatrix();
            {
    #if(ORIGINAL_TEXT_COORDINATES)
                applyTransformations(GoldenYearsSceneTextTransformations[OBJECT_SATURN]);
    #else
                applyTransformations(g_Transformations[OBJECT_SATURN]);
    #endif
                drawSaturn(g_fSaturnRotation, g_fRingRotation, SHOW_ROCKS_GOLDEN_YEARS_SCENE);
            }
            glPopMatrix();
        }

        fadeOutEffect(g_fAlpha_Scene1);
    }
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
}

void updateSceneFive(void)
{
    // function declarations
    void uninitializeSceneFive(void);
    void GoldenYearsTimer(HWND hwnd, UINT iMsg, UINT_PTR idEvent, DWORD dwTime);

    // local variables
    static BOOL bFadeInText1 = FALSE;
    static BOOL bFadeInText2 = FALSE;
    static BOOL bFadeInText3 = FALSE;
    static BOOL bFadeInText4 = FALSE;
    static BOOL bFadeInText5 = FALSE;
    static BOOL bFadeInScene = FALSE;

    static BOOL bFadeOutText1 = FALSE;
    static BOOL bFadeOutText2 = FALSE;
    static BOOL bFadeOutText3 = FALSE;
    static BOOL bFadeOutText4 = FALSE;
    static BOOL bFadeOutText5 = FALSE;
    static BOOL bFadeOutScene = FALSE;

    static BOOL bSlowDownCamera;

    static int iCnt = 0;
    static int iText = 0;
    GLfloat fInterpolateValue;
    GLfloat fAngle;
    static GLfloat tVal = 0.0f;

    // 
    if(totalElapsedTime >= 29.7)
    {
        uninitializeSceneFive();
        ResetGlobalTimer();
        setCameraCoordinates({{0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 1.0f, 0.0f}, 0.0f, 270.0f});
        currentScene = SCENE_LIFE;
    }

    // rotate skybox
    updateRotationValue(&goldenYearsSkyboxRotation, 360.0f / (FPS * 1000.0f));

    updateRangeIcosahedrons_Asteroids(riseOfPhoenixRockAttribs_shot2, 
                                        RISE_OF_PHOENIX_NUM_ROCKS, 
                                        {0.0f, 0.0f, 0.0f}, 
                                        0.001,
                                        1.0f);

    // set local timer
    if (!g_bGoldenYearsTimerIsOn)
    {
        SetTimer(ghwnd, IDT_GOLDEN_YEARS_TIMER, 1000, (TIMERPROC)GoldenYearsTimer);
        g_bGoldenYearsTimerIsOn = TRUE;
    }

    updateRotationValue(&g_fSaturnRotation, 1.0f / (FPS * 15.0f));
    updateRotationValue(&g_fRingRotation, 1.0f / (FPS * 15.0f));

    if(turnoncamera)
    {
        cameraCoordinates.angleY -= CAMERA_SPEED;
        updateCameraVector();
        cameraCoordinates.position.x = g_OrbitRotation[iCnt].x;
        cameraCoordinates.position.z = g_OrbitRotation[iCnt].z;
        iCnt++;
        fAngle = getInterpolatedValue(0.0f, g_iNumberOfElements, 0.0f, 360.0f, iCnt);
        fInterpolateValue = getInterpolatedValue(0.0f, 360.0f, 0.0f, g_iNumberOfElements, 90.0f);
    }

    if (lerpcamera == TRUE)
    {
        lerpCamera(&g_InitialPos, &g_TargetPos, &tVal, 1 / (FPS * 4.0f));
    }

    /**
    if (iCnt > g_iNumberOfElements)
    {
        iCnt = g_iNumberOfElements;
        cameraCoordinates.angleY = 270.0f;
    }
    /**/

    //if(cameraCoordinates.angleY >= 255.0f && cameraCoordinates.angleY <= 260.0f)
    //{
    //    //fprintf(gpFILE, "Fade in text 1.\n");
    //    bFadeInText1 = FALSE;
    //    bFadeOutText1 = TRUE;
    //}
    //else if (cameraCoordinates.angleY >= 230.0f && cameraCoordinates.angleY <= 235.0f)
    //{
    //    //fprintf(gpFILE, "Fade in text 2.\n");
    //    bFadeInText2 = FALSE;
    //    bFadeOutText2 = TRUE;
    //}
    //else if (cameraCoordinates.angleY >= 211.0f && cameraCoordinates.angleY <= 216.0f)
    //{
    //    //fprintf(gpFILE, "Fade in text 3.\n");
    //    bFadeInText3 = FALSE;
    //    bFadeOutText3 = TRUE;
    //}
    //else if (cameraCoordinates.angleY >= 195.0f && cameraCoordinates.angleY <= 200.0f)
    //{
    //    //fprintf(gpFILE, "Fade in text 4.\n");
    //    bFadeInText4 = FALSE;
    //    bFadeOutText4 = TRUE;
    //}

    //if (cameraCoordinates.angleY >= 177.0f && cameraCoordinates.angleY <= 182.0f)
    //{
    //    bFadeOutText1 = FALSE;
    //    bFadeOutText2 = FALSE;
    //    bFadeOutText3 = FALSE;

    //    bFadeInText1 = FALSE;
    //    bFadeInText2 = FALSE;
    //    bFadeInText3 = FALSE;

    //    turnoncamera = FALSE;
    //}

    //if ((unsigned long long)g_GoldenYearsSceneElapsedSeconds == 18)
    //{
    //    bDrawSaturn = TRUE;
    //    //bFadeOutScene = FALSE;
    //}

    //if ((unsigned long long)g_GoldenYearsSceneElapsedSeconds == 23)
    //{
    //    bFadeOutScene = TRUE;
    //}

    switch (g_GoldenYearsSceneElapsedSeconds)
    {
    //case 139:
    case TEXT_1:
        bFadeOutText1 = TRUE;
        break;

    //case 143:
    case TEXT_2:
        bFadeOutText2 = TRUE;
        break;

    //case 147:
    case TEXT_3:
        bFadeOutText3 = TRUE;
        break;

    //case 151:
    case TEXT_4:
        bFadeOutText4 = TRUE;
        break;

    //case 154:
    case TEXT_FADE_OUT:
        bFadeOutText1 = FALSE;
        bFadeOutText2 = FALSE;
        bFadeOutText3 = FALSE;
        bFadeOutText4 = FALSE;
        break;

    //case 155: // 2:35
    case TEXT_5:
        bFadeOutText5 = TRUE;
        break;

    //case 156:
    case STOP_CAMERA:
        turnoncamera = FALSE;
        break;

    //case 158:
    case DRAW_SATURN:
        bDrawSaturn = TRUE;
        break;

    //case 160:
    case LERP_CAMERA:
        lerpcamera = TRUE;
        break;

    //case 162:
    case SCENE_FADEOUT:
        bFadeOutScene = TRUE;
        break;
    }

    if (bSlowDownCamera)
    {
        g_fCameraSpeed += 0.01f;
        if (g_fCameraSpeed <= 0.0f)
            turnoncamera = TRUE;
    }

    if (bDrawSaturn)
    {
#if(ORIGINAL_TEXT_COORDINATES)
        if (GoldenYearsSceneTextTransformations[OBJECT_SATURN].translation.x > -0.6)
            GoldenYearsSceneTextTransformations[OBJECT_SATURN].translation.x -= 0.008f;
#else
        if (g_Transformations[OBJECT_SATURN].translation.x > -0.6)
            g_Transformations[OBJECT_SATURN].translation.x -= 0.008f;
#endif
    }

    if (bFadeOutScene)
    {
        if(updateAlpha(&g_fAlpha_Scene1, 1.0f / (FPS * 5.0f), FADE_OUT_FLAG))
        {
            // uninitializeSceneFive();
            // currentScene = SCENE_LIFE;
        }
    }
    else
    {
        updateAlpha(&g_fAlpha_Scene1, 1.0f / (FPS * 5.0f), FADE_IN_FLAG);
    }

    if (bFadeOutText1)
    {
        updateAlpha(&g_fAlpha_Text1, 1.0f / (FPS * 5.0f), FADE_OUT_FLAG);
    }
    else
    {
        updateAlpha(&g_fAlpha_Text1, 1.0f / (FPS * 5.0f), FADE_IN_FLAG);
    }

    if (bFadeOutText2)
    {
        updateAlpha(&g_fAlpha_Text2, 1.0f / (FPS * 5.0f), FADE_OUT_FLAG);
    }
    else
    {
        updateAlpha(&g_fAlpha_Text2, 1.0f / (FPS * 5.0f), FADE_IN_FLAG);
    }

    if (bFadeOutText3)
    {
        updateAlpha(&g_fAlpha_Text3, 1.0f / (FPS * 5.0f), FADE_OUT_FLAG);
    }
    else
    {
        updateAlpha(&g_fAlpha_Text3, 1.0f / (FPS * 5.0f), FADE_IN_FLAG);
    }

    if (bFadeOutText4)
    {
        updateAlpha(&g_fAlpha_Text4, 1.0f / (FPS * 5.0f), FADE_OUT_FLAG);
    }
    else
    {
        updateAlpha(&g_fAlpha_Text4, 1.0f / (FPS * 5.0f), FADE_IN_FLAG);
    }

    if (bFadeOutText5)
    {
        updateAlpha(&g_fAlpha_Text5, 1.0f / (FPS * 5.0f), FADE_OUT_FLAG);
    }
    else
    {
        updateAlpha(&g_fAlpha_Text5, 1.0f / (FPS * 5.0f), FADE_IN_FLAG);
    }
}

void uninitializeSceneFive(void)
{
    if (g_OrbitRotation)
    {
        free(g_OrbitRotation);
        g_OrbitRotation = NULL;
    }
    
    stopALSource(g_SceneSources[3]);
    SetCurrentSourceForStats(0, FALSE, FALSE);
}

void GoldenYearsTimer(HWND hwnd, UINT iMsg, UINT_PTR idEvent, DWORD dwTime)
{
    // code
    KillTimer(hwnd, idEvent);
    g_GoldenYearsSceneElapsedSeconds++;
    g_bGoldenYearsTimerIsOn = FALSE;
}

void InitializeTextPos(void)
{
    int i = 0;
    for (i = 0; i < TOTAL_NUMBERS; i++)
    {
#if(ORIGINAL_TEXT_COORDINATES)
        GoldenYearsSceneTextTransformations[i].scale.x = 0.3f;
        GoldenYearsSceneTextTransformations[i].scale.y = 0.3f;
        GoldenYearsSceneTextTransformations[i].scale.z = 0.3f;
#else
        g_Transformations[i].scale.x = 0.3f;
        g_Transformations[i].scale.y = 0.3f;
        g_Transformations[i].scale.z = 0.3f;
#endif
    }

    //
#if(ORIGINAL_TEXT_COORDINATES)
    GoldenYearsSceneTextTransformations[TEXT_2009].rotation.y = -13.20f;
    GoldenYearsSceneTextTransformations[TEXT_2009].translation.x = 9.4f;
    GoldenYearsSceneTextTransformations[TEXT_2009].translation.y = 0.1f;
    GoldenYearsSceneTextTransformations[TEXT_2009].translation.z = -7.3f;

    GoldenYearsSceneTextTransformations[TEXT_APPLE_FCP].rotation.y = -13.20f;
    GoldenYearsSceneTextTransformations[TEXT_APPLE_FCP].translation.x = 9.05f;
    GoldenYearsSceneTextTransformations[TEXT_APPLE_FCP].translation.y = -0.2f;
    GoldenYearsSceneTextTransformations[TEXT_APPLE_FCP].translation.z = -7.3f;

    GoldenYearsSceneTextTransformations[TEXT_MASTER].rotation.y = -13.20f;
    GoldenYearsSceneTextTransformations[TEXT_MASTER].translation.x = 9.3f;
    GoldenYearsSceneTextTransformations[TEXT_MASTER].translation.y = -0.55f;
    GoldenYearsSceneTextTransformations[TEXT_MASTER].translation.z = -7.3f;

    GoldenYearsSceneTextTransformations[TEXT_2012].scale.x = 0.2f;
    GoldenYearsSceneTextTransformations[TEXT_2012].scale.y = 0.2f;
    GoldenYearsSceneTextTransformations[TEXT_2012].scale.z = 0.2f;
    GoldenYearsSceneTextTransformations[TEXT_2012].rotation.y = 37.9f;
    GoldenYearsSceneTextTransformations[TEXT_2012].translation.x = 5.7f;
    GoldenYearsSceneTextTransformations[TEXT_2012].translation.y = 0.05f;
    GoldenYearsSceneTextTransformations[TEXT_2012].translation.z = -6.9f;

    GoldenYearsSceneTextTransformations[TEXT_FUNDAMENTAL1].scale.x = 0.2f;
    GoldenYearsSceneTextTransformations[TEXT_FUNDAMENTAL1].scale.y = 0.2f;
    GoldenYearsSceneTextTransformations[TEXT_FUNDAMENTAL1].scale.z = 0.2f;
    GoldenYearsSceneTextTransformations[TEXT_FUNDAMENTAL1].rotation.y = 37.9f;
    GoldenYearsSceneTextTransformations[TEXT_FUNDAMENTAL1].translation.x = 5.5f;
    GoldenYearsSceneTextTransformations[TEXT_FUNDAMENTAL1].translation.y = -0.15f;
    GoldenYearsSceneTextTransformations[TEXT_FUNDAMENTAL1].translation.z = -6.7f;

    GoldenYearsSceneTextTransformations[TEXT_FUNDAMENTAL2].scale.x = 0.2f;
    GoldenYearsSceneTextTransformations[TEXT_FUNDAMENTAL2].scale.y = 0.2f;
    GoldenYearsSceneTextTransformations[TEXT_FUNDAMENTAL2].scale.z = 0.2f;
    GoldenYearsSceneTextTransformations[TEXT_FUNDAMENTAL2].rotation.y = 37.9f;
    GoldenYearsSceneTextTransformations[TEXT_FUNDAMENTAL2].translation.x = 5.65f;
    GoldenYearsSceneTextTransformations[TEXT_FUNDAMENTAL2].translation.y = -0.35f;
    GoldenYearsSceneTextTransformations[TEXT_FUNDAMENTAL2].translation.z = -6.8f;

    GoldenYearsSceneTextTransformations[TEXT_2016].rotation.y = -336.0f;
    GoldenYearsSceneTextTransformations[TEXT_2016].translation.x = 3.85f;
    GoldenYearsSceneTextTransformations[TEXT_2016].translation.y = 0.35f;
    GoldenYearsSceneTextTransformations[TEXT_2016].translation.z = -10.69f;

    GoldenYearsSceneTextTransformations[TEXT_ARM_ARM].rotation.y = -336.0f;
    GoldenYearsSceneTextTransformations[TEXT_ARM_ARM].translation.x = 3.9f;
    GoldenYearsSceneTextTransformations[TEXT_ARM_ARM].translation.y = 0.1f;
    GoldenYearsSceneTextTransformations[TEXT_ARM_ARM].translation.z = -10.59f;

    GoldenYearsSceneTextTransformations[TEXT_ARM_ACCREDITED].rotation.y = -336.0f;
    GoldenYearsSceneTextTransformations[TEXT_ARM_ACCREDITED].translation.x = 3.55f;
    GoldenYearsSceneTextTransformations[TEXT_ARM_ACCREDITED].translation.y = -0.15f;
    GoldenYearsSceneTextTransformations[TEXT_ARM_ACCREDITED].translation.z = -10.44f;

    GoldenYearsSceneTextTransformations[TEXT_ARM_ENGINEER].rotation.y = -336.0f;
    GoldenYearsSceneTextTransformations[TEXT_ARM_ENGINEER].translation.x = 3.7f;
    GoldenYearsSceneTextTransformations[TEXT_ARM_ENGINEER].translation.y = -0.4f;
    GoldenYearsSceneTextTransformations[TEXT_ARM_ENGINEER].translation.z = -10.44f;

    GoldenYearsSceneTextTransformations[TEXT_2017].rotation.y = -281.0f;
    GoldenYearsSceneTextTransformations[TEXT_2017].translation.x = 0.55f;
    GoldenYearsSceneTextTransformations[TEXT_2017].translation.y = -0.1f;
    GoldenYearsSceneTextTransformations[TEXT_2017].translation.z = -8.6f;

    GoldenYearsSceneTextTransformations[TEXT_RTR].rotation.y = -281.0f;
    GoldenYearsSceneTextTransformations[TEXT_RTR].translation.x = 0.55f;
    GoldenYearsSceneTextTransformations[TEXT_RTR].translation.y = -0.35f;
    GoldenYearsSceneTextTransformations[TEXT_RTR].translation.z = -8.6f;

    GoldenYearsSceneTextTransformations[TEXT_2022].rotation.y = -271.0f;
    GoldenYearsSceneTextTransformations[TEXT_2022].translation.x = -1.09f;
    GoldenYearsSceneTextTransformations[TEXT_2022].translation.y = 0.3f;
    GoldenYearsSceneTextTransformations[TEXT_2022].translation.z = -9.54f;

    GoldenYearsSceneTextTransformations[TEXT_WINDEV].rotation.y = -271.0f;
    GoldenYearsSceneTextTransformations[TEXT_WINDEV].translation.x = -1.09f;
    GoldenYearsSceneTextTransformations[TEXT_WINDEV].translation.y = 0.05f;
    GoldenYearsSceneTextTransformations[TEXT_WINDEV].translation.z = -9.39f;

    GoldenYearsSceneTextTransformations[OBJECT_SATURN].scale.x = 0.2f;
    GoldenYearsSceneTextTransformations[OBJECT_SATURN].scale.y = 0.2f;
    GoldenYearsSceneTextTransformations[OBJECT_SATURN].scale.z = 0.2f;
    GoldenYearsSceneTextTransformations[OBJECT_SATURN].rotation.y = -257.0f;
    GoldenYearsSceneTextTransformations[OBJECT_SATURN].translation.x = 0.7f;
    GoldenYearsSceneTextTransformations[OBJECT_SATURN].translation.y = 0.05f;
    GoldenYearsSceneTextTransformations[OBJECT_SATURN].translation.z = -9.99f;
#else
    g_Transformations[TEXT_2009].rotation.y = -13.20f;
    g_Transformations[TEXT_2009].translation.x = 9.4f;
    g_Transformations[TEXT_2009].translation.y = 0.1f;
    g_Transformations[TEXT_2009].translation.z = -7.3f;

    g_Transformations[TEXT_APPLE_FCP].rotation.y = -13.20f;
    g_Transformations[TEXT_APPLE_FCP].translation.x = 9.05f;
    g_Transformations[TEXT_APPLE_FCP].translation.y = -0.2f;
    g_Transformations[TEXT_APPLE_FCP].translation.z = -7.3f;

    g_Transformations[TEXT_MASTER].rotation.y = -13.20f;
    g_Transformations[TEXT_MASTER].translation.x = 9.3f;
    g_Transformations[TEXT_MASTER].translation.y = -0.55f;
    g_Transformations[TEXT_MASTER].translation.z = -7.3f;

    g_Transformations[TEXT_2012].scale.x = 0.2f;
    g_Transformations[TEXT_2012].scale.y = 0.2f;
    g_Transformations[TEXT_2012].scale.z = 0.2f;
    g_Transformations[TEXT_2012].rotation.y = 37.9f;
    g_Transformations[TEXT_2012].translation.x = 5.7f;
    g_Transformations[TEXT_2012].translation.y = 0.05f;
    g_Transformations[TEXT_2012].translation.z = -6.9f;

    g_Transformations[TEXT_FUNDAMENTAL1].scale.x = 0.2f;
    g_Transformations[TEXT_FUNDAMENTAL1].scale.y = 0.2f;
    g_Transformations[TEXT_FUNDAMENTAL1].scale.z = 0.2f;
    g_Transformations[TEXT_FUNDAMENTAL1].rotation.y = 37.9f;
    g_Transformations[TEXT_FUNDAMENTAL1].translation.x = 5.5f;
    g_Transformations[TEXT_FUNDAMENTAL1].translation.y = -0.15f;
    g_Transformations[TEXT_FUNDAMENTAL1].translation.z = -6.7f;

    g_Transformations[TEXT_FUNDAMENTAL2].scale.x = 0.2f;
    g_Transformations[TEXT_FUNDAMENTAL2].scale.y = 0.2f;
    g_Transformations[TEXT_FUNDAMENTAL2].scale.z = 0.2f;
    g_Transformations[TEXT_FUNDAMENTAL2].rotation.y = 37.9f;
    g_Transformations[TEXT_FUNDAMENTAL2].translation.x = 5.65f;
    g_Transformations[TEXT_FUNDAMENTAL2].translation.y = -0.35f;
    g_Transformations[TEXT_FUNDAMENTAL2].translation.z = -6.8f;

    g_Transformations[TEXT_2016].rotation.y = -336.0f;
    g_Transformations[TEXT_2016].translation.x = 3.85f;
    g_Transformations[TEXT_2016].translation.y = 0.35f;
    g_Transformations[TEXT_2016].translation.z = -10.69f;

    g_Transformations[TEXT_ARM_ARM].rotation.y = -336.0f;
    g_Transformations[TEXT_ARM_ARM].translation.x = 3.9f;
    g_Transformations[TEXT_ARM_ARM].translation.y = 0.1f;
    g_Transformations[TEXT_ARM_ARM].translation.z = -10.59f;

    g_Transformations[TEXT_ARM_ACCREDITED].rotation.y = -336.0f;
    g_Transformations[TEXT_ARM_ACCREDITED].translation.x = 3.55f;
    g_Transformations[TEXT_ARM_ACCREDITED].translation.y = -0.15f;
    g_Transformations[TEXT_ARM_ACCREDITED].translation.z = -10.44f;

    g_Transformations[TEXT_ARM_ENGINEER].rotation.y = -336.0f;
    g_Transformations[TEXT_ARM_ENGINEER].translation.x = 3.7f;
    g_Transformations[TEXT_ARM_ENGINEER].translation.y = -0.4f;
    g_Transformations[TEXT_ARM_ENGINEER].translation.z = -10.44f;


    g_Transformations[TEXT_2017].rotation.y = -281.0f;
    g_Transformations[TEXT_2017].translation.x = 0.55f;
    g_Transformations[TEXT_2017].translation.y = -0.1f;
    g_Transformations[TEXT_2017].translation.z = -8.6f;

    g_Transformations[TEXT_RTR].rotation.y = -281.0f;
    g_Transformations[TEXT_RTR].translation.x = 0.55f;
    g_Transformations[TEXT_RTR].translation.y = -0.35f;
    g_Transformations[TEXT_RTR].translation.z = -8.6f;

    g_Transformations[TEXT_2022].rotation.y = -271.0f;
    g_Transformations[TEXT_2022].translation.x = -1.09f;
    g_Transformations[TEXT_2022].translation.y = 0.3f;
    g_Transformations[TEXT_2022].translation.z = -9.54f;

    g_Transformations[TEXT_WINDEV].rotation.y = -271.0f;
    g_Transformations[TEXT_WINDEV].translation.x = -1.09f;
    g_Transformations[TEXT_WINDEV].translation.y = 0.05f;
    g_Transformations[TEXT_WINDEV].translation.z = -9.39f;

    g_Transformations[OBJECT_SATURN].scale.x = 0.2f;
    g_Transformations[OBJECT_SATURN].scale.y = 0.2f;
    g_Transformations[OBJECT_SATURN].scale.z = 0.2f;
    g_Transformations[OBJECT_SATURN].rotation.y = -257.0f;
    g_Transformations[OBJECT_SATURN].translation.x = 0.7f;
    g_Transformations[OBJECT_SATURN].translation.y = 0.05f;
    g_Transformations[OBJECT_SATURN].translation.z = -9.99f;
#endif
}
