#pragma once

#include "../Common/Common.h"

// function declarations
void initializeSaturn(void);                                                                                    // call in initialize()
void drawSaturn(GLfloat satYRot, GLfloat ringYRot, BOOL showRock);                                              // call in display(), apply transformations if needed
void drawSaturn_v2(GLfloat satYRot, GLfloat ringYRot, GLfloat ringAlpha, BOOL showRock);                        // made for opening scene
void drawSaturn_v3(GLfloat satYRot, GLfloat ringYRot, GLfloat saturnAlpha, GLfloat ringAlpha, BOOL showRocks);  // made for creditsScene
void drawSaturnWithoutRing(GLfloat satYRot);                                                                    // made for new journey scene
void updateSaturn(void);                                                                                        // call in update()
void uninitializeSaturn(void);                                                                                  // call in uninitialize()
