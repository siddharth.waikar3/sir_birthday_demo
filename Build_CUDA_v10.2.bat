cls

del Common.obj OGL.obj Common.obj Camera.obj OpeningScene.obj JourneyScene.obj ObstaclesScene.obj RiseOfPhoenixScene.obj GoldenYearsScene.obj LifeScene.obj ^
NewJourneyScene.obj HandingOverLegacyScene.obj TheEndScene.obj OGL.res RenderText.exp RenderText.obj CreditsScene.obj TitleScene.obj Astroid.obj Years.obj SlideText.obj Skybox.obj Texture.obj Orbit.obj ^
Saturn.obj Icosahedron.obj OrbitText.obj dagad.obj CUDALibrary.obj SplashScreen.obj

nvcc --compile -o CUDALibrary.obj ./Common/CUDA/CUDALibrary.cu

cl.exe /c /EHsc /D DEBUG /D UNICODE /D ENABLE_CUDA /I "C:\Program Files (x86)\OpenAL 1.1 SDK\include" /I "C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v10.2\include" ^
OGL.cpp ^
.\Common\Common.cpp ^
.\Common\Camera.cpp ^
.\Common\Texture.cpp ^
.\Scenes\OpeningScene.cpp ^
.\Scenes\JourneyScene.cpp ^
.\Scenes\ObstaclesScene.cpp ^
.\Scenes\RiseOfPhoenixScene.cpp ^
.\Scenes\GoldenYearsScene.cpp ^
.\Scenes\LifeScene.cpp ^
.\Scenes\NewJourneyScene.cpp ^
.\Scenes\HandingOverLegacyScene.cpp ^
.\Scenes\TheEndScene.cpp ^
.\Scenes\TitleScene.cpp ^
.\Scenes\CreditsScene.cpp ^
.\Scenes\TestScene.cpp ^
.\Objects\Astroid.cpp ^
.\Objects\Years.cpp ^
.\Objects\SlideText.cpp ^
.\Objects\Skybox.cpp ^
.\Objects\Orbit.cpp ^
.\Objects\SphereCollision.cpp ^
.\Objects\Saturn.cpp ^
.\Objects\Icosahedron.cpp ^
.\Objects\OrbitText.cpp ^
.\Objects\dagad.cpp ^
.\Common\SplashScreen.cpp

rc.exe /I "C:\Program Files (x86)\OpenAL 1.1 SDK\include" OGL.rc

link.exe /out:Sir_Birthday_24_10_2023.exe /LIBPATH:"C:\Program Files (x86)\OpenAL 1.1 SDK\libs\Win64" /LIBPATH:"C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v10.2\lib\x64" ^
OGL.obj ^
Common.obj ^
Camera.obj ^
OpeningScene.obj ^
JourneyScene.obj ^
ObstaclesScene.obj ^
RiseOfPhoenixScene.obj ^
GoldenYearsScene.obj ^
LifeScene.obj ^
NewJourneyScene.obj ^
HandingOverLegacyScene.obj ^
TheEndScene.obj ^
TitleScene.obj ^
CreditsScene.obj ^
TestScene.obj ^
Astroid.obj ^
Years.obj ^
SlideText.obj ^
OGL.res ^
Skybox.obj ^
Texture.obj ^
Orbit.obj ^
SphereCollision.obj ^
Saturn.obj ^
Icosahedron.obj ^
OrbitText.obj ^
dagad.obj ^
CUDALibrary.obj ^
SplashScreen.obj ^
User32.lib GDI32.lib Winmm.lib OpenAL32.lib cuda.lib cudart.lib /SUBSYSTEM:WINDOWS

Sir_Birthday_24_10_2023.exe
