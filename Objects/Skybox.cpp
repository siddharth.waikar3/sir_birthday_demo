// header files
#include "..\Common\Common.h"
#include "..\Common\Texture.h"
#include <stdio.h>

// external global variables
extern FILE* gpFILE;
extern HWND ghwnd;

// global variables
char *skyBoxTexturePaths[] =
{
    ".\\Resources\\Textures\\Skybox\\front.png",
    ".\\Resources\\Textures\\Skybox\\back.png",
    ".\\Resources\\Textures\\Skybox\\right.png",
    ".\\Resources\\Textures\\Skybox\\left.png",
    ".\\Resources\\Textures\\Skybox\\top.png",
    ".\\Resources\\Textures\\Skybox\\bottom.png"
};

GLuint skyBoxTextures[6];

// function definitions
void initializeSkybox(void)
{
    // code
    for(int i = 0; i < 6; i++)
    {
        if(LoadCubeMapTexture(&skyBoxTextures[i], skyBoxTexturePaths[i]) == FALSE)
        {
            fprintf(gpFILE, "Failed to load skybox textures.\n");
            DestroyWindow(ghwnd);
        }
    }
}

void drawTexturedSkybox(void)
{
    // code
    glPushMatrix();
    {
        glScalef(1000.0f, 1000.0f, 1000.0f);

        glColor4f(1.0f, 1.0f, 1.0f, 1.0f);

        // front
        glBindTexture(GL_TEXTURE_2D, skyBoxTextures[0]);
        glBegin(GL_QUADS);
        {
            glTexCoord2f(1.0f, 1.0f);
            glVertex3f(1.0f, 1.0f, 1.0f);

            glTexCoord2f(0.0f, 1.0f);
            glVertex3f(-1.0f, 1.0f, 1.0f);

            glTexCoord2f(0.0f, 0.0f);
            glVertex3f(-1.0f, -1.0f, 1.0f);
            
            glTexCoord2f(1.0f, 0.0f);
            glVertex3f(1.0f, -1.0f, 1.0f);
        }
        glEnd();
        glBindTexture(GL_TEXTURE_2D, 0);

        // back
        glBindTexture(GL_TEXTURE_2D, skyBoxTextures[1]);
        glBegin(GL_QUADS);
        {
            glTexCoord2f(1.0f, 1.0f);
            glVertex3f(-1.0f, 1.0f, -1.0f);

            glTexCoord2f(0.0f, 1.0f);
            glVertex3f(1.0f, 1.0f, -1.0f);

            glTexCoord2f(0.0f, 0.0f);
            glVertex3f(1.0f, -1.0f, -1.0f);
            
            glTexCoord2f(1.0f, 0.0f);
            glVertex3f(-1.0f, -1.0f, -1.0f);
        }
        glEnd();
        glBindTexture(GL_TEXTURE_2D, 0);

        // left
        glBindTexture(GL_TEXTURE_2D, skyBoxTextures[2]);
        glBegin(GL_QUADS);
        {
            glTexCoord2f(0.0f, 1.0f);
            glVertex3f(-1.0f, 1.0f, -1.0f);
            
            glTexCoord2f(1.0f, 1.0f);
            glVertex3f(-1.0f, 1.0f, 1.0f);

            glTexCoord2f(1.0f, 0.0f);
            glVertex3f(-1.0f, -1.0f, 1.0f);
            
            glTexCoord2f(0.0f, 0.0f);
            glVertex3f(-1.0f, -1.0f, -1.0f);
        }
        glEnd();
        glBindTexture(GL_TEXTURE_2D, 0);

        // right
        glBindTexture(GL_TEXTURE_2D, skyBoxTextures[3]);
        glBegin(GL_QUADS);
        {
            glTexCoord2f(0.0f, 1.0f);
            glVertex3f(1.0f, 1.0f, 1.0f);

            glTexCoord2f(1.0f, 1.0f);
            glVertex3f(1.0f, 1.0f, -1.0f);

            glTexCoord2f(1.0f, 0.0f);
            glVertex3f(1.0f, -1.0f, -1.0f);

            glTexCoord2f(0.0f, 0.0f);
            glVertex3f(1.0f, -1.0f, 1.0f);
        }
        glEnd();
        glBindTexture(GL_TEXTURE_2D, 0);

        // top
        glBindTexture(GL_TEXTURE_2D, skyBoxTextures[4]);
        glBegin(GL_QUADS);
        {
            glTexCoord2f(0.0f, 1.0f);
            glVertex3f(-1.0f, 1.0f, -1.0f);
            
            glTexCoord2f(1.0f, 1.0f);
            glVertex3f(1.0f, 1.0f, -1.0f);

            glTexCoord2f(1.0f, 0.0f);
            glVertex3f(1.0f, 1.0f, 1.0f);

            glTexCoord2f(0.0f, 0.0f);
            glVertex3f(-1.0f, 1.0f, 1.0f);
        }
        glEnd();
        glBindTexture(GL_TEXTURE_2D, 0);

        // bottom
        glBindTexture(GL_TEXTURE_2D, skyBoxTextures[5]);
        glBegin(GL_QUADS);
        {
            glTexCoord2f(1.0f, 1.0f);
            glVertex3f(1.0f, -1.0f, 1.0f);

            glTexCoord2f(0.0f, 1.0f);
            glVertex3f(-1.0f, -1.0f, 1.0f);

            glTexCoord2f(0.0f, 0.0f);
            glVertex3f(-1.0f, -1.0f, -1.0f);
            
            glTexCoord2f(1.0f, 0.0f);
            glVertex3f(1.0f, -1.0f, -1.0f);
        }
        glEnd();
        glBindTexture(GL_TEXTURE_2D, 0);

        // glBindTexture(GL_TEXTURE_2D, skyBoxTexture);
        // glBegin(GL_QUADS);
        // {
        //     glTexCoord2f(0.5f, 0.625f);
        //     glVertex3f(1.0f, 1.0f, 1.0f);

        //     glTexCoord2f(0.25f, 0.625f);
        //     glVertex3f(-1.0f, 1.0f, 1.0f);

        //     glTexCoord2f(0.25f, 0.375f);
        //     glVertex3f(-1.0f, -1.0f, 1.0f);
            
        //     glTexCoord2f(0.5f, 0.375f);
        //     glVertex3f(1.0f, -1.0f, 1.0f);
        // }
        // glEnd();
        // glBindTexture(GL_TEXTURE_2D, 0);

        // // back
        // glBindTexture(GL_TEXTURE_2D, skyBoxTexture);
        // glBegin(GL_QUADS);
        // {
        //     glTexCoord2f(1.0f, 0.625f);
        //     glVertex3f(-1.0f, 1.0f, -1.0f);

        //     glTexCoord2f(0.75f, 0.625f);
        //     glVertex3f(1.0f, 1.0f, -1.0f);

        //     glTexCoord2f(0.75f, 0.375f);
        //     glVertex3f(1.0f, -1.0f, -1.0f);
            
        //     glTexCoord2f(1.0f, 0.375f);
        //     glVertex3f(-1.0f, -1.0f, -1.0f);
        // }
        // glEnd();
        // glBindTexture(GL_TEXTURE_2D, 0);

        // // left
        // glBindTexture(GL_TEXTURE_2D, skyBoxTexture);
        // glBegin(GL_QUADS);
        // {
        //     glTexCoord2f(0.25f, 0.625f);
        //     glVertex3f(-1.0f, 1.0f, -1.0f);
            
        //     glTexCoord2f(0.0f, 0.625f);
        //     glVertex3f(-1.0f, 1.0f, 1.0f);

        //     glTexCoord2f(0.0f, 0.375f);
        //     glVertex3f(-1.0f, -1.0f, 1.0f);
            
        //     glTexCoord2f(0.25f, 0.375f);
        //     glVertex3f(-1.0f, -1.0f, -1.0f);
        // }
        // glEnd();
        // glBindTexture(GL_TEXTURE_2D, 0);

        // // right
        // glBindTexture(GL_TEXTURE_2D, skyBoxTexture);
        // glBegin(GL_QUADS);
        // {
        //     glTexCoord2f(0.75f, 0.625f);
        //     glVertex3f(1.0f, 1.0f, 1.0f);

        //     glTexCoord2f(0.5f, 0.625f);
        //     glVertex3f(1.0f, 1.0f, -1.0f);

        //     glTexCoord2f(0.5f, 0.375f);
        //     glVertex3f(1.0f, -1.0f, -1.0f);

        //     glTexCoord2f(0.75f, 0.375f);
        //     glVertex3f(1.0f, -1.0f, 1.0f);
        // }
        // glEnd();
        // glBindTexture(GL_TEXTURE_2D, 0);

        // // top
        // glBindTexture(GL_TEXTURE_2D, skyBoxTexture);
        // glBegin(GL_QUADS);
        // {
        //     glTexCoord2f(0.5f, 1.0f);
        //     glVertex3f(-1.0f, 1.0f, -1.0f);
            
        //     glTexCoord2f(0.25f, 1.0f);
        //     glVertex3f(1.0f, 1.0f, -1.0f);

        //     glTexCoord2f(0.25f, 0.625f);
        //     glVertex3f(1.0f, 1.0f, 1.0f);

        //     glTexCoord2f(0.5f, 0.625f);
        //     glVertex3f(-1.0f, 1.0f, 1.0f);
        // }
        // glEnd();
        // glBindTexture(GL_TEXTURE_2D, 0);

        // // bottom
        // glBindTexture(GL_TEXTURE_2D, skyBoxTexture);
        // glBegin(GL_QUADS);
        // {
        //     glTexCoord2f(0.5f, 0.375f);
        //     glVertex3f(1.0f, -1.0f, 1.0f);

        //     glTexCoord2f(0.25f, 0.375f);
        //     glVertex3f(-1.0f, -1.0f, 1.0f);

        //     glTexCoord2f(0.25f, 0.0f);
        //     glVertex3f(-1.0f, -1.0f, -1.0f);
            
        //     glTexCoord2f(0.5f, 0.0f);
        //     glVertex3f(1.0f, -1.0f, -1.0f);
        // }
        // glEnd();
        // glBindTexture(GL_TEXTURE_2D, 0);
    }
    glPopMatrix();
}

void drawTexturedSkyboxWithRotation(GLfloat angle)
{
    // code
    glPushMatrix();
    {
        glRotatef(angle, 0.0f, 1.0f, 0.0f);
        glScalef(1000.0f, 1000.0f, 1000.0f);

        glColor4f(1.0f, 1.0f, 1.0f, 1.0f);

        // front
        glBindTexture(GL_TEXTURE_2D, skyBoxTextures[0]);
        glBegin(GL_QUADS);
        {
            glTexCoord2f(1.0f, 1.0f);
            glVertex3f(1.0f, 1.0f, 1.0f);

            glTexCoord2f(0.0f, 1.0f);
            glVertex3f(-1.0f, 1.0f, 1.0f);

            glTexCoord2f(0.0f, 0.0f);
            glVertex3f(-1.0f, -1.0f, 1.0f);
            
            glTexCoord2f(1.0f, 0.0f);
            glVertex3f(1.0f, -1.0f, 1.0f);
        }
        glEnd();
        glBindTexture(GL_TEXTURE_2D, 0);

        // back
        glBindTexture(GL_TEXTURE_2D, skyBoxTextures[1]);
        glBegin(GL_QUADS);
        {
            glTexCoord2f(1.0f, 1.0f);
            glVertex3f(-1.0f, 1.0f, -1.0f);

            glTexCoord2f(0.0f, 1.0f);
            glVertex3f(1.0f, 1.0f, -1.0f);

            glTexCoord2f(0.0f, 0.0f);
            glVertex3f(1.0f, -1.0f, -1.0f);
            
            glTexCoord2f(1.0f, 0.0f);
            glVertex3f(-1.0f, -1.0f, -1.0f);
        }
        glEnd();
        glBindTexture(GL_TEXTURE_2D, 0);

        // left
        glBindTexture(GL_TEXTURE_2D, skyBoxTextures[2]);
        glBegin(GL_QUADS);
        {
            glTexCoord2f(0.0f, 1.0f);
            glVertex3f(-1.0f, 1.0f, -1.0f);
            
            glTexCoord2f(1.0f, 1.0f);
            glVertex3f(-1.0f, 1.0f, 1.0f);

            glTexCoord2f(1.0f, 0.0f);
            glVertex3f(-1.0f, -1.0f, 1.0f);
            
            glTexCoord2f(0.0f, 0.0f);
            glVertex3f(-1.0f, -1.0f, -1.0f);
        }
        glEnd();
        glBindTexture(GL_TEXTURE_2D, 0);

        // right
        glBindTexture(GL_TEXTURE_2D, skyBoxTextures[3]);
        glBegin(GL_QUADS);
        {
            glTexCoord2f(0.0f, 1.0f);
            glVertex3f(1.0f, 1.0f, 1.0f);

            glTexCoord2f(1.0f, 1.0f);
            glVertex3f(1.0f, 1.0f, -1.0f);

            glTexCoord2f(1.0f, 0.0f);
            glVertex3f(1.0f, -1.0f, -1.0f);

            glTexCoord2f(0.0f, 0.0f);
            glVertex3f(1.0f, -1.0f, 1.0f);
        }
        glEnd();
        glBindTexture(GL_TEXTURE_2D, 0);

        // top
        glBindTexture(GL_TEXTURE_2D, skyBoxTextures[4]);
        glBegin(GL_QUADS);
        {
            glTexCoord2f(0.0f, 1.0f);
            glVertex3f(-1.0f, 1.0f, -1.0f);
            
            glTexCoord2f(1.0f, 1.0f);
            glVertex3f(1.0f, 1.0f, -1.0f);

            glTexCoord2f(1.0f, 0.0f);
            glVertex3f(1.0f, 1.0f, 1.0f);

            glTexCoord2f(0.0f, 0.0f);
            glVertex3f(-1.0f, 1.0f, 1.0f);
        }
        glEnd();
        glBindTexture(GL_TEXTURE_2D, 0);

        // bottom
        glBindTexture(GL_TEXTURE_2D, skyBoxTextures[5]);
        glBegin(GL_QUADS);
        {
            glTexCoord2f(1.0f, 1.0f);
            glVertex3f(1.0f, -1.0f, 1.0f);

            glTexCoord2f(0.0f, 1.0f);
            glVertex3f(-1.0f, -1.0f, 1.0f);

            glTexCoord2f(0.0f, 0.0f);
            glVertex3f(-1.0f, -1.0f, -1.0f);
            
            glTexCoord2f(1.0f, 0.0f);
            glVertex3f(1.0f, -1.0f, -1.0f);
        }
        glEnd();
        glBindTexture(GL_TEXTURE_2D, 0);
    }
    glPopMatrix();
}

void uninitializeSkybox(void)
{
    // code
    glDeleteTextures(6, skyBoxTextures);
}

