#pragma once

// struct definitions
struct Vec3CUDA
{
    float x, y, z;
};

struct ColorCUDA
{
    float r, g, b, alpha;
};

struct SphereAttribs
{
    struct Vec3CUDA acc;
    struct Vec3CUDA grav;
    struct Vec3CUDA old_pos;
    struct Vec3CUDA pos;
    float mass;
    float drag;
    struct ColorCUDA color;
};

struct BoundingSphere
{
    GLUquadric *qbSphereObj;
    Point *vertices;
    Vec3CUDA origin;
    GLfloat radius;
    GLfloat color[4];
};
