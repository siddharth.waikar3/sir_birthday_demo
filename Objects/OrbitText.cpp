// NOTE : this file is made by Vivek for Orbit text for the journey scene
// header files
#include "../Common/Common.h"
#include "../Common/RenderText.h"
#include "OrbitText.h"

// extern global variables
extern HDC ghdc;
extern HWND ghwnd;
extern FILE *gpFILE;

// macros
#define JOURNEY_SCENE_FONT_DISPLAYLISTS_OFFSET 23597891

// global variables
RT_Library journeySceneFont = {0};
struct Translation journeySceneTextInitialTranslations[] =
{
    {-3.350000f, 1.000000f, 0.000000f}, // for "2002"
    {-4.000000f, 0.000000f, 0.000000f}, // for "WinDev"
    {0.050000f, 1.000000f, 0.000000f},  // for "2003"
    {0.000000f, 0.000000f, 0.000000f},  // for "UNIX"
    {0.000000f, 1.000000f, 0.000000f},  // for "2004"
    {6.000000f, 1.000000f, 0.000000f},  // for "2006"
    {4.950000f, 0.000000f, 0.000000f},  // for "SunBeam"
    {1.500000f, 1.000000f, 0.000000f},  // for "2008"
    {-0.020000f, 0.000000f, 0.000000f}  // for "OS Seminar"
};

// function definitions
void initializeJourneySceneText(void)
{
    // code
    RT_Initialize_Library(&journeySceneFont, ghdc, TEXT("./Resources/fonts/Rounded_Elegance.ttf"), TEXT("Rounded Elegance"), RT_FW_DONTCARE, 0, JOURNEY_SCENE_FONT_DISPLAYLISTS_OFFSET);
}

void drawJourneySceneText(int iWhichText, struct Translation translation, GLfloat yRotation, GLfloat alpha)
{
    // local variables
    static Color color = {1.0f, 1.0f, 1.0f, 1.0f};
    static Color gradientColor = {0.3f, 0.3f, 0.3f, 1.0f};

    // code
    color.alpha = alpha;
    gradientColor.alpha = alpha;

    switch(iWhichText)
    {
    case WINDEV_2002_TEXT:
        glPushMatrix();
        {
            glTranslatef(journeySceneTextInitialTranslations[0].x, journeySceneTextInitialTranslations[0].y, journeySceneTextInitialTranslations[0].z);
            drawGradedText("2002", color, gradientColor, JOURNEY_SCENE_FONT_DISPLAYLISTS_OFFSET);
            // RT_DrawText("2002", 4, JOURNEY_SCENE_FONT_DISPLAYLISTS_OFFSET);
        }
        glPopMatrix();

        glPushMatrix();
        {
            glTranslatef(journeySceneTextInitialTranslations[1].x, journeySceneTextInitialTranslations[1].y, journeySceneTextInitialTranslations[1].z);
            drawGradedText("WinDev", color, gradientColor, JOURNEY_SCENE_FONT_DISPLAYLISTS_OFFSET);
            // RT_DrawText("WinDev", 6, JOURNEY_SCENE_FONT_DISPLAYLISTS_OFFSET);
        }
        glPopMatrix();
        break;
    case UNIX_2003_TEXT:
        glPushMatrix();
        {
            glTranslatef(journeySceneTextInitialTranslations[2].x, journeySceneTextInitialTranslations[2].y, journeySceneTextInitialTranslations[2].z);
            glTranslatef(translation.x, translation.y, translation.z);
            glRotatef(yRotation, 0.0f, 1.0f, 0.0f);
            drawGradedText("2003", color, gradientColor, JOURNEY_SCENE_FONT_DISPLAYLISTS_OFFSET);
            // RT_DrawText("2003", 4, JOURNEY_SCENE_FONT_DISPLAYLISTS_OFFSET);
        }
        glPopMatrix();

        glPushMatrix();
        {
            glTranslatef(journeySceneTextInitialTranslations[3].x, journeySceneTextInitialTranslations[3].y, journeySceneTextInitialTranslations[3].z);
            glTranslatef(translation.x, translation.y, translation.z);
            glRotatef(yRotation, 0.0f, 1.0f, 0.0f);
            drawGradedText("UNIX", color, gradientColor, JOURNEY_SCENE_FONT_DISPLAYLISTS_OFFSET);
            // RT_DrawText("UNIX", 4, JOURNEY_SCENE_FONT_DISPLAYLISTS_OFFSET);
        }
        glPopMatrix();
        break;
    case YEAR_2004_TEXT:
        glPushMatrix();
        {
            glTranslatef(journeySceneTextInitialTranslations[4].x, journeySceneTextInitialTranslations[4].y, journeySceneTextInitialTranslations[4].z);
            glTranslatef(translation.x, translation.y, translation.z);
            glRotatef(yRotation, 0.0f, 1.0f, 0.0f);
            drawGradedText("2004", color, gradientColor, JOURNEY_SCENE_FONT_DISPLAYLISTS_OFFSET);
            // RT_DrawText("2004", 4, JOURNEY_SCENE_FONT_DISPLAYLISTS_OFFSET);
        }
        glPopMatrix();
        break;
    case SUNBEAM_2006_TEXT:
        glPushMatrix();
        {
            glTranslatef(journeySceneTextInitialTranslations[5].x, journeySceneTextInitialTranslations[5].y, journeySceneTextInitialTranslations[5].z);
            glTranslatef(translation.x, translation.y, translation.z);
            glRotatef(yRotation, 0.0f, 1.0f, 0.0f);
            drawGradedText("2004", color, gradientColor, JOURNEY_SCENE_FONT_DISPLAYLISTS_OFFSET);
            // RT_DrawText("2004", 4, JOURNEY_SCENE_FONT_DISPLAYLISTS_OFFSET);
        }
        glPopMatrix();

        glPushMatrix();
        {
            glTranslatef(journeySceneTextInitialTranslations[6].x, journeySceneTextInitialTranslations[6].y, journeySceneTextInitialTranslations[6].z);
            glTranslatef(translation.x, translation.y, translation.z);
            glRotatef(yRotation, 0.0f, 1.0f, 0.0f);
            drawGradedText("SunBeam", color, gradientColor, JOURNEY_SCENE_FONT_DISPLAYLISTS_OFFSET);
            // RT_DrawText("SunBeam", 7, JOURNEY_SCENE_FONT_DISPLAYLISTS_OFFSET);
        }
        glPopMatrix();
        break;
    case OS_SEMINAR_2008_TEXT:
        glPushMatrix();
        {
            glTranslatef(journeySceneTextInitialTranslations[7].x, journeySceneTextInitialTranslations[7].y, journeySceneTextInitialTranslations[7].z);
            glTranslatef(translation.x, translation.y, translation.z);
            glRotatef(yRotation, 0.0f, 1.0f, 0.0f);
            drawGradedText("2007", color, gradientColor, JOURNEY_SCENE_FONT_DISPLAYLISTS_OFFSET);
            // RT_DrawText("2007", 4, JOURNEY_SCENE_FONT_DISPLAYLISTS_OFFSET);
        }
        glPopMatrix();

        glPushMatrix();
        {
            glTranslatef(journeySceneTextInitialTranslations[8].x, journeySceneTextInitialTranslations[8].y, journeySceneTextInitialTranslations[8].z);
            glTranslatef(translation.x, translation.y, translation.z);
            glRotatef(yRotation, 0.0f, 1.0f, 0.0f);
            drawGradedText("OS Seminar", color, gradientColor, JOURNEY_SCENE_FONT_DISPLAYLISTS_OFFSET);
            // RT_DrawText("OS Seminar", 10, JOURNEY_SCENE_FONT_DISPLAYLISTS_OFFSET);
        }
        glPopMatrix();
        break;
    default:
        break;
    }   
}

void uninitializeJourneySceneText(void)
{
    // code
    RT_Uninitialize_Library(&journeySceneFont, JOURNEY_SCENE_FONT_DISPLAYLISTS_OFFSET);
}
