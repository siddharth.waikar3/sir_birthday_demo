#pragma once

// function declarations
void initializeSceneThree(void);
void displaySceneThree(void);
void updateSceneThree(void);
void uninitializeSceneThree(void);
void drawIcosahedron(int);
