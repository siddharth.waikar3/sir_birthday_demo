// header files
#include "..\Common\Common.h"
#include "..\objects\Orbit.h"
#include "..\objects\Saturn.h"
#include "..\Common\Camera.h"
#include "..\objects\years.hpp"
#include "..\Objects\Skybox.h"
#include "..\Common\RenderText.h"


// imgui
#include "../imgui/imgui.h"

// defines
#define SHOW_ROCKS_NEW_JOURNEY_SCENE SHOW_ROCKS_GLOBAL

#define ORBIT_INNER_RADIUS  20.0f
#define ORBIT_OUTER_RADIUS  20.05f
#define BABY_SATURN_INNER_ORBIT_RADIUS 10.0f
#define BABY_SATURN_OUTER_ORBIT_RADIUS 10.05f
#define ORBIT_RADIUS (ORBIT_INNER_RADIUS + ORBIT_OUTER_RADIUS) / 2.0f

#define JOURNEY_SCENE_TIMER_ID 1
#define WINDEV_TEXT_TIMER_ID 2
#define BABY_SATURN_DELAY_TIMER 3
#define BABY_SATURN_ORBIT_TIMER 4

#define BABY_STATURN_BORN_ANGLE_START 88.5f
#define BABY_STATURN_BORN_ANGLE_END 83.0f
#define BABY_SATURN_BORN_STATE_SPEED (fabs(BABY_STATURN_BORN_ANGLE_END - BABY_STATURN_BORN_ANGLE_START) / (FPS * 10.0f))
#define BABY_SATURN_WINDDEV_STATE_SPEED (fabs(BABY_STATURN_BORN_ANGLE_END - BABY_STATURN_BORN_ANGLE_START) / (FPS * 2.1f))

#define NEW_JOURNEY_SCENE_FONT_DISPLAYLISTS_OFFSET 5353539

enum NewJourneySceneStates
{
    BABY_SATURN_BORN_STATE,
    BABY_SATURN_WINDEV_STATE,
    BABY_SATURN_JOURNEY_START_STATE,
    BABY_SATURN_ORBIT_ROTATE_STATE,
    BABY_SATURN_NEW_ORBIT_ROTATE_STATE,
    END
};

enum NewJourneySceneStates newJourneySceneState;

//extern global variables

extern struct Transformations currentTransformation;
extern struct Transformations newJourneySceneTransformation;
extern struct Transformations g_Transformations[NUM_TRANSFORMATION_ELEMENTS];
extern HWND ghwnd;
extern FILE* gpFILE;
extern HDC ghdc;

extern enum Scene currentScene;
extern CameraCoordinates cameraCoordinates;

BOOL journeySceneTimer = FALSE;
BOOL journeySceneFlag = TRUE;

BOOL winDevTextTimer = FALSE;
BOOL winDevTextFlag = TRUE;

BOOL cameraDelay = FALSE;
BOOL timerFlag = TRUE;
BOOL startJourney = FALSE;

BOOL babySaturnTimer = FALSE;
BOOL babySaturnFlag = TRUE;

BOOL showRing = FALSE;

GLfloat saturnAngle = 0.0f;
GLfloat ringAngle = 0.0f;
GLfloat gol = 0.0f;
GLfloat saturnX = 0.0f;
GLfloat saturnZ = 0.0f;
GLfloat babySaturnX = 0.0f;
GLfloat babySaturnZ = 0.0f;
GLfloat radius = 5.8f;
GLfloat angleInDegree = 360.0f;
GLfloat angle = 70.0f;
GLfloat angleInRadiant;
GLfloat saturnNewX = 0.0f;
GLfloat saturnNewZ = 0.0f;
GLfloat fdAlpha = 0.0f;
GLfloat baby_saturn_speed = 0.5f;
GLfloat sRadius = ORBIT_RADIUS;
GLfloat satRadius = 10.0f;
GLfloat Degree = 210.0f;
GLfloat sAngle = 0.0f;
GLfloat Tradius = 10.0f;
GLfloat x, y;
GLfloat Mangle = -250.0f;
GLfloat trajectoryAlpha = 1.0f;
GLfloat orbitAlpha = 0.0f;
RT_Library newJourneySceneFont = { 0 };


CameraCoordinates newJournySceneInitialCameraPosition =
{
    {1.992220f, 1.773973f, 23.937511f},
    {-0.129039f, -0.374607f, -0.918161f},
    {0.918161f, 0.000000f, -0.129039f},
    {-0.048339f, 0.859670f, -0.343949f},
    -22.000000f,
    262.000000f
};

CameraCoordinates nextSceneCameraPosition1 = 
{
    {1.520000f, 0.8f, 19.939997f},
    {1.000000f, 0.000000f, 0.000000f},
    {0.000000f, 0.000000f, 1.000000f},
    {0.000000f, 1.000000f, 0.000000f},
    0.000000f,
    0.000000f
};

CameraCoordinates nextSceneCameraPosition2 =
{
    /* {1.520000f, 2.251097f, 19.939997f},
    {0.984808f, -0.173648f, 0.000000f},
    {-0.000000f, 0.000000f, 0.984808f},
    {0.171010f, 0.969846f, 0.000000f},
    -10.000000f,
    0.000000f */
 
    {1.520000f, 3.500000f, 19.939997f},
    {0.970296f, -0.241922f, 0.000000f},
    {-0.000000f, 0.000000f, 0.970296f},
    {0.234736f, 0.941474f, 0.000000f},
    -14.000000f,
    0.000000f
};

CameraCoordinates nextSceneCameraPosition3 =
{
    {-0.041384f, 2.140403f, 12.635752f},
    {0.743145f, 0.000000f, -0.669131f},
    {0.669131f, -0.000000f, 0.743145f},
    {0.000000f, 1.000000f, 0.000000f},
    0.000000f,
    318.000000f
};

CameraCoordinates nextSceneCameraPosition4 =
{
    /* {-3.219500f, 24.998373f, 2.759658f},
    {0.120527f, -0.990268f, -0.069587f},
    {0.069587f, -0.000000f, 0.120527f},
    {0.119354f, 0.019369f, -0.068909f},
    -82.000000f,
    330.000000f */

    {-4.540261f, 25.640404f, 3.903049f},
    {0.154508f, -0.978148f, -0.139120f},
    {0.139120f, -0.000000f, 0.154508f},
    {0.151132f, 0.043227f, -0.136080f},
    -78.000000f,
    318.000000f
};

const struct Transformations orbitTransformation =
{
    0.000000f, -0.050000f, 0.000000f,
    90.000000f, 0.000000f, 0.000000f,
    1.000000f, 1.000000f, 1.000000f
};

const struct Transformations babySaturnOrbitTransformation =
{
    0.000000f, 0.000000f, 0.000000f,
    90.000000f, 0.000000f, 0.000000f,
    1.000000f, 1.000000f, 1.000000f
};

struct Transformations saturnSteady =
{
    -1.00000f, 0.000000f, 0.000000f,
    0.000000f, 0.000000f, 0.000000f,
    1.000000f, 1.000000f, 1.000000f
};

struct Transformations saturnSteady2 =
{
    0.00000f, 0.000000f, 0.000000f,
    0.000000f, 0.000000f, 0.000000f,
    1.000000f, 1.000000f, 1.000000f
};


struct Transformations babySaturnSteady =
{
    -0.500000f, 0.000000f, 0.000000f,
    0.000000f, 0.000000f, 0.000000f,
    1.000000f, 1.000000f, 1.000000f
};

struct Transformations babySaturnStartPosition =
{
    -1.000000f, 0.000000f, 0.000000f,
    0.000000f, 0.000000f, 0.000000f,
    0.400001f, 0.400001f, 0.400001f
};

struct Transformations babySaturnStartPosition2 =
{
    0.000000f, 0.000000f, 0.000000f,
    0.000000f, 0.000000f, 0.000000f,
    0.400001f, 0.400001f, 0.400001f
};

const struct Transformations winDevTextPosition =
{
//  7.100010f, 0.600000f, 20.000000f,
    7.100010f, 0.700000f, 20.300000f,
    0.000000f, -90.000000f, 0.000000f,
    1.000000f, 1.000000f, 1.000000f
};

const struct Transformations yearTextPosition =
{ 
//   6.850009f, 0.000000f, 19.650005f,
    7.000010f, 0.000000f, 19.650005f,
    0.000000f, -90.000000f, 0.000000f,
    1.000000f, 1.000000f, 1.000000f
};

const struct Transformations trajectoryPosition =
{
    1.000000f, 0.000000f, 2.0f,
    90.000000f, 0.000000f, 0.000000f,
    1.000000f, 1.000000f, 1.000000f
};

struct Transformations currentBabySaturnPosition;



void journeySceneTimerFunc(HWND hwnd, UINT iMsg, UINT_PTR idEvent, DWORD dwTime)
{
    KillTimer(hwnd, idEvent);
    if (idEvent == JOURNEY_SCENE_TIMER_ID) {
        journeySceneFlag = FALSE;
    }

    if (idEvent == WINDEV_TEXT_TIMER_ID) {
        winDevTextFlag = FALSE;
    }

    if (idEvent == BABY_SATURN_DELAY_TIMER) {
        timerFlag = FALSE;
    }

    if (idEvent == BABY_SATURN_ORBIT_TIMER) {
        babySaturnFlag = FALSE;
    }

    babySaturnTimer = FALSE;
    journeySceneTimer = FALSE;
    winDevTextTimer = FALSE;
    cameraDelay = FALSE;
}

// function definitions
void initializeSceneSeven(void)
{
    // code
    newJourneySceneState = BABY_SATURN_BORN_STATE;

    // saturn translation
    // fixed 1st shot position
    //setCameraCoordinates(newJournySceneInitialCameraPosition);

    // fixed 2-a shot position
    //setCameraCoordinates(nextSceneCameraPosition1);

    // fixed 2-b shot position
    //setCameraCoordinates(nextSceneCameraPosition3);

    angleInDegree = 90.0f;
    angleInRadiant = (angleInDegree * M_PI) / 180.0f;
    saturnSteady.translation.x  = (ORBIT_RADIUS * cosf(angleInRadiant));
    saturnSteady.translation.z =  (ORBIT_RADIUS * sinf(angleInRadiant));
    

    angleInDegree = BABY_STATURN_BORN_ANGLE_START;
    angleInRadiant = (angleInDegree * M_PI) / 180.0f;
    babySaturnStartPosition.translation.x = (ORBIT_RADIUS * cosf(angleInRadiant));
    babySaturnStartPosition.translation.z = (ORBIT_RADIUS * sinf(angleInRadiant));
  
    saturnSteady2.translation.x = (ORBIT_RADIUS - 10.0f) * cosf(angleInRadiant);
    saturnSteady2.translation.z = (ORBIT_RADIUS - 10.0f) * sinf(angleInRadiant);
    
    babySaturnStartPosition2.translation.x = (ORBIT_RADIUS - 10.0f) * cosf(angleInRadiant);
    babySaturnStartPosition2.translation.z = (ORBIT_RADIUS - 10.0f) * sinf(angleInRadiant);

    RT_Initialize_Library(&newJourneySceneFont, ghdc, TEXT("./Resources/fonts/Rounded_Elegance.ttf"), TEXT("Rounded Elegance"), RT_FW_DONTCARE, 0, NEW_JOURNEY_SCENE_FONT_DISPLAYLISTS_OFFSET);
    // seekALSource(g_CompleteTrackSource, 0, 3, 7);
}

void displaySceneSeven(void)
{
    // local variables
    static BOOL bInit = FALSE;
    static BOOL bOnce = FALSE;
    static BOOL bOnce2 = FALSE;
    static Color color = { 1.0f, 1.0f, 1.0f, 1.0f };
    static Color gradientColor = { 0.3f, 0.3f, 0.3f, 1.0f };
  
    if(!bInit)
    {
        setCameraCoordinates(newJournySceneInitialCameraPosition);
        bInit = TRUE;
    }
    drawTexturedSkybox();

    switch (newJourneySceneState)
    {
   
    case BABY_SATURN_BORN_STATE:

        // draw the orbit
        glPushMatrix();
        {
            applyTransformations(orbitTransformation);
            DrawOrbit(ORBIT_INNER_RADIUS, ORBIT_OUTER_RADIUS);
        }
        glPopMatrix();

        // draw baby saturn
        glPushMatrix();
        {
            applyTransformations(babySaturnStartPosition);
            drawSaturnWithoutRing(saturnAngle);
        }
        glPopMatrix();

        // draw the saturn
        glPushMatrix();
        {
            applyTransformations(saturnSteady);
            drawSaturn(saturnAngle, ringAngle, SHOW_ROCKS_NEW_JOURNEY_SCENE);
        }
        glPopMatrix();
        break;

    case BABY_SATURN_WINDEV_STATE:
        if(!bOnce)
        {
            setCameraCoordinates(nextSceneCameraPosition1);
            bOnce = TRUE;
        }
        
        // draw the orbit
        glPushMatrix();
        {
            applyTransformations(orbitTransformation);
            DrawOrbit(ORBIT_INNER_RADIUS, ORBIT_OUTER_RADIUS);
        }
        glPopMatrix();

        // draw WINDEV text
        glPushMatrix();
        {
            applyTransformations(winDevTextPosition);
            drawGradedText("2023", color, gradientColor, NEW_JOURNEY_SCENE_FONT_DISPLAYLISTS_OFFSET);
        }
        glPopMatrix();

        glPushMatrix();
        {
            applyTransformations(yearTextPosition);
            drawGradedText("WinDev", color, gradientColor, NEW_JOURNEY_SCENE_FONT_DISPLAYLISTS_OFFSET);
        }
        glPopMatrix();
        break;

    case BABY_SATURN_JOURNEY_START_STATE:
        
        // draw the orbit
        glPushMatrix();
        {
            applyTransformations(orbitTransformation);
            DrawOrbit(ORBIT_INNER_RADIUS, ORBIT_OUTER_RADIUS);
        }
        glPopMatrix();

        // draw WINDEV text
        glPushMatrix();
        {
            applyTransformations(winDevTextPosition);
            drawGradedText("2023", color, gradientColor, NEW_JOURNEY_SCENE_FONT_DISPLAYLISTS_OFFSET);
        }
        glPopMatrix();

        glPushMatrix();
        {
            applyTransformations(yearTextPosition);
            drawGradedText("WinDev", color, gradientColor, NEW_JOURNEY_SCENE_FONT_DISPLAYLISTS_OFFSET);
        }
        glPopMatrix();

        // draw saturn 
        glPushMatrix();
        {
            applyTransformations(saturnSteady);
            drawSaturn(saturnAngle, ringAngle, SHOW_ROCKS_NEW_JOURNEY_SCENE);
        }
        glPopMatrix();

        // draw baby saturn
        glPushMatrix();
        {
            applyTransformations(babySaturnSteady);
            drawSaturn_v2(saturnAngle, ringAngle, 0, SHOW_ROCKS_NEW_JOURNEY_SCENE);
        }
        glPopMatrix();
        break;

    case BABY_SATURN_ORBIT_ROTATE_STATE:
        if(!bOnce2)
        {
            setCameraCoordinates(nextSceneCameraPosition3);
            bOnce2 = TRUE;
        }

        // draw the orbit
        glPushMatrix();
        {
            applyTransformations(orbitTransformation);
            DrawOrbit(ORBIT_INNER_RADIUS-10.0f, ORBIT_OUTER_RADIUS-10.0f);
        }
        glPopMatrix();

        // draw saturn 
        glPushMatrix();
        {
            applyTransformations(saturnSteady2);
            drawSaturn(saturnAngle, ringAngle, SHOW_ROCKS_NEW_JOURNEY_SCENE);
        }
        glPopMatrix();

        // draw baby saturn
        glPushMatrix();
        {
            applyTransformations(babySaturnStartPosition2);
            drawSaturn_v2(saturnAngle, ringAngle, 0, SHOW_ROCKS_NEW_JOURNEY_SCENE);
        }
        glPopMatrix();
        break;

    case BABY_SATURN_NEW_ORBIT_ROTATE_STATE:
        // draw the orbit
        glPushMatrix();
        {
            applyTransformations(orbitTransformation);
            DrawOrbit(ORBIT_INNER_RADIUS - 10.0f, ORBIT_OUTER_RADIUS - 10.0f);
        }
        glPopMatrix();

        // draw saturn 
        glPushMatrix();
        {
            applyTransformations(saturnSteady2);
            drawSaturn(saturnAngle, ringAngle, SHOW_ROCKS_NEW_JOURNEY_SCENE);
        }
        glPopMatrix();

        // draw baby saturn
        glPushMatrix();
        {
            applyTransformations(babySaturnStartPosition2);
            drawSaturn_v2(saturnAngle, ringAngle, showRing, SHOW_ROCKS_NEW_JOURNEY_SCENE);
        }
        glPopMatrix();

        //draw baby saturn orbit
        glPushMatrix();
        {
            applyTransformations(babySaturnOrbitTransformation);
            DrawOrbitEx(7.0f, 7.05f, orbitAlpha);
        }
        glPopMatrix();

        // glPushMatrix();
        // {
        //     applyTransformations(trajectoryPosition);
        //     glColor4f(1.0f, 1.0f, 1.0f, trajectoryAlpha);

        //     glBegin(GL_LINE_STRIP);
        //     {
        //         for (float trDegree = -270.0f; trDegree > -397.0f; trDegree = trDegree - 0.2f)
        //         {   
        //             //fprintf(gpFILE, " trDegree = %f\t Tradius = %f\n ", trDegree, Tradius);
        //             GLfloat Radiant = (trDegree * M_PI) / 180.0f;
        //             x = 0 + (Tradius * cosf(Radiant));
        //             y = 0 + (Tradius * sinf(Radiant));
        //             glVertex3f(x, y, 0.0f);
        //             if (Tradius >= 7.0f) {
        //                 Tradius = Tradius - 0.006; 
        //             }
        //             //trDegree = trDegree - 0.15f;
        //         }
        //     }
        //     glEnd();
        // }
        // glPopMatrix();

        break;

    default: 
        break;
    }
    fadeOutEffect(fdAlpha);
}

void updateSceneSeven(void)
{
    void uninitializeSceneSeven(void);
    
    BOOL Done = TRUE;
    BOOL bFadeInFlag = FALSE;
    static GLfloat cameraRefrence = 0.0f;
    static GLfloat cameraReference1 = 0.0f;
    static BOOL bOnce = FALSE;
    static ALfloat position = 0.0f;

    // code
    if(!bOnce)
    {
        getALSourcePlaybackPositionInSeconds(g_SceneSources[2], &position);

        // seek & play only if necessary
        if(!(position >= 19.0f && position <= 20.0f))
        {
            // bind the source
            SetCurrentSourceForStats(g_SceneSources[2], FALSE, FALSE);

            seekALSourcef(g_SceneSources[2], 0.0f, 0.0f, 19.330f); 
            playALSource(g_SceneSources[2]);
        }
        bOnce = TRUE;
    }

    if(totalElapsedTime >= (69.0))
    {
        newJourneySceneState = END;
    }

    saturnAngle = saturnAngle + 1.0f;
    if (saturnAngle >= 360.0f)
    {
        saturnAngle = saturnAngle - 360.0f;
    }

    ringAngle = ringAngle + 1.0f;
    if (ringAngle >= 360.0f)
    {
        ringAngle = ringAngle - 360.0f;
    }

    switch (newJourneySceneState)
    {
    case BABY_SATURN_BORN_STATE:

        angleInDegree = angleInDegree - BABY_SATURN_BORN_STATE_SPEED;

        angleInRadiant = (angleInDegree * M_PI) / 180.0f;
        babySaturnStartPosition.translation.x = (ORBIT_RADIUS * cosf(angleInRadiant));
        babySaturnStartPosition.translation.z = (ORBIT_RADIUS * sinf(angleInRadiant));
               
        if (!journeySceneTimer && journeySceneFlag ) {
            SetTimer(ghwnd, JOURNEY_SCENE_TIMER_ID, 9000, (TIMERPROC)journeySceneTimerFunc);
            journeySceneTimer = TRUE;
        }

        if (!journeySceneFlag) {
            if (updateAlpha(&fdAlpha, 1.0f / FPS, FADE_OUT_FLAG)) {
                newJourneySceneState = BABY_SATURN_WINDEV_STATE;
            }
        }

        break;

    case BABY_SATURN_WINDEV_STATE:

        if (!bFadeInFlag)
            bFadeInFlag = updateAlpha(&fdAlpha, 1.0f / FPS, FADE_IN_FLAG);

        if (!winDevTextTimer && winDevTextFlag ) {
            SetTimer(ghwnd, WINDEV_TEXT_TIMER_ID, 3000, (TIMERPROC)journeySceneTimerFunc);
            winDevTextTimer = TRUE;
        }

        if (!winDevTextFlag) {
            newJourneySceneState = BABY_SATURN_JOURNEY_START_STATE;
        }
        break;

    case BABY_SATURN_JOURNEY_START_STATE:

        if (!timerFlag && cameraDelay) {
            SetTimer(ghwnd, BABY_SATURN_DELAY_TIMER, 3000, (TIMERPROC)journeySceneTimerFunc);
            timerFlag = TRUE;
        }

        if (!cameraDelay) {
            lerpCamera(&nextSceneCameraPosition1, &nextSceneCameraPosition2, &cameraRefrence, 1.0f / (FPS * 10.0f));
        }

        babySaturnSteady = babySaturnStartPosition;
        angleInDegree = angleInDegree - BABY_SATURN_WINDDEV_STATE_SPEED;
        angleInRadiant = (angleInDegree * M_PI) / 180.0f;
        babySaturnStartPosition.translation.x = (ORBIT_RADIUS * cosf(angleInRadiant));
        babySaturnStartPosition.translation.z = (ORBIT_RADIUS * sinf(angleInRadiant));
        
        if (babySaturnStartPosition.translation.x > 8) {
            if (updateAlpha(&fdAlpha, 1.0f/(FPS) , FADE_OUT_FLAG))
            {
                bFadeInFlag = FALSE;
                newJourneySceneState = BABY_SATURN_ORBIT_ROTATE_STATE;
            }
        }
        break;

    case BABY_SATURN_ORBIT_ROTATE_STATE:

        if (!bFadeInFlag)
            bFadeInFlag = updateAlpha(&fdAlpha, 1.0f / ( FPS * 2.5), FADE_IN_FLAG);

        lerpCamera(&nextSceneCameraPosition3, &nextSceneCameraPosition4, &cameraReference1, 1.2f / (FPS * 24.0f));
        
        angle = angle - 0.21;
        if (angle > -250.0f)
        {
            angleInRadiant = (angle * M_PI) / 180.0f;
            babySaturnStartPosition2.translation.x = (ORBIT_RADIUS-10.0f) * cosf(angleInRadiant);
            babySaturnStartPosition2.translation.z = (ORBIT_RADIUS-10.0f) * sinf(angleInRadiant);
        }
        else
        {
            newJourneySceneState = BABY_SATURN_NEW_ORBIT_ROTATE_STATE;
        }
        break;

    case BABY_SATURN_NEW_ORBIT_ROTATE_STATE:

        if (orbitAlpha <= 1.0f) {
            orbitAlpha = orbitAlpha + 0.001f;
        }
        // trajectory to inner orbit
        Mangle = Mangle - 0.2f;
        satRadius = satRadius - 0.007f;
        if (satRadius >= 7.0f)
        {
            //fprintf(gpFILE, "Angle = %f\t Radius = %f\n", Mangle, satRadius);
            angleInRadiant = (Mangle * M_PI) / 180.0f;
            babySaturnStartPosition2.translation.x = (satRadius * cosf(angleInRadiant));
            babySaturnStartPosition2.translation.z = (satRadius * sinf(angleInRadiant));
        }
        
        // rotate baby saturn in inner orbit
        Mangle = Mangle - 0.15f;
        if (satRadius < 7.0f)
        {
            if (trajectoryAlpha <= 1.0f) {
                trajectoryAlpha = trajectoryAlpha - 0.1f;
            }
            showRing = TRUE;
            angleInRadiant = (Mangle * M_PI) / 180.0f;
            babySaturnStartPosition2.translation.x = (7.0f * cosf(angleInRadiant));
            babySaturnStartPosition2.translation.z = (7.05f * sinf(angleInRadiant));
        }

        if (!babySaturnTimer && babySaturnFlag) {
            SetTimer(ghwnd, BABY_SATURN_ORBIT_TIMER, 17000, (TIMERPROC)journeySceneTimerFunc);
            babySaturnTimer = TRUE;
        }

        if (!babySaturnFlag)
        {
            updateAlpha(&fdAlpha, 1.0f / (FPS * 3.3f), FADE_OUT_FLAG);
                // newJourneySceneState = END;
        }
        break;

    case END:
        uninitializeSceneSeven();
        ResetGlobalTimer();
        setCameraCoordinates({{0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 1.0f, 0.0f}, 0.0f, 270.0f});
        currentScene = SCENE_HANDING_OVER_LEGACY;
        break;

    default: break;
    }
}

void uninitializeSceneSeven(void)
{
    // code 
    stopALSource(g_SceneSources[2]);
    SetCurrentSourceForStats(0, TRUE, FALSE);
    RT_Uninitialize_Library(&newJourneySceneFont, NEW_JOURNEY_SCENE_FONT_DISPLAYLISTS_OFFSET);
}
