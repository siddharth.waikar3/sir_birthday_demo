// header files
#include "..\Common\Common.h"
#include "..\Common\Camera.h"
#include "..\Common\Timer.h"
#include "..\Common\RenderText.h"
#include "..\Objects\Skybox.h"
#include "..\Objects\Saturn.h"

// macros
// general-purpose
#define PLAY_CREDITS_SCENE_MUSIC 0
#define SHOW_CREDITS_SCENE_SATURN_ROCKS SHOW_ROCKS_GLOBAL

// timer
#define IDT_CREDITS_SCENE_INITIAL_DELAY 159120
#define IDT_GROUP_LEADER_TEXT_DELAY     159121
#define IDT_PROJECT_LEADER_TEXT_DELAY   159122
#define IDT_ABHI_ADI_TEXT_DELAY         159123
#define IDT_SWITCH_TO_FADE_OUT_STATE    159124
#define IDT_SWITCH_TO_THE_END_SCENE     159125

// for text
#define CREDITS_TEXT_SCALE 0.25f
#define CREDITS_TEXT_CIRCLE_RADIUS 1.25f
#define CREDITS_SCENE_TEXT_SPEED 1.0f
#define CREDITS_SCENE_TEXT_HEADING_Y_TRANSLATION 0.5f
#define CREDITS_SCENE_TEXT_HEADING2_Y_TRANSLATION 0.65f
#define CREDITS_SCENE_TEXT_Y_TRANSLATION 0.25f
#define CREDITS_SCENE_TEXT2_Y_TRANSLATION 0.35f
#define CREDITS_SCENE_TEXT3_Y_TRANSLATION 0.15f

#define CREDITS_TEXT_ONE_SPEED_SUPER_FAST 1.0f / (FPS * 1.0f)
#define CREDITS_TEXT_ONE_SPEED_FAST 1.0f / (FPS * 2.25f)
#define CREDITS_TEXT_ONE_SPEED_SLOW 1.0f / (FPS * 15.0f)
#define CREDITS_GROUP_LEADER_TEXT_SPEED_SLOW 1.0f / (FPS * 22.5f)
#define CREDITS_SIR_TEXT_SPEED_SLOW 1.0f / (FPS * 20.0f)
#define CREDITS_THANK_YOU_FADE_OUT_SPEED 1.0f / (FPS * 3.0f)
#define CREDITS_THANK_YOU_FADE_IN_SPEED 1.0f / (FPS * 1.5f)
#define CREDITS_CAMERA_SPEED 1.0f / (FPS * 4.5f)

// colors
#define CREDITS_SCENE_TEXT_VIBRANT_COLOR          GOLD_COLOR_LIGHT      //1.000000f, 0.498039f, 0.313725f //0.658824f, 0.815686f, 0.901961f
#define CREDITS_SCENE_TEXT_VIBRANT_COLOR_GRADIENT GOLD_COLOR_DARK       //0.807843f, 0.078431f, 0.117647f //0.215686f, 0.278431f, 0.521569f

// no alpha component, RGB colors
#define CREDITS_SCENE_COLOR_RGB                   GOLD_COLOR_LIGHT_RGB
#define CREDITS_SCENE_GRADIENT_COLOR_RGB          GOLD_COLOR_DARK_RGB

// text display lists offset
#define MY_FONT_CREDITS 500
#define MY_FONT_IGNITED 1000
#define MY_FONT_SIR_NAME 1500

// enum
enum CreditsSceneStates
{
    // CREDITS_INITIAL_DELAY,
    CREDITS_GROUP_LEADER_FAST,
    CREDITS_GROUP_LEADER_SLOW,
    CREDITS_GROUP_LEADER_BACK_FAST,
    CREDITS_PROJECT_LEADER_FAST,
    CREDITS_PROJECT_LEADER_SLOW,
    CREDITS_PROJECT_LEADER_BACK_FAST,
    CREDITS_FADE_OUT_GROUP_MEMBER,
    CREDITS_ABHI_AND_ADITYA_FAST,
    CREDITS_ABHI_AND_ADITYA_SLOW,
    CREDITS_ABHI_AND_ADITYA_BACK_FAST,
    CREDITS_DILIP_AND_FAUZAN_FAST,
    CREDITS_DILIP_AND_FAUZAN_SLOW,
    CREDITS_DILIP_AND_FAUZAN_BACK_FAST,
    CREDITS_POOJA_AND_SHRADDHA_FAST,
    CREDITS_POOJA_AND_SHRADDHA_SLOW,
    CREDITS_POOJA_AND_SHRADDHA_BACK_FAST,
    CREDITS_VIVEK_FAST,
    CREDITS_VIVEK_SLOW,
    CREDITS_VIVEK_BACK_FAST,
    CREDITS_SPECIAL_THANKS_FADE_OUT,
    CREDITS_SPECIAL_THANKS_FAST,
    CREDITS_SPECIAL_THANKS_SLOW,
    CREDITS_SPECIAL_THANKS_BACK_FAST,
    CREDITS_SIR_TEXT_FAST,
    CREDITS_SIR_TEXT_SLOW,
    CREDITS_SIR_TEXT_BACK_FAST,
    CREDITS_CAMERA_LOOKING_UP_ANIMATION,
    CREDITS_THANK_YOU_FADE_OUT,
    CREDITS_THANK_YOU_FADE_IN,
    CREDITS_SCENE_FADE_OUT,
    END
};

enum CreditsSceneStates creditsSceneState;

// extern global variables
extern enum Scene currentScene;
extern HWND ghwnd;
extern HDC ghdc;
extern struct Transformations creditsSceneTransformation;
extern struct Transformations g_Transformations[NUM_TRANSFORMATION_ELEMENTS];
RT_Library credits_font, ignited_font, sir_name_font;

// global variables
// timer
BOOL g_bCreditsSceneTimerIsOn = FALSE;

// saturn
GLfloat creditsSceneSaturnAngle, creditsSceneSaturnRingAngle;

// skybox
GLfloat creditsSceneSkyboxAngle = 0.0f;

// text
Vec3 *creditsSceneTextCircleVertices = NULL;
GLint creditsSceneTextCircleTotalVertices = 0;

Lerp_t groupLeaderRotation = {-190.0f /* x */, -190.0f /* x0 */, -25.0f /* x1 */, CREDITS_TEXT_ONE_SPEED_FAST /* speed */, 0.0f /* tVal */};
Lerp_t ravirajIndiRotation = {190.0f  /* x */, 190.0f  /* x0 */, -19.0f /* x1 */, CREDITS_TEXT_ONE_SPEED_FAST /* speed */, 0.0f /* tVal */};

Lerp_t projectLeaderRotation = {-190.0f, -190.0f, -25.0f, CREDITS_TEXT_ONE_SPEED_FAST, 0.0f};
Lerp_t siddharthWaikarRotation = {169.0f, 169.0f, -30.0f, CREDITS_TEXT_ONE_SPEED_FAST, 0.0f};

Lerp_t abhishekDahiphaleRotation = {-195.0f, -195.0f, -38.000000f, CREDITS_TEXT_ONE_SPEED_FAST, 0.0f};
Lerp_t adityaPujariRotation = {190.0f, 190.0f, -25.000000f, CREDITS_TEXT_ONE_SPEED_FAST, 0.0f};

Lerp_t dilipDabhadeRotation = {-190.0f, -190.0f, -25.599998f, CREDITS_TEXT_ONE_SPEED_FAST, 0.0f};
Lerp_t fauzanKhanRotation = {190.0f, 190.0f, -23.500149f, CREDITS_TEXT_ONE_SPEED_FAST, 0.0f};

Lerp_t poojaJagaveRotation = {-190.0f, -190.0f, -25.600006f, CREDITS_TEXT_ONE_SPEED_FAST, 0.0f};
Lerp_t shraddhaChorageRotation = {180.0f, 180.0f, -34.299995f, CREDITS_TEXT_ONE_SPEED_FAST, 0.0f};

Lerp_t groupMemberRotation = {-27.0f, -27.0f, -190.0f, CREDITS_TEXT_ONE_SPEED_FAST, 0.0f};
Lerp_t vivekRishiRotation = {-190.0f, -190.0f, -25.0f, CREDITS_TEXT_ONE_SPEED_FAST, 0.0f};

Lerp_t ramaGokhaleMadamRotation = {-190.0f, -190.0f, -36.0f, CREDITS_TEXT_ONE_SPEED_FAST, 0.0f};
Lerp_t pradnyaGokhaleMadamRotation = {164.0f, 164.0f, -42.0f, CREDITS_TEXT_ONE_SPEED_FAST, 0.0f};

Lerp_t ignitedByRotation = {-190.0f, -190.0f, -19.500002f, CREDITS_TEXT_ONE_SPEED_FAST, 0.0f};
Lerp_t sirTextRotation = {170.0f, 170.0f, -35.000000f, CREDITS_TEXT_ONE_SPEED_FAST, 0.0f};

BOOL g_bReverseGroupLeaderText = FALSE;
BOOL g_bReverseProjectLeaderText = FALSE;
BOOL g_bReverseAbhiAdiText = FALSE;

GLfloat groupMemberAlpha = 0.0f;
GLfloat specialThanksAlpha = 0.0f;

GLfloat creditsSceneSaturnAlpha = 1.0f;
GLfloat thankYouAlpha = 1.0f;

GLfloat creditsSceneFadeAlpha = 0.0f;

char members_name[13][80] = { {"Abhishek Dahiphale"}, {"AdityaPujari"}, {"Dilip Dabhade"}, {"Fauzan Khan"},
                              {"Pooja Jagave"}, { "Shradha Chorage" }, { "Siddharth Waikar" }, {"Vivek Rishi "}, 
                              {"Raviraj Indi"}, {"Dr. Rama Gokhale Madam"},
                              {"Pradnya Gokhale Madam"}, {"Dr. Vijay D. Gokhale"} };

// camera
CameraCoordinates creditsSceneCamera =
{
	{-0.000000f, 0.500000f, 3.000000f},
	{0.000000f, 0.000000f, -1.000000f},
	{1.000000f, -0.000000f, 0.000000f},
	{0.000000f, 1.000000f, 0.000000f},
	0.000000f,
	270.000000f
};

CameraCoordinates creditsSceneCameraEnd =
{
	{-0.000000f, 2.000000f, 7.500000f},
	{0.000000f, 0.707107f, -0.707107f},
	{0.707107f, -0.000000f, 0.000000f},
	{-0.000000f, 0.500000f, 0.500000f},
	25.000000f,
	270.000000f
};

Transformations thankYouTransformations = 
{
	{-1.700000f, 3.649999f, 3.299998f},
	{25.000000f, 0.000000f, 0.000000f},
	{1.000000f, 1.000000f, 1.000000f}
};

// function definitions
void initializeSceneCredits(void)
{
    // code
    // initialize font library
    RT_Initialize_Library(&credits_font, ghdc, TEXT("./Resources/fonts/Jupiteroid-Regular.ttf"), TEXT("Jupiteroid"), RT_FW_THIN, NULL, MY_FONT_CREDITS);

    // initialize credits scene state
    creditsSceneState = CREDITS_GROUP_LEADER_FAST;
}

void displaySceneCredits(void)
{
    // function prototypes
    void drawGroupLeader(GLfloat angle);
    void drawRavirajIndi(GLfloat angle);
    void drawProjectLeader(GLfloat angle);
    void drawSiddharthWaikar(GLfloat angle);
    void drawGroupMember(GLfloat alpha, GLfloat angle);
    void drawAbhishekDahiphale(GLfloat angle);
    void drawAdityaPujari(GLfloat angle);
    void drawDilipDabhade(GLfloat angle);
    void drawFauzanKhan(GLfloat angle);
    void drawPoojaJagave(GLfloat angle);
    void drawShraddhaChorage(GLfloat angle);
    void drawVivekRishi(GLfloat angle);
    void drawSpecialThanks(GLfloat alpha);
    void drawRamaGokhaleMadam(GLfloat angle);
    void drawPradnyaGokhaleMadam(GLfloat angle);
    void drawIgnitedBy(GLfloat angle);
    void drawSirText(GLfloat angle);
    void drawThankYou(GLfloat alpha);

    // local variables
    static BOOL bInit = FALSE;

    // code
    if(!bInit)
    {
        setCameraCoordinatesUsingLookAt(creditsSceneCamera);
        bInit = TRUE;
    }

    // draw skybox
    drawTexturedSkyboxWithRotation(creditsSceneSkyboxAngle);

    // draw saturn
    drawSaturn_v3(creditsSceneSaturnAngle, creditsSceneSaturnRingAngle, creditsSceneSaturnAlpha, creditsSceneSaturnAlpha, SHOW_CREDITS_SCENE_SATURN_ROCKS);

    if(creditsSceneState <= CREDITS_GROUP_LEADER_BACK_FAST)
    {
        // draw "Group Leader"
        drawGroupLeader(groupLeaderRotation.x);

        // draw "Raviraj Indi"
        drawRavirajIndi(ravirajIndiRotation.x);
    }

    if(creditsSceneState >= CREDITS_PROJECT_LEADER_FAST && creditsSceneState <= CREDITS_PROJECT_LEADER_BACK_FAST)
    {
        // draw "Project Leader"
        drawProjectLeader(projectLeaderRotation.x);

        // draw "Siddharth Waikar"
        drawSiddharthWaikar(siddharthWaikarRotation.x);
    }

    if(creditsSceneState >= CREDITS_FADE_OUT_GROUP_MEMBER && creditsSceneState <= CREDITS_VIVEK_BACK_FAST)
    {
        // draw "Group Member"
        drawGroupMember(groupMemberAlpha, groupMemberRotation.x);
    }

    if(creditsSceneState >= CREDITS_ABHI_AND_ADITYA_FAST && creditsSceneState <= CREDITS_ABHI_AND_ADITYA_BACK_FAST)
    {
        // draw "Abhishek Dahiphale"
        drawAbhishekDahiphale(abhishekDahiphaleRotation.x);

        // draw "Aditya Pujari"
        drawAdityaPujari(adityaPujariRotation.x);
    }

    if(creditsSceneState >= CREDITS_DILIP_AND_FAUZAN_FAST && creditsSceneState <= CREDITS_DILIP_AND_FAUZAN_BACK_FAST)
    {
        // draw "Dilip Dabhade"
        drawDilipDabhade(dilipDabhadeRotation.x);

        // draw "Fazuan Khan"
        drawFauzanKhan(fauzanKhanRotation.x);
    }

    if(creditsSceneState >=  CREDITS_POOJA_AND_SHRADDHA_FAST && creditsSceneState <= CREDITS_POOJA_AND_SHRADDHA_BACK_FAST)
    {
        // draw "Pooja Jagave"
        drawPoojaJagave(poojaJagaveRotation.x);
        
        // draw "Shraddha Chorage"
        drawShraddhaChorage(shraddhaChorageRotation.x);
    }
    
    if(creditsSceneState >= CREDITS_VIVEK_FAST && creditsSceneState <= CREDITS_VIVEK_BACK_FAST)
    {
        // draw "Vivek Rishi"
        drawVivekRishi(vivekRishiRotation.x);
    }

    if(creditsSceneState >= CREDITS_SPECIAL_THANKS_FADE_OUT && creditsSceneState <= CREDITS_SPECIAL_THANKS_BACK_FAST)
    {
        // draw "Special Thanks"
        drawSpecialThanks(specialThanksAlpha);
    }

    if(creditsSceneState >= CREDITS_SPECIAL_THANKS_FAST && creditsSceneState <= CREDITS_SPECIAL_THANKS_BACK_FAST)
    {
        // draw "Rama Gokhale Madam"
        drawRamaGokhaleMadam(ramaGokhaleMadamRotation.x);
        
        // draw "Pradnya Gokhale Madam"
        drawPradnyaGokhaleMadam(pradnyaGokhaleMadamRotation.x);
    }

    if(creditsSceneState >= CREDITS_SIR_TEXT_FAST && creditsSceneState <= CREDITS_SIR_TEXT_BACK_FAST)
    {
        // draw "Ignited By"
        drawIgnitedBy(ignitedByRotation.x);
        
        // draw "Dr. Vijay D. Gokhale Sir"
        drawSirText(sirTextRotation.x);
    }
    
    // draw "Thank You"
    glPushMatrix();
    {
        applyTransformations(thankYouTransformations);
        drawThankYou(thankYouAlpha);
    }
    glPopMatrix();

    if(creditsSceneState >= CREDITS_SCENE_FADE_OUT)
    {
        fadeOutEffect(creditsSceneFadeAlpha);
    }
}

void drawGroupLeader(GLfloat angle)
{
    // local variables
    const char *str = "GroupLeader";
    GLfloat pos[] =
    {
        0.000000f,
        4.500000f,
        7.500000f,
        12.000000f,
        16.500000f,
        24.500000f,
        28.500000f,
        33.000000f,
        38.000000f,
        42.500000f,
        47.000000f
    };

    // code
    glPushMatrix();
    {
        glColor4f(CREDITS_SCENE_COLOR_RGB, 1.0f);
     
        glTranslatef(0.0f, CREDITS_SCENE_TEXT_HEADING_Y_TRANSLATION, 0.0f);
        glRotatef(angle, 0.0f, 1.0f, 0.0f);

        for(int i = 0; i < strlen(str); i++)
        {
            glPushMatrix();
            {
                glRotatef(pos[i], 0.0f, 1.0f, 0.0f);
                glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS);
                glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, 0.005f);
                RT_DrawText(&str[i], 1, MY_FONT_CREDITS);
            }
            glPopMatrix();
        }

        glColor4f(CREDITS_SCENE_GRADIENT_COLOR_RGB, 1.0f);

        for(int i = 0; i < strlen(str); i++)
        {
            glPushMatrix();
            {
                glRotatef(pos[i], 0.0f, 1.0f, 0.0f);
                glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS - 0.001f);
                glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE);
                RT_DrawText(&str[i], 1, MY_FONT_CREDITS);
            }
            glPopMatrix();
        }
    }
    glPopMatrix();
}

void drawRavirajIndi(GLfloat angle)
{
    // local variables
    const char *str = "RavirajIndi";
    const GLfloat pos[] =
    {
        0.000000f,
        4.000000f,
        8.000000f,
        12.500000f,
        14.000000f,
        17.000000f,
        20.500000f,
        26.000000f,
        27.500000f,
        32.000000f,
        37.000000f
    };

    // code
    glPushMatrix();
    {
        glColor4f(CREDITS_SCENE_COLOR_RGB, 1.0f);
     
        glTranslatef(0.0f, CREDITS_SCENE_TEXT_Y_TRANSLATION, 0.0f);
        glRotatef(angle, 0.0f, 1.0f, 0.0f);

        for(int i = 0; i < strlen(str); i++)
        {
            glPushMatrix();
            {
                glRotatef(pos[i], 0.0f, 1.0f, 0.0f);
                glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS);
                glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, 0.005f);
                RT_DrawText(&str[i], 1, MY_FONT_CREDITS);
            }
            glPopMatrix();
        }

        glColor4f(CREDITS_SCENE_GRADIENT_COLOR_RGB, 1.0f);

        for(int i = 0; i < strlen(str); i++)
        {
            glPushMatrix();
            {
                glRotatef(pos[i], 0.0f, 1.0f, 0.0f);
                glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS - 0.001f);
                glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE);
                RT_DrawText(&str[i], 1, MY_FONT_CREDITS);
            }
            glPopMatrix();
        }
    }
    glPopMatrix();
}

void drawProjectLeader(GLfloat angle)
{
    // local variables
    const char *str = "ProjectLeader";
    GLfloat pos[] =
    {
        0.000000f,
        4.500000f,
        7.500000f,
        11.000000f,
        13.500000f,
        18.000000f,
        21.500000f,
        27.500000f,
        31.500000f,
        36.000000f,
        40.500000f,
        45.000000f,
        49.500000f,
    };

    // code
    glPushMatrix();
    {
        glColor4f(CREDITS_SCENE_COLOR_RGB, 1.0f);
     
        glTranslatef(0.0f, CREDITS_SCENE_TEXT_HEADING_Y_TRANSLATION, 0.0f);
        glRotatef(angle, 0.0f, 1.0f, 0.0f);

        for(int i = 0; i < strlen(str); i++)
        {
            glPushMatrix();
            {
                glRotatef(pos[i], 0.0f, 1.0f, 0.0f);
                glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS);
                glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, 0.005f);
                RT_DrawText(&str[i], 1, MY_FONT_CREDITS);
            }
            glPopMatrix();
        }

        glColor4f(CREDITS_SCENE_GRADIENT_COLOR_RGB, 1.0f);

        for(int i = 0; i < strlen(str); i++)
        {
            glPushMatrix();
            {
                glRotatef(pos[i], 0.0f, 1.0f, 0.0f);
                glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS - 0.001f);
                glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE);
                RT_DrawText(&str[i], 1, MY_FONT_CREDITS);
            }
            glPopMatrix();
        }
    }
    glPopMatrix();
}

void drawSiddharthWaikar(GLfloat angle)
{
    // local variables
    const char *str = "SiddharthWaikar";
    GLfloat pos[] =
    {
        0.000000f,
        4.500000f,
        5.500000f,
        10.000000f,
        15.000000f,
        19.500000f,
        24.000000f,
        27.500000f,
        31.500000f,
        39.000000f,
        43.500000f,
        48.000000f,
        49.500000f,
        53.500000f,
        58.000000f,
    };

    // code
    glPushMatrix();
    {
        glColor4f(CREDITS_SCENE_COLOR_RGB, 1.0f);
     
        glTranslatef(0.0f, CREDITS_SCENE_TEXT_Y_TRANSLATION, 0.0f);
        glRotatef(angle, 0.0f, 1.0f, 0.0f);

        for(int i = 0; i < strlen(str); i++)
        {
            glPushMatrix();
            {
                glRotatef(pos[i], 0.0f, 1.0f, 0.0f);
                glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS);
                glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, 0.005f);
                RT_DrawText(&str[i], 1, MY_FONT_CREDITS);
            }
            glPopMatrix();
        }

        glColor4f(CREDITS_SCENE_GRADIENT_COLOR_RGB, 1.0f);

        for(int i = 0; i < strlen(str); i++)
        {
            glPushMatrix();
            {
                glRotatef(pos[i], 0.0f, 1.0f, 0.0f);
                glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS - 0.001f);
                glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE);
                RT_DrawText(&str[i], 1, MY_FONT_CREDITS);
            }
            glPopMatrix();
        }
    }
    glPopMatrix();
}

void drawGroupMember(GLfloat alpha, GLfloat angle)
{
    // code
    glPushMatrix();
    {
        glColor4f(CREDITS_SCENE_COLOR_RGB, alpha);

        glTranslatef(0.0f, CREDITS_SCENE_TEXT_HEADING2_Y_TRANSLATION, 0.0f);
        glRotatef(angle, 0.0f, 1.0f, 0.0f);

        // ------------------------------------------------------ "Group" ------------------------------------------------------
        glPushMatrix();
        {   
            glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS);
            glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, 0.005f);
            RT_DrawText("G", 1, MY_FONT_CREDITS);
        }
        glPopMatrix();

        glPushMatrix();
        {   
            glRotatef(5.000000f, 0.0f, 1.0f, 0.0f);
            glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS);
            glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, 0.005f);
            RT_DrawText("r", 1, MY_FONT_CREDITS);
        }
        glPopMatrix();

        glPushMatrix();
        {   
            glRotatef(8.150001f, 0.0f, 1.0f, 0.0f);
            glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS);
            glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, 0.005f);
            RT_DrawText("o", 1, MY_FONT_CREDITS);
        }
        glPopMatrix();

        glPushMatrix();
        {   
            glRotatef(13.000000f, 0.0f, 1.0f, 0.0f);
            glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS);
            glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, 0.005f);
            RT_DrawText("u", 1, MY_FONT_CREDITS);
        }
        glPopMatrix();
    
        glPushMatrix();
        {   
            glRotatef(18.399998f, 0.0f, 1.0f, 0.0f);
            glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS);
            glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, 0.005f);
            RT_DrawText("p", 1, MY_FONT_CREDITS);
        }
        glPopMatrix();

        // -------------------------------------------------- "Member" --------------------------------------------------
        glPushMatrix();
        {   
            glRotatef(26.000000f, 0.0f, 1.0f, 0.0f);
            glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS);
            glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, 0.005f);
            RT_DrawText("M", 1, MY_FONT_CREDITS);
        }
        glPopMatrix();

        glPushMatrix();
        {   
            glRotatef(31.000000f, 0.0f, 1.0f, 0.0f);
            glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS);
            glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, 0.005f);
            RT_DrawText("e", 1, MY_FONT_CREDITS);
        }
        glPopMatrix();
    
        glPushMatrix();
        {   
            glRotatef(36.000000f, 0.0f, 1.0f, 0.0f);
            glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS);
            glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, 0.005f);
            RT_DrawText("m", 1, MY_FONT_CREDITS);
        }
        glPopMatrix();

        glPushMatrix();
        {   
            glRotatef(42.000000f, 0.0f, 1.0f, 0.0f);
            glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS);
            glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, 0.005f);
            RT_DrawText("b", 1, MY_FONT_CREDITS);
        }
        glPopMatrix();

        glPushMatrix();
        {   
            glRotatef(47.000000f, 0.0f, 1.0f, 0.0f);
            glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS);
            glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, 0.005f);
            RT_DrawText("e", 1, MY_FONT_CREDITS);
        }
        glPopMatrix();

        glPushMatrix();
        {   
            glRotatef(52.000000f, 0.0f, 1.0f, 0.0f);
            glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS);
            glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, 0.005f);
            RT_DrawText("r", 1, MY_FONT_CREDITS);
        }
        glPopMatrix();

        // ------------------------------------------------------ GRADATION ------------------------------------------------------
        glColor4f(CREDITS_SCENE_GRADIENT_COLOR_RGB, alpha);

        // ------------------------------------------------------ "Group" ------------------------------------------------------
        glPushMatrix();
        {   
            glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS - 0.001f);
            glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE);
            RT_DrawText("G", 1, MY_FONT_CREDITS);
        }
        glPopMatrix();

        glPushMatrix();
        {   
            glRotatef(5.000000f, 0.0f, 1.0f, 0.0f);
            glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS - 0.001f);
            glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE);
            RT_DrawText("r", 1, MY_FONT_CREDITS);
        }
        glPopMatrix();

        glPushMatrix();
        {   
            glRotatef(8.150001f, 0.0f, 1.0f, 0.0f);
            glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS - 0.001f);
            glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE);
            RT_DrawText("o", 1, MY_FONT_CREDITS);
        }
        glPopMatrix();

        glPushMatrix();
        {   
            glRotatef(13.000000f, 0.0f, 1.0f, 0.0f);
            glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS - 0.001f);
            glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE);
            RT_DrawText("u", 1, MY_FONT_CREDITS);
        }
        glPopMatrix();
    
        glPushMatrix();
        {   
            glRotatef(18.399998f, 0.0f, 1.0f, 0.0f);
            glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS - 0.001f);
            glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE);
            RT_DrawText("p", 1, MY_FONT_CREDITS);
        }
        glPopMatrix();

        // -------------------------------------------------- "Member" --------------------------------------------------
        glPushMatrix();
        {   
            glRotatef(26.000000f, 0.0f, 1.0f, 0.0f);
            glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS - 0.001f);
            glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE);
            RT_DrawText("M", 1, MY_FONT_CREDITS);
        }
        glPopMatrix();

        glPushMatrix();
        {   
            glRotatef(31.000000f, 0.0f, 1.0f, 0.0f);
            glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS - 0.001f);
            glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE);
            RT_DrawText("e", 1, MY_FONT_CREDITS);
        }
        glPopMatrix();
    
        glPushMatrix();
        {   
            glRotatef(36.000000f, 0.0f, 1.0f, 0.0f);
            glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS - 0.001f);
            glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE);
            RT_DrawText("m", 1, MY_FONT_CREDITS);
        }
        glPopMatrix();

        glPushMatrix();
        {   
            glRotatef(42.000000f, 0.0f, 1.0f, 0.0f);
            glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS - 0.001f);
            glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE);
            RT_DrawText("b", 1, MY_FONT_CREDITS);
        }
        glPopMatrix();

        glPushMatrix();
        {   
            glRotatef(47.000000f, 0.0f, 1.0f, 0.0f);
            glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS - 0.001f);
            glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE);
            RT_DrawText("e", 1, MY_FONT_CREDITS);
        }
        glPopMatrix();

        glPushMatrix();
        {   
            glRotatef(52.000000f, 0.0f, 1.0f, 0.0f);
            glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS - 0.001f);
            glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE);
            RT_DrawText("r", 1, MY_FONT_CREDITS);
        }
        glPopMatrix();
    }
    glPopMatrix();
}

void drawAbhishekDahiphale(GLfloat angle)
{
    // code
    glPushMatrix();
    {
        glColor4f(CREDITS_SCENE_TEXT_VIBRANT_COLOR);
     
        glTranslatef(0.0f, CREDITS_SCENE_TEXT2_Y_TRANSLATION, 0.0f);
        glRotatef(angle, 0.0f, 1.0f, 0.0f);

        glPushMatrix();
        {
            glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS);
            glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, 0.005f);
            RT_DrawText("A", 1, MY_FONT_CREDITS);
        }
        glPopMatrix();

        glPushMatrix();
        {
            glRotatef(5.049996f, 0.0f, 1.0f, 0.0f);
            glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS);
            glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, 0.005f);
            RT_DrawText("b", 1, MY_FONT_CREDITS);
        }
        glPopMatrix();

        glPushMatrix();
        {
            glRotatef(9.849996f, 0.0f, 1.0f, 0.0f);
            glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS);
            glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, 0.005f);
            RT_DrawText("h", 1, MY_FONT_CREDITS);
        }
        glPopMatrix();

        glPushMatrix();
        {
            glRotatef(15.000004f, 0.0f, 1.0f, 0.0f);
            glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS);
            glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, 0.005f);
            RT_DrawText("i", 1, MY_FONT_CREDITS);
        }
        glPopMatrix();

        glPushMatrix();
        {
            glRotatef(16.699995f, 0.0f, 1.0f, 0.0f);
            glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS);
            glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, 0.005f);
            RT_DrawText("s", 1, MY_FONT_CREDITS);
        }
        glPopMatrix();

        glPushMatrix();
        {
            glRotatef(21.399990f, 0.0f, 1.0f, 0.0f);
            glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS);
            glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, 0.005f);
            RT_DrawText("h", 1, MY_FONT_CREDITS);
        }
        glPopMatrix();

        glPushMatrix();
        {
            glRotatef(26.099989f, 0.0f, 1.0f, 0.0f);
            glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS);
            glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, 0.005f);
            RT_DrawText("e", 1, MY_FONT_CREDITS);
        }
        glPopMatrix();

        glPushMatrix();
        {
            glRotatef(30.770201f, 0.0f, 1.0f, 0.0f);
            glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS);
            glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, 0.005f);
            RT_DrawText("k", 1, MY_FONT_CREDITS);
        }
        glPopMatrix();

        glPushMatrix();
        {
            glRotatef(37.400059f, 0.0f, 1.0f, 0.0f);
            glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS);
            glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, 0.005f);
            RT_DrawText("D", 1, MY_FONT_CREDITS);
        }
        glPopMatrix();

        glPushMatrix();
        {
            glRotatef(42.400055f, 0.0f, 1.0f, 0.0f);
            glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS);
            glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, 0.005f);
            RT_DrawText("a", 1, MY_FONT_CREDITS);
        }
        glPopMatrix();

        glPushMatrix();
        {
            glRotatef(47.800003f, 0.0f, 1.0f, 0.0f);
            glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS);
            glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, 0.005f);
            RT_DrawText("h", 1, MY_FONT_CREDITS);
        }
        glPopMatrix();

        glPushMatrix();
        {
            glRotatef(53.000000f, 0.0f, 1.0f, 0.0f);
            glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS);
            glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, 0.005f);
            RT_DrawText("i", 1, MY_FONT_CREDITS);
        }
        glPopMatrix();

        glPushMatrix();
        {
            glRotatef(54.900002f, 0.0f, 1.0f, 0.0f);
            glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS);
            glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, 0.005f);
            RT_DrawText("p", 1, MY_FONT_CREDITS);
        }
        glPopMatrix();

        glPushMatrix();
        {
            glRotatef(59.700005f, 0.0f, 1.0f, 0.0f);
            glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS);
            glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, 0.005f);
            RT_DrawText("h", 1, MY_FONT_CREDITS);
        }
        glPopMatrix();

        glPushMatrix();
        {
            glRotatef(64.599991f, 0.0f, 1.0f, 0.0f);
            glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS);
            glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, 0.005f);
            RT_DrawText("a", 1, MY_FONT_CREDITS);
        }
        glPopMatrix();

        glPushMatrix();
        {
            glRotatef(70.000000f, 0.0f, 1.0f, 0.0f);
            glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS);
            glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, 0.005f);
            RT_DrawText("l", 1, MY_FONT_CREDITS);
        }
        glPopMatrix();

        glPushMatrix();
        {
            glRotatef(71.900040f, 0.0f, 1.0f, 0.0f);
            glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS);
            glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, 0.005f);
            RT_DrawText("e", 1, MY_FONT_CREDITS);
        }
        glPopMatrix();

        glColor4f(CREDITS_SCENE_TEXT_VIBRANT_COLOR_GRADIENT);

        glPushMatrix();
        {
            glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS - 0.001f);
            glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE);
            RT_DrawText("A", 1, MY_FONT_CREDITS);
        }
        glPopMatrix();

        glPushMatrix();
        {
            glRotatef(5.049996f, 0.0f, 1.0f, 0.0f);
            glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS - 0.001f);
            glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE);
            RT_DrawText("b", 1, MY_FONT_CREDITS);
        }
        glPopMatrix();

        glPushMatrix();
        {
            glRotatef(9.849996f, 0.0f, 1.0f, 0.0f);
            glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS - 0.001f);
            glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE);
            RT_DrawText("h", 1, MY_FONT_CREDITS);
        }
        glPopMatrix();

        glPushMatrix();
        {
            glRotatef(15.000004f, 0.0f, 1.0f, 0.0f);
            glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS - 0.001f);
            glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE);
            RT_DrawText("i", 1, MY_FONT_CREDITS);
        }
        glPopMatrix();

        glPushMatrix();
        {
            glRotatef(16.699995f, 0.0f, 1.0f, 0.0f);
            glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS - 0.001f);
            glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE);
            RT_DrawText("s", 1, MY_FONT_CREDITS);
        }
        glPopMatrix();

        glPushMatrix();
        {
            glRotatef(21.399990f, 0.0f, 1.0f, 0.0f);
            glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS - 0.001f);
            glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE);
            RT_DrawText("h", 1, MY_FONT_CREDITS);
        }
        glPopMatrix();

        glPushMatrix();
        {
            glRotatef(26.099989f, 0.0f, 1.0f, 0.0f);
            glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS - 0.001f);
            glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE);
            RT_DrawText("e", 1, MY_FONT_CREDITS);
        }
        glPopMatrix();

        glPushMatrix();
        {
            glRotatef(30.770201f, 0.0f, 1.0f, 0.0f);
            glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS - 0.001f);
            glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE);
            RT_DrawText("k", 1, MY_FONT_CREDITS);
        }
        glPopMatrix();

        glPushMatrix();
        {
            glRotatef(37.400059f, 0.0f, 1.0f, 0.0f);
            glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS - 0.001f);
            glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE);
            RT_DrawText("D", 1, MY_FONT_CREDITS);
        }
        glPopMatrix();

        glPushMatrix();
        {
            glRotatef(42.400055f, 0.0f, 1.0f, 0.0f);
            glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS - 0.001f);
            glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE);
            RT_DrawText("a", 1, MY_FONT_CREDITS);
        }
        glPopMatrix();

        glPushMatrix();
        {
            glRotatef(47.800003f, 0.0f, 1.0f, 0.0f);
            glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS - 0.001f);
            glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE);
            RT_DrawText("h", 1, MY_FONT_CREDITS);
        }
        glPopMatrix();

        glPushMatrix();
        {
            glRotatef(53.000000f, 0.0f, 1.0f, 0.0f);
            glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS - 0.001f);
            glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE);
            RT_DrawText("i", 1, MY_FONT_CREDITS);
        }
        glPopMatrix();

        glPushMatrix();
        {
            glRotatef(54.900002f, 0.0f, 1.0f, 0.0f);
            glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS - 0.001f);
            glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE);
            RT_DrawText("p", 1, MY_FONT_CREDITS);
        }
        glPopMatrix();

        glPushMatrix();
        {
            glRotatef(59.700005f, 0.0f, 1.0f, 0.0f);
            glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS - 0.001f);
            glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE);
            RT_DrawText("h", 1, MY_FONT_CREDITS);
        }
        glPopMatrix();

        glPushMatrix();
        {
            glRotatef(64.599991f, 0.0f, 1.0f, 0.0f);
            glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS - 0.001f);
            glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE);
            RT_DrawText("a", 1, MY_FONT_CREDITS);
        }
        glPopMatrix();

        glPushMatrix();
        {
            glRotatef(70.000000f, 0.0f, 1.0f, 0.0f);
            glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS - 0.001f);
            glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE);
            RT_DrawText("l", 1, MY_FONT_CREDITS);
        }
        glPopMatrix();

        glPushMatrix();
        {
            glRotatef(71.900040f, 0.0f, 1.0f, 0.0f);
            glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS - 0.001f);
            glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE);
            RT_DrawText("e", 1, MY_FONT_CREDITS);
        }
        glPopMatrix();
    }
    glPopMatrix();
}

void drawAdityaPujari(GLfloat angle)
{
    // local variables
    const char *str = "AdityaPujari";
    GLfloat rotationPos[] = {
                                0.000000f,
                                4.500000f,
                                10.000000f,
                                11.500000f,
                                15.500000f,
                                19.749996f,
                                27.650000f,
                                32.000000f,
                                36.500000f,
                                40.000000f,
                                45.500000f,
                                49.500000f,
                            };

    // code
    glPushMatrix();
    {
        glColor4f(CREDITS_SCENE_TEXT_VIBRANT_COLOR);
     
        glTranslatef(0.0f, CREDITS_SCENE_TEXT3_Y_TRANSLATION, 0.0f);
        glRotatef(angle, 0.0f, 1.0f, 0.0f);

        for(int i = 0; i < strlen(str); i++)
        {
            glPushMatrix();
            {
                glRotatef(rotationPos[i], 0.0f, 1.0f, 0.0f);
                glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS);
                glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, 0.005f);
                RT_DrawText(&str[i], 1, MY_FONT_CREDITS);
            }
            glPopMatrix();
        }

        glColor4f(CREDITS_SCENE_TEXT_VIBRANT_COLOR_GRADIENT);

        for(int i = 0; i < strlen(str); i++)
        {
            glPushMatrix();
            {
                glRotatef(rotationPos[i], 0.0f, 1.0f, 0.0f);
                glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS - 0.001f);
                glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE);
                RT_DrawText(&str[i], 1, MY_FONT_CREDITS);
            }
            glPopMatrix();
        }
    }
    glPopMatrix();
}

void drawDilipDabhade(GLfloat angle)
{
    // local variables
    const char *str = "DilipDabhade";
    GLfloat rotationPos[] = {
                                0.500000f,
                                5.500000f,
                                7.500000f,
                                9.500000f,
                                11.500000f,
                                19.000000f,
                                24.000000f,
                                29.500000f,
                                34.500000f,
                                39.500000f,
                                44.500000f,
                                49.500000f
                            };

    // code
    glPushMatrix();
    {
        glColor4f(CREDITS_SCENE_TEXT_VIBRANT_COLOR);
     
        glTranslatef(0.0f, CREDITS_SCENE_TEXT2_Y_TRANSLATION, 0.0f);
        glRotatef(angle, 0.0f, 1.0f, 0.0f);

        for(int i = 0; i < strlen(str); i++)
        {
            glPushMatrix();
            {
                glRotatef(rotationPos[i], 0.0f, 1.0f, 0.0f);
                glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS);
                glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, 0.005f);
                RT_DrawText(&str[i], 1, MY_FONT_CREDITS);
            }
            glPopMatrix();
        }

        glColor4f(CREDITS_SCENE_TEXT_VIBRANT_COLOR_GRADIENT);

        for(int i = 0; i < strlen(str); i++)
        {
            glPushMatrix();
            {
                glRotatef(rotationPos[i], 0.0f, 1.0f, 0.0f);
                glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS - 0.001f);
                glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE);
                RT_DrawText(&str[i], 1, MY_FONT_CREDITS);
            }
            glPopMatrix();
        }
    }
    glPopMatrix();

}

void drawFauzanKhan(GLfloat angle)
{
    // local variables
    const char *str = "FauzanKhan";
    GLfloat rotationPos[] = {
                                0.000000f,
                                4.500000f,
                                9.500000f,
                                14.500000f,
                                19.000000f,
                                24.000000f,
                                31.500000f,
                                36.149998f,
                                41.000000f,
                                46.250004f
                            };

    // code
    glPushMatrix();
    {
        glColor4f(CREDITS_SCENE_TEXT_VIBRANT_COLOR);
     
        glTranslatef(0.0f, CREDITS_SCENE_TEXT3_Y_TRANSLATION, 0.0f);
        glRotatef(angle, 0.0f, 1.0f, 0.0f);

        for(int i = 0; i < strlen(str); i++)
        {
            glPushMatrix();
            {
                glRotatef(rotationPos[i], 0.0f, 1.0f, 0.0f);
                glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS);
                glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, 0.005f);
                RT_DrawText(&str[i], 1, MY_FONT_CREDITS);
            }
            glPopMatrix();
        }

        glColor4f(CREDITS_SCENE_TEXT_VIBRANT_COLOR_GRADIENT);

        for(int i = 0; i < strlen(str); i++)
        {
            glPushMatrix();
            {
                glRotatef(rotationPos[i], 0.0f, 1.0f, 0.0f);
                glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS - 0.001f);
                glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE);
                RT_DrawText(&str[i], 1, MY_FONT_CREDITS);
            }
            glPopMatrix();
        }
    }
    glPopMatrix();

}

void drawPoojaJagave(GLfloat angle)
{
    // local variables
    const char *str = "PoojaJagave";
    GLfloat rotationPos[] = {
                               1.000000f,
                                4.500000f,
                                9.150001f,
                                12.500000f,
                                15.099999f,
                                23.000000f,
                                28.000000f,
                                33.000000f,
                                38.000000f,
                                42.500000f,
                                47.500000f
                            };

    // code
    glPushMatrix();
    {
        glColor4f(CREDITS_SCENE_TEXT_VIBRANT_COLOR);
     
        glTranslatef(0.0f, CREDITS_SCENE_TEXT2_Y_TRANSLATION, 0.0f);
        glRotatef(angle, 0.0f, 1.0f, 0.0f);

        for(int i = 0; i < strlen(str); i++)
        {
            glPushMatrix();
            {
                glRotatef(rotationPos[i], 0.0f, 1.0f, 0.0f);
                glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS);
                glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, 0.005f);
                RT_DrawText(&str[i], 1, MY_FONT_CREDITS);
            }
            glPopMatrix();
        }

        glColor4f(CREDITS_SCENE_TEXT_VIBRANT_COLOR_GRADIENT);

        for(int i = 0; i < strlen(str); i++)
        {
            glPushMatrix();
            {
                glRotatef(rotationPos[i], 0.0f, 1.0f, 0.0f);
                glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS - 0.001f);
                glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE);
                RT_DrawText(&str[i], 1, MY_FONT_CREDITS);
            }
            glPopMatrix();
        }
    }
    glPopMatrix();
}

void drawShraddhaChorage(GLfloat angle)
{
    // local variables
    const char *str = "ShraddhaChorage";
    GLfloat rotationPos[] = {
                                0.000000f,
                                4.500000f,
                                9.500000f,
                                13.000000f,
                                17.500000f,
                                22.000000f,
                                27.000000f,
                                31.500000f,
                                39.000000f,
                                43.500000f,
                                48.000000f,
                                52.500000f,
                                55.500000f,
                                60.000000f,
                                64.500000f,
                            };

    // code
    glPushMatrix();
    {
        glColor4f(CREDITS_SCENE_TEXT_VIBRANT_COLOR);
     
        glTranslatef(0.0f, CREDITS_SCENE_TEXT3_Y_TRANSLATION, 0.0f);
        glRotatef(angle, 0.0f, 1.0f, 0.0f);

        for(int i = 0; i < strlen(str); i++)
        {
            glPushMatrix();
            {
                glRotatef(rotationPos[i], 0.0f, 1.0f, 0.0f);
                glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS);
                glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, 0.005f);
                RT_DrawText(&str[i], 1, MY_FONT_CREDITS);
            }
            glPopMatrix();
        }

        glColor4f(CREDITS_SCENE_TEXT_VIBRANT_COLOR_GRADIENT);

        for(int i = 0; i < strlen(str); i++)
        {
            glPushMatrix();
            {
                glRotatef(rotationPos[i], 0.0f, 1.0f, 0.0f);
                glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS - 0.001f);
                glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE);
                RT_DrawText(&str[i], 1, MY_FONT_CREDITS);
            }
            glPopMatrix();
        }
    }
    glPopMatrix();
}

void drawVivekRishi(GLfloat angle)
{
    // local variables
    const char *str = "VivekRishi";
    GLfloat rotationPos[] = {
                                0.000000f,
                                4.500000f,
                                6.000000f,
                                10.500000f,
                                15.000000f,
                                22.000000f,
                                27.000000f,
                                28.500000f,
                                33.000000f,
                                38.000000f,
                            };

    // code
    glPushMatrix();
    {
        glColor4f(CREDITS_SCENE_TEXT_VIBRANT_COLOR);
     
        glTranslatef(0.0f, CREDITS_SCENE_TEXT_Y_TRANSLATION, 0.0f);
        glRotatef(angle, 0.0f, 1.0f, 0.0f);

        for(int i = 0; i < strlen(str); i++)
        {
            glPushMatrix();
            {
                glRotatef(rotationPos[i], 0.0f, 1.0f, 0.0f);
                glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS);
                glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, 0.005f);
                RT_DrawText(&str[i], 1, MY_FONT_CREDITS);
            }
            glPopMatrix();
        }

        glColor4f(CREDITS_SCENE_TEXT_VIBRANT_COLOR_GRADIENT);

        for(int i = 0; i < strlen(str); i++)
        {
            glPushMatrix();
            {
                glRotatef(rotationPos[i], 0.0f, 1.0f, 0.0f);
                glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS - 0.001f);
                glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE);
                RT_DrawText(&str[i], 1, MY_FONT_CREDITS);
            }
            glPopMatrix();
        }
    }
    glPopMatrix();
}

void drawSpecialThanks(GLfloat alpha)
{
    // local variables
    const char *str = "SpecialThanks";
    GLfloat rotationPos[] = {
                                0.000000f,
                                4.000000f,
                                8.500000f,
                                13.000000f,
                                17.500000f,
                                19.000000f,
                                24.000000f,
                                28.500000f,
                                33.000000f,
                                38.000000f,
                                43.000000f,
                                48.000000f,
                                52.000000f,
                            };

    // code
    glPushMatrix();
    {
        glColor4f(CREDITS_SCENE_COLOR_RGB, alpha);
     
        glTranslatef(0.0f, CREDITS_SCENE_TEXT_HEADING2_Y_TRANSLATION, 0.0f);
        glRotatef(-28.000000f, 0.0f, 1.0f, 0.0f);

        for(int i = 0; i < strlen(str); i++)
        {
            glPushMatrix();
            {
                glRotatef(rotationPos[i], 0.0f, 1.0f, 0.0f);
                glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS);
                glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, 0.005f);
                RT_DrawText(&str[i], 1, MY_FONT_CREDITS);
            }
            glPopMatrix();
        }

        glColor4f(CREDITS_SCENE_GRADIENT_COLOR_RGB, alpha);

        for(int i = 0; i < strlen(str); i++)
        {
            glPushMatrix();
            {
                glRotatef(rotationPos[i], 0.0f, 1.0f, 0.0f);
                glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS - 0.001f);
                glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE);
                RT_DrawText(&str[i], 1, MY_FONT_CREDITS);
            }
            glPopMatrix();
        }
    }
    glPopMatrix();
}

void drawRamaGokhaleMadam(GLfloat angle)
{
    // local variables
    const char *str = "RamaGokhaleMadam";
    GLfloat pos[] = 
    {
        0.000000f,
        4.500000f,
        9.000000f,
        14.000000f,
        20.000000f,
        24.500000f,
        29.000000f,
        33.500000f,
        38.000000f,
        43.000000f,
        44.500000f,
        50.500000f,
        55.000000f,
        59.500000f,
        64.000000f,
        68.500000f,
    };

    // code
    glPushMatrix();
    {
        glColor4f(CREDITS_SCENE_COLOR_RGB, 1.0f);
     
        glTranslatef(0.0f, CREDITS_SCENE_TEXT2_Y_TRANSLATION, 0.0f);
        glRotatef(angle, 0.0f, 1.0f, 0.0f);

        for(int i = 0; i < strlen(str); i++)
        {
            glPushMatrix();
            {
                glRotatef(pos[i], 0.0f, 1.0f, 0.0f);
                glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS);
                glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, 0.005f);
                RT_DrawText(&str[i], 1, MY_FONT_CREDITS);
            }
            glPopMatrix();
        }

        glColor4f(CREDITS_SCENE_GRADIENT_COLOR_RGB, 1.0f);

        for(int i = 0; i < strlen(str); i++)
        {
            glPushMatrix();
            {
                glRotatef(pos[i], 0.0f, 1.0f, 0.0f);
                glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS - 0.001f);
                glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE);
                RT_DrawText(&str[i], 1, MY_FONT_CREDITS);
            }
            glPopMatrix();
        }
    }
    glPopMatrix();
}

void drawPradnyaGokhaleMadam(GLfloat angle)
{
    // local variables
    const char *str = "PradnyaGokhaleMadam";
    GLfloat pos[] =
    {
        0.000000f,
        4.500000f,
        7.500000f,
        12.000000f,
        16.500000f,
        20.500000f,
        24.500000f,
        30.500000f,
        35.000000f,
        39.500000f,
        44.000000f,
        48.500000f,
        53.000000f,
        54.000000f,
        60.000000f,
        64.500000f,
        69.000000f,
        73.500000f,
        78.500000f,
    };

    // code
    glPushMatrix();
    {
        glColor4f(CREDITS_SCENE_COLOR_RGB, 1.0f);
     
        glTranslatef(0.0f, CREDITS_SCENE_TEXT3_Y_TRANSLATION, 0.0f);
        glRotatef(angle, 0.0f, 1.0f, 0.0f);

        for(int i = 0; i < strlen(str); i++)
        {
            glPushMatrix();
            {
                glRotatef(pos[i], 0.0f, 1.0f, 0.0f);
                glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS);
                glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, 0.005f);
                RT_DrawText(&str[i], 1, MY_FONT_CREDITS);
            }
            glPopMatrix();
        }

        glColor4f(CREDITS_SCENE_GRADIENT_COLOR_RGB, 1.0f);

        for(int i = 0; i < strlen(str); i++)
        {
            glPushMatrix();
            {
                glRotatef(pos[i], 0.0f, 1.0f, 0.0f);
                glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS - 0.001f);
                glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE);
                RT_DrawText(&str[i], 1, MY_FONT_CREDITS);
            }
            glPopMatrix();
        }
    }
    glPopMatrix();
}

void drawIgnitedBy(GLfloat angle)
{
    // local variables
    const char *str = "IgnitedBy";
    GLfloat pos[] =
    {
        3.000000f,
        4.500000f,
        9.000000f,
        13.500000f,
        14.500000f,
        18.000000f,
        22.500000f,
        29.500000f,
        33.500000f,
    };

    // code
    glPushMatrix();
    {
        glColor4f(CREDITS_SCENE_COLOR_RGB, 1.0f);
     
        glTranslatef(0.0f, CREDITS_SCENE_TEXT_HEADING_Y_TRANSLATION, 0.0f);
        glRotatef(angle, 0.0f, 1.0f, 0.0f);

        for(int i = 0; i < strlen(str); i++)
        {
            glPushMatrix();
            {
                glRotatef(pos[i], 0.0f, 1.0f, 0.0f);
                glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS);
                glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, 0.005f);
                RT_DrawText(&str[i], 1, MY_FONT_CREDITS);
            }
            glPopMatrix();
        }

        glColor4f(CREDITS_SCENE_GRADIENT_COLOR_RGB, 1.0f);

        for(int i = 0; i < strlen(str); i++)
        {
            glPushMatrix();
            {
                glRotatef(pos[i], 0.0f, 1.0f, 0.0f);
                glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS - 0.001f);
                glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE);
                RT_DrawText(&str[i], 1, MY_FONT_CREDITS);
            }
            glPopMatrix();
        }
    }
    glPopMatrix();
}

void drawSirText(GLfloat angle)
{
    // local variables
    const char *str = "Dr.VijayD.GokhaleSir";
    GLfloat pos[] =
    {
        -0.500000f,
        4.000000f,
        6.500000f,
        9.500000f,
        14.000000f,
        14.500000f,
        17.500000f,
        21.500000f,
        27.000000f,
        31.500000f,
        34.500000f,
        39.000000f,
        43.500000f,
        48.000000f,
        52.500000f,
        57.000000f,
        58.000000f,
        64.000000f,
        68.500000f,
        70.000000f,
    };

    // code
    glPushMatrix();
    {
        glColor4f(CREDITS_SCENE_COLOR_RGB, 1.0f);
     
        glTranslatef(0.0f, CREDITS_SCENE_TEXT_Y_TRANSLATION, 0.0f);
        glRotatef(angle, 0.0f, 1.0f, 0.0f);

        for(int i = 0; i < strlen(str); i++)
        {
            glPushMatrix();
            {
                glRotatef(pos[i], 0.0f, 1.0f, 0.0f);
                glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS);
                glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, 0.005f);
                RT_DrawText(&str[i], 1, MY_FONT_CREDITS);
            }
            glPopMatrix();
        }

        glColor4f(CREDITS_SCENE_GRADIENT_COLOR_RGB, 1.0f);

        for(int i = 0; i < strlen(str); i++)
        {
            glPushMatrix();
            {
                glRotatef(pos[i], 0.0f, 1.0f, 0.0f);
                glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS - 0.001f);
                glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE);
                RT_DrawText(&str[i], 1, MY_FONT_CREDITS);
            }
            glPopMatrix();
        }
    }
    glPopMatrix();
}

void drawThankYou(GLfloat alpha)
{
    // local variables
    Color color1 = {CREDITS_SCENE_COLOR_RGB, alpha};
    Color color2 = {CREDITS_SCENE_GRADIENT_COLOR_RGB, alpha};

    // code   
    drawGradedText("Thank You", color1, color2, MY_FONT_CREDITS);
}

/*void drawRoundedText(const char *str, GLfloat *rotationPos, GLfloat angle, GLfloat alpha)
{
    // code
    glPushMatrix();
    {
        glColor4f(CREDITS_SCENE_COLOR_RGB, alpha);
     
        glTranslatef(0.0f, CREDITS_SCENE_TEXT_HEADING_Y_TRANSLATION, 0.0f);
        glRotatef(g_Transformations[0].rotation.y, 0.0f, 1.0f, 0.0f);

        for(int i = 0; i < strlen(str); i++)
        {
            glPushMatrix();
            {
                glRotatef(g_Transformations[i].translation.x, 0.0f, 1.0f, 0.0f);
                glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS);
                glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, 0.005f);
                RT_DrawText(&str[i], 1, MY_FONT_CREDITS);
            }
            glPopMatrix();
        }

        glColor4f(CREDITS_SCENE_GRADIENT_COLOR_RGB, alpha);

        for(int i = 0; i < strlen(str); i++)
        {
            glPushMatrix();
            {
                glRotatef(g_Transformations[i].translation.x, 0.0f, 1.0f, 0.0f);
                glTranslatef(0.0f, 0.0f, CREDITS_TEXT_CIRCLE_RADIUS - 0.001f);
                glScalef(CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE, CREDITS_TEXT_SCALE);
                RT_DrawText(&str[i], 1, MY_FONT_CREDITS);
            }
            glPopMatrix();
        }
    }
    glPopMatrix();
}*/

void updateSceneCredits(void)
{
    // function prototypes
    void creditsSceneTimer(HWND hwnd, UINT iMsg, UINT_PTR idEvent, DWORD dwTime);
    void uninitializeSceneCredits(void);

    // local variables
    static int iArr = 0;
    static BOOL bStartPlayingMusic = TRUE;
    static GLfloat tVal = 0.0f;

    // code
    // play music
#if PLAY_CREDITS_SCENE_MUSIC
    if(bStartPlayingMusic)
    {
        // SetCurrentSourceForStats(g_CompleteTrackSource, FALSE, FALSE);
        // seekALSource(g_CompleteTrackSource, 0, 4, 56);
        // playALSource(g_CompleteTrackSource);
        bStartPlayingMusic = FALSE;
    }
#endif

    // update saturn and skybox angles
    updateRotationValue(&creditsSceneSkyboxAngle, 360.0f / (FPS * 1000.0f));
    updateRotationValue(&creditsSceneSaturnAngle, 360.0f / (FPS * 100.0f));
    updateRotationValue(&creditsSceneSaturnRingAngle, 360.0f / (FPS * 100.0f));

    switch(creditsSceneState)
    {
    // case CREDITS_INITIAL_DELAY:
    //     if(!g_bCreditsSceneTimerIsOn)
    //     {
    //         SetTimer(ghwnd, IDT_CREDITS_SCENE_INITIAL_DELAY, 3000, (TIMERPROC)creditsSceneTimer);
    //         g_bCreditsSceneTimerIsOn = TRUE;
    //     }
    //     break;
    case CREDITS_GROUP_LEADER_FAST:
        // lerp "Group Leader" and "Raviraj Indi" angles to 80% to their middle position with fast speed
        lerpMyValue(&groupLeaderRotation, 0.8f); /* cover 80% of the distance */
        lerpMyValue(&ravirajIndiRotation, 0.8f); /* cover 80% of the distance */

        if(groupLeaderRotation.bReached && ravirajIndiRotation.bReached)
        {
            groupLeaderRotation.speed = CREDITS_GROUP_LEADER_TEXT_SPEED_SLOW;
            ravirajIndiRotation.speed = CREDITS_GROUP_LEADER_TEXT_SPEED_SLOW;
            creditsSceneState = CREDITS_GROUP_LEADER_SLOW;
        }
        break;
    case CREDITS_GROUP_LEADER_SLOW:
        // lerp "Group Leader" and "Raviraj Indi" angles to their middle position with slow speed
        lerpMyValue(&groupLeaderRotation, 1.0f); 
        lerpMyValue(&ravirajIndiRotation, 1.0f);

        if(groupLeaderRotation.bReached && ravirajIndiRotation.bReached)
        {
            groupLeaderRotation.tVal = 0.0f;
            groupLeaderRotation.x0 = groupLeaderRotation.x1;
            groupLeaderRotation.x1 = 175.0f;
            groupLeaderRotation.speed = CREDITS_TEXT_ONE_SPEED_FAST;

            ravirajIndiRotation.tVal = 0.0f;
            ravirajIndiRotation.x0 = ravirajIndiRotation.x1;
            ravirajIndiRotation.x1 = -175.0f;
            ravirajIndiRotation.speed = CREDITS_TEXT_ONE_SPEED_FAST;

            creditsSceneState = CREDITS_GROUP_LEADER_BACK_FAST;
        }
        break;
    case CREDITS_GROUP_LEADER_BACK_FAST:
        // lerp "Group Leader" and "Raviraj Indi" angles back to their original position with super fast speed
        lerpMyValue(&groupLeaderRotation, 1.0f); 
        lerpMyValue(&ravirajIndiRotation, 1.0f);
        
        if(groupLeaderRotation.bReached && ravirajIndiRotation.bReached)
        {
            creditsSceneState = CREDITS_PROJECT_LEADER_FAST;
        }
        break;
    case CREDITS_PROJECT_LEADER_FAST:
        // lerp "Project Leader" and "Siddharth Waikar" to 80% to the final position with fast speed
        lerpMyValue(&projectLeaderRotation, 0.8f);
        lerpMyValue(&siddharthWaikarRotation, 0.8f);

        if(projectLeaderRotation.bReached && siddharthWaikarRotation.bReached)
        {
            projectLeaderRotation.speed = CREDITS_TEXT_ONE_SPEED_SLOW;
            siddharthWaikarRotation.speed = CREDITS_TEXT_ONE_SPEED_SLOW;
            creditsSceneState = CREDITS_PROJECT_LEADER_SLOW;
        }
        break;
    case CREDITS_PROJECT_LEADER_SLOW:
        lerpMyValue(&projectLeaderRotation, 1.0f);
        lerpMyValue(&siddharthWaikarRotation, 1.0f);

        if(projectLeaderRotation.bReached && siddharthWaikarRotation.bReached)
        {
            GLfloat temp = projectLeaderRotation.x0;

            projectLeaderRotation.tVal = 0.0f;
            projectLeaderRotation.x0 = projectLeaderRotation.x1;
            projectLeaderRotation.x1 = 185.0f;
            projectLeaderRotation.speed = CREDITS_TEXT_ONE_SPEED_FAST;

            temp = siddharthWaikarRotation.x0;

            siddharthWaikarRotation.tVal = 0.0f;
            siddharthWaikarRotation.x0 = siddharthWaikarRotation.x1;
            siddharthWaikarRotation.x1 = -185.0f;
            siddharthWaikarRotation.speed = CREDITS_TEXT_ONE_SPEED_FAST;

            creditsSceneState = CREDITS_PROJECT_LEADER_BACK_FAST;
        }
        break;
    case CREDITS_PROJECT_LEADER_BACK_FAST:
        lerpMyValue(&projectLeaderRotation, 1.0f);
        lerpMyValue(&siddharthWaikarRotation, 1.0f);
        
        if(projectLeaderRotation.bReached && siddharthWaikarRotation.bReached)
        {
            creditsSceneState = CREDITS_FADE_OUT_GROUP_MEMBER;
        }
        break;
    case CREDITS_FADE_OUT_GROUP_MEMBER:
        if(updateAlpha(&groupMemberAlpha, 1.0f / (FPS * 1.5f), FADE_OUT_FLAG))
        {
            creditsSceneState = CREDITS_ABHI_AND_ADITYA_FAST;
        }
        break;
    case CREDITS_ABHI_AND_ADITYA_FAST:
        lerpMyValue(&abhishekDahiphaleRotation, 0.8f);
        lerpMyValue(&adityaPujariRotation, 0.8f);

        if(abhishekDahiphaleRotation.bReached && adityaPujariRotation.bReached)
        {
            abhishekDahiphaleRotation.speed = CREDITS_TEXT_ONE_SPEED_SLOW;
            adityaPujariRotation.speed = CREDITS_TEXT_ONE_SPEED_SLOW;
            creditsSceneState = CREDITS_ABHI_AND_ADITYA_SLOW;
        }
        break;
    case CREDITS_ABHI_AND_ADITYA_SLOW:
        lerpMyValue(&abhishekDahiphaleRotation, 1.0f);
        lerpMyValue(&adityaPujariRotation, 1.0f);

        if(abhishekDahiphaleRotation.bReached && adityaPujariRotation.bReached)
        {
            GLfloat temp = abhishekDahiphaleRotation.x0;

            abhishekDahiphaleRotation.tVal = 0.0f;
            abhishekDahiphaleRotation.x0 = abhishekDahiphaleRotation.x1;
            abhishekDahiphaleRotation.x1 = 155.0f;
            abhishekDahiphaleRotation.speed = CREDITS_TEXT_ONE_SPEED_FAST;

            temp = adityaPujariRotation.x0;

            adityaPujariRotation.tVal = 0.0f;
            adityaPujariRotation.x0 = adityaPujariRotation.x1;
            adityaPujariRotation.x1 = -temp;
            adityaPujariRotation.speed = CREDITS_TEXT_ONE_SPEED_FAST;

            creditsSceneState = CREDITS_ABHI_AND_ADITYA_BACK_FAST;
        }
        break;
    case CREDITS_ABHI_AND_ADITYA_BACK_FAST:
        lerpMyValue(&abhishekDahiphaleRotation, 1.0f);
        lerpMyValue(&adityaPujariRotation, 1.0f);
        
        if(abhishekDahiphaleRotation.bReached && adityaPujariRotation.bReached)
        {
            creditsSceneState = CREDITS_DILIP_AND_FAUZAN_FAST;
        }
        break;
    case CREDITS_DILIP_AND_FAUZAN_FAST:
        lerpMyValue(&dilipDabhadeRotation, 0.8f);
        lerpMyValue(&fauzanKhanRotation, 0.8f);

        if(dilipDabhadeRotation.bReached && fauzanKhanRotation.bReached)
        {
            dilipDabhadeRotation.speed = CREDITS_TEXT_ONE_SPEED_SLOW;
            fauzanKhanRotation.speed = CREDITS_TEXT_ONE_SPEED_SLOW;
            creditsSceneState = CREDITS_DILIP_AND_FAUZAN_SLOW;
        }
        break;
    case CREDITS_DILIP_AND_FAUZAN_SLOW:
        lerpMyValue(&dilipDabhadeRotation, 1.0f);
        lerpMyValue(&fauzanKhanRotation, 1.0f);

        if(dilipDabhadeRotation.bReached && fauzanKhanRotation.bReached)
        {
            GLfloat temp = dilipDabhadeRotation.x0;

            dilipDabhadeRotation.tVal = 0.0f;
            dilipDabhadeRotation.x0 = dilipDabhadeRotation.x1;
            dilipDabhadeRotation.x1 = (-temp) - 20.0f;
            dilipDabhadeRotation.speed = CREDITS_TEXT_ONE_SPEED_FAST;

            temp = fauzanKhanRotation.x0;

            fauzanKhanRotation.tVal = 0.0f;
            fauzanKhanRotation.x0 = fauzanKhanRotation.x1;
            fauzanKhanRotation.x1 = -temp + 5.0f;
            fauzanKhanRotation.speed = CREDITS_TEXT_ONE_SPEED_FAST;

            creditsSceneState = CREDITS_DILIP_AND_FAUZAN_BACK_FAST;
        }
        break;

    case CREDITS_DILIP_AND_FAUZAN_BACK_FAST:
        lerpMyValue(&dilipDabhadeRotation, 1.0f);
        lerpMyValue(&fauzanKhanRotation, 1.0f);

        if(dilipDabhadeRotation.bReached && fauzanKhanRotation.bReached)
        {
            creditsSceneState = CREDITS_POOJA_AND_SHRADDHA_FAST;
        }
        break;
    case CREDITS_POOJA_AND_SHRADDHA_FAST:
        lerpMyValue(&poojaJagaveRotation, 0.8f);
        lerpMyValue(&shraddhaChorageRotation, 0.8f);

        if(poojaJagaveRotation.bReached && shraddhaChorageRotation.bReached)
        {
            poojaJagaveRotation.speed = CREDITS_TEXT_ONE_SPEED_SLOW;
            shraddhaChorageRotation.speed = CREDITS_TEXT_ONE_SPEED_SLOW;
            creditsSceneState = CREDITS_POOJA_AND_SHRADDHA_SLOW;
        }
        break;
    case CREDITS_POOJA_AND_SHRADDHA_SLOW:
        lerpMyValue(&poojaJagaveRotation, 1.0f);
        lerpMyValue(&shraddhaChorageRotation, 1.0f);
        
        if(poojaJagaveRotation.bReached && shraddhaChorageRotation.bReached)
        {
            GLfloat temp = poojaJagaveRotation.x0;

            poojaJagaveRotation.tVal = 0.0f;
            poojaJagaveRotation.x0 = poojaJagaveRotation.x1;
            poojaJagaveRotation.x1 = (-temp) - 10.0f;
            poojaJagaveRotation.speed = CREDITS_TEXT_ONE_SPEED_FAST;

            temp = shraddhaChorageRotation.x0;

            shraddhaChorageRotation.tVal = 0.0f;
            shraddhaChorageRotation.x0 = shraddhaChorageRotation.x1;
            shraddhaChorageRotation.x1 = -temp;
            shraddhaChorageRotation.speed = CREDITS_TEXT_ONE_SPEED_FAST;

            creditsSceneState = CREDITS_POOJA_AND_SHRADDHA_BACK_FAST;
        }
        break;
    case CREDITS_POOJA_AND_SHRADDHA_BACK_FAST:
        lerpMyValue(&poojaJagaveRotation, 1.0f);
        lerpMyValue(&shraddhaChorageRotation, 1.0f);
        
        if(poojaJagaveRotation.bReached && shraddhaChorageRotation.bReached)
        {
            creditsSceneState = CREDITS_VIVEK_FAST;
        }
        break;
    case CREDITS_VIVEK_FAST:
        lerpMyValue(&vivekRishiRotation, 0.8f);

        if(vivekRishiRotation.bReached)
        {
            vivekRishiRotation.speed = CREDITS_TEXT_ONE_SPEED_SLOW;
            creditsSceneState = CREDITS_VIVEK_SLOW;
        }
        break;
    case CREDITS_VIVEK_SLOW:
        lerpMyValue(&vivekRishiRotation, 1.0f);

        if(vivekRishiRotation.bReached)
        {
            GLfloat temp = vivekRishiRotation.x0;

            vivekRishiRotation.tVal = 0.0f;
            vivekRishiRotation.x0 = vivekRishiRotation.x1;
            vivekRishiRotation.x1 = -temp;
            vivekRishiRotation.speed = CREDITS_TEXT_ONE_SPEED_FAST;

            creditsSceneState = CREDITS_VIVEK_BACK_FAST;
        }
        break;
    case CREDITS_VIVEK_BACK_FAST:
        lerpMyValue(&groupMemberRotation, 1.0f);
        lerpMyValue(&vivekRishiRotation, 1.0f);
        
        if(groupMemberRotation.bReached && vivekRishiRotation.bReached)
        {
            creditsSceneState = CREDITS_SPECIAL_THANKS_FADE_OUT;
        }
        break;
    case CREDITS_SPECIAL_THANKS_FADE_OUT:
        if(updateAlpha(&specialThanksAlpha, 1.0f / (FPS * 1.0f), FADE_OUT_FLAG))
        {
            creditsSceneState = CREDITS_SPECIAL_THANKS_FAST;
        }
        break;
    case CREDITS_SPECIAL_THANKS_FAST:
        lerpMyValue(&ramaGokhaleMadamRotation, 0.8f);
        lerpMyValue(&pradnyaGokhaleMadamRotation, 0.8f);

        if(ramaGokhaleMadamRotation.bReached && pradnyaGokhaleMadamRotation.bReached)
        {
            ramaGokhaleMadamRotation.speed = CREDITS_TEXT_ONE_SPEED_SLOW;
            pradnyaGokhaleMadamRotation.speed = CREDITS_TEXT_ONE_SPEED_SLOW;
            creditsSceneState = CREDITS_SPECIAL_THANKS_SLOW;
        }
        break;
    case CREDITS_SPECIAL_THANKS_SLOW:
        lerpMyValue(&ramaGokhaleMadamRotation, 1.0f);
        lerpMyValue(&pradnyaGokhaleMadamRotation, 1.0f);

        if(ramaGokhaleMadamRotation.bReached && pradnyaGokhaleMadamRotation.bReached)
        {
            GLfloat temp = ramaGokhaleMadamRotation.x0;

            ramaGokhaleMadamRotation.tVal = 0.0f;
            ramaGokhaleMadamRotation.x0 = ramaGokhaleMadamRotation.x1;
            ramaGokhaleMadamRotation.x1 = (-temp) - 25.0f;
            ramaGokhaleMadamRotation.speed = CREDITS_TEXT_ONE_SPEED_FAST;

            temp = pradnyaGokhaleMadamRotation.x0;

            pradnyaGokhaleMadamRotation.tVal = 0.0f;
            pradnyaGokhaleMadamRotation.x0 = pradnyaGokhaleMadamRotation.x1;
            pradnyaGokhaleMadamRotation.x1 = -temp - 32.5f;
            pradnyaGokhaleMadamRotation.speed = CREDITS_TEXT_ONE_SPEED_FAST;

            creditsSceneState = CREDITS_SPECIAL_THANKS_BACK_FAST;
        }
        break;
    case CREDITS_SPECIAL_THANKS_BACK_FAST:
        lerpMyValue(&ramaGokhaleMadamRotation, 1.0f);
        lerpMyValue(&pradnyaGokhaleMadamRotation, 1.0f);
        
        if(ramaGokhaleMadamRotation.bReached && pradnyaGokhaleMadamRotation.bReached && 
            updateAlpha(&specialThanksAlpha, 1.0f / (FPS * 1.0f), FADE_IN_FLAG))
        {
            creditsSceneState = CREDITS_SIR_TEXT_FAST;
        }
        break;
    case CREDITS_SIR_TEXT_FAST:
        lerpMyValue(&ignitedByRotation, 0.8f);
        lerpMyValue(&sirTextRotation, 0.8f);

        if(ignitedByRotation.bReached && sirTextRotation.bReached)
        {
            ignitedByRotation.speed = CREDITS_SIR_TEXT_SPEED_SLOW;
            sirTextRotation.speed = CREDITS_SIR_TEXT_SPEED_SLOW;
            creditsSceneState = CREDITS_SIR_TEXT_SLOW;
        }
        break;
    case CREDITS_SIR_TEXT_SLOW:
        lerpMyValue(&ignitedByRotation, 1.0f);
        lerpMyValue(&sirTextRotation, 1.0f);

        if(ignitedByRotation.bReached && sirTextRotation.bReached)
        {
            GLfloat temp = ignitedByRotation.x0;

            ignitedByRotation.tVal = 0.0f;
            ignitedByRotation.x0 = ignitedByRotation.x1;
            ignitedByRotation.x1 = -temp;
            ignitedByRotation.speed = CREDITS_TEXT_ONE_SPEED_FAST;

            temp = sirTextRotation.x0;

            sirTextRotation.tVal = 0.0f;
            sirTextRotation.x0 = sirTextRotation.x1;
            sirTextRotation.x1 = -temp - 20.0f;
            sirTextRotation.speed = CREDITS_TEXT_ONE_SPEED_FAST;

            creditsSceneState = CREDITS_SIR_TEXT_BACK_FAST;
        }
        break;
    case CREDITS_SIR_TEXT_BACK_FAST:
        lerpMyValue(&ignitedByRotation, 1.0f);
        lerpMyValue(&sirTextRotation, 1.0f);

        if(ignitedByRotation.bReached && sirTextRotation.bReached)
        {
            creditsSceneState = CREDITS_CAMERA_LOOKING_UP_ANIMATION;
        }
        break;
    case CREDITS_CAMERA_LOOKING_UP_ANIMATION:
        if(lerpCamera(&creditsSceneCamera, &creditsSceneCameraEnd, &tVal, CREDITS_CAMERA_SPEED))
        {
            if(!g_bCreditsSceneTimerIsOn)
            {
                SetTimer(ghwnd, IDT_SWITCH_TO_FADE_OUT_STATE, 3000, (TIMERPROC)creditsSceneTimer);
                g_bCreditsSceneTimerIsOn = TRUE;
            }
        }
        break;
    case CREDITS_SCENE_FADE_OUT:
        if(updateAlpha(&creditsSceneFadeAlpha, 1.0f / (FPS * 1.5f), FADE_OUT_FLAG))
        {
            creditsSceneState = END;
        }
        break;
    case END:
        uninitializeSceneCredits();
        if(!g_bCreditsSceneTimerIsOn)
        {
            SetTimer(ghwnd, IDT_SWITCH_TO_THE_END_SCENE, 5000, (TIMERPROC)creditsSceneTimer);
            g_bCreditsSceneTimerIsOn = TRUE;
        }
        break;
    }
}

void uninitializeSceneCredits(void)
{
    // code
#if PLAY_CREDITS_SCENE_MUSIC
    // stop the source
    stopALSource(g_CompleteTrackSource);
    
    // unbind the source
    SetCurrentSourceForStats(0, FALSE, FALSE);
#endif

    // stop the source
    stopALSource(g_SceneSources[4]);
    
    // unbind the source
    SetCurrentSourceForStats(0, FALSE, FALSE);


    // uninitialize the RT lib
    RT_Uninitialize_Library(&credits_font,MY_FONT_CREDITS);
}

void creditsSceneTimer(HWND hwnd, UINT iMsg, UINT_PTR idEvent, DWORD dwTime)
{
    // code
    KillTimer(hwnd, idEvent);
    switch(idEvent)
    { 
    case IDT_CREDITS_SCENE_INITIAL_DELAY:
        creditsSceneState = CREDITS_GROUP_LEADER_FAST;
        break;
    case IDT_GROUP_LEADER_TEXT_DELAY:
        g_bReverseGroupLeaderText = TRUE;
        break;
    case IDT_PROJECT_LEADER_TEXT_DELAY:
        g_bReverseProjectLeaderText = TRUE;
        break;
    case IDT_ABHI_ADI_TEXT_DELAY:
        g_bReverseAbhiAdiText = TRUE;
        break;
    case IDT_SWITCH_TO_FADE_OUT_STATE:
        creditsSceneState = CREDITS_SCENE_FADE_OUT;
        break;
    case IDT_SWITCH_TO_THE_END_SCENE:
        setCameraCoordinates({{0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 1.0f, 0.0f}, 0.0f, 270.0f});
        currentScene = SCENE_THE_END;
        break;
    default:
        break;
    }

    g_bCreditsSceneTimerIsOn = FALSE;
}
