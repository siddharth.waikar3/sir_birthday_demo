#pragma once

// header files
#include "Common.h"

// function declarations
void TopDownMergeSort(GLfloat *a, GLfloat *b, GLint n);
