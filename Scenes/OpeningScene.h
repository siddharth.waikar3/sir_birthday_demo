#pragma once

// function declarations
void initializeSceneOne(void);
void displaySceneOne(void);
void updateSceneOne(void);
void uninitializeSceneOne(void);
