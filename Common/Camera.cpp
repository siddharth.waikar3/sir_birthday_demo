// header files
#include "Camera.h"

// extern global variables
extern CameraCoordinates cameraCoordinates;
extern GLfloat step;

// camera
void moveForward(void)
{
    // code
    cameraCoordinates.position.x = cameraCoordinates.position.x + cameraCoordinates.front.x * (step * CAMERA_TRANSLATION_SPEED);
    cameraCoordinates.position.y = cameraCoordinates.position.y + cameraCoordinates.front.y * (step * CAMERA_TRANSLATION_SPEED);
    cameraCoordinates.position.z = cameraCoordinates.position.z + cameraCoordinates.front.z * (step * CAMERA_TRANSLATION_SPEED);
}

void moveBackward(void)
{
    // code
    cameraCoordinates.position.x = cameraCoordinates.position.x - cameraCoordinates.front.x * (step * CAMERA_TRANSLATION_SPEED);
    cameraCoordinates.position.y = cameraCoordinates.position.y - cameraCoordinates.front.y * (step * CAMERA_TRANSLATION_SPEED);
    cameraCoordinates.position.z = cameraCoordinates.position.z - cameraCoordinates.front.z * (step * CAMERA_TRANSLATION_SPEED);
}

void moveLeft(void)
{
    // code
    cameraCoordinates.position.x = cameraCoordinates.position.x - cameraCoordinates.right.x * (step * CAMERA_TRANSLATION_SPEED);
    cameraCoordinates.position.y = cameraCoordinates.position.y - cameraCoordinates.right.y * (step * CAMERA_TRANSLATION_SPEED);
    cameraCoordinates.position.z = cameraCoordinates.position.z - cameraCoordinates.right.z * (step * CAMERA_TRANSLATION_SPEED);
}

void moveRight(void)
{
    // code
    cameraCoordinates.position.x = cameraCoordinates.position.x + cameraCoordinates.right.x * (step * CAMERA_TRANSLATION_SPEED);
    cameraCoordinates.position.y = cameraCoordinates.position.y + cameraCoordinates.right.y * (step * CAMERA_TRANSLATION_SPEED);
    cameraCoordinates.position.z = cameraCoordinates.position.z + cameraCoordinates.right.z * (step * CAMERA_TRANSLATION_SPEED);
}

void moveUp(void)
{
    // code
    cameraCoordinates.position.x = cameraCoordinates.position.x + cameraCoordinates.up.x * (step * CAMERA_TRANSLATION_SPEED);
    cameraCoordinates.position.y = cameraCoordinates.position.y + cameraCoordinates.up.y * (step * CAMERA_TRANSLATION_SPEED);
    cameraCoordinates.position.z = cameraCoordinates.position.z + cameraCoordinates.up.z * (step * CAMERA_TRANSLATION_SPEED);
}

void moveDown(void)
{
    // code
    cameraCoordinates.position.x = cameraCoordinates.position.x - cameraCoordinates.up.x * (step * CAMERA_TRANSLATION_SPEED);
    cameraCoordinates.position.y = cameraCoordinates.position.y - cameraCoordinates.up.y * (step * CAMERA_TRANSLATION_SPEED);
    cameraCoordinates.position.z = cameraCoordinates.position.z - cameraCoordinates.up.z * (step * CAMERA_TRANSLATION_SPEED);
}

void turnUp(void)
{
    // function prototypes
    void updateCameraVector(void);

    // code
    cameraCoordinates.angleX = cameraCoordinates.angleX + (step * CAMERA_ROTATION_SPEED);
    // do not cross 90.0f
    if(cameraCoordinates.angleX >= 90.0f)
    {
        cameraCoordinates.angleX = -89.0f;
    }

    updateCameraVector();
}

void turnDown(void)
{
    // function prototypes
    void updateCameraVector(void);

    // code
    cameraCoordinates.angleX = cameraCoordinates.angleX - (step * CAMERA_ROTATION_SPEED);
    // do not cross 90.0f
    if(cameraCoordinates.angleX <= -90.0f)
    {
        cameraCoordinates.angleX = 89.0f;
    }

    updateCameraVector();
}

void turnLeft(void)
{
    // function prototypes
    void updateCameraVector(void);

    // code
    cameraCoordinates.angleY = cameraCoordinates.angleY - (step * CAMERA_ROTATION_SPEED);
    if(cameraCoordinates.angleY <= 0.0f)
    {
        cameraCoordinates.angleY = 360.0f + cameraCoordinates.angleY;
    }

    updateCameraVector();
}

void turnRight(void)
{
    // function prototypes
    void updateCameraVector(void);

    // code
    cameraCoordinates.angleY = cameraCoordinates.angleY + (step * CAMERA_ROTATION_SPEED);
    if(cameraCoordinates.angleY >= 360.0f)
    {
        cameraCoordinates.angleY = cameraCoordinates.angleY - 360.0f;
    }

    updateCameraVector();
}

void updateCameraVector(void)
{
    // code
    // convert angles from degrees to radians
    GLfloat xAngle = cameraCoordinates.angleX * (M_PI / 180.0f);
    GLfloat yAngle = cameraCoordinates.angleY * (M_PI / 180.0f);

    // calculate camera front vector
    cameraCoordinates.front.x = cosf(yAngle) * cosf(xAngle);
    cameraCoordinates.front.y = sinf(xAngle);
    cameraCoordinates.front.z = sinf(yAngle) * cosf(xAngle);

    // calculate camera right vector
    cameraCoordinates.right.x = (cameraCoordinates.front.y * 0.0f) - (1.0f * cameraCoordinates.front.z);
    cameraCoordinates.right.y = (cameraCoordinates.front.z * 0.0f) - (0.0f * cameraCoordinates.front.x);
    cameraCoordinates.right.z = (cameraCoordinates.front.x * 1.0f) - (0.0f * cameraCoordinates.front.y);

    /*
    * cameraCoordinates.right.x = cameraCoordinates.front.z;
    * cameraCoordinates.right.y = 0.0f;
    * cameraCoordinates.right.z = cameraCoordintes.front.x;
    */

    cameraCoordinates.up.x = (cameraCoordinates.right.y * cameraCoordinates.front.z) -
                             (cameraCoordinates.front.y * cameraCoordinates.right.z);

    cameraCoordinates.up.y = (cameraCoordinates.right.z * cameraCoordinates.front.x) -
                             (cameraCoordinates.front.z * cameraCoordinates.right.x);
    
    cameraCoordinates.up.z = (cameraCoordinates.right.x * cameraCoordinates.front.y) -
                             (cameraCoordinates.front.x * cameraCoordinates.right.y);

    /*
    * cameraCoordinates.up.x = cameraCoordinates.front.y * cameraCoordinates.right.z;
    * cameraCoordinates.up.y = (cameraCoordinates.right.z * cameraCoordinates.front.x) - 
    *						   (cameraCoordinates.front.z * cameraCoordinates.right.x);
    * cameraCoordinates.up.z = (cameraCoordinates.right.x * cameraCoordinates.front.y);
    */
}

void setCameraCoordinates(CameraCoordinates coordinates)
{
    // code
	cameraCoordinates = coordinates;
    updateCameraVector();
}

void setCameraCoordinatesUsingLookAt(CameraCoordinates coordinates)
{
    // code
	cameraCoordinates = coordinates;
    updateCameraVector();

    glLoadIdentity();

    // recalculate camera matrix using gluLookAt()
    gluLookAt(cameraCoordinates.position.x, cameraCoordinates.position.y, cameraCoordinates.position.z,
              cameraCoordinates.position.x + cameraCoordinates.front.x, 
              cameraCoordinates.position.y + cameraCoordinates.front.y, 
              cameraCoordinates.position.z + cameraCoordinates.front.z,
              cameraCoordinates.up.x, cameraCoordinates.up.y, cameraCoordinates.up.z);
}

BOOL lerpCamera(const CameraCoordinates *initial, const CameraCoordinates *final, GLfloat *lerpVal, GLfloat speed)
{
	// code
    *lerpVal = *lerpVal + speed;

	cameraCoordinates.position.x = getInterpolatedValue(0.0f, 1.0f, initial->position.x, final->position.x, *lerpVal);
	cameraCoordinates.position.y = getInterpolatedValue(0.0f, 1.0f, initial->position.y, final->position.y, *lerpVal);
	cameraCoordinates.position.z = getInterpolatedValue(0.0f, 1.0f, initial->position.z, final->position.z, *lerpVal);
	
	cameraCoordinates.angleX = getInterpolatedValue(0.0f, 1.0f, initial->angleX, final->angleX, *lerpVal);
	cameraCoordinates.angleY = getInterpolatedValue(0.0f, 1.0f, initial->angleY, final->angleY, *lerpVal);

    updateCameraVector();

    if(*lerpVal > 1.0f)
    {
        *lerpVal = 1.0f;

        // recalculate coordinates to minimize camera "jumps"
        cameraCoordinates.position.x = getInterpolatedValue(0.0f, 1.0f, initial->position.x, final->position.x, *lerpVal);
        cameraCoordinates.position.y = getInterpolatedValue(0.0f, 1.0f, initial->position.y, final->position.y, *lerpVal);
        cameraCoordinates.position.z = getInterpolatedValue(0.0f, 1.0f, initial->position.z, final->position.z, *lerpVal);
        
        cameraCoordinates.angleX = getInterpolatedValue(0.0f, 1.0f, initial->angleX, final->angleX, *lerpVal);
        cameraCoordinates.angleY = getInterpolatedValue(0.0f, 1.0f, initial->angleY, final->angleY, *lerpVal);

        updateCameraVector();
    	return(LERP_REACHED);
    }
    else
	    return(LERP_NOT_REACHED);
}

void lerpCamera_v2(LerpCameraCoordinates *coords, GLfloat condition)
{
	// code
    coords->tVal = coords->tVal + coords->speed;

    if(coords->tVal > condition)
    {
        coords->tVal = condition;
        coords->bReached = TRUE;
    }
    else
    {
        coords->bReached = FALSE;
    }

	cameraCoordinates.position.x = getInterpolatedValue(0.0f, 1.0f, coords->begin.position.x, coords->end.position.x, coords->tVal);
	cameraCoordinates.position.y = getInterpolatedValue(0.0f, 1.0f, coords->begin.position.y, coords->end.position.y, coords->tVal);
	cameraCoordinates.position.z = getInterpolatedValue(0.0f, 1.0f, coords->begin.position.z, coords->end.position.z, coords->tVal);
	
	cameraCoordinates.angleX = getInterpolatedValue(0.0f, 1.0f, coords->begin.angleX, coords->end.angleX, coords->tVal);
	cameraCoordinates.angleY = getInterpolatedValue(0.0f, 1.0f, coords->begin.angleY, coords->end.angleY, coords->tVal);

    updateCameraVector();
}

BOOL moveCameraInCircle(GLfloat deltaRadius, GLfloat circlingSpeed, GLfloat totalRotation, GLfloat *rotation, GLfloat totalRotateAfter, GLfloat *rotateAfter)
{
    // function prototypes
    void updateCameraVector(void);

    // code
    // change camera angle
    *rotation = *rotation + circlingSpeed;
    
    if(*rotation >= totalRotation)
    {
        // more rotation
        *rotateAfter = *rotateAfter + circlingSpeed;
        if(*rotateAfter >= totalRotateAfter)
        {
            updateCameraVector();
            return(TRUE);
        }

        // increment camera angle, but don't translate
        cameraCoordinates.angleY = cameraCoordinates.angleY + circlingSpeed;
    }
    else
    {
        // increment camera angle
        cameraCoordinates.angleY = cameraCoordinates.angleY + circlingSpeed;

        // move (translate) in circle
        cameraCoordinates.position.x = cameraCoordinates.position.x + cameraCoordinates.front.x * circlingSpeed * deltaRadius; //0.35f;
        cameraCoordinates.position.y = cameraCoordinates.position.y + cameraCoordinates.front.y * circlingSpeed * deltaRadius; //0.35f;
        cameraCoordinates.position.z = cameraCoordinates.position.z + cameraCoordinates.front.z * circlingSpeed * deltaRadius; //0.35f;
    }

    // update camera vector
    updateCameraVector();
    return(FALSE);
}

void tiltCamera(CameraTilt *tilt, GLfloat speed, GLuint tiltFlag)
{
    // code
    // increment tilt
    tilt->tilt = tilt->tilt + speed;
    
    switch(tiltFlag)
    {
    case CAMERA_POSITIVE_TILT_FLAG:
        // if tilt is greater than totalTilt, tilt has reached its destination
        if(tilt->tilt > tilt->totalTilt)
        {
            tilt->tilt = tilt->totalTilt;
            tilt->bReached = TRUE;
        } // if not, tilt has not reached its destination
        else
        {
            tilt->bReached = FALSE;
        }
        break;
    case CAMERA_NEGATIVE_TILT_FLAG:
        // if tilt is less than totalTilt, tilt has reached its destination
        if(tilt->tilt < tilt->totalTilt)
        {
            tilt->tilt = tilt->totalTilt;
            tilt->bReached = TRUE;
        } // if not, tilt has not reached its destination
        else
        {
            tilt->bReached = FALSE;
        }
        break;
    }

    // update actual camera angleY
    cameraCoordinates.angleX = cameraCoordinates.angleX + speed;

    // update camera vector
    updateCameraVector(); 
}

void panCamera(CameraPan *pan, GLfloat speed, GLuint panFlag)
{
    // code
    // increment pan
    pan->pan = pan->pan + speed;
    
    switch(panFlag)
    {
    case CAMERA_POSITIVE_PAN_FLAG:
        // if pan is greater than totalPan, pan has reached its destination
        if(pan->pan > pan->totalPan)
        {
            pan->pan = pan->totalPan;
            pan->bReached = TRUE;
        } // if not, pan has not reached its destination
        else
        {
            pan->bReached = FALSE;
        }
        break;
    case CAMERA_NEGATIVE_PAN_FLAG:
        // if pan is less than totalPan, pan has reached its destination
        if(pan->pan < pan->totalPan)
        {
            pan->pan = pan->totalPan;
            pan->bReached = TRUE;
        } // if not, pan has not reached its destination
        else
        {
            pan->bReached = FALSE;
        }
        break;
    }

    // update actual camera angleY
    cameraCoordinates.angleY = cameraCoordinates.angleY + speed;

    // update camera vector
    updateCameraVector(); 
}
